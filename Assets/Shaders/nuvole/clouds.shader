Shader "clouds"
{
    Properties
    {
        Vector4_5537CA4E("rotateprojection", Vector) = (1,0,0,0)
Vector1_1F258ECB("noisescale", Float) = 0
Vector1_FEF9B246("movement", Float) = 1
Vector1_7D67B347("nuvole", Float) = 1
Vector4_E855A2D9("noiseremap", Vector) = (0,1,-1,1)
[HDR]Color_518716D8("color2", Color) = (0,0,0,0)
[HDR]Color_702AB34C("color1", Color) = (0,0,0,0)
Vector1_742F243A("Noise Edge 1", Float) = 1
Vector1_AAE278D5("Noise Edge 2", Float) = 1
Vector1_F997F91D("basic scale", Float) = 5
Vector1_A09568FA("base speed", Float) = 1
Vector1_50DCEA20("floof", Float) = 1
Vector1_E3371B04("vase strenght", Float) = 1
Vector1_6642FA1C("curvature radius", Float) = 1
Vector1_1245587D("fresnel", Float) = 1
Vector1_D2EDE3A7("colorize fresnel", Float) = 0.5
Vector1_828AA966("fadedepth", Float) = 10

    }
    SubShader
    {
        Tags
        {
            "RenderPipeline"="HDRenderPipeline"
            "RenderType"="Transparent"
            "Queue"="Transparent+0"
        }
        
        Pass
        {
            // based on HDUnlitPassForward.template
            Name "ShadowCaster"
            Tags { "LightMode" = "ShadowCaster" }
        
            //-------------------------------------------------------------------------------------
            // Render Modes (Blend, Cull, ZTest, Stencil, etc)
            //-------------------------------------------------------------------------------------
            Blend One OneMinusSrcAlpha
        
            Cull Back
        
            ZTest LEqual
        
            ZWrite Off
        
            // Default Stencil
        
            ColorMask 0
        
            //-------------------------------------------------------------------------------------
            // End Render Modes
            //-------------------------------------------------------------------------------------
        
            HLSLPROGRAM
        
            #pragma target 4.5
            #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
            //#pragma enable_d3d11_debug_symbols
        
            //enable GPU instancing support
            #pragma multi_compile_instancing
        
            //-------------------------------------------------------------------------------------
            // Variant Definitions (active field translations to HDRP defines)
            //-------------------------------------------------------------------------------------
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _BLENDMODE_ALPHA 1
            //-------------------------------------------------------------------------------------
            // End Variant Definitions
            //-------------------------------------------------------------------------------------
        
            #pragma vertex Vert
            #pragma fragment Frag
        
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Wind.hlsl"
        
            // define FragInputs structure
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"
        
            //-------------------------------------------------------------------------------------
            // Defines
            //-------------------------------------------------------------------------------------
                    #define SHADERPASS SHADERPASS_SHADOWS
                #define USE_LEGACY_UNITY_MATRIX_VARIABLES
                #define REQUIRE_DEPTH_TEXTURE
        
            // this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
            #define ATTRIBUTES_NEED_NORMAL
            #define VARYINGS_NEED_POSITION_WS
            #define HAVE_MESH_MODIFICATION
        
            //-------------------------------------------------------------------------------------
            // End Defines
            //-------------------------------------------------------------------------------------
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
        #ifdef DEBUG_DISPLAY
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Debug/DebugDisplay.hlsl"
        #endif
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"
        
            // Used by SceneSelectionPass
            int _ObjectId;
            int _PassValue;
        
            //-------------------------------------------------------------------------------------
            // Interpolator Packing And Struct Declarations
            //-------------------------------------------------------------------------------------
        // Generated Type: AttributesMesh
        struct AttributesMesh {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        
        // Generated Type: VaryingsMeshToPS
        struct VaryingsMeshToPS {
            float4 positionCS : SV_Position;
            float3 positionRWS; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        struct PackedVaryingsMeshToPS {
            float3 interp00 : TEXCOORD0; // auto-packed
            float4 positionCS : SV_Position; // unpacked
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC; // unpacked
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
        {
            PackedVaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.interp00.xyz = input.positionRWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
        {
            VaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.positionRWS = input.interp00.xyz;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        
        // Generated Type: VaryingsMeshToDS
        struct VaryingsMeshToDS {
            float3 positionRWS;
            float3 normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        struct PackedVaryingsMeshToDS {
            float3 interp00 : TEXCOORD0; // auto-packed
            float3 interp01 : TEXCOORD1; // auto-packed
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC; // unpacked
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
        {
            PackedVaryingsMeshToDS output;
            output.interp00.xyz = input.positionRWS;
            output.interp01.xyz = input.normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
        {
            VaryingsMeshToDS output;
            output.positionRWS = input.interp00.xyz;
            output.normalWS = input.interp01.xyz;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        
            //-------------------------------------------------------------------------------------
            // End Interpolator Packing And Struct Declarations
            //-------------------------------------------------------------------------------------
        
            //-------------------------------------------------------------------------------------
            // Graph generated code
            //-------------------------------------------------------------------------------------
                    // Shared Graph Properties (uniform inputs)
                    CBUFFER_START(UnityPerMaterial)
                    float4 Vector4_5537CA4E;
                    float Vector1_1F258ECB;
                    float Vector1_FEF9B246;
                    float Vector1_7D67B347;
                    float4 Vector4_E855A2D9;
                    float4 Color_518716D8;
                    float4 Color_702AB34C;
                    float Vector1_742F243A;
                    float Vector1_AAE278D5;
                    float Vector1_F997F91D;
                    float Vector1_A09568FA;
                    float Vector1_50DCEA20;
                    float Vector1_E3371B04;
                    float Vector1_6642FA1C;
                    float Vector1_1245587D;
                    float Vector1_D2EDE3A7;
                    float Vector1_828AA966;
                    CBUFFER_END
                
                
                // Vertex Graph Inputs
                    struct VertexDescriptionInputs {
                        float3 ObjectSpaceNormal; // optional
                        float3 WorldSpaceNormal; // optional
                        float3 ObjectSpacePosition; // optional
                        float3 WorldSpacePosition; // optional
                    };
                // Vertex Graph Outputs
                    struct VertexDescription
                    {
                        float3 Position;
                    };
                    
                // Pixel Graph Inputs
                    struct SurfaceDescriptionInputs {
                        float3 WorldSpacePosition; // optional
                        float4 ScreenPosition; // optional
                    };
                // Pixel Graph Outputs
                    struct SurfaceDescription
                    {
                        float Alpha;
                        float AlphaClipThreshold;
                    };
                    
                // Shared Graph Node Functions
                
                    void Unity_SceneDepth_Eye_float(float4 UV, out float Out)
                    {
                        Out = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH(UV.xy), _ZBufferParams);
                    }
                
                    void Unity_Subtract_float(float A, float B, out float Out)
                    {
                        Out = A - B;
                    }
                
                    void Unity_Divide_float(float A, float B, out float Out)
                    {
                        Out = A / B;
                    }
                
                    void Unity_Saturate_float(float In, out float Out)
                    {
                        Out = saturate(In);
                    }
                
                    void Unity_Distance_float3(float3 A, float3 B, out float Out)
                    {
                        Out = distance(A, B);
                    }
                
                    void Unity_Power_float(float A, float B, out float Out)
                    {
                        Out = pow(A, B);
                    }
                
                    void Unity_Multiply_float (float3 A, float3 B, out float3 Out)
                    {
                        Out = A * B;
                    }
                
                    void Unity_Rotate_About_Axis_Degrees_float(float3 In, float3 Axis, float Rotation, out float3 Out)
                    {
                        float s = sin(Rotation);
                        float c = cos(Rotation);
                        float one_minus_c = 1.0 - c;
                        
                        Axis = normalize(Axis);
                
                        float3x3 rot_mat = { one_minus_c * Axis.x * Axis.x + c,            one_minus_c * Axis.x * Axis.y - Axis.z * s,     one_minus_c * Axis.z * Axis.x + Axis.y * s,
                                                   one_minus_c * Axis.x * Axis.y + Axis.z * s,   one_minus_c * Axis.y * Axis.y + c,              one_minus_c * Axis.y * Axis.z - Axis.x * s,
                                                   one_minus_c * Axis.z * Axis.x - Axis.y * s,   one_minus_c * Axis.y * Axis.z + Axis.x * s,     one_minus_c * Axis.z * Axis.z + c
                                                 };
                
                        Out = mul(rot_mat,  In);
                    }
                
                    void Unity_Multiply_float (float A, float B, out float Out)
                    {
                        Out = A * B;
                    }
                
                    void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
                    {
                        Out = UV * Tiling + Offset;
                    }
                
                
    float2 unity_gradientNoise_dir(float2 p)
    {
        // Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
        p = p % 289;
        float x = (34 * p.x + 1) * p.x % 289 + p.y;
        x = (34 * x + 1) * x % 289;
        x = frac(x / 41) * 2 - 1;
        return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
    }

                
    float unity_gradientNoise(float2 p)
    {
        float2 ip = floor(p);
        float2 fp = frac(p);
        float d00 = dot(unity_gradientNoise_dir(ip), fp);
        float d01 = dot(unity_gradientNoise_dir(ip + float2(0, 1)), fp - float2(0, 1));
        float d10 = dot(unity_gradientNoise_dir(ip + float2(1, 0)), fp - float2(1, 0));
        float d11 = dot(unity_gradientNoise_dir(ip + float2(1, 1)), fp - float2(1, 1));
        fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
        return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x);
    }

                    void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
                    { Out = unity_gradientNoise(UV * Scale) + 0.5; }
                
                    void Unity_Add_float(float A, float B, out float Out)
                    {
                        Out = A + B;
                    }
                
                    void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
                    {
                        RGBA = float4(R, G, B, A);
                        RGB = float3(R, G, B);
                        RG = float2(R, G);
                    }
                
                    void Unity_Remap_float(float In, float2 InMinMax, float2 OutMinMax, out float Out)
                    {
                        Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
                    }
                
                    void Unity_Absolute_float(float In, out float Out)
                    {
                        Out = abs(In);
                    }
                
                    void Unity_Smoothstep_float(float Edge1, float Edge2, float In, out float Out)
                    {
                        Out = smoothstep(Edge1, Edge2, In);
                    }
                
                    void Unity_Add_float3(float3 A, float3 B, out float3 Out)
                    {
                        Out = A + B;
                    }
                
                // Vertex Graph Evaluation
                    VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
                    {
                        VertexDescription description = (VertexDescription)0;
                        float _Distance_63B151C3_Out;
                        Unity_Distance_float3(SHADERGRAPH_OBJECT_POSITION, IN.WorldSpacePosition, _Distance_63B151C3_Out);
                        float _Property_F4DF73F4_Out = Vector1_6642FA1C;
                        float _Divide_5E236CDB_Out;
                        Unity_Divide_float(_Distance_63B151C3_Out, _Property_F4DF73F4_Out, _Divide_5E236CDB_Out);
                        float _Power_1995172_Out;
                        Unity_Power_float(_Divide_5E236CDB_Out, 3, _Power_1995172_Out);
                        float3 _Multiply_69F36212_Out;
                        Unity_Multiply_float(IN.WorldSpaceNormal, (_Power_1995172_Out.xxx), _Multiply_69F36212_Out);
                    
                        float _Property_A5D6BEA2_Out = Vector1_742F243A;
                        float _Property_E0560113_Out = Vector1_AAE278D5;
                        float4 _Property_6F049BAF_Out = Vector4_5537CA4E;
                        float _Split_C5D3F9C2_R = _Property_6F049BAF_Out[0];
                        float _Split_C5D3F9C2_G = _Property_6F049BAF_Out[1];
                        float _Split_C5D3F9C2_B = _Property_6F049BAF_Out[2];
                        float _Split_C5D3F9C2_A = _Property_6F049BAF_Out[3];
                        float3 _RotateAboutAxis_4186120F_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_6F049BAF_Out.xyz), _Split_C5D3F9C2_A, _RotateAboutAxis_4186120F_Out);
                        float _Property_35D7EF57_Out = Vector1_FEF9B246;
                        float _Multiply_14C29873_Out;
                        Unity_Multiply_float(_Time.y, _Property_35D7EF57_Out, _Multiply_14C29873_Out);
                    
                        float2 _TilingAndOffset_2773ADC5_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), (_Multiply_14C29873_Out.xx), _TilingAndOffset_2773ADC5_Out);
                        float _Property_49DEE58A_Out = Vector1_1F258ECB;
                        float _GradientNoise_6B4CD361_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_2773ADC5_Out, _Property_49DEE58A_Out, _GradientNoise_6B4CD361_Out);
                        float2 _TilingAndOffset_23282D51_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), float2 (0,0), _TilingAndOffset_23282D51_Out);
                        float _GradientNoise_2DAA6CD5_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_23282D51_Out, _Property_49DEE58A_Out, _GradientNoise_2DAA6CD5_Out);
                        float _Add_ABFEF962_Out;
                        Unity_Add_float(_GradientNoise_6B4CD361_Out, _GradientNoise_2DAA6CD5_Out, _Add_ABFEF962_Out);
                        float _Divide_BED3250F_Out;
                        Unity_Divide_float(_Add_ABFEF962_Out, 2, _Divide_BED3250F_Out);
                        float _Saturate_6BA02F15_Out;
                        Unity_Saturate_float(_Divide_BED3250F_Out, _Saturate_6BA02F15_Out);
                        float _Property_4D85CCDA_Out = Vector1_50DCEA20;
                        float _Power_BC168E33_Out;
                        Unity_Power_float(_Saturate_6BA02F15_Out, _Property_4D85CCDA_Out, _Power_BC168E33_Out);
                        float4 _Property_7006A80A_Out = Vector4_E855A2D9;
                        float _Split_796B44F0_R = _Property_7006A80A_Out[0];
                        float _Split_796B44F0_G = _Property_7006A80A_Out[1];
                        float _Split_796B44F0_B = _Property_7006A80A_Out[2];
                        float _Split_796B44F0_A = _Property_7006A80A_Out[3];
                        float4 _Combine_979EF6BC_RGBA;
                        float3 _Combine_979EF6BC_RGB;
                        float2 _Combine_979EF6BC_RG;
                        Unity_Combine_float(_Split_796B44F0_R, _Split_796B44F0_G, 0, 0, _Combine_979EF6BC_RGBA, _Combine_979EF6BC_RGB, _Combine_979EF6BC_RG);
                        float4 _Combine_6F7F4E7C_RGBA;
                        float3 _Combine_6F7F4E7C_RGB;
                        float2 _Combine_6F7F4E7C_RG;
                        Unity_Combine_float(_Split_796B44F0_B, _Split_796B44F0_A, 0, 0, _Combine_6F7F4E7C_RGBA, _Combine_6F7F4E7C_RGB, _Combine_6F7F4E7C_RG);
                        float _Remap_FEC86696_Out;
                        Unity_Remap_float(_Power_BC168E33_Out, _Combine_979EF6BC_RG, _Combine_6F7F4E7C_RG, _Remap_FEC86696_Out);
                        float _Absolute_164E9E26_Out;
                        Unity_Absolute_float(_Remap_FEC86696_Out, _Absolute_164E9E26_Out);
                        float _Smoothstep_9A76A679_Out;
                        Unity_Smoothstep_float(_Property_A5D6BEA2_Out, _Property_E0560113_Out, _Absolute_164E9E26_Out, _Smoothstep_9A76A679_Out);
                        float4 _Property_FF51AF93_Out = Vector4_5537CA4E;
                        float _Split_8BA32C12_R = _Property_FF51AF93_Out[0];
                        float _Split_8BA32C12_G = _Property_FF51AF93_Out[1];
                        float _Split_8BA32C12_B = _Property_FF51AF93_Out[2];
                        float _Split_8BA32C12_A = _Property_FF51AF93_Out[3];
                        float3 _RotateAboutAxis_311AD17D_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_FF51AF93_Out.xyz), _Split_8BA32C12_A, _RotateAboutAxis_311AD17D_Out);
                        float _Property_4DFEC70D_Out = Vector1_A09568FA;
                        float _Multiply_54A56B7E_Out;
                        Unity_Multiply_float(_Time.y, _Property_4DFEC70D_Out, _Multiply_54A56B7E_Out);
                    
                        float2 _TilingAndOffset_A02A18F9_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_311AD17D_Out.xy), float2 (1,1), (_Multiply_54A56B7E_Out.xx), _TilingAndOffset_A02A18F9_Out);
                        float _Property_95BB6BB6_Out = Vector1_F997F91D;
                        float _GradientNoise_B18F1DE_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_A02A18F9_Out, _Property_95BB6BB6_Out, _GradientNoise_B18F1DE_Out);
                        float _Property_9A64B15E_Out = Vector1_E3371B04;
                        float _Multiply_B28321D7_Out;
                        Unity_Multiply_float(_GradientNoise_B18F1DE_Out, _Property_9A64B15E_Out, _Multiply_B28321D7_Out);
                    
                        float _Add_5282EE67_Out;
                        Unity_Add_float(_Smoothstep_9A76A679_Out, _Multiply_B28321D7_Out, _Add_5282EE67_Out);
                        float _Add_B2A28D15_Out;
                        Unity_Add_float(1, _Property_9A64B15E_Out, _Add_B2A28D15_Out);
                        float _Divide_A0FE7B8_Out;
                        Unity_Divide_float(_Add_5282EE67_Out, _Add_B2A28D15_Out, _Divide_A0FE7B8_Out);
                        float3 _Multiply_8BCEAEFC_Out;
                        Unity_Multiply_float(IN.ObjectSpaceNormal, (_Divide_A0FE7B8_Out.xxx), _Multiply_8BCEAEFC_Out);
                    
                        float _Property_2C9980A6_Out = Vector1_7D67B347;
                        float3 _Multiply_C3D96CA2_Out;
                        Unity_Multiply_float(_Multiply_8BCEAEFC_Out, (_Property_2C9980A6_Out.xxx), _Multiply_C3D96CA2_Out);
                    
                        float3 _Add_815814F_Out;
                        Unity_Add_float3(IN.ObjectSpacePosition, _Multiply_C3D96CA2_Out, _Add_815814F_Out);
                        float3 _Add_D43B41A9_Out;
                        Unity_Add_float3(_Multiply_69F36212_Out, _Add_815814F_Out, _Add_D43B41A9_Out);
                        description.Position = _Add_D43B41A9_Out;
                        return description;
                    }
                    
                // Pixel Graph Evaluation
                    SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                    {
                        SurfaceDescription surface = (SurfaceDescription)0;
                        float _SceneDepth_8ABCD70C_Out;
                        Unity_SceneDepth_Eye_float(float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0), _SceneDepth_8ABCD70C_Out);
                        float4 _ScreenPosition_B973BFDF_Out = IN.ScreenPosition;
                        float _Split_E1759A96_R = _ScreenPosition_B973BFDF_Out[0];
                        float _Split_E1759A96_G = _ScreenPosition_B973BFDF_Out[1];
                        float _Split_E1759A96_B = _ScreenPosition_B973BFDF_Out[2];
                        float _Split_E1759A96_A = _ScreenPosition_B973BFDF_Out[3];
                        float _Subtract_D12B8E5A_Out;
                        Unity_Subtract_float(_Split_E1759A96_A, 1, _Subtract_D12B8E5A_Out);
                        float _Subtract_501D47FC_Out;
                        Unity_Subtract_float(_SceneDepth_8ABCD70C_Out, _Subtract_D12B8E5A_Out, _Subtract_501D47FC_Out);
                        float _Property_7D938F14_Out = Vector1_828AA966;
                        float _Divide_90AB178C_Out;
                        Unity_Divide_float(_Subtract_501D47FC_Out, _Property_7D938F14_Out, _Divide_90AB178C_Out);
                        float _Saturate_D0EAEC86_Out;
                        Unity_Saturate_float(_Divide_90AB178C_Out, _Saturate_D0EAEC86_Out);
                        surface.Alpha = _Saturate_D0EAEC86_Out;
                        surface.AlphaClipThreshold = 0;
                        return surface;
                    }
                    
            //-------------------------------------------------------------------------------------
            // End graph generated code
            //-------------------------------------------------------------------------------------
        
        
        //-------------------------------------------------------------------------------------
        // TEMPLATE INCLUDE : VertexAnimation.template.hlsl
        //-------------------------------------------------------------------------------------
        
        VertexDescriptionInputs AttributesMeshToVertexDescriptionInputs(AttributesMesh input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);
        
            output.ObjectSpaceNormal =           input.normalOS;
            output.WorldSpaceNormal =            TransformObjectToWorldNormal(input.normalOS);
            output.ObjectSpacePosition =         input.positionOS;
            output.WorldSpacePosition =          GetAbsolutePositionWS(TransformObjectToWorld(input.positionOS));
        
            return output;
        }
        
        AttributesMesh ApplyMeshModification(AttributesMesh input)
        {
            // build graph inputs
            VertexDescriptionInputs vertexDescriptionInputs = AttributesMeshToVertexDescriptionInputs(input);
        
            // evaluate vertex graph
            VertexDescription vertexDescription = VertexDescriptionFunction(vertexDescriptionInputs);
        
            // copy graph output to the results
            input.positionOS = vertexDescription.Position;
        
            return input;
        }
        
        //-------------------------------------------------------------------------------------
        // END TEMPLATE INCLUDE : VertexAnimation.template.hlsl
        //-------------------------------------------------------------------------------------
        
        
        
        //-------------------------------------------------------------------------------------
        // TEMPLATE INCLUDE : SharedCode.template.hlsl
        //-------------------------------------------------------------------------------------
            FragInputs BuildFragInputs(VaryingsMeshToPS input)
            {
                FragInputs output;
                ZERO_INITIALIZE(FragInputs, output);
        
                // Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
                // TODO: this is a really poor workaround, but the variable is used in a bunch of places
                // to compute normals which are then passed on elsewhere to compute other values...
                output.worldToTangent = k_identity3x3;
                output.positionSS = input.positionCS;       // input.positionCS is SV_Position
        
                output.positionRWS = input.positionRWS;
                #if SHADER_STAGE_FRAGMENT
                #endif // SHADER_STAGE_FRAGMENT
        
                return output;
            }
        
            SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);
        
                output.WorldSpacePosition =          GetAbsolutePositionWS(input.positionRWS);
                output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
        
                return output;
            }
        
            // existing HDRP code uses the combined function to go directly from packed to frag inputs
            FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
            {
                UNITY_SETUP_INSTANCE_ID(input);
                VaryingsMeshToPS unpacked= UnpackVaryingsMeshToPS(input);
                return BuildFragInputs(unpacked);
            }
        
        //-------------------------------------------------------------------------------------
        // END TEMPLATE INCLUDE : SharedCode.template.hlsl
        //-------------------------------------------------------------------------------------
        
        
        
            void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
            {
                // setup defaults -- these are used if the graph doesn't output a value
                ZERO_INITIALIZE(SurfaceData, surfaceData);
        
                // copy across graph values, if defined
        
        #if defined(DEBUG_DISPLAY)
                if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
                {
                    // TODO
                }
        #endif
            }
        
            void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
            {
                SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
                SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);
        
                // Perform alpha test very early to save performance (a killed pixel will not sample textures)
                // TODO: split graph evaluation to grab just alpha dependencies first? tricky..
        
                BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);
        
                // Builtin Data
                ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
        
                builtinData.opacity = surfaceDescription.Alpha;
        
        
        #if (SHADERPASS == SHADERPASS_DISTORTION)
                builtinData.distortion = surfaceDescription.Distortion;
                builtinData.distortionBlur = surfaceDescription.DistortionBlur;
        #else
                builtinData.distortion = float2(0.0, 0.0);
                builtinData.distortionBlur = 0.0;
        #endif
            }
        
            //-------------------------------------------------------------------------------------
            // Pass Includes
            //-------------------------------------------------------------------------------------
                #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassDepthOnly.hlsl"
            //-------------------------------------------------------------------------------------
            // End Pass Includes
            //-------------------------------------------------------------------------------------
        
            ENDHLSL
        }
        
        Pass
        {
            // based on HDUnlitPassForward.template
            Name "META"
            Tags { "LightMode" = "META" }
        
            //-------------------------------------------------------------------------------------
            // Render Modes (Blend, Cull, ZTest, Stencil, etc)
            //-------------------------------------------------------------------------------------
            Blend One OneMinusSrcAlpha
        
            Cull Off
        
            ZTest LEqual
        
            ZWrite Off
        
            // Default Stencil
        
            
            //-------------------------------------------------------------------------------------
            // End Render Modes
            //-------------------------------------------------------------------------------------
        
            HLSLPROGRAM
        
            #pragma target 4.5
            #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
            //#pragma enable_d3d11_debug_symbols
        
            //enable GPU instancing support
            #pragma multi_compile_instancing
        
            //-------------------------------------------------------------------------------------
            // Variant Definitions (active field translations to HDRP defines)
            //-------------------------------------------------------------------------------------
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _BLENDMODE_ALPHA 1
            //-------------------------------------------------------------------------------------
            // End Variant Definitions
            //-------------------------------------------------------------------------------------
        
            #pragma vertex Vert
            #pragma fragment Frag
        
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Wind.hlsl"
        
            // define FragInputs structure
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"
        
            //-------------------------------------------------------------------------------------
            // Defines
            //-------------------------------------------------------------------------------------
                    #define SHADERPASS SHADERPASS_LIGHT_TRANSPORT
                #define REQUIRE_DEPTH_TEXTURE
        
            // this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define ATTRIBUTES_NEED_TEXCOORD2
            #define ATTRIBUTES_NEED_COLOR
            #define VARYINGS_NEED_POSITION_WS
            #define VARYINGS_NEED_TANGENT_TO_WORLD
            #define HAVE_MESH_MODIFICATION
        
            //-------------------------------------------------------------------------------------
            // End Defines
            //-------------------------------------------------------------------------------------
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
        #ifdef DEBUG_DISPLAY
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Debug/DebugDisplay.hlsl"
        #endif
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"
        
            // Used by SceneSelectionPass
            int _ObjectId;
            int _PassValue;
        
            //-------------------------------------------------------------------------------------
            // Interpolator Packing And Struct Declarations
            //-------------------------------------------------------------------------------------
        // Generated Type: AttributesMesh
        struct AttributesMesh {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL; // optional
            float4 tangentOS : TANGENT; // optional
            float4 uv0 : TEXCOORD0; // optional
            float4 uv1 : TEXCOORD1; // optional
            float4 uv2 : TEXCOORD2; // optional
            float4 color : COLOR; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        
        // Generated Type: VaryingsMeshToPS
        struct VaryingsMeshToPS {
            float4 positionCS : SV_Position;
            float3 positionRWS; // optional
            float3 normalWS; // optional
            float4 tangentWS; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        struct PackedVaryingsMeshToPS {
            float3 interp00 : TEXCOORD0; // auto-packed
            float3 interp01 : TEXCOORD1; // auto-packed
            float4 interp02 : TEXCOORD2; // auto-packed
            float4 positionCS : SV_Position; // unpacked
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC; // unpacked
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
        {
            PackedVaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.interp00.xyz = input.positionRWS;
            output.interp01.xyz = input.normalWS;
            output.interp02.xyzw = input.tangentWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
        {
            VaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.positionRWS = input.interp00.xyz;
            output.normalWS = input.interp01.xyz;
            output.tangentWS = input.interp02.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        
        // Generated Type: VaryingsMeshToDS
        struct VaryingsMeshToDS {
            float3 positionRWS;
            float3 normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        struct PackedVaryingsMeshToDS {
            float3 interp00 : TEXCOORD0; // auto-packed
            float3 interp01 : TEXCOORD1; // auto-packed
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC; // unpacked
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
        {
            PackedVaryingsMeshToDS output;
            output.interp00.xyz = input.positionRWS;
            output.interp01.xyz = input.normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
        {
            VaryingsMeshToDS output;
            output.positionRWS = input.interp00.xyz;
            output.normalWS = input.interp01.xyz;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        
            //-------------------------------------------------------------------------------------
            // End Interpolator Packing And Struct Declarations
            //-------------------------------------------------------------------------------------
        
            //-------------------------------------------------------------------------------------
            // Graph generated code
            //-------------------------------------------------------------------------------------
                    // Shared Graph Properties (uniform inputs)
                    CBUFFER_START(UnityPerMaterial)
                    float4 Vector4_5537CA4E;
                    float Vector1_1F258ECB;
                    float Vector1_FEF9B246;
                    float Vector1_7D67B347;
                    float4 Vector4_E855A2D9;
                    float4 Color_518716D8;
                    float4 Color_702AB34C;
                    float Vector1_742F243A;
                    float Vector1_AAE278D5;
                    float Vector1_F997F91D;
                    float Vector1_A09568FA;
                    float Vector1_50DCEA20;
                    float Vector1_E3371B04;
                    float Vector1_6642FA1C;
                    float Vector1_1245587D;
                    float Vector1_D2EDE3A7;
                    float Vector1_828AA966;
                    CBUFFER_END
                
                
                // Vertex Graph Inputs
                    struct VertexDescriptionInputs {
                    };
                // Vertex Graph Outputs
                    struct VertexDescription
                    {
                    };
                    
                // Pixel Graph Inputs
                    struct SurfaceDescriptionInputs {
                        float3 WorldSpaceNormal; // optional
                        float3 WorldSpaceViewDirection; // optional
                        float3 WorldSpacePosition; // optional
                        float4 ScreenPosition; // optional
                    };
                // Pixel Graph Outputs
                    struct SurfaceDescription
                    {
                        float3 Color;
                        float Alpha;
                        float AlphaClipThreshold;
                    };
                    
                // Shared Graph Node Functions
                
                    void Unity_Rotate_About_Axis_Degrees_float(float3 In, float3 Axis, float Rotation, out float3 Out)
                    {
                        float s = sin(Rotation);
                        float c = cos(Rotation);
                        float one_minus_c = 1.0 - c;
                        
                        Axis = normalize(Axis);
                
                        float3x3 rot_mat = { one_minus_c * Axis.x * Axis.x + c,            one_minus_c * Axis.x * Axis.y - Axis.z * s,     one_minus_c * Axis.z * Axis.x + Axis.y * s,
                                                   one_minus_c * Axis.x * Axis.y + Axis.z * s,   one_minus_c * Axis.y * Axis.y + c,              one_minus_c * Axis.y * Axis.z - Axis.x * s,
                                                   one_minus_c * Axis.z * Axis.x - Axis.y * s,   one_minus_c * Axis.y * Axis.z + Axis.x * s,     one_minus_c * Axis.z * Axis.z + c
                                                 };
                
                        Out = mul(rot_mat,  In);
                    }
                
                    void Unity_Multiply_float (float A, float B, out float Out)
                    {
                        Out = A * B;
                    }
                
                    void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
                    {
                        Out = UV * Tiling + Offset;
                    }
                
                
    float2 unity_gradientNoise_dir(float2 p)
    {
        // Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
        p = p % 289;
        float x = (34 * p.x + 1) * p.x % 289 + p.y;
        x = (34 * x + 1) * x % 289;
        x = frac(x / 41) * 2 - 1;
        return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
    }

                
    float unity_gradientNoise(float2 p)
    {
        float2 ip = floor(p);
        float2 fp = frac(p);
        float d00 = dot(unity_gradientNoise_dir(ip), fp);
        float d01 = dot(unity_gradientNoise_dir(ip + float2(0, 1)), fp - float2(0, 1));
        float d10 = dot(unity_gradientNoise_dir(ip + float2(1, 0)), fp - float2(1, 0));
        float d11 = dot(unity_gradientNoise_dir(ip + float2(1, 1)), fp - float2(1, 1));
        fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
        return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x);
    }

                    void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
                    { Out = unity_gradientNoise(UV * Scale) + 0.5; }
                
                    void Unity_Add_float(float A, float B, out float Out)
                    {
                        Out = A + B;
                    }
                
                    void Unity_Divide_float(float A, float B, out float Out)
                    {
                        Out = A / B;
                    }
                
                    void Unity_Saturate_float(float In, out float Out)
                    {
                        Out = saturate(In);
                    }
                
                    void Unity_Power_float(float A, float B, out float Out)
                    {
                        Out = pow(A, B);
                    }
                
                    void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
                    {
                        RGBA = float4(R, G, B, A);
                        RGB = float3(R, G, B);
                        RG = float2(R, G);
                    }
                
                    void Unity_Remap_float(float In, float2 InMinMax, float2 OutMinMax, out float Out)
                    {
                        Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
                    }
                
                    void Unity_Absolute_float(float In, out float Out)
                    {
                        Out = abs(In);
                    }
                
                    void Unity_Smoothstep_float(float Edge1, float Edge2, float In, out float Out)
                    {
                        Out = smoothstep(Edge1, Edge2, In);
                    }
                
                    void Unity_Lerp_float4(float4 A, float4 B, float4 T, out float4 Out)
                    {
                        Out = lerp(A, B, T);
                    }
                
                    void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
                    {
                        Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
                    }
                
                    void Unity_Add_float4(float4 A, float4 B, out float4 Out)
                    {
                        Out = A + B;
                    }
                
                    void Unity_SceneDepth_Eye_float(float4 UV, out float Out)
                    {
                        Out = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH(UV.xy), _ZBufferParams);
                    }
                
                    void Unity_Subtract_float(float A, float B, out float Out)
                    {
                        Out = A - B;
                    }
                
                // Vertex Graph Evaluation
                    VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
                    {
                        VertexDescription description = (VertexDescription)0;
                        return description;
                    }
                    
                // Pixel Graph Evaluation
                    SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                    {
                        SurfaceDescription surface = (SurfaceDescription)0;
                        float4 _Property_BA1EC9E1_Out = Color_702AB34C;
                        float4 _Property_5D3D943D_Out = Color_518716D8;
                        float _Property_A5D6BEA2_Out = Vector1_742F243A;
                        float _Property_E0560113_Out = Vector1_AAE278D5;
                        float4 _Property_6F049BAF_Out = Vector4_5537CA4E;
                        float _Split_C5D3F9C2_R = _Property_6F049BAF_Out[0];
                        float _Split_C5D3F9C2_G = _Property_6F049BAF_Out[1];
                        float _Split_C5D3F9C2_B = _Property_6F049BAF_Out[2];
                        float _Split_C5D3F9C2_A = _Property_6F049BAF_Out[3];
                        float3 _RotateAboutAxis_4186120F_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_6F049BAF_Out.xyz), _Split_C5D3F9C2_A, _RotateAboutAxis_4186120F_Out);
                        float _Property_35D7EF57_Out = Vector1_FEF9B246;
                        float _Multiply_14C29873_Out;
                        Unity_Multiply_float(_Time.y, _Property_35D7EF57_Out, _Multiply_14C29873_Out);
                    
                        float2 _TilingAndOffset_2773ADC5_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), (_Multiply_14C29873_Out.xx), _TilingAndOffset_2773ADC5_Out);
                        float _Property_49DEE58A_Out = Vector1_1F258ECB;
                        float _GradientNoise_6B4CD361_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_2773ADC5_Out, _Property_49DEE58A_Out, _GradientNoise_6B4CD361_Out);
                        float2 _TilingAndOffset_23282D51_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), float2 (0,0), _TilingAndOffset_23282D51_Out);
                        float _GradientNoise_2DAA6CD5_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_23282D51_Out, _Property_49DEE58A_Out, _GradientNoise_2DAA6CD5_Out);
                        float _Add_ABFEF962_Out;
                        Unity_Add_float(_GradientNoise_6B4CD361_Out, _GradientNoise_2DAA6CD5_Out, _Add_ABFEF962_Out);
                        float _Divide_BED3250F_Out;
                        Unity_Divide_float(_Add_ABFEF962_Out, 2, _Divide_BED3250F_Out);
                        float _Saturate_6BA02F15_Out;
                        Unity_Saturate_float(_Divide_BED3250F_Out, _Saturate_6BA02F15_Out);
                        float _Property_4D85CCDA_Out = Vector1_50DCEA20;
                        float _Power_BC168E33_Out;
                        Unity_Power_float(_Saturate_6BA02F15_Out, _Property_4D85CCDA_Out, _Power_BC168E33_Out);
                        float4 _Property_7006A80A_Out = Vector4_E855A2D9;
                        float _Split_796B44F0_R = _Property_7006A80A_Out[0];
                        float _Split_796B44F0_G = _Property_7006A80A_Out[1];
                        float _Split_796B44F0_B = _Property_7006A80A_Out[2];
                        float _Split_796B44F0_A = _Property_7006A80A_Out[3];
                        float4 _Combine_979EF6BC_RGBA;
                        float3 _Combine_979EF6BC_RGB;
                        float2 _Combine_979EF6BC_RG;
                        Unity_Combine_float(_Split_796B44F0_R, _Split_796B44F0_G, 0, 0, _Combine_979EF6BC_RGBA, _Combine_979EF6BC_RGB, _Combine_979EF6BC_RG);
                        float4 _Combine_6F7F4E7C_RGBA;
                        float3 _Combine_6F7F4E7C_RGB;
                        float2 _Combine_6F7F4E7C_RG;
                        Unity_Combine_float(_Split_796B44F0_B, _Split_796B44F0_A, 0, 0, _Combine_6F7F4E7C_RGBA, _Combine_6F7F4E7C_RGB, _Combine_6F7F4E7C_RG);
                        float _Remap_FEC86696_Out;
                        Unity_Remap_float(_Power_BC168E33_Out, _Combine_979EF6BC_RG, _Combine_6F7F4E7C_RG, _Remap_FEC86696_Out);
                        float _Absolute_164E9E26_Out;
                        Unity_Absolute_float(_Remap_FEC86696_Out, _Absolute_164E9E26_Out);
                        float _Smoothstep_9A76A679_Out;
                        Unity_Smoothstep_float(_Property_A5D6BEA2_Out, _Property_E0560113_Out, _Absolute_164E9E26_Out, _Smoothstep_9A76A679_Out);
                        float4 _Property_FF51AF93_Out = Vector4_5537CA4E;
                        float _Split_8BA32C12_R = _Property_FF51AF93_Out[0];
                        float _Split_8BA32C12_G = _Property_FF51AF93_Out[1];
                        float _Split_8BA32C12_B = _Property_FF51AF93_Out[2];
                        float _Split_8BA32C12_A = _Property_FF51AF93_Out[3];
                        float3 _RotateAboutAxis_311AD17D_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_FF51AF93_Out.xyz), _Split_8BA32C12_A, _RotateAboutAxis_311AD17D_Out);
                        float _Property_4DFEC70D_Out = Vector1_A09568FA;
                        float _Multiply_54A56B7E_Out;
                        Unity_Multiply_float(_Time.y, _Property_4DFEC70D_Out, _Multiply_54A56B7E_Out);
                    
                        float2 _TilingAndOffset_A02A18F9_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_311AD17D_Out.xy), float2 (1,1), (_Multiply_54A56B7E_Out.xx), _TilingAndOffset_A02A18F9_Out);
                        float _Property_95BB6BB6_Out = Vector1_F997F91D;
                        float _GradientNoise_B18F1DE_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_A02A18F9_Out, _Property_95BB6BB6_Out, _GradientNoise_B18F1DE_Out);
                        float _Property_9A64B15E_Out = Vector1_E3371B04;
                        float _Multiply_B28321D7_Out;
                        Unity_Multiply_float(_GradientNoise_B18F1DE_Out, _Property_9A64B15E_Out, _Multiply_B28321D7_Out);
                    
                        float _Add_5282EE67_Out;
                        Unity_Add_float(_Smoothstep_9A76A679_Out, _Multiply_B28321D7_Out, _Add_5282EE67_Out);
                        float _Add_B2A28D15_Out;
                        Unity_Add_float(1, _Property_9A64B15E_Out, _Add_B2A28D15_Out);
                        float _Divide_A0FE7B8_Out;
                        Unity_Divide_float(_Add_5282EE67_Out, _Add_B2A28D15_Out, _Divide_A0FE7B8_Out);
                        float4 _Lerp_FA68C3C9_Out;
                        Unity_Lerp_float4(_Property_BA1EC9E1_Out, _Property_5D3D943D_Out, (_Divide_A0FE7B8_Out.xxxx), _Lerp_FA68C3C9_Out);
                        float _Property_985F6FC4_Out = Vector1_1245587D;
                        float _FresnelEffect_3C9252FF_Out;
                        Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, _Property_985F6FC4_Out, _FresnelEffect_3C9252FF_Out);
                        float _Multiply_6BC11FC5_Out;
                        Unity_Multiply_float(_Divide_A0FE7B8_Out, _FresnelEffect_3C9252FF_Out, _Multiply_6BC11FC5_Out);
                    
                        float _Property_7530A78F_Out = Vector1_D2EDE3A7;
                        float _Multiply_4EB09C19_Out;
                        Unity_Multiply_float(_Multiply_6BC11FC5_Out, _Property_7530A78F_Out, _Multiply_4EB09C19_Out);
                    
                        float4 _Add_AF1DCE31_Out;
                        Unity_Add_float4(_Lerp_FA68C3C9_Out, (_Multiply_4EB09C19_Out.xxxx), _Add_AF1DCE31_Out);
                        float _SceneDepth_8ABCD70C_Out;
                        Unity_SceneDepth_Eye_float(float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0), _SceneDepth_8ABCD70C_Out);
                        float4 _ScreenPosition_B973BFDF_Out = IN.ScreenPosition;
                        float _Split_E1759A96_R = _ScreenPosition_B973BFDF_Out[0];
                        float _Split_E1759A96_G = _ScreenPosition_B973BFDF_Out[1];
                        float _Split_E1759A96_B = _ScreenPosition_B973BFDF_Out[2];
                        float _Split_E1759A96_A = _ScreenPosition_B973BFDF_Out[3];
                        float _Subtract_D12B8E5A_Out;
                        Unity_Subtract_float(_Split_E1759A96_A, 1, _Subtract_D12B8E5A_Out);
                        float _Subtract_501D47FC_Out;
                        Unity_Subtract_float(_SceneDepth_8ABCD70C_Out, _Subtract_D12B8E5A_Out, _Subtract_501D47FC_Out);
                        float _Property_7D938F14_Out = Vector1_828AA966;
                        float _Divide_90AB178C_Out;
                        Unity_Divide_float(_Subtract_501D47FC_Out, _Property_7D938F14_Out, _Divide_90AB178C_Out);
                        float _Saturate_D0EAEC86_Out;
                        Unity_Saturate_float(_Divide_90AB178C_Out, _Saturate_D0EAEC86_Out);
                        surface.Color = (_Add_AF1DCE31_Out.xyz);
                        surface.Alpha = _Saturate_D0EAEC86_Out;
                        surface.AlphaClipThreshold = 0;
                        return surface;
                    }
                    
            //-------------------------------------------------------------------------------------
            // End graph generated code
            //-------------------------------------------------------------------------------------
        
        
        //-------------------------------------------------------------------------------------
        // TEMPLATE INCLUDE : VertexAnimation.template.hlsl
        //-------------------------------------------------------------------------------------
        
        VertexDescriptionInputs AttributesMeshToVertexDescriptionInputs(AttributesMesh input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);
        
        
            return output;
        }
        
        AttributesMesh ApplyMeshModification(AttributesMesh input)
        {
            // build graph inputs
            VertexDescriptionInputs vertexDescriptionInputs = AttributesMeshToVertexDescriptionInputs(input);
        
            // evaluate vertex graph
            VertexDescription vertexDescription = VertexDescriptionFunction(vertexDescriptionInputs);
        
            // copy graph output to the results
        
            return input;
        }
        
        //-------------------------------------------------------------------------------------
        // END TEMPLATE INCLUDE : VertexAnimation.template.hlsl
        //-------------------------------------------------------------------------------------
        
        
        
        //-------------------------------------------------------------------------------------
        // TEMPLATE INCLUDE : SharedCode.template.hlsl
        //-------------------------------------------------------------------------------------
            FragInputs BuildFragInputs(VaryingsMeshToPS input)
            {
                FragInputs output;
                ZERO_INITIALIZE(FragInputs, output);
        
                // Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
                // TODO: this is a really poor workaround, but the variable is used in a bunch of places
                // to compute normals which are then passed on elsewhere to compute other values...
                output.worldToTangent = k_identity3x3;
                output.positionSS = input.positionCS;       // input.positionCS is SV_Position
        
                output.positionRWS = input.positionRWS;
                output.worldToTangent = BuildWorldToTangent(input.tangentWS, input.normalWS);
                #if SHADER_STAGE_FRAGMENT
                #endif // SHADER_STAGE_FRAGMENT
        
                return output;
            }
        
            SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);
        
                output.WorldSpaceNormal =            normalize(input.worldToTangent[2].xyz);
                output.WorldSpaceViewDirection =     normalize(viewWS);
                output.WorldSpacePosition =          GetAbsolutePositionWS(input.positionRWS);
                output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
        
                return output;
            }
        
            // existing HDRP code uses the combined function to go directly from packed to frag inputs
            FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
            {
                UNITY_SETUP_INSTANCE_ID(input);
                VaryingsMeshToPS unpacked= UnpackVaryingsMeshToPS(input);
                return BuildFragInputs(unpacked);
            }
        
        //-------------------------------------------------------------------------------------
        // END TEMPLATE INCLUDE : SharedCode.template.hlsl
        //-------------------------------------------------------------------------------------
        
        
        
            void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
            {
                // setup defaults -- these are used if the graph doesn't output a value
                ZERO_INITIALIZE(SurfaceData, surfaceData);
        
                // copy across graph values, if defined
                surfaceData.color = surfaceDescription.Color;
        
        #if defined(DEBUG_DISPLAY)
                if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
                {
                    // TODO
                }
        #endif
            }
        
            void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
            {
                SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
                SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);
        
                // Perform alpha test very early to save performance (a killed pixel will not sample textures)
                // TODO: split graph evaluation to grab just alpha dependencies first? tricky..
        
                BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);
        
                // Builtin Data
                ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
        
                builtinData.opacity = surfaceDescription.Alpha;
        
        
        #if (SHADERPASS == SHADERPASS_DISTORTION)
                builtinData.distortion = surfaceDescription.Distortion;
                builtinData.distortionBlur = surfaceDescription.DistortionBlur;
        #else
                builtinData.distortion = float2(0.0, 0.0);
                builtinData.distortionBlur = 0.0;
        #endif
            }
        
            //-------------------------------------------------------------------------------------
            // Pass Includes
            //-------------------------------------------------------------------------------------
                #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassLightTransport.hlsl"
            //-------------------------------------------------------------------------------------
            // End Pass Includes
            //-------------------------------------------------------------------------------------
        
            ENDHLSL
        }
        
        Pass
        {
            // based on HDUnlitPassForward.template
            Name "SceneSelectionPass"
            Tags { "LightMode" = "SceneSelectionPass" }
        
            //-------------------------------------------------------------------------------------
            // Render Modes (Blend, Cull, ZTest, Stencil, etc)
            //-------------------------------------------------------------------------------------
            Blend One OneMinusSrcAlpha
        
            Cull Back
        
            ZTest LEqual
        
            ZWrite Off
        
            // Default Stencil
        
            ColorMask 0
        
            //-------------------------------------------------------------------------------------
            // End Render Modes
            //-------------------------------------------------------------------------------------
        
            HLSLPROGRAM
        
            #pragma target 4.5
            #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
            //#pragma enable_d3d11_debug_symbols
        
            //enable GPU instancing support
            #pragma multi_compile_instancing
        
            //-------------------------------------------------------------------------------------
            // Variant Definitions (active field translations to HDRP defines)
            //-------------------------------------------------------------------------------------
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _BLENDMODE_ALPHA 1
            //-------------------------------------------------------------------------------------
            // End Variant Definitions
            //-------------------------------------------------------------------------------------
        
            #pragma vertex Vert
            #pragma fragment Frag
        
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Wind.hlsl"
        
            // define FragInputs structure
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"
        
            //-------------------------------------------------------------------------------------
            // Defines
            //-------------------------------------------------------------------------------------
                    #define SHADERPASS SHADERPASS_DEPTH_ONLY
                #define SCENESELECTIONPASS
                #define REQUIRE_DEPTH_TEXTURE
        
            // this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
            #define ATTRIBUTES_NEED_NORMAL
            #define VARYINGS_NEED_POSITION_WS
            #define HAVE_MESH_MODIFICATION
        
            //-------------------------------------------------------------------------------------
            // End Defines
            //-------------------------------------------------------------------------------------
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
        #ifdef DEBUG_DISPLAY
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Debug/DebugDisplay.hlsl"
        #endif
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"
        
            // Used by SceneSelectionPass
            int _ObjectId;
            int _PassValue;
        
            //-------------------------------------------------------------------------------------
            // Interpolator Packing And Struct Declarations
            //-------------------------------------------------------------------------------------
        // Generated Type: AttributesMesh
        struct AttributesMesh {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        
        // Generated Type: VaryingsMeshToPS
        struct VaryingsMeshToPS {
            float4 positionCS : SV_Position;
            float3 positionRWS; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        struct PackedVaryingsMeshToPS {
            float3 interp00 : TEXCOORD0; // auto-packed
            float4 positionCS : SV_Position; // unpacked
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC; // unpacked
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
        {
            PackedVaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.interp00.xyz = input.positionRWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
        {
            VaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.positionRWS = input.interp00.xyz;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        
        // Generated Type: VaryingsMeshToDS
        struct VaryingsMeshToDS {
            float3 positionRWS;
            float3 normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        struct PackedVaryingsMeshToDS {
            float3 interp00 : TEXCOORD0; // auto-packed
            float3 interp01 : TEXCOORD1; // auto-packed
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC; // unpacked
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
        {
            PackedVaryingsMeshToDS output;
            output.interp00.xyz = input.positionRWS;
            output.interp01.xyz = input.normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
        {
            VaryingsMeshToDS output;
            output.positionRWS = input.interp00.xyz;
            output.normalWS = input.interp01.xyz;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        
            //-------------------------------------------------------------------------------------
            // End Interpolator Packing And Struct Declarations
            //-------------------------------------------------------------------------------------
        
            //-------------------------------------------------------------------------------------
            // Graph generated code
            //-------------------------------------------------------------------------------------
                    // Shared Graph Properties (uniform inputs)
                    CBUFFER_START(UnityPerMaterial)
                    float4 Vector4_5537CA4E;
                    float Vector1_1F258ECB;
                    float Vector1_FEF9B246;
                    float Vector1_7D67B347;
                    float4 Vector4_E855A2D9;
                    float4 Color_518716D8;
                    float4 Color_702AB34C;
                    float Vector1_742F243A;
                    float Vector1_AAE278D5;
                    float Vector1_F997F91D;
                    float Vector1_A09568FA;
                    float Vector1_50DCEA20;
                    float Vector1_E3371B04;
                    float Vector1_6642FA1C;
                    float Vector1_1245587D;
                    float Vector1_D2EDE3A7;
                    float Vector1_828AA966;
                    CBUFFER_END
                
                
                // Vertex Graph Inputs
                    struct VertexDescriptionInputs {
                        float3 ObjectSpaceNormal; // optional
                        float3 WorldSpaceNormal; // optional
                        float3 ObjectSpacePosition; // optional
                        float3 WorldSpacePosition; // optional
                    };
                // Vertex Graph Outputs
                    struct VertexDescription
                    {
                        float3 Position;
                    };
                    
                // Pixel Graph Inputs
                    struct SurfaceDescriptionInputs {
                        float3 WorldSpacePosition; // optional
                        float4 ScreenPosition; // optional
                    };
                // Pixel Graph Outputs
                    struct SurfaceDescription
                    {
                        float Alpha;
                        float AlphaClipThreshold;
                    };
                    
                // Shared Graph Node Functions
                
                    void Unity_SceneDepth_Eye_float(float4 UV, out float Out)
                    {
                        Out = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH(UV.xy), _ZBufferParams);
                    }
                
                    void Unity_Subtract_float(float A, float B, out float Out)
                    {
                        Out = A - B;
                    }
                
                    void Unity_Divide_float(float A, float B, out float Out)
                    {
                        Out = A / B;
                    }
                
                    void Unity_Saturate_float(float In, out float Out)
                    {
                        Out = saturate(In);
                    }
                
                    void Unity_Distance_float3(float3 A, float3 B, out float Out)
                    {
                        Out = distance(A, B);
                    }
                
                    void Unity_Power_float(float A, float B, out float Out)
                    {
                        Out = pow(A, B);
                    }
                
                    void Unity_Multiply_float (float3 A, float3 B, out float3 Out)
                    {
                        Out = A * B;
                    }
                
                    void Unity_Rotate_About_Axis_Degrees_float(float3 In, float3 Axis, float Rotation, out float3 Out)
                    {
                        float s = sin(Rotation);
                        float c = cos(Rotation);
                        float one_minus_c = 1.0 - c;
                        
                        Axis = normalize(Axis);
                
                        float3x3 rot_mat = { one_minus_c * Axis.x * Axis.x + c,            one_minus_c * Axis.x * Axis.y - Axis.z * s,     one_minus_c * Axis.z * Axis.x + Axis.y * s,
                                                   one_minus_c * Axis.x * Axis.y + Axis.z * s,   one_minus_c * Axis.y * Axis.y + c,              one_minus_c * Axis.y * Axis.z - Axis.x * s,
                                                   one_minus_c * Axis.z * Axis.x - Axis.y * s,   one_minus_c * Axis.y * Axis.z + Axis.x * s,     one_minus_c * Axis.z * Axis.z + c
                                                 };
                
                        Out = mul(rot_mat,  In);
                    }
                
                    void Unity_Multiply_float (float A, float B, out float Out)
                    {
                        Out = A * B;
                    }
                
                    void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
                    {
                        Out = UV * Tiling + Offset;
                    }
                
                
    float2 unity_gradientNoise_dir(float2 p)
    {
        // Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
        p = p % 289;
        float x = (34 * p.x + 1) * p.x % 289 + p.y;
        x = (34 * x + 1) * x % 289;
        x = frac(x / 41) * 2 - 1;
        return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
    }

                
    float unity_gradientNoise(float2 p)
    {
        float2 ip = floor(p);
        float2 fp = frac(p);
        float d00 = dot(unity_gradientNoise_dir(ip), fp);
        float d01 = dot(unity_gradientNoise_dir(ip + float2(0, 1)), fp - float2(0, 1));
        float d10 = dot(unity_gradientNoise_dir(ip + float2(1, 0)), fp - float2(1, 0));
        float d11 = dot(unity_gradientNoise_dir(ip + float2(1, 1)), fp - float2(1, 1));
        fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
        return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x);
    }

                    void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
                    { Out = unity_gradientNoise(UV * Scale) + 0.5; }
                
                    void Unity_Add_float(float A, float B, out float Out)
                    {
                        Out = A + B;
                    }
                
                    void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
                    {
                        RGBA = float4(R, G, B, A);
                        RGB = float3(R, G, B);
                        RG = float2(R, G);
                    }
                
                    void Unity_Remap_float(float In, float2 InMinMax, float2 OutMinMax, out float Out)
                    {
                        Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
                    }
                
                    void Unity_Absolute_float(float In, out float Out)
                    {
                        Out = abs(In);
                    }
                
                    void Unity_Smoothstep_float(float Edge1, float Edge2, float In, out float Out)
                    {
                        Out = smoothstep(Edge1, Edge2, In);
                    }
                
                    void Unity_Add_float3(float3 A, float3 B, out float3 Out)
                    {
                        Out = A + B;
                    }
                
                // Vertex Graph Evaluation
                    VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
                    {
                        VertexDescription description = (VertexDescription)0;
                        float _Distance_63B151C3_Out;
                        Unity_Distance_float3(SHADERGRAPH_OBJECT_POSITION, IN.WorldSpacePosition, _Distance_63B151C3_Out);
                        float _Property_F4DF73F4_Out = Vector1_6642FA1C;
                        float _Divide_5E236CDB_Out;
                        Unity_Divide_float(_Distance_63B151C3_Out, _Property_F4DF73F4_Out, _Divide_5E236CDB_Out);
                        float _Power_1995172_Out;
                        Unity_Power_float(_Divide_5E236CDB_Out, 3, _Power_1995172_Out);
                        float3 _Multiply_69F36212_Out;
                        Unity_Multiply_float(IN.WorldSpaceNormal, (_Power_1995172_Out.xxx), _Multiply_69F36212_Out);
                    
                        float _Property_A5D6BEA2_Out = Vector1_742F243A;
                        float _Property_E0560113_Out = Vector1_AAE278D5;
                        float4 _Property_6F049BAF_Out = Vector4_5537CA4E;
                        float _Split_C5D3F9C2_R = _Property_6F049BAF_Out[0];
                        float _Split_C5D3F9C2_G = _Property_6F049BAF_Out[1];
                        float _Split_C5D3F9C2_B = _Property_6F049BAF_Out[2];
                        float _Split_C5D3F9C2_A = _Property_6F049BAF_Out[3];
                        float3 _RotateAboutAxis_4186120F_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_6F049BAF_Out.xyz), _Split_C5D3F9C2_A, _RotateAboutAxis_4186120F_Out);
                        float _Property_35D7EF57_Out = Vector1_FEF9B246;
                        float _Multiply_14C29873_Out;
                        Unity_Multiply_float(_Time.y, _Property_35D7EF57_Out, _Multiply_14C29873_Out);
                    
                        float2 _TilingAndOffset_2773ADC5_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), (_Multiply_14C29873_Out.xx), _TilingAndOffset_2773ADC5_Out);
                        float _Property_49DEE58A_Out = Vector1_1F258ECB;
                        float _GradientNoise_6B4CD361_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_2773ADC5_Out, _Property_49DEE58A_Out, _GradientNoise_6B4CD361_Out);
                        float2 _TilingAndOffset_23282D51_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), float2 (0,0), _TilingAndOffset_23282D51_Out);
                        float _GradientNoise_2DAA6CD5_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_23282D51_Out, _Property_49DEE58A_Out, _GradientNoise_2DAA6CD5_Out);
                        float _Add_ABFEF962_Out;
                        Unity_Add_float(_GradientNoise_6B4CD361_Out, _GradientNoise_2DAA6CD5_Out, _Add_ABFEF962_Out);
                        float _Divide_BED3250F_Out;
                        Unity_Divide_float(_Add_ABFEF962_Out, 2, _Divide_BED3250F_Out);
                        float _Saturate_6BA02F15_Out;
                        Unity_Saturate_float(_Divide_BED3250F_Out, _Saturate_6BA02F15_Out);
                        float _Property_4D85CCDA_Out = Vector1_50DCEA20;
                        float _Power_BC168E33_Out;
                        Unity_Power_float(_Saturate_6BA02F15_Out, _Property_4D85CCDA_Out, _Power_BC168E33_Out);
                        float4 _Property_7006A80A_Out = Vector4_E855A2D9;
                        float _Split_796B44F0_R = _Property_7006A80A_Out[0];
                        float _Split_796B44F0_G = _Property_7006A80A_Out[1];
                        float _Split_796B44F0_B = _Property_7006A80A_Out[2];
                        float _Split_796B44F0_A = _Property_7006A80A_Out[3];
                        float4 _Combine_979EF6BC_RGBA;
                        float3 _Combine_979EF6BC_RGB;
                        float2 _Combine_979EF6BC_RG;
                        Unity_Combine_float(_Split_796B44F0_R, _Split_796B44F0_G, 0, 0, _Combine_979EF6BC_RGBA, _Combine_979EF6BC_RGB, _Combine_979EF6BC_RG);
                        float4 _Combine_6F7F4E7C_RGBA;
                        float3 _Combine_6F7F4E7C_RGB;
                        float2 _Combine_6F7F4E7C_RG;
                        Unity_Combine_float(_Split_796B44F0_B, _Split_796B44F0_A, 0, 0, _Combine_6F7F4E7C_RGBA, _Combine_6F7F4E7C_RGB, _Combine_6F7F4E7C_RG);
                        float _Remap_FEC86696_Out;
                        Unity_Remap_float(_Power_BC168E33_Out, _Combine_979EF6BC_RG, _Combine_6F7F4E7C_RG, _Remap_FEC86696_Out);
                        float _Absolute_164E9E26_Out;
                        Unity_Absolute_float(_Remap_FEC86696_Out, _Absolute_164E9E26_Out);
                        float _Smoothstep_9A76A679_Out;
                        Unity_Smoothstep_float(_Property_A5D6BEA2_Out, _Property_E0560113_Out, _Absolute_164E9E26_Out, _Smoothstep_9A76A679_Out);
                        float4 _Property_FF51AF93_Out = Vector4_5537CA4E;
                        float _Split_8BA32C12_R = _Property_FF51AF93_Out[0];
                        float _Split_8BA32C12_G = _Property_FF51AF93_Out[1];
                        float _Split_8BA32C12_B = _Property_FF51AF93_Out[2];
                        float _Split_8BA32C12_A = _Property_FF51AF93_Out[3];
                        float3 _RotateAboutAxis_311AD17D_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_FF51AF93_Out.xyz), _Split_8BA32C12_A, _RotateAboutAxis_311AD17D_Out);
                        float _Property_4DFEC70D_Out = Vector1_A09568FA;
                        float _Multiply_54A56B7E_Out;
                        Unity_Multiply_float(_Time.y, _Property_4DFEC70D_Out, _Multiply_54A56B7E_Out);
                    
                        float2 _TilingAndOffset_A02A18F9_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_311AD17D_Out.xy), float2 (1,1), (_Multiply_54A56B7E_Out.xx), _TilingAndOffset_A02A18F9_Out);
                        float _Property_95BB6BB6_Out = Vector1_F997F91D;
                        float _GradientNoise_B18F1DE_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_A02A18F9_Out, _Property_95BB6BB6_Out, _GradientNoise_B18F1DE_Out);
                        float _Property_9A64B15E_Out = Vector1_E3371B04;
                        float _Multiply_B28321D7_Out;
                        Unity_Multiply_float(_GradientNoise_B18F1DE_Out, _Property_9A64B15E_Out, _Multiply_B28321D7_Out);
                    
                        float _Add_5282EE67_Out;
                        Unity_Add_float(_Smoothstep_9A76A679_Out, _Multiply_B28321D7_Out, _Add_5282EE67_Out);
                        float _Add_B2A28D15_Out;
                        Unity_Add_float(1, _Property_9A64B15E_Out, _Add_B2A28D15_Out);
                        float _Divide_A0FE7B8_Out;
                        Unity_Divide_float(_Add_5282EE67_Out, _Add_B2A28D15_Out, _Divide_A0FE7B8_Out);
                        float3 _Multiply_8BCEAEFC_Out;
                        Unity_Multiply_float(IN.ObjectSpaceNormal, (_Divide_A0FE7B8_Out.xxx), _Multiply_8BCEAEFC_Out);
                    
                        float _Property_2C9980A6_Out = Vector1_7D67B347;
                        float3 _Multiply_C3D96CA2_Out;
                        Unity_Multiply_float(_Multiply_8BCEAEFC_Out, (_Property_2C9980A6_Out.xxx), _Multiply_C3D96CA2_Out);
                    
                        float3 _Add_815814F_Out;
                        Unity_Add_float3(IN.ObjectSpacePosition, _Multiply_C3D96CA2_Out, _Add_815814F_Out);
                        float3 _Add_D43B41A9_Out;
                        Unity_Add_float3(_Multiply_69F36212_Out, _Add_815814F_Out, _Add_D43B41A9_Out);
                        description.Position = _Add_D43B41A9_Out;
                        return description;
                    }
                    
                // Pixel Graph Evaluation
                    SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                    {
                        SurfaceDescription surface = (SurfaceDescription)0;
                        float _SceneDepth_8ABCD70C_Out;
                        Unity_SceneDepth_Eye_float(float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0), _SceneDepth_8ABCD70C_Out);
                        float4 _ScreenPosition_B973BFDF_Out = IN.ScreenPosition;
                        float _Split_E1759A96_R = _ScreenPosition_B973BFDF_Out[0];
                        float _Split_E1759A96_G = _ScreenPosition_B973BFDF_Out[1];
                        float _Split_E1759A96_B = _ScreenPosition_B973BFDF_Out[2];
                        float _Split_E1759A96_A = _ScreenPosition_B973BFDF_Out[3];
                        float _Subtract_D12B8E5A_Out;
                        Unity_Subtract_float(_Split_E1759A96_A, 1, _Subtract_D12B8E5A_Out);
                        float _Subtract_501D47FC_Out;
                        Unity_Subtract_float(_SceneDepth_8ABCD70C_Out, _Subtract_D12B8E5A_Out, _Subtract_501D47FC_Out);
                        float _Property_7D938F14_Out = Vector1_828AA966;
                        float _Divide_90AB178C_Out;
                        Unity_Divide_float(_Subtract_501D47FC_Out, _Property_7D938F14_Out, _Divide_90AB178C_Out);
                        float _Saturate_D0EAEC86_Out;
                        Unity_Saturate_float(_Divide_90AB178C_Out, _Saturate_D0EAEC86_Out);
                        surface.Alpha = _Saturate_D0EAEC86_Out;
                        surface.AlphaClipThreshold = 0;
                        return surface;
                    }
                    
            //-------------------------------------------------------------------------------------
            // End graph generated code
            //-------------------------------------------------------------------------------------
        
        
        //-------------------------------------------------------------------------------------
        // TEMPLATE INCLUDE : VertexAnimation.template.hlsl
        //-------------------------------------------------------------------------------------
        
        VertexDescriptionInputs AttributesMeshToVertexDescriptionInputs(AttributesMesh input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);
        
            output.ObjectSpaceNormal =           input.normalOS;
            output.WorldSpaceNormal =            TransformObjectToWorldNormal(input.normalOS);
            output.ObjectSpacePosition =         input.positionOS;
            output.WorldSpacePosition =          GetAbsolutePositionWS(TransformObjectToWorld(input.positionOS));
        
            return output;
        }
        
        AttributesMesh ApplyMeshModification(AttributesMesh input)
        {
            // build graph inputs
            VertexDescriptionInputs vertexDescriptionInputs = AttributesMeshToVertexDescriptionInputs(input);
        
            // evaluate vertex graph
            VertexDescription vertexDescription = VertexDescriptionFunction(vertexDescriptionInputs);
        
            // copy graph output to the results
            input.positionOS = vertexDescription.Position;
        
            return input;
        }
        
        //-------------------------------------------------------------------------------------
        // END TEMPLATE INCLUDE : VertexAnimation.template.hlsl
        //-------------------------------------------------------------------------------------
        
        
        
        //-------------------------------------------------------------------------------------
        // TEMPLATE INCLUDE : SharedCode.template.hlsl
        //-------------------------------------------------------------------------------------
            FragInputs BuildFragInputs(VaryingsMeshToPS input)
            {
                FragInputs output;
                ZERO_INITIALIZE(FragInputs, output);
        
                // Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
                // TODO: this is a really poor workaround, but the variable is used in a bunch of places
                // to compute normals which are then passed on elsewhere to compute other values...
                output.worldToTangent = k_identity3x3;
                output.positionSS = input.positionCS;       // input.positionCS is SV_Position
        
                output.positionRWS = input.positionRWS;
                #if SHADER_STAGE_FRAGMENT
                #endif // SHADER_STAGE_FRAGMENT
        
                return output;
            }
        
            SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);
        
                output.WorldSpacePosition =          GetAbsolutePositionWS(input.positionRWS);
                output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
        
                return output;
            }
        
            // existing HDRP code uses the combined function to go directly from packed to frag inputs
            FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
            {
                UNITY_SETUP_INSTANCE_ID(input);
                VaryingsMeshToPS unpacked= UnpackVaryingsMeshToPS(input);
                return BuildFragInputs(unpacked);
            }
        
        //-------------------------------------------------------------------------------------
        // END TEMPLATE INCLUDE : SharedCode.template.hlsl
        //-------------------------------------------------------------------------------------
        
        
        
            void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
            {
                // setup defaults -- these are used if the graph doesn't output a value
                ZERO_INITIALIZE(SurfaceData, surfaceData);
        
                // copy across graph values, if defined
        
        #if defined(DEBUG_DISPLAY)
                if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
                {
                    // TODO
                }
        #endif
            }
        
            void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
            {
                SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
                SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);
        
                // Perform alpha test very early to save performance (a killed pixel will not sample textures)
                // TODO: split graph evaluation to grab just alpha dependencies first? tricky..
        
                BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);
        
                // Builtin Data
                ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
        
                builtinData.opacity = surfaceDescription.Alpha;
        
        
        #if (SHADERPASS == SHADERPASS_DISTORTION)
                builtinData.distortion = surfaceDescription.Distortion;
                builtinData.distortionBlur = surfaceDescription.DistortionBlur;
        #else
                builtinData.distortion = float2(0.0, 0.0);
                builtinData.distortionBlur = 0.0;
        #endif
            }
        
            //-------------------------------------------------------------------------------------
            // Pass Includes
            //-------------------------------------------------------------------------------------
                #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassDepthOnly.hlsl"
            //-------------------------------------------------------------------------------------
            // End Pass Includes
            //-------------------------------------------------------------------------------------
        
            ENDHLSL
        }
        
        Pass
        {
            // based on HDUnlitPassForward.template
            Name "ForwardOnly"
            Tags { "LightMode" = "ForwardOnly" }
        
            //-------------------------------------------------------------------------------------
            // Render Modes (Blend, Cull, ZTest, Stencil, etc)
            //-------------------------------------------------------------------------------------
            Blend One OneMinusSrcAlpha
        
            Cull Back
        
            ZTest LEqual
        
            ZWrite On
        
            // Default Stencil
        
            
            //-------------------------------------------------------------------------------------
            // End Render Modes
            //-------------------------------------------------------------------------------------
        
            HLSLPROGRAM
        
            #pragma target 4.5
            #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
            //#pragma enable_d3d11_debug_symbols
        
            //enable GPU instancing support
            #pragma multi_compile_instancing
        
            //-------------------------------------------------------------------------------------
            // Variant Definitions (active field translations to HDRP defines)
            //-------------------------------------------------------------------------------------
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _BLENDMODE_ALPHA 1
            //-------------------------------------------------------------------------------------
            // End Variant Definitions
            //-------------------------------------------------------------------------------------
        
            #pragma vertex Vert
            #pragma fragment Frag
        
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Wind.hlsl"
        
            // define FragInputs structure
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"
        
            //-------------------------------------------------------------------------------------
            // Defines
            //-------------------------------------------------------------------------------------
                    #define SHADERPASS SHADERPASS_FORWARD_UNLIT
                #pragma multi_compile _ DEBUG_DISPLAY
                #pragma multi_compile _ LIGHTMAP_ON
                #pragma multi_compile _ DIRLIGHTMAP_COMBINED
                #pragma multi_compile _ DYNAMICLIGHTMAP_ON
                #define REQUIRE_DEPTH_TEXTURE
        
            // this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define VARYINGS_NEED_POSITION_WS
            #define VARYINGS_NEED_TANGENT_TO_WORLD
            #define HAVE_MESH_MODIFICATION
        
            //-------------------------------------------------------------------------------------
            // End Defines
            //-------------------------------------------------------------------------------------
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
        #ifdef DEBUG_DISPLAY
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Debug/DebugDisplay.hlsl"
        #endif
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"
        
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
            #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"
        
            // Used by SceneSelectionPass
            int _ObjectId;
            int _PassValue;
        
            //-------------------------------------------------------------------------------------
            // Interpolator Packing And Struct Declarations
            //-------------------------------------------------------------------------------------
        // Generated Type: AttributesMesh
        struct AttributesMesh {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL; // optional
            float4 tangentOS : TANGENT; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        
        // Generated Type: VaryingsMeshToPS
        struct VaryingsMeshToPS {
            float4 positionCS : SV_Position;
            float3 positionRWS; // optional
            float3 normalWS; // optional
            float4 tangentWS; // optional
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        struct PackedVaryingsMeshToPS {
            float3 interp00 : TEXCOORD0; // auto-packed
            float3 interp01 : TEXCOORD1; // auto-packed
            float4 interp02 : TEXCOORD2; // auto-packed
            float4 positionCS : SV_Position; // unpacked
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC; // unpacked
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
        {
            PackedVaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.interp00.xyz = input.positionRWS;
            output.interp01.xyz = input.normalWS;
            output.interp02.xyzw = input.tangentWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
        {
            VaryingsMeshToPS output;
            output.positionCS = input.positionCS;
            output.positionRWS = input.interp00.xyz;
            output.normalWS = input.interp01.xyz;
            output.tangentWS = input.interp02.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        
        // Generated Type: VaryingsMeshToDS
        struct VaryingsMeshToDS {
            float3 positionRWS;
            float3 normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        struct PackedVaryingsMeshToDS {
            float3 interp00 : TEXCOORD0; // auto-packed
            float3 interp01 : TEXCOORD1; // auto-packed
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC; // unpacked
            #endif // UNITY_ANY_INSTANCING_ENABLED
        };
        PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
        {
            PackedVaryingsMeshToDS output;
            output.interp00.xyz = input.positionRWS;
            output.interp01.xyz = input.normalWS;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
        {
            VaryingsMeshToDS output;
            output.positionRWS = input.interp00.xyz;
            output.normalWS = input.interp01.xyz;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif // UNITY_ANY_INSTANCING_ENABLED
            return output;
        }
        
            //-------------------------------------------------------------------------------------
            // End Interpolator Packing And Struct Declarations
            //-------------------------------------------------------------------------------------
        
            //-------------------------------------------------------------------------------------
            // Graph generated code
            //-------------------------------------------------------------------------------------
                    // Shared Graph Properties (uniform inputs)
                    CBUFFER_START(UnityPerMaterial)
                    float4 Vector4_5537CA4E;
                    float Vector1_1F258ECB;
                    float Vector1_FEF9B246;
                    float Vector1_7D67B347;
                    float4 Vector4_E855A2D9;
                    float4 Color_518716D8;
                    float4 Color_702AB34C;
                    float Vector1_742F243A;
                    float Vector1_AAE278D5;
                    float Vector1_F997F91D;
                    float Vector1_A09568FA;
                    float Vector1_50DCEA20;
                    float Vector1_E3371B04;
                    float Vector1_6642FA1C;
                    float Vector1_1245587D;
                    float Vector1_D2EDE3A7;
                    float Vector1_828AA966;
                    CBUFFER_END
                
                
                // Vertex Graph Inputs
                    struct VertexDescriptionInputs {
                        float3 ObjectSpaceNormal; // optional
                        float3 WorldSpaceNormal; // optional
                        float3 ObjectSpacePosition; // optional
                        float3 WorldSpacePosition; // optional
                    };
                // Vertex Graph Outputs
                    struct VertexDescription
                    {
                        float3 Position;
                    };
                    
                // Pixel Graph Inputs
                    struct SurfaceDescriptionInputs {
                        float3 WorldSpaceNormal; // optional
                        float3 WorldSpaceViewDirection; // optional
                        float3 WorldSpacePosition; // optional
                        float4 ScreenPosition; // optional
                    };
                // Pixel Graph Outputs
                    struct SurfaceDescription
                    {
                        float3 Color;
                        float Alpha;
                        float AlphaClipThreshold;
                    };
                    
                // Shared Graph Node Functions
                
                    void Unity_Rotate_About_Axis_Degrees_float(float3 In, float3 Axis, float Rotation, out float3 Out)
                    {
                        float s = sin(Rotation);
                        float c = cos(Rotation);
                        float one_minus_c = 1.0 - c;
                        
                        Axis = normalize(Axis);
                
                        float3x3 rot_mat = { one_minus_c * Axis.x * Axis.x + c,            one_minus_c * Axis.x * Axis.y - Axis.z * s,     one_minus_c * Axis.z * Axis.x + Axis.y * s,
                                                   one_minus_c * Axis.x * Axis.y + Axis.z * s,   one_minus_c * Axis.y * Axis.y + c,              one_minus_c * Axis.y * Axis.z - Axis.x * s,
                                                   one_minus_c * Axis.z * Axis.x - Axis.y * s,   one_minus_c * Axis.y * Axis.z + Axis.x * s,     one_minus_c * Axis.z * Axis.z + c
                                                 };
                
                        Out = mul(rot_mat,  In);
                    }
                
                    void Unity_Multiply_float (float A, float B, out float Out)
                    {
                        Out = A * B;
                    }
                
                    void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
                    {
                        Out = UV * Tiling + Offset;
                    }
                
                
    float2 unity_gradientNoise_dir(float2 p)
    {
        // Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
        p = p % 289;
        float x = (34 * p.x + 1) * p.x % 289 + p.y;
        x = (34 * x + 1) * x % 289;
        x = frac(x / 41) * 2 - 1;
        return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
    }

                
    float unity_gradientNoise(float2 p)
    {
        float2 ip = floor(p);
        float2 fp = frac(p);
        float d00 = dot(unity_gradientNoise_dir(ip), fp);
        float d01 = dot(unity_gradientNoise_dir(ip + float2(0, 1)), fp - float2(0, 1));
        float d10 = dot(unity_gradientNoise_dir(ip + float2(1, 0)), fp - float2(1, 0));
        float d11 = dot(unity_gradientNoise_dir(ip + float2(1, 1)), fp - float2(1, 1));
        fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
        return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x);
    }

                    void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
                    { Out = unity_gradientNoise(UV * Scale) + 0.5; }
                
                    void Unity_Add_float(float A, float B, out float Out)
                    {
                        Out = A + B;
                    }
                
                    void Unity_Divide_float(float A, float B, out float Out)
                    {
                        Out = A / B;
                    }
                
                    void Unity_Saturate_float(float In, out float Out)
                    {
                        Out = saturate(In);
                    }
                
                    void Unity_Power_float(float A, float B, out float Out)
                    {
                        Out = pow(A, B);
                    }
                
                    void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
                    {
                        RGBA = float4(R, G, B, A);
                        RGB = float3(R, G, B);
                        RG = float2(R, G);
                    }
                
                    void Unity_Remap_float(float In, float2 InMinMax, float2 OutMinMax, out float Out)
                    {
                        Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
                    }
                
                    void Unity_Absolute_float(float In, out float Out)
                    {
                        Out = abs(In);
                    }
                
                    void Unity_Smoothstep_float(float Edge1, float Edge2, float In, out float Out)
                    {
                        Out = smoothstep(Edge1, Edge2, In);
                    }
                
                    void Unity_Lerp_float4(float4 A, float4 B, float4 T, out float4 Out)
                    {
                        Out = lerp(A, B, T);
                    }
                
                    void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
                    {
                        Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
                    }
                
                    void Unity_Add_float4(float4 A, float4 B, out float4 Out)
                    {
                        Out = A + B;
                    }
                
                    void Unity_SceneDepth_Eye_float(float4 UV, out float Out)
                    {
                        Out = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH(UV.xy), _ZBufferParams);
                    }
                
                    void Unity_Subtract_float(float A, float B, out float Out)
                    {
                        Out = A - B;
                    }
                
                    void Unity_Distance_float3(float3 A, float3 B, out float Out)
                    {
                        Out = distance(A, B);
                    }
                
                    void Unity_Multiply_float (float3 A, float3 B, out float3 Out)
                    {
                        Out = A * B;
                    }
                
                    void Unity_Add_float3(float3 A, float3 B, out float3 Out)
                    {
                        Out = A + B;
                    }
                
                // Vertex Graph Evaluation
                    VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
                    {
                        VertexDescription description = (VertexDescription)0;
                        float _Distance_63B151C3_Out;
                        Unity_Distance_float3(SHADERGRAPH_OBJECT_POSITION, IN.WorldSpacePosition, _Distance_63B151C3_Out);
                        float _Property_F4DF73F4_Out = Vector1_6642FA1C;
                        float _Divide_5E236CDB_Out;
                        Unity_Divide_float(_Distance_63B151C3_Out, _Property_F4DF73F4_Out, _Divide_5E236CDB_Out);
                        float _Power_1995172_Out;
                        Unity_Power_float(_Divide_5E236CDB_Out, 3, _Power_1995172_Out);
                        float3 _Multiply_69F36212_Out;
                        Unity_Multiply_float(IN.WorldSpaceNormal, (_Power_1995172_Out.xxx), _Multiply_69F36212_Out);
                    
                        float _Property_A5D6BEA2_Out = Vector1_742F243A;
                        float _Property_E0560113_Out = Vector1_AAE278D5;
                        float4 _Property_6F049BAF_Out = Vector4_5537CA4E;
                        float _Split_C5D3F9C2_R = _Property_6F049BAF_Out[0];
                        float _Split_C5D3F9C2_G = _Property_6F049BAF_Out[1];
                        float _Split_C5D3F9C2_B = _Property_6F049BAF_Out[2];
                        float _Split_C5D3F9C2_A = _Property_6F049BAF_Out[3];
                        float3 _RotateAboutAxis_4186120F_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_6F049BAF_Out.xyz), _Split_C5D3F9C2_A, _RotateAboutAxis_4186120F_Out);
                        float _Property_35D7EF57_Out = Vector1_FEF9B246;
                        float _Multiply_14C29873_Out;
                        Unity_Multiply_float(_Time.y, _Property_35D7EF57_Out, _Multiply_14C29873_Out);
                    
                        float2 _TilingAndOffset_2773ADC5_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), (_Multiply_14C29873_Out.xx), _TilingAndOffset_2773ADC5_Out);
                        float _Property_49DEE58A_Out = Vector1_1F258ECB;
                        float _GradientNoise_6B4CD361_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_2773ADC5_Out, _Property_49DEE58A_Out, _GradientNoise_6B4CD361_Out);
                        float2 _TilingAndOffset_23282D51_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), float2 (0,0), _TilingAndOffset_23282D51_Out);
                        float _GradientNoise_2DAA6CD5_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_23282D51_Out, _Property_49DEE58A_Out, _GradientNoise_2DAA6CD5_Out);
                        float _Add_ABFEF962_Out;
                        Unity_Add_float(_GradientNoise_6B4CD361_Out, _GradientNoise_2DAA6CD5_Out, _Add_ABFEF962_Out);
                        float _Divide_BED3250F_Out;
                        Unity_Divide_float(_Add_ABFEF962_Out, 2, _Divide_BED3250F_Out);
                        float _Saturate_6BA02F15_Out;
                        Unity_Saturate_float(_Divide_BED3250F_Out, _Saturate_6BA02F15_Out);
                        float _Property_4D85CCDA_Out = Vector1_50DCEA20;
                        float _Power_BC168E33_Out;
                        Unity_Power_float(_Saturate_6BA02F15_Out, _Property_4D85CCDA_Out, _Power_BC168E33_Out);
                        float4 _Property_7006A80A_Out = Vector4_E855A2D9;
                        float _Split_796B44F0_R = _Property_7006A80A_Out[0];
                        float _Split_796B44F0_G = _Property_7006A80A_Out[1];
                        float _Split_796B44F0_B = _Property_7006A80A_Out[2];
                        float _Split_796B44F0_A = _Property_7006A80A_Out[3];
                        float4 _Combine_979EF6BC_RGBA;
                        float3 _Combine_979EF6BC_RGB;
                        float2 _Combine_979EF6BC_RG;
                        Unity_Combine_float(_Split_796B44F0_R, _Split_796B44F0_G, 0, 0, _Combine_979EF6BC_RGBA, _Combine_979EF6BC_RGB, _Combine_979EF6BC_RG);
                        float4 _Combine_6F7F4E7C_RGBA;
                        float3 _Combine_6F7F4E7C_RGB;
                        float2 _Combine_6F7F4E7C_RG;
                        Unity_Combine_float(_Split_796B44F0_B, _Split_796B44F0_A, 0, 0, _Combine_6F7F4E7C_RGBA, _Combine_6F7F4E7C_RGB, _Combine_6F7F4E7C_RG);
                        float _Remap_FEC86696_Out;
                        Unity_Remap_float(_Power_BC168E33_Out, _Combine_979EF6BC_RG, _Combine_6F7F4E7C_RG, _Remap_FEC86696_Out);
                        float _Absolute_164E9E26_Out;
                        Unity_Absolute_float(_Remap_FEC86696_Out, _Absolute_164E9E26_Out);
                        float _Smoothstep_9A76A679_Out;
                        Unity_Smoothstep_float(_Property_A5D6BEA2_Out, _Property_E0560113_Out, _Absolute_164E9E26_Out, _Smoothstep_9A76A679_Out);
                        float4 _Property_FF51AF93_Out = Vector4_5537CA4E;
                        float _Split_8BA32C12_R = _Property_FF51AF93_Out[0];
                        float _Split_8BA32C12_G = _Property_FF51AF93_Out[1];
                        float _Split_8BA32C12_B = _Property_FF51AF93_Out[2];
                        float _Split_8BA32C12_A = _Property_FF51AF93_Out[3];
                        float3 _RotateAboutAxis_311AD17D_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_FF51AF93_Out.xyz), _Split_8BA32C12_A, _RotateAboutAxis_311AD17D_Out);
                        float _Property_4DFEC70D_Out = Vector1_A09568FA;
                        float _Multiply_54A56B7E_Out;
                        Unity_Multiply_float(_Time.y, _Property_4DFEC70D_Out, _Multiply_54A56B7E_Out);
                    
                        float2 _TilingAndOffset_A02A18F9_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_311AD17D_Out.xy), float2 (1,1), (_Multiply_54A56B7E_Out.xx), _TilingAndOffset_A02A18F9_Out);
                        float _Property_95BB6BB6_Out = Vector1_F997F91D;
                        float _GradientNoise_B18F1DE_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_A02A18F9_Out, _Property_95BB6BB6_Out, _GradientNoise_B18F1DE_Out);
                        float _Property_9A64B15E_Out = Vector1_E3371B04;
                        float _Multiply_B28321D7_Out;
                        Unity_Multiply_float(_GradientNoise_B18F1DE_Out, _Property_9A64B15E_Out, _Multiply_B28321D7_Out);
                    
                        float _Add_5282EE67_Out;
                        Unity_Add_float(_Smoothstep_9A76A679_Out, _Multiply_B28321D7_Out, _Add_5282EE67_Out);
                        float _Add_B2A28D15_Out;
                        Unity_Add_float(1, _Property_9A64B15E_Out, _Add_B2A28D15_Out);
                        float _Divide_A0FE7B8_Out;
                        Unity_Divide_float(_Add_5282EE67_Out, _Add_B2A28D15_Out, _Divide_A0FE7B8_Out);
                        float3 _Multiply_8BCEAEFC_Out;
                        Unity_Multiply_float(IN.ObjectSpaceNormal, (_Divide_A0FE7B8_Out.xxx), _Multiply_8BCEAEFC_Out);
                    
                        float _Property_2C9980A6_Out = Vector1_7D67B347;
                        float3 _Multiply_C3D96CA2_Out;
                        Unity_Multiply_float(_Multiply_8BCEAEFC_Out, (_Property_2C9980A6_Out.xxx), _Multiply_C3D96CA2_Out);
                    
                        float3 _Add_815814F_Out;
                        Unity_Add_float3(IN.ObjectSpacePosition, _Multiply_C3D96CA2_Out, _Add_815814F_Out);
                        float3 _Add_D43B41A9_Out;
                        Unity_Add_float3(_Multiply_69F36212_Out, _Add_815814F_Out, _Add_D43B41A9_Out);
                        description.Position = _Add_D43B41A9_Out;
                        return description;
                    }
                    
                // Pixel Graph Evaluation
                    SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                    {
                        SurfaceDescription surface = (SurfaceDescription)0;
                        float4 _Property_BA1EC9E1_Out = Color_702AB34C;
                        float4 _Property_5D3D943D_Out = Color_518716D8;
                        float _Property_A5D6BEA2_Out = Vector1_742F243A;
                        float _Property_E0560113_Out = Vector1_AAE278D5;
                        float4 _Property_6F049BAF_Out = Vector4_5537CA4E;
                        float _Split_C5D3F9C2_R = _Property_6F049BAF_Out[0];
                        float _Split_C5D3F9C2_G = _Property_6F049BAF_Out[1];
                        float _Split_C5D3F9C2_B = _Property_6F049BAF_Out[2];
                        float _Split_C5D3F9C2_A = _Property_6F049BAF_Out[3];
                        float3 _RotateAboutAxis_4186120F_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_6F049BAF_Out.xyz), _Split_C5D3F9C2_A, _RotateAboutAxis_4186120F_Out);
                        float _Property_35D7EF57_Out = Vector1_FEF9B246;
                        float _Multiply_14C29873_Out;
                        Unity_Multiply_float(_Time.y, _Property_35D7EF57_Out, _Multiply_14C29873_Out);
                    
                        float2 _TilingAndOffset_2773ADC5_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), (_Multiply_14C29873_Out.xx), _TilingAndOffset_2773ADC5_Out);
                        float _Property_49DEE58A_Out = Vector1_1F258ECB;
                        float _GradientNoise_6B4CD361_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_2773ADC5_Out, _Property_49DEE58A_Out, _GradientNoise_6B4CD361_Out);
                        float2 _TilingAndOffset_23282D51_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_4186120F_Out.xy), float2 (1,1), float2 (0,0), _TilingAndOffset_23282D51_Out);
                        float _GradientNoise_2DAA6CD5_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_23282D51_Out, _Property_49DEE58A_Out, _GradientNoise_2DAA6CD5_Out);
                        float _Add_ABFEF962_Out;
                        Unity_Add_float(_GradientNoise_6B4CD361_Out, _GradientNoise_2DAA6CD5_Out, _Add_ABFEF962_Out);
                        float _Divide_BED3250F_Out;
                        Unity_Divide_float(_Add_ABFEF962_Out, 2, _Divide_BED3250F_Out);
                        float _Saturate_6BA02F15_Out;
                        Unity_Saturate_float(_Divide_BED3250F_Out, _Saturate_6BA02F15_Out);
                        float _Property_4D85CCDA_Out = Vector1_50DCEA20;
                        float _Power_BC168E33_Out;
                        Unity_Power_float(_Saturate_6BA02F15_Out, _Property_4D85CCDA_Out, _Power_BC168E33_Out);
                        float4 _Property_7006A80A_Out = Vector4_E855A2D9;
                        float _Split_796B44F0_R = _Property_7006A80A_Out[0];
                        float _Split_796B44F0_G = _Property_7006A80A_Out[1];
                        float _Split_796B44F0_B = _Property_7006A80A_Out[2];
                        float _Split_796B44F0_A = _Property_7006A80A_Out[3];
                        float4 _Combine_979EF6BC_RGBA;
                        float3 _Combine_979EF6BC_RGB;
                        float2 _Combine_979EF6BC_RG;
                        Unity_Combine_float(_Split_796B44F0_R, _Split_796B44F0_G, 0, 0, _Combine_979EF6BC_RGBA, _Combine_979EF6BC_RGB, _Combine_979EF6BC_RG);
                        float4 _Combine_6F7F4E7C_RGBA;
                        float3 _Combine_6F7F4E7C_RGB;
                        float2 _Combine_6F7F4E7C_RG;
                        Unity_Combine_float(_Split_796B44F0_B, _Split_796B44F0_A, 0, 0, _Combine_6F7F4E7C_RGBA, _Combine_6F7F4E7C_RGB, _Combine_6F7F4E7C_RG);
                        float _Remap_FEC86696_Out;
                        Unity_Remap_float(_Power_BC168E33_Out, _Combine_979EF6BC_RG, _Combine_6F7F4E7C_RG, _Remap_FEC86696_Out);
                        float _Absolute_164E9E26_Out;
                        Unity_Absolute_float(_Remap_FEC86696_Out, _Absolute_164E9E26_Out);
                        float _Smoothstep_9A76A679_Out;
                        Unity_Smoothstep_float(_Property_A5D6BEA2_Out, _Property_E0560113_Out, _Absolute_164E9E26_Out, _Smoothstep_9A76A679_Out);
                        float4 _Property_FF51AF93_Out = Vector4_5537CA4E;
                        float _Split_8BA32C12_R = _Property_FF51AF93_Out[0];
                        float _Split_8BA32C12_G = _Property_FF51AF93_Out[1];
                        float _Split_8BA32C12_B = _Property_FF51AF93_Out[2];
                        float _Split_8BA32C12_A = _Property_FF51AF93_Out[3];
                        float3 _RotateAboutAxis_311AD17D_Out;
                        Unity_Rotate_About_Axis_Degrees_float(IN.WorldSpacePosition, (_Property_FF51AF93_Out.xyz), _Split_8BA32C12_A, _RotateAboutAxis_311AD17D_Out);
                        float _Property_4DFEC70D_Out = Vector1_A09568FA;
                        float _Multiply_54A56B7E_Out;
                        Unity_Multiply_float(_Time.y, _Property_4DFEC70D_Out, _Multiply_54A56B7E_Out);
                    
                        float2 _TilingAndOffset_A02A18F9_Out;
                        Unity_TilingAndOffset_float((_RotateAboutAxis_311AD17D_Out.xy), float2 (1,1), (_Multiply_54A56B7E_Out.xx), _TilingAndOffset_A02A18F9_Out);
                        float _Property_95BB6BB6_Out = Vector1_F997F91D;
                        float _GradientNoise_B18F1DE_Out;
                        Unity_GradientNoise_float(_TilingAndOffset_A02A18F9_Out, _Property_95BB6BB6_Out, _GradientNoise_B18F1DE_Out);
                        float _Property_9A64B15E_Out = Vector1_E3371B04;
                        float _Multiply_B28321D7_Out;
                        Unity_Multiply_float(_GradientNoise_B18F1DE_Out, _Property_9A64B15E_Out, _Multiply_B28321D7_Out);
                    
                        float _Add_5282EE67_Out;
                        Unity_Add_float(_Smoothstep_9A76A679_Out, _Multiply_B28321D7_Out, _Add_5282EE67_Out);
                        float _Add_B2A28D15_Out;
                        Unity_Add_float(1, _Property_9A64B15E_Out, _Add_B2A28D15_Out);
                        float _Divide_A0FE7B8_Out;
                        Unity_Divide_float(_Add_5282EE67_Out, _Add_B2A28D15_Out, _Divide_A0FE7B8_Out);
                        float4 _Lerp_FA68C3C9_Out;
                        Unity_Lerp_float4(_Property_BA1EC9E1_Out, _Property_5D3D943D_Out, (_Divide_A0FE7B8_Out.xxxx), _Lerp_FA68C3C9_Out);
                        float _Property_985F6FC4_Out = Vector1_1245587D;
                        float _FresnelEffect_3C9252FF_Out;
                        Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, _Property_985F6FC4_Out, _FresnelEffect_3C9252FF_Out);
                        float _Multiply_6BC11FC5_Out;
                        Unity_Multiply_float(_Divide_A0FE7B8_Out, _FresnelEffect_3C9252FF_Out, _Multiply_6BC11FC5_Out);
                    
                        float _Property_7530A78F_Out = Vector1_D2EDE3A7;
                        float _Multiply_4EB09C19_Out;
                        Unity_Multiply_float(_Multiply_6BC11FC5_Out, _Property_7530A78F_Out, _Multiply_4EB09C19_Out);
                    
                        float4 _Add_AF1DCE31_Out;
                        Unity_Add_float4(_Lerp_FA68C3C9_Out, (_Multiply_4EB09C19_Out.xxxx), _Add_AF1DCE31_Out);
                        float _SceneDepth_8ABCD70C_Out;
                        Unity_SceneDepth_Eye_float(float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0), _SceneDepth_8ABCD70C_Out);
                        float4 _ScreenPosition_B973BFDF_Out = IN.ScreenPosition;
                        float _Split_E1759A96_R = _ScreenPosition_B973BFDF_Out[0];
                        float _Split_E1759A96_G = _ScreenPosition_B973BFDF_Out[1];
                        float _Split_E1759A96_B = _ScreenPosition_B973BFDF_Out[2];
                        float _Split_E1759A96_A = _ScreenPosition_B973BFDF_Out[3];
                        float _Subtract_D12B8E5A_Out;
                        Unity_Subtract_float(_Split_E1759A96_A, 1, _Subtract_D12B8E5A_Out);
                        float _Subtract_501D47FC_Out;
                        Unity_Subtract_float(_SceneDepth_8ABCD70C_Out, _Subtract_D12B8E5A_Out, _Subtract_501D47FC_Out);
                        float _Property_7D938F14_Out = Vector1_828AA966;
                        float _Divide_90AB178C_Out;
                        Unity_Divide_float(_Subtract_501D47FC_Out, _Property_7D938F14_Out, _Divide_90AB178C_Out);
                        float _Saturate_D0EAEC86_Out;
                        Unity_Saturate_float(_Divide_90AB178C_Out, _Saturate_D0EAEC86_Out);
                        surface.Color = (_Add_AF1DCE31_Out.xyz);
                        surface.Alpha = _Saturate_D0EAEC86_Out;
                        surface.AlphaClipThreshold = 0;
                        return surface;
                    }
                    
            //-------------------------------------------------------------------------------------
            // End graph generated code
            //-------------------------------------------------------------------------------------
        
        
        //-------------------------------------------------------------------------------------
        // TEMPLATE INCLUDE : VertexAnimation.template.hlsl
        //-------------------------------------------------------------------------------------
        
        VertexDescriptionInputs AttributesMeshToVertexDescriptionInputs(AttributesMesh input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);
        
            output.ObjectSpaceNormal =           input.normalOS;
            output.WorldSpaceNormal =            TransformObjectToWorldNormal(input.normalOS);
            output.ObjectSpacePosition =         input.positionOS;
            output.WorldSpacePosition =          GetAbsolutePositionWS(TransformObjectToWorld(input.positionOS));
        
            return output;
        }
        
        AttributesMesh ApplyMeshModification(AttributesMesh input)
        {
            // build graph inputs
            VertexDescriptionInputs vertexDescriptionInputs = AttributesMeshToVertexDescriptionInputs(input);
        
            // evaluate vertex graph
            VertexDescription vertexDescription = VertexDescriptionFunction(vertexDescriptionInputs);
        
            // copy graph output to the results
            input.positionOS = vertexDescription.Position;
        
            return input;
        }
        
        //-------------------------------------------------------------------------------------
        // END TEMPLATE INCLUDE : VertexAnimation.template.hlsl
        //-------------------------------------------------------------------------------------
        
        
        
        //-------------------------------------------------------------------------------------
        // TEMPLATE INCLUDE : SharedCode.template.hlsl
        //-------------------------------------------------------------------------------------
            FragInputs BuildFragInputs(VaryingsMeshToPS input)
            {
                FragInputs output;
                ZERO_INITIALIZE(FragInputs, output);
        
                // Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
                // TODO: this is a really poor workaround, but the variable is used in a bunch of places
                // to compute normals which are then passed on elsewhere to compute other values...
                output.worldToTangent = k_identity3x3;
                output.positionSS = input.positionCS;       // input.positionCS is SV_Position
        
                output.positionRWS = input.positionRWS;
                output.worldToTangent = BuildWorldToTangent(input.tangentWS, input.normalWS);
                #if SHADER_STAGE_FRAGMENT
                #endif // SHADER_STAGE_FRAGMENT
        
                return output;
            }
        
            SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);
        
                output.WorldSpaceNormal =            normalize(input.worldToTangent[2].xyz);
                output.WorldSpaceViewDirection =     normalize(viewWS);
                output.WorldSpacePosition =          GetAbsolutePositionWS(input.positionRWS);
                output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
        
                return output;
            }
        
            // existing HDRP code uses the combined function to go directly from packed to frag inputs
            FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
            {
                UNITY_SETUP_INSTANCE_ID(input);
                VaryingsMeshToPS unpacked= UnpackVaryingsMeshToPS(input);
                return BuildFragInputs(unpacked);
            }
        
        //-------------------------------------------------------------------------------------
        // END TEMPLATE INCLUDE : SharedCode.template.hlsl
        //-------------------------------------------------------------------------------------
        
        
        
            void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
            {
                // setup defaults -- these are used if the graph doesn't output a value
                ZERO_INITIALIZE(SurfaceData, surfaceData);
        
                // copy across graph values, if defined
                surfaceData.color = surfaceDescription.Color;
        
        #if defined(DEBUG_DISPLAY)
                if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
                {
                    // TODO
                }
        #endif
            }
        
            void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
            {
                SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
                SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);
        
                // Perform alpha test very early to save performance (a killed pixel will not sample textures)
                // TODO: split graph evaluation to grab just alpha dependencies first? tricky..
        
                BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);
        
                // Builtin Data
                ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
        
                builtinData.opacity = surfaceDescription.Alpha;
        
        
        #if (SHADERPASS == SHADERPASS_DISTORTION)
                builtinData.distortion = surfaceDescription.Distortion;
                builtinData.distortionBlur = surfaceDescription.DistortionBlur;
        #else
                builtinData.distortion = float2(0.0, 0.0);
                builtinData.distortionBlur = 0.0;
        #endif
            }
        
            //-------------------------------------------------------------------------------------
            // Pass Includes
            //-------------------------------------------------------------------------------------
                #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassForwardUnlit.hlsl"
            //-------------------------------------------------------------------------------------
            // End Pass Includes
            //-------------------------------------------------------------------------------------
        
            ENDHLSL
        }
        
    }
    CustomEditor "UnityEditor.ShaderGraph.HDUnlitGUI"
    FallBack "Hidden/InternalErrorShader"
}
