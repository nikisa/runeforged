﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class RotationTest : MonoBehaviour
{
    //Inspector
    [Header("ReSferences")]
    public GameObject Mount;
    public GameObject Knight;
    public GameObject Target;

    [Header("Values")]
    [Range(0,90)]
    public float maxTurnAngle;
    public float rotationSpeed;
    public float TweeningRotationTime;
    public Ease TweeningRotationEase;

    //Private
    private float lookAtAngle;
    private bool positioning;
    private bool looking;

    private void Start() {
        positioning = false;
        looking = false;
    }


    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.P)) positioning = !positioning;
        if (Input.GetKeyDown(KeyCode.L)) looking = !looking;


        if (positioning) ResetRotationTest(rotationSpeed);
        if (looking) RotateOnTarget();
    }

    public void ResetRotationTest(float _rotationSpeed) {
        Vector3 _target = Mount.transform.forward;
        Quaternion rotationToTarget = Quaternion.LookRotation(_target);
        Knight.transform.rotation = Quaternion.RotateTowards(Knight.transform.rotation, rotationToTarget, _rotationSpeed * Time.deltaTime);
    }

    public void RotateOnTarget() {

        Vector3 playerDirection = Vector3.ProjectOnPlane(Target.transform.position - Mount.transform.position, Vector3.up);
        lookAtAngle = Vector3.SignedAngle(Mount.transform.forward, playerDirection, Vector3.up);
        Knight.transform.DORotate(new Vector3(0, (Mount.transform.eulerAngles.y + Mathf.Clamp(lookAtAngle , -maxTurnAngle,  maxTurnAngle)), 0), TweeningRotationTime).SetEase(TweeningRotationEase);

    }

    public Vector3 DirFromAngle(float angleInDegrees) {
        angleInDegrees += transform.eulerAngles.y;
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));

    }
}
