﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class FreeLookCamera : MonoBehaviour
{

    //Inspector
    [Header("Movement")]
    [Tooltip("Normal speed of camera movement")]
    public float movementSpeed = 10f;
    [Tooltip("Speed of camera movement when LB is held down")]
    public float fastMovementSpeed = 100f;
    [Tooltip("Speed of camera movement when RB is held down")]
    public float slowMovementSpeed = 2f;
    [Tooltip("Sensitivity for free look")]
    public float freeLookSensitivity = 3f;
    [Header("Time")]
    public float scaleOfTime;
    [Header("DeadZones")]
    [Range(0.1f , 0.9f)]
    public float MovementDeadZoneValue;
    [Range(0.1f, 0.9f)]
    public float RotationDeadZoneValue;
    [Header("References")]
    public GameObject boss;
    public CharacterController playerCC;
    public CameraTracker camera;

    //Private
    Vector3 gameplayCameraPos;
    float horizontalMovement;
    float verticalMovement;
    float horizontalRotation;
    float verticalRotation;
    float movementUpAndDown;
    bool selectButton;
    bool movementUp; 
    bool movementDown;
    bool isTimeFreezed;
    bool triangle;

    private void Awake() {
        gameplayCameraPos = camera.transform.eulerAngles;
    }

    private void OnEnable() {
        playerCC.GetComponent<CharacterController>().enabled = false;
        //gameplayCameraPos.position = camera.transform.position;
        if (boss.GetComponent<CharacterController>() != null) {
            boss.GetComponent<CharacterController>().enabled = false;
        }
        else {
            boss.GetComponent<Animator>().enabled = false;
        }
        isTimeFreezed = true;
    }

    private void OnDisable() {
        Time.timeScale = 1;
        playerCC.GetComponent<CharacterController>().enabled = true;
        
        if (boss.GetComponent<CharacterController>() != null) {
            boss.GetComponent<CharacterController>().enabled = true;
        }
        else {
            boss.GetComponent<Animator>().enabled = true;
        }

        camera.GetComponent<CameraTracker>().enabled = true;
        //camera.transform.position = gameplayCameraPos.position;
        camera.transform.rotation = Quaternion.identity;
        camera.transform.Rotate(gameplayCameraPos.x, gameplayCameraPos.y, gameplayCameraPos.z);

    }


    void Update() {

        #region Commands
        horizontalMovement = Input.GetAxis("Horizontal");
        verticalMovement = Input.GetAxis("Vertical");
        horizontalRotation = Input.GetAxis("HorizontalLook");
        verticalRotation = Input.GetAxis("VerticalLook");
        movementUp = Input.GetButton("ShootXBOX");
        movementDown = Input.GetButton("Dash");
        movementUpAndDown = Input.GetAxis("BoostCharge");
        selectButton = Input.GetButtonDown("Select");
        triangle = Input.GetButtonDown("Triangle");
        #endregion

        if (triangle) {
            this.GetComponent<FreeLookCamera>().enabled = false;
        }

        bool fastMode = movementUp;
        bool slowMode = movementDown;
        float movementSpeed = this.movementSpeed;

        if (fastMode) {
            movementSpeed = this.fastMovementSpeed;
        }
        else if (slowMode) {
            movementSpeed = this.slowMovementSpeed;
        }

        if (Mathf.Pow(horizontalRotation, 2) + Mathf.Pow(verticalRotation, 2) >= Mathf.Pow(MovementDeadZoneValue, 2)) {
            float newRotationX = transform.localEulerAngles.y + horizontalRotation * freeLookSensitivity;
            float newRotationY = transform.localEulerAngles.x - verticalRotation * freeLookSensitivity;
            transform.localEulerAngles = new Vector3(newRotationY, newRotationX, 0f);
        }

        if (Mathf.Pow(horizontalMovement, 2) + Mathf.Pow(verticalMovement, 2) >= Mathf.Pow(MovementDeadZoneValue, 2)) {
             if (horizontalMovement != 0) {
                 float value = horizontalMovement;
                 transform.position = transform.position + (value * transform.right * movementSpeed * Time.deltaTime);
             }

             if (verticalMovement != 0) {
                 float value = verticalMovement;
                 transform.position = transform.position + (value * transform.forward * movementSpeed * Time.deltaTime);
             }
         }

        if (movementUpAndDown > 0) {
            transform.position = transform.position + (transform.up * movementSpeed * Time.deltaTime);
        }
        else if (movementUpAndDown < 0) {
            transform.position = transform.position + (-transform.up * movementSpeed * Time.deltaTime);
        }

        if (selectButton) {
            isTimeFreezed = !isTimeFreezed;
        }

        if (isTimeFreezed) {
            Time.timeScale = scaleOfTime;
        }
        else {
            Time.timeScale = 1;
        }

    }
}