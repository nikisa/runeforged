﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadMovementTest : MonoBehaviour
{
    //Inspector
    public GameObject Player;
    public GameObject Target;
    [Range(0,120)]
    public float maxHeadAngle;
    public float TweeningLookingTime;
    public Ease TweeningLookingEase;

    //Private
    float lookAtAngle;

    void Update() {
        HeadRotation();
    }

    private void HeadRotation() {
        lookAtAngle = Vector3.SignedAngle(Player.transform.position, Target.transform.position, Vector3.up);

        if (lookAtAngle > 0) {
            if (lookAtAngle <= maxHeadAngle) {
                transform.DORotate(new Vector3(0, 180 - (lookAtAngle + 180), 0), TweeningLookingTime).SetEase(TweeningLookingEase);
                Debug.Log("lookAtAngle: " + lookAtAngle);
            }
            else {
                transform.DORotate(new Vector3(0, 180 - (maxHeadAngle - 180), 0), TweeningLookingTime).SetEase(TweeningLookingEase);
                Debug.Log("lookAtAngle: " + lookAtAngle);
            }
        }
        else {
            if (lookAtAngle >= -maxHeadAngle) {
                transform.DORotate(new Vector3(0, 180 - (lookAtAngle + 180), 0), TweeningLookingTime).SetEase(TweeningLookingEase);
                Debug.Log("lookAtAngle: " + lookAtAngle);
            }
            else {
                transform.DORotate(new Vector3(0, 180 - (-maxHeadAngle + 180), 0), TweeningLookingTime).SetEase(TweeningLookingEase);
                Debug.Log("lookAtAngle: " + lookAtAngle);
            }
        }
    }

}
