﻿using DG.Tweening;
using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class SecondBossOrbitTest : MonoBehaviour
{

    //Inspector
    public GameObject bossCenter;
    public GameObject orbitCenter;
    public float initialRadius;
    public float AngularVelocity;
    public float StartingAngle;
    [Space]
    public float revolutionPerSeconds;
    public float angularMinVelocity;
    
    //public float RevolutionMultiplier;

    //Public 
    //public AnimationCurve revolutionCurve;
    [HideInInspector]
    public AnimationCurve accelerationCurve;
    [HideInInspector]
    public AnimationCurve decelerationCurve;
    //[HideInInspector]
    public AnimationCurve attackAnticipationCurve;
    [HideInInspector]
    public AnimationCurve attackEntryCurve;
    [HideInInspector]
    public AnimationCurve attackActiveCurve;
    [HideInInspector]
    public AnimationCurve attackExitCurve;
    //public float SSTimer;
    [Space]
    public bool Attack;
    private bool isAnticipation;
    private bool isAttacking;
    public GameObject Target;
    public Ease AttackEase;
    [Space]
    public float AnticipationTime;
    public int AnticipationRevolutions = 1;
    [Range(0, 180)]
    public float AttackAngle;
    float anticipationFinalVelocity;
    public float AttackVelocity;
    public float minAttackRange;
    public float maxAttackRange;
    [Space]
    public float AttackAngularVelocity;


    //Private
    float timer;
    float AngularAccelerationModule;
    float Drag;
    float angularMaxVelocity;
    float currentRadius;
    int iterations;
    Vector3 VelocityVector;

    private float distanceFromTarget;
    float deltaTimeEntry;
    float deltaTimeExit;
    float deltaTimeEnd;

    private void Start() {
        iterations = 1;
        //RevolutionMultiplier = 2 * 360 * Mathf.Abs(revolutionPerSeconds) / (angularMaxVelocity + angularMinVelocity);
        currentRadius = initialRadius;
        angularMaxVelocity = 720 * Mathf.Abs(revolutionPerSeconds) - angularMinVelocity; // deltaW = 360 * revolutionPerSeconds = deltaAngSpace (in deltaTime = 1 / revolutionPerSeconds); deltaW = (angVel0 + angVel1) / 2. 
        transform.localEulerAngles += new Vector3(0, StartingAngle, 0);

        //SetAccelerationCurve();
        //SetDecelerationCurve();

        SetRevolutionCurve();
    }

    private void Update() {
        if (isAttacking) // creare la funzione di pre-Attack, che fa da anticipation e permette di posizionare correttamente il centro?
        {
            doAttack(deltaTimeEntry, deltaTimeExit, deltaTimeEnd);
        }
        else if (isAnticipation) {
            doAttackAnticipation();
        }
        else {
            doRevolution();
            //SetAttackAnticipation();
        }
    }

    public void RotateAroud(AnimationCurve _dashCurve, float _t0, float _t1, int _iterations, float _verse) {

        transform.localEulerAngles += new Vector3(0, Integration.IntegrateCurve(_dashCurve, _t0, _t1, _iterations) * Mathf.Sign(_verse), 0) /** RevolutionMultiplier*/;
        transform.position = orbitCenter.transform.position + currentRadius * transform.forward;

        #region BACKUP
        //AngularAccelerationModule = _angularMaxSpeed / _angularAccelerationTime;
        //Drag = AngularAccelerationModule / _angularMaxSpeed * Time.fixedDeltaTime;

        //AngularVelocity -= AngularVelocity * Drag;
        //transform.eulerAngles += new Vector3(0, AngularVelocity * Time.fixedDeltaTime + 0.5f * AngularAccelerationModule * Mathf.Pow(Time.fixedDeltaTime, 2), 0);
        //transform.position = new Vector3(orbitCenter.transform.position.x + currentRadius * Mathf.Sin((transform.eulerAngles.y) * Mathf.Deg2Rad), orbitCenter.transform.position.y, orbitCenter.transform.position.z + currentRadius * Mathf.Cos((transform.eulerAngles.y) * Mathf.Deg2Rad));
        ////AngularVelocity += AngularAccelerationModule * Time.fixedDeltaTime + Mathf.CeilToInt(Mathf.Abs((Mathf.Sin((timerinSeconds /** multiplier*/) / (numerOfCircumference / 2)) /** _angularMaxSpeed*/)));
        //AngularVelocity = Mathf.CeilToInt(Mathf.Abs((Mathf.Sin((timerinSeconds /** multiplier*/) / (numerOfCircumference / 2)) * _angularMaxSpeed)));
        //AngularVelocity = Mathf.Clamp(AngularVelocity, minVelocity, _angularMaxSpeed);
        //VelocityVector = transform.right * (AngularVelocity * Mathf.Deg2Rad * currentRadius);
        #endregion
    }

    //Solo per visualizzazione (vedi SetAttack())
    public Vector3 DirAngle(float angleInDegrees) {
        angleInDegrees += bossCenter.transform.eulerAngles.y;
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad)); //diverse ideuzze per l'Y
    }


    //REVOLUTION
    #region REVOLUTION FUNCTIONS
    public void SetRevolutionCurve() {
        accelerationCurve.AddKey(0, angularMinVelocity);
        accelerationCurve.AddKey(Mathf.Abs(1 / revolutionPerSeconds) / 2, angularMaxVelocity);
        decelerationCurve.AddKey((Mathf.Abs(1 / revolutionPerSeconds) / 2), angularMaxVelocity);
        decelerationCurve.AddKey(Mathf.Abs(1 / revolutionPerSeconds), angularMinVelocity);
    }

    public void doRevolution() {

        if (Attack) {
            SetAttackAnticipation();
            timer = 0;
            isAnticipation = true;
            return;
        }


        float t0 = timer;
        timer += Time.deltaTime;

        if (t0 <= Mathf.Abs(1 / revolutionPerSeconds) / 2) {
            if (timer <= Mathf.Abs(1 / (revolutionPerSeconds / 2))) {
                RotateAroud(accelerationCurve, t0, timer, iterations, revolutionPerSeconds);
                AngularVelocity = accelerationCurve.Evaluate(timer);
            }
            else {
                RotateAroud(accelerationCurve, t0, (Mathf.Abs(1 / revolutionPerSeconds) / 2), iterations, revolutionPerSeconds);
                RotateAroud(decelerationCurve, (Mathf.Abs(1 / revolutionPerSeconds) / 2), timer, iterations, revolutionPerSeconds);
                AngularVelocity = decelerationCurve.Evaluate(timer);
            }
        }
        else //t0 > 0;
        {
            if (timer <= Mathf.Abs(1 / revolutionPerSeconds)) {
                RotateAroud(decelerationCurve, t0, timer, iterations, revolutionPerSeconds);
                AngularVelocity = decelerationCurve.Evaluate(timer);
            }
            else {
                //RotateAroud(decelerationCurve, t0, Mathf.Abs(1 / revolutionPerSeconds), iterations, revolutionPerSeconds);
                //if (!Attack) // con anticipation non c'Ã¨ bisogno di questo if. IF ATTACK va messo o all'inizio o alla fine.
                //{
                //    timer -= Mathf.Abs(1 / revolutionPerSeconds);
                //    RotateAroud(accelerationCurve, 0, timer, iterations, revolutionPerSeconds);
                //    AngularVelocity = accelerationCurve.Evaluate(timer);
                //}
                //else
                //{
                //    timer = 0;
                //    isAttacking = true;
                //    Attack = false;
                //    SetAttack();
                //    doAttack(deltaTimeEntry, deltaTimeExit);
                //}

                //Scommenta sopra per farlo funzionare come prima
                RotateAroud(decelerationCurve, t0, Mathf.Abs(1 / revolutionPerSeconds), iterations, revolutionPerSeconds);
                timer -= Mathf.Abs(1 / revolutionPerSeconds);
                RotateAroud(accelerationCurve, 0, timer, iterations, revolutionPerSeconds);
                AngularVelocity = accelerationCurve.Evaluate(timer);
            }
        }
    }
    #endregion

    //ATTACK ANTICIPATION

    //IDEE PER ANTICIPATION ATTACK
    //In entrata: il deltaTime dell'anticipation
    //Alla fine del delta time, la palla deve:
    //1. Completare il giro attuale
    //2. Compiere un altro giro completo
    //La cosa qui sopra Ã¨ da controllare o da permettere di settare â–º trovare FORMULA
    //Comunque sia, in questo lasso di tempo, il centro della rotazione puÃ² spostarsi (posizione e rotazione) in una posizione di attacco predefinita. Il currentRadius della palla chiodata puÃ² variare.
    //Questo significa che c'Ã¨ un deltaAngle (da calcolare) da ricoprire in quel deltaTime (l'anticipation time), e abbiamo una VelocitÃ  Angolare iniziale (quella calcolata nel momento esatto in cui l'attacco viene triggerato).
    //Con la solita formula si calcola la VelocitÃ  Angolare finale, che sarÃ  la VelocitÃ  Angolare di partenza dell'attacco
    //La velocitÃ  di uscita dell'attacco, invece, sarÃ  AngularMinVelocity, in quanto tutto rietrerÃ  nella DoRotation Standard. Il centro della rotazione ritorna nella posizione iniziale. â–º Cambia FORMULA attack nel settaggio curva e calcolo dei tempi!

    void SetAttackAnticipation() {
        float deltaAngle;
        //Ho bisogno di sapere: AngoloPallaChiodata; il suo StartingAngle; VelocitÃ Angolare della sfera; AnticipationTime;
        //da AngoloPallaChiodata e StartingAngle dovrei ricavare quanto gli manca per concludere il giro;

        //deltaAngle = 360 - (transform.localEulerAngles.y + StartingAngle); //quanto manca per completare il giro corrente

        //Ricava Vettore in base a Starting Angle. Poi trova soluzione con SignedAngle.
        Vector3 vectorZeroAngle = Quaternion.AngleAxis(StartingAngle, orbitCenter.transform.up) * orbitCenter.transform.forward;
        Debug.DrawLine(orbitCenter.transform.position, orbitCenter.transform.position + vectorZeroAngle);

        deltaAngle = Vector3.SignedAngle(vectorZeroAngle, transform.forward, orbitCenter.transform.up) /* + StartingAngle*/;
        Debug.Log("deltaAngle: " + deltaAngle);

        //IF revolutionPerSeconds è negativo...
        //&& IF deltaAngle è negativo: deltaAngle += 360;
        //&& IF deltaAngle è positivo: fai un cazzo;
        //IF revolutionPerSecond è positivo...
        //&& IF deltaAngle è negativo: deltaAngle = Abs(deltaAngle);
        //&& IF deltaAngle è positivo: deltaAngle = 360 - deltaAngle;
        deltaAngle = 350 - Mathf.Sign(revolutionPerSeconds) * deltaAngle;
        deltaAngle -= Mathf.Floor(deltaAngle/360) * 360;
        anticipationFinalVelocity = (2 * (360 * AnticipationRevolutions + deltaAngle) / AnticipationTime) - AngularVelocity;

        //Setta la curva e inizia il movimento del center;

        SetAttackAnticipationCurve();
    }

    public void SetAttackAnticipationCurve() {
        attackAnticipationCurve.keys = null;

        attackAnticipationCurve.AddKey(0, AngularVelocity);
        attackAnticipationCurve.AddKey(AnticipationTime, anticipationFinalVelocity);
    }

    public void doAttackAnticipation() {
        float t0 = timer;
        timer += Time.deltaTime;

        if (timer < AnticipationTime) {
            RotateAroud(attackAnticipationCurve, t0, timer, iterations, revolutionPerSeconds);
        }
        else {
            //timer -= AnticipationTime;
            timer = 0;
            RotateAroud(attackAnticipationCurve, t0, AnticipationTime, iterations, revolutionPerSeconds);
            isAttacking = true;
            Attack = false;
            isAnticipation = false;
            SetAttack();
            doAttack(deltaTimeEntry, deltaTimeExit, deltaTimeEnd);
        }
    }

    //ATTACK
    #region ATTACK FUNCTIONS

    private void SetAttack() {
        distanceFromTarget = Mathf.Clamp((Target.transform.position - bossCenter.transform.position).magnitude, minAttackRange, maxAttackRange);
        //Solo per visualizzazione
        Debug.DrawLine(bossCenter.transform.position, bossCenter.transform.position + DirAngle(-AttackAngle / 2) * distanceFromTarget);
        Debug.DrawLine(bossCenter.transform.position, bossCenter.transform.position + DirAngle(AttackAngle / 2) * distanceFromTarget);
        //

        AttackAngularVelocity = (AttackVelocity / distanceFromTarget) * Mathf.Rad2Deg;

        //deltaTimeEntry = 2 * (180 - (AttackAngle / 2)) / (angularMinVelocity + AttackAngularVelocity); //Il deltaTime di entrata cambia, in quanto la velocitÃ  di ingresso Ã¨ differente.
        deltaTimeEntry = 2 * (180 - (AttackAngle / 2)) / (anticipationFinalVelocity + AttackAngularVelocity); //Il deltaTime di entrata cambia, in quanto la velocitÃ  di ingresso Ã¨ differente.
        deltaTimeExit = deltaTimeEntry + (AttackAngle / AttackAngularVelocity); //il deltaTime di uscita, invece, resta invariato. Ma quello di ingresso cambia!

        //new
        deltaTimeEnd = deltaTimeExit + 2 * (180 - (AttackAngle / 2)) / (AttackAngularVelocity + angularMinVelocity);

        SetAttackCurve(deltaTimeEntry, deltaTimeExit, deltaTimeEnd);
    }

    public void SetAttackCurve(float entryTime, float exitTime, float endTime) {
        attackEntryCurve.keys = null;
        attackActiveCurve.keys = null;
        attackExitCurve.keys = null;

        //attackEntryCurve.AddKey(0, angularMinVelocity);
        attackEntryCurve.AddKey(0, anticipationFinalVelocity);
        attackEntryCurve.AddKey(entryTime, AttackAngularVelocity);
        attackActiveCurve.AddKey(entryTime, AttackAngularVelocity);
        attackActiveCurve.AddKey(exitTime, AttackAngularVelocity);
        attackExitCurve.AddKey(exitTime, AttackAngularVelocity);
        //attackExitCurve.AddKey(exitTime + entryTime, angularMinVelocity);
        attackExitCurve.AddKey(endTime, angularMinVelocity);
    }

    public void doAttack(float entryTime, float exitTime, float endTime) {
        float t0 = timer;
        timer += Time.deltaTime;

        if (t0 <= entryTime) {
            if (timer <= entryTime) {
                currentRadius = DOVirtual.EasedValue(initialRadius, distanceFromTarget, timer / entryTime, AttackEase);
                RotateAroud(attackEntryCurve, t0, timer, iterations, revolutionPerSeconds);
            }
            else {
                RotateAroud(attackEntryCurve, t0, entryTime, iterations, revolutionPerSeconds);
                currentRadius = distanceFromTarget;
                RotateAroud(attackActiveCurve, entryTime, timer, iterations, revolutionPerSeconds);
            }
        }
        else if (t0 > exitTime) {
            if (timer <= endTime) {
                currentRadius = DOVirtual.EasedValue(distanceFromTarget, initialRadius, (timer - exitTime) / (endTime - exitTime), AttackEase);
                RotateAroud(attackExitCurve, t0, timer, iterations, revolutionPerSeconds);
            }
            else {
                timer = 0;
                currentRadius = initialRadius;
                RotateAroud(attackExitCurve, t0, endTime, iterations, revolutionPerSeconds);
                isAttacking = false; //per scopo di debug. Potrebbe fare un nuovo SetAttack();
                doRevolution();
            }
        }
        else {
            if (timer <= exitTime) {
                RotateAroud(attackActiveCurve, t0, timer, iterations, revolutionPerSeconds);
            }
            else {
                RotateAroud(attackActiveCurve, t0, exitTime, iterations, revolutionPerSeconds);
                currentRadius = DOVirtual.EasedValue(distanceFromTarget, initialRadius, (timer - exitTime) / (endTime - exitTime), AttackEase);
                RotateAroud(attackExitCurve, exitTime, timer, iterations, revolutionPerSeconds);
            }
        }
    }
    #endregion




    //public void SetAccelerationCurve() {        
    //    revolutionCurve.AddKey(0 , angularMinVelocity);
    //    revolutionCurve.AddKey((Mathf.Abs(1 / revolutionPerSeconds) / 2), angularMaxVelocity);
    //    //accelerationCurve.AddKey(Mathf.Abs(1 / revolutionPerSeconds), angularMinVelocity);
    //}

    //public void SetDecelerationCurve() {
    //    decelerationCurve.AddKey((Mathf.Abs(1 / revolutionPerSeconds) / 2), angularMaxVelocity);
    //    decelerationCurve.AddKey(Mathf.Abs(1 / revolutionPerSeconds) , angularMinVelocity);
    //}

    //public void doRevolutionAcceleration(float _timer) {
    //    SSTimer = timer;

    //    if (timer <= Mathf.Abs(2 / revolutionPerSeconds)) {
    //        RotateAroud(accelerationCurve , _timer - Time.deltaTime , _timer, iterations, revolutionPerSeconds);
    //    }
    //    else {
    //        SSTimer -= Mathf.Abs(2 / revolutionPerSeconds);
    //        RotateAroud(accelerationCurve, _timer - Time.deltaTime, Mathf.Abs(2 / revolutionPerSeconds), iterations, revolutionPerSeconds);
    //        RotateAroud(decelerationCurve, 0, SSTimer, iterations, revolutionPerSeconds);
    //    }



    //}

    //public void doRevolutionDeceleration(float _timer) {
    //    if (_timer > Mathf.Abs(2 / revolutionPerSeconds)) {
    //        RotateAroud(decelerationCurve, _timer - Time.deltaTime, timer, iterations, revolutionPerSeconds);
    //    }
    //    else {
    //        SSTimer -= Mathf.Abs(2 / revolutionPerSeconds);
    //        RotateAroud(accelerationCurve, _timer - Time.deltaTime, Mathf.Abs(2 / revolutionPerSeconds), iterations, revolutionPerSeconds);
    //        RotateAroud(decelerationCurve, 0, SSTimer, iterations, revolutionPerSeconds);
    //    }

    //}

    //public void doRevolution() {
    //    float t0 = timer;
    //    timer += Time.deltaTime;

    //    if (t0 <= Mathf.Abs(1 / revolutionPerSeconds) / 2 && timer <= Mathf.Abs(1 / (revolutionPerSeconds / 2))) {
    //        RotateAroud(accelerationCurve, t0, timer, iterations, revolutionPerSeconds);
    //    }
    //    else if (t0 <= Mathf.Abs(1 / revolutionPerSeconds) / 2 && timer > Mathf.Abs(1 / revolutionPerSeconds) / 2) {
    //        RotateAroud(accelerationCurve, t0 , Mathf.Abs(1 / revolutionPerSeconds ) / 2, iterations, revolutionPerSeconds);
    //        RotateAroud(decelerationCurve, Mathf.Abs(1 / revolutionPerSeconds) / 2, timer, iterations, revolutionPerSeconds);
    //    }
    //    else if (t0 > Mathf.Abs(1 / revolutionPerSeconds) / 2 && timer <= Mathf.Abs(1 / revolutionPerSeconds)) {
    //        RotateAroud(decelerationCurve, t0, timer, iterations, revolutionPerSeconds);
    //    }
    //    else if (t0 > Mathf.Abs(1 / revolutionPerSeconds) / 2 && timer > Mathf.Abs(1 / revolutionPerSeconds)) {
    //        RotateAroud(decelerationCurve, t0, Mathf.Abs(1 / revolutionPerSeconds), iterations, revolutionPerSeconds);
    //        RotateAroud(accelerationCurve, 0 , timer - Mathf.Abs(1 / revolutionPerSeconds), iterations, revolutionPerSeconds);
    //        timer -= Mathf.Abs(1 / revolutionPerSeconds);
    //    }

    //}

}
