﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ATATController : MonoBehaviour
{
    //Inspector
    [Header("References")]
    public Animator bossAnimator;
    public SecondBossController boss;
    [Header("Values")]
    [SerializeField]
    protected int collisionsRequired;
    public float initialRadius;
    public float chokedRadius;
    public float chokingTime;
    public Ease chokingEase;


    #region TESTING 
    //public Material notSorroundedMaterial;
    //public Material sorroundedMaterial;

    //transform.gameObject.GetComponent<Renderer>().material = sorroundedMaterial;
    //transform.gameObject.GetComponent<Renderer>().material = notSorroundedMaterial;
    #endregion
        
    //Protected
    protected int actualCollsions;

    //Private
    private string sorrounded = "Sorrounded";
    private float currentRadius;


    public void addCollision() {
        actualCollsions++;
    }

    public void removeCollision() {
        actualCollsions--;
    }


    public void SorroundedController() {
        if (actualCollsions >= collisionsRequired) {
            float timer = 0;
            timer += Time.deltaTime; 
            Debug.Log(sorrounded);
            currentRadius = transform.GetComponent<CapsuleCollider>().radius;
            currentRadius = DOVirtual.EasedValue(initialRadius, chokedRadius, timer / chokingTime, chokingEase);
            //boss.MountGraphics.GetComponent<Renderer>().material = boss.chokedMat;
            bossAnimator.SetTrigger("Fall");
            //TODO: DoVirtual from initialScale to ChockedScale

            //currentRadius = DOVirtual.EasedValue(initialRadius, distanceFromTarget, timer / entryTime, AttackEase);

        }
        else {
            Debug.Log("not " + sorrounded);
            //bossAnimator.SetBool(sorrounded , false);
            float timer = 0;
            timer += Time.deltaTime;
            //boss.MountGraphics.GetComponent<Renderer>().material = boss.fallingMat;
            currentRadius = transform.GetComponent<CapsuleCollider>().radius;
            currentRadius = DOVirtual.EasedValue(chokedRadius, initialRadius, timer / chokingTime, chokingEase);
            //TODO: DoVirtual from ChockedScale to initialScale
        }
    }

}
