﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Core.Easing;
using UnityEngine.Rendering;

public class ShaderColorChange : MonoBehaviour
{

    //Inspector
    public Renderer mat;
    public Ease ease;
    public float changeTime;

    #region First Color
    float oldRed = 34;
    float oldGreen = 191;
    float oldBlue = 172;
    #endregion

    #region Second Color
    float newRed = 235;
    float newGreen = 187;
    float newBlue = 196;
    #endregion


    #region Current Color
    [SerializeField]
    float currentRed;
    [SerializeField]
    float currentGreen;
    [SerializeField]
    float currentBlue;
    #endregion

    //Private
    int intesityRegulator = 300;
    bool current;
    float timer;
    float timePercentage;

    private void Start() {
        current = false; 

        currentRed = oldRed;
        currentGreen = oldGreen;
        currentBlue = oldBlue;
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space)) {
            current = !current;
            timer = 0;
        }
        
        if (current) {
            ChangeColorShader();
        }
        else {
            ResetColorShader();
        }
        

    }

    void ChangeColorShader() {
        timer += Time.deltaTime;
        timePercentage = Mathf.Clamp01(timer / changeTime);
        timer = Mathf.Clamp01(timer);
        currentRed = DOVirtual.EasedValue(oldRed , newRed , timePercentage, ease);
        currentGreen = DOVirtual.EasedValue(oldGreen , newGreen , timePercentage, ease);
        currentBlue = DOVirtual.EasedValue(oldBlue , newBlue , timePercentage, ease);

        Color newColor = new Color(currentRed , currentGreen , currentBlue , 0) / intesityRegulator;
        mat.material.SetColor("Color_928ADAC2", newColor); 

    }

    void ResetColorShader() {
        timer += Time.deltaTime;
        timePercentage = Mathf.Clamp01(timer / changeTime);
        currentRed = DOVirtual.EasedValue(newRed, oldRed, timePercentage, ease);
        currentGreen = DOVirtual.EasedValue(newGreen, oldGreen, timePercentage, ease);
        currentBlue = DOVirtual.EasedValue(newBlue, oldBlue, timePercentage, ease);

        Color newColor = new Color(currentRed , currentGreen , currentBlue , 0) / intesityRegulator;
        mat.material.SetColor("Color_928ADAC2", newColor);
    }





    //void UpdateColor() {

    //}

}
