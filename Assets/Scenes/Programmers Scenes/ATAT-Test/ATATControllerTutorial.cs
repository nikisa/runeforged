﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ATATControllerTutorial : ATATController
{
    //Inspector
    [Header("References")]
    public Animator animator;
    public GrappleManager grappleManager;
    [Header("Materials")]
    public Material notSorroundedMaterial;
    public Material sorroundedMaterial;
    public Material destroyedMaterial;


    #region TESTING 
    //transform.gameObject.GetComponent<Renderer>().material = sorroundedMaterial;
    //transform.gameObject.GetComponent<Renderer>().material = notSorroundedMaterial;
    #endregion


    //Private
    private bool hasBeenDestroyed = false;
    private int actualBrokenColumns;

    private void Update() {
        grappleManager.CollisionController();
        SorroundedColumnController();
        animator.SetInteger("ActualCollision" , actualCollsions);
    }



    public void SorroundedColumnController() {
        if (actualCollsions >= collisionsRequired) {
            if (grappleManager.Player.isTugging) {
                transform.gameObject.GetComponent<Renderer>().material = destroyedMaterial;
                if (!hasBeenDestroyed) {
                    hasBeenDestroyed = true;
                    actualBrokenColumns++;
                    animator.SetInteger("DestroyedColumns" , actualBrokenColumns);
                }
            }
            else {
                transform.gameObject.GetComponent<Renderer>().material = sorroundedMaterial;
            }
        }
        else {
            Debug.Log("Not Sorrounded");
            transform.gameObject.GetComponent<Renderer>().material = notSorroundedMaterial;
        }
    }

}
