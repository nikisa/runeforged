﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{

    public abstract class BaseFSM : MonoBehaviour
    {

        protected FiniteBaseState CurrentState {
            get { return _currentState; }
            set {
                if (_currentState != null)
                    _currentState.Exit();

                _currentState = value;

                if (_currentState != null)
                    _currentState.Enter();
            }
        }
        private FiniteBaseState _currentState;

        public abstract void StartSM();

        public void StopSM() {
            CurrentState = null;
        }

        private void FixedUpdate() {
            StateTick();
        }

        protected virtual void StateStart() {
        }

        protected virtual void StateTick() {
            if (CurrentState != null)
                CurrentState.Tick();
        }

        protected void setCurrentState(FiniteBaseState _nextState) {
            //IContext ctx = CurrentState.context;
            _nextState.context = CurrentState.context;
            CurrentState = _nextState;
            //CurrentState.context = ctx;
        }

    }

}