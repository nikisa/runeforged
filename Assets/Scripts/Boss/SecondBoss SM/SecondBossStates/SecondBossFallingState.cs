﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondBossFallingState : SecondBossState{

    public override void Enter() {
        boss.Graphics.BossGraphicAnimator.SetTrigger(G_FALL);
        boss.Player.grappleManager.Unhook();
    }

    public override void Tick() {
        base.Tick();
    }

    public override void Exit() {
        //base.Exit();
        //boss.Player.Victory();
    }

}
    
