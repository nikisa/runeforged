﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SecondBossMoveToState : SecondBossState
{
    //Inspector
    public MoveToData moveToData;
    public bool Debugging;


    //Private 
    GameObject Target;
    Vector3 targetPosition;
    float startY;
    float timeStartAcceleration;
    float timeStartTrail;
    float timeStartMoveTo;
    float timeMoveTo;
    int iterations;
    int layerResult;

    int layerWall;
    int layerPlayer;
    float accelerationModule;
    Vector3 targetDir;


    public override void Enter() {

        base.Enter();
        SetOrbitTag(moveToData.OrbitTag);
        graphicsAnimator.SetTrigger(G_CHARGE);
        iterations = 30;
        layerWall = 10;
        layerPlayer = 11;
        Target = null;
        if (moveToData.Target != null) {
            Target = moveToData.Target.instance;
            boss.Target = Target;
        }

        accelerationModule = moveToData.MaxSpeed / moveToData.TimeAcceleration;
        boss.AccelerationVector = accelerationModule * boss.MountGraphics.transform.forward;
        //boss.MovementReset();
        MoveToEnter();
        boss.actualMaxSpeed = moveToData.MaxSpeed;

    }
    public override void Tick() {
        base.Tick();
        //Debug.DrawRay(boss.transform.position + new Vector3(0, 6, 0), boss.AccelerationVector * 10, Color.blue, .1f);

        if (moveToData.isZigZagging)
        {
            ZigZagBehavior();
        }
        ChargeAttack();
        MoveToTick();
        SetSpeed();
        SetCycleTimer();

        if (isCastingBullets) {
            foreach (var bulletData in moveToData.BulletData) {
                SetBulletValues(bulletData, boss.CycleTimer , moveToData.Emitter.instance.transform);
            }
        }
    }


    public override void Exit() {
        base.Exit();
        boss.IsPrevStateReinitialize = false;
        CheckVulnerability();
        animator.SetBool("MoveToOrbit", false);
        animator.SetInteger("Layer", 0);
    }


    //Set direction and position of the Target
    public void ChargeAttack() {
        if (Target != null) {
            NewDir();
        }
        else {
            Debug.Log("NULL");
            DefaultDir();
        }
    }
    public void NewDir() {
        //targetPosition = new Vector3(Target.transform.position.x, startY, Target.transform.position.z); 
        float step = moveToData.BendingSpeed * Time.deltaTime;
        targetDir = Vector3.ProjectOnPlane(Target.transform.position - boss.transform.position , Vector3.up).normalized;
        //boss.AccelerationVector = Vector3.RotateTowards(boss.AccelerationVector , targetDir , step , boss.AccelerationModule);
        boss.AccelerationVector = Vector3.RotateTowards(boss.AccelerationVector , targetDir , step , accelerationModule);
        //boss.AccelerationVector = boss.AccelerationModule * targetDir;
        boss.RotateTarget(targetDir);
    }


    public void DefaultDir() {
        targetDir = Vector3.ProjectOnPlane(boss.MountGraphics.transform.forward, Vector3.up);
        boss.AccelerationVector = accelerationModule * targetDir;
    }

    public void MoveToEnter() {

        if (moveToData.SetInitialSpeed == true)
        {
            float initialSpeed = moveToData.InitialSpeed;
            initialSpeed = Mathf.Clamp(initialSpeed, -moveToData.MaxSpeed, moveToData.MaxSpeed);
            boss.VelocityVector = initialSpeed * boss.MountGraphics.transform.forward;
        }
        

        boss.moveToData = moveToData;
        boss.MaxSpeed = moveToData.MaxSpeed;
        boss.MoveSpeed += moveToData.AddToVelocity;
        startY = boss.transform.position.y;
        ChargeAttack();
        timeStartMoveTo = Time.time;
    }


    public void MoveToTick() {

        //layerResult = boss.MovingDetectPlayer(iterations);

        //layerResult = boss.DetectCollision(boss.nextPosition);

        //if (layerResult == layerWall && boss.VelocityVector.magnitude > 20) {
        //    animator.SetInteger("Layer", layerResult);
        //}
        //else {

        //}
        //if (boss.CharacterController == null) {

            //Debug.DrawRay(boss.transform.position, boss.AccelerationVector, Color.red, 0.02f);
            boss.Movement(boss.AccelerationVector, moveToData.MaxSpeed, accelerationModule);
        //}
        //else {
        //    boss.CharacterControllerMovement(boss.AccelerationVector, moveToData.MaxSpeed, accelerationModule);
        //}
    }

    //Set speed parameter in the animator
    public void SetSpeed() {
        animator.SetFloat("Speed", boss.VelocityVector.magnitude);
    }

    float oldSineAngle;
    float sineAngle;

    public void ZigZagBehavior()
    {
        oldSineAngle = sineAngle;
        sineAngle = Mathf.Sin(Mathf.Deg2Rad * (timer * 360 / moveToData.zigZagRotationFrequency)) * moveToData.zigZagAngleRange / 2;
        //boss.transform.localEulerAngles += new Vector3(0, sineAngle -oldSineAngle,  0);
        boss.MountGraphics.transform.localEulerAngles += new Vector3(0, sineAngle - oldSineAngle, 0);
    }

}
