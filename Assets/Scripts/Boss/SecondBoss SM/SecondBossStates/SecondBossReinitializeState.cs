﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondBossReinitializeState : SecondBossState
{
    public AnticipationData anticipationData;

    public override void Enter() {
        base.Enter();
        graphicsAnimator.SetTrigger(G_IDLE);
        SetOrbitTag(anticipationData.OrbitTag);
        boss.loops = anticipationData.Loops + 1;//+1 altrimenti fa ogni volta un ciclo in meno
    }

    public override void Tick() {
        boss.IsPrevStateReinitialize = true;
        animator.SetInteger("Loops", boss.loops);
        animator.SetTrigger(END_STATE_TRIGGER);

        if (isCastingBullets) {
            foreach (var bulletData in anticipationData.BulletData) {
                SetBulletValues(bulletData, boss.CycleTimer, anticipationData.Emitter.instance.transform);
            }
        }

    }


    public override void Exit() {
        base.Exit();
    }


}
