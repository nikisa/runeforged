﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondBossVictoryState : SecondBossState{

    public override void Enter() {
        boss.Player.Victory();
    }

    public override void Tick() {
        base.Tick();
    }

    public override void Exit() {

    }

}
    
