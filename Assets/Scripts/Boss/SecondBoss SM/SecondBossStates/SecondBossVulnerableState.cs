﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SecondBossVulnerableState : SecondBossState
{

    //Inpsector
    public DecelerationData decelerationData;

    //Private
    Vector3 prefallDirection;
    float rotationStep;


    public override void Enter() {

        boss.animator.SetTrigger(VULNERABLE);
        boss.animator.SetBool(VULNERABLE_BOOL, true);
        boss.canMoveAsRigidBody = false;

        prefallDirection = boss.transform.position - boss.ArenaCenter.transform.position;

        //boss.Graphics.BossGraphicAnimator.SetTrigger(G_DEFENSLESS);
        boss.Graphics.BossGraphicAnimator.SetTrigger(G_PREFALL_IDLE);
        
        //boss.MountGraphics.GetComponent<Renderer>().material = boss.fallingMat;
        boss.atatController.transform.gameObject.SetActive(true);

    }

    public override void Tick() {
        base.Tick();
        rotationStep = boss.preFallingROtationSpeed * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(boss.transform.forward , prefallDirection , rotationStep , 0.0f);
        boss.atatController.SorroundedController();

    }

    public override void Exit() {
        base.Exit();
        boss.animator.SetBool(VULNERABLE_BOOL, false);
        boss.animator.ResetTrigger(VULNERABLE);
        //boss.MountGraphics.GetComponent<Renderer>().material = boss.baseMat;

        foreach (HookPointSpring vulnerablePoint in boss.vulnerablePoints) {
            vulnerablePoint.transform.gameObject.SetActive(false);
        }
    }


}
