﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondBossDecelerationState : SecondBossState
{
    //Inspector
    public DecelerationData decelerationData;

    //Private
    float timer;
    float finalDeltaTime;
    int iterations;


    public override void Enter()
    {
        iterations = 1;
        base.Enter();

        if (decelerationData.isRecovering) {
            graphicsAnimator.SetTrigger(G_IDLE);
        }
        SetOrbitTag(decelerationData.OrbitTag);
        graphicsAnimator.SetTrigger(G_IDLE);
        animator.SetInteger("Layer", 0);
        timer = 0;
        boss.DecelerationModule = decelerationData.Deceleration;
        setMovementDecelerationCurve();


    }
    public override void Tick()
    {

        timer += Time.deltaTime;
        base.Tick();
        //setChaseRadius();
        Deceleration();
        SetCycleTimer();
        SetSpeed();


        if (isCastingBullets) {
            foreach (var bulletData in decelerationData.BulletData) {
                SetBulletValues(bulletData, boss.CycleTimer, decelerationData.Emitter.instance.transform);
            }
        }

    }

 

    public override void Exit()
    {
        base.Exit();
        graphicsAnimator.SetTrigger(G_IDLE);
        //boss.MountGraphics.GetComponent<Renderer>().material = boss.baseMat;
        boss.IsPrevStateReinitialize = false;
        CheckVulnerability();

        
    }

    //Set speed parameter in the animator
    public void SetSpeed() {
        animator.SetFloat("Speed", boss.VelocityVector.magnitude);
    }

    public void Deceleration()
    {

        if (timer <= finalDeltaTime)
        {
            boss.Deceleration(decelerationData.MovementDecelerationCurve , timer - Time.deltaTime , timer , iterations);
        }
        else 
        {
            boss.Deceleration(decelerationData.MovementDecelerationCurve, timer - Time.deltaTime, finalDeltaTime, iterations);
            animator.SetTrigger(END_STATE_TRIGGER);
        }
    }

    //public void setChaseRadius()
    //{
    //    float distance = (boss.Target.transform.position - boss.transform.position).magnitude;
    //    animator.SetFloat("ChaseRadius", distance);
    //}

    void setMovementDecelerationCurve() {

        decelerationData.MovementDecelerationCurve.keys = null;
        finalDeltaTime = boss.VelocityVector.magnitude / boss.DecelerationModule;

        decelerationData.MovementDecelerationCurve.AddKey(0, boss.VelocityVector.magnitude);
        decelerationData.MovementDecelerationCurve.AddKey(finalDeltaTime , 0);

    }

    


}
