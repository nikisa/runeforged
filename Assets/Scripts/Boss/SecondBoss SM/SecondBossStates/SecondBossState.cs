﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondBossState : BaseState
{

    //Inspector 
    public bool isCastingBullets;

    //protected BossOrbitManager bossOrbitManager;
    protected SecondBossController boss;
    protected Animator graphicsAnimator;

    //SateMachine Parameters
    protected const string IDLE = "Idle";
    protected const string ANTICIPATION = "Anticipation";
    protected const string MOVETO = "MoveTo";
    protected const string RECOVERY = "Recovery";
    protected const string DECELERATION = "Deceleration";
    protected const string END_STATE_TRIGGER = "EndState";
    protected const string TIMER = "Timer";
    protected const string VULNERABLE = "Vulnerable";
    protected const string VULNERABLE_BOOL = "VulnerableBool";

    //Graphic SM with Mount Parameters 
    protected const string G_IDLE = "Idle";
    protected const string G_ANTICIPATION_CHARGE = "AnticipationCharge";
    protected const string G_CHARGE = "Charge";
    protected const string G_TURN_LEFT = "TurnLeft";
    protected const string G_TURN_RIGHT = "TurnRight";
    protected const string G_PREFALL_IDLE = "PrefallIdle";
    protected const string G_PREFALL_STOP = "PrefallStop";
    protected const string G_FALL = "Fall";

    ////Graphic SM without Mount Parameters 
    //protected const string G_IDLE = "Idle";
    //protected const string G_AWAKE = "Awake";
    //protected const string G_DECELERATION = "Deceleration";
    //protected const string G_RECOVERY = "Recovery";
    //protected const string G_MOVETO = "Charge";
    //protected const string G_DEFENSLESS = "Defensless";

    protected float timer = 0;
    
    //private
    int layerResult = 0;
    bool finishedBullets;
    float anglesToRotate;


    public override void Enter() {
        animator.SetFloat(TIMER, timer = 0);
        graphicsAnimator = boss.Graphics.BossGraphicAnimator;
        //BossOrbitManager.ChangedStateEvent();
    }

    public override void Tick()
    {
        timer += Time.deltaTime;
        animator.SetFloat(TIMER, timer);

    }

    public override void Exit() {
        timer = 0;
        anglesToRotate = 0;
        layerResult = 0;
        //boss.DestroyAllBullets();
        finishedBullets = false;
        boss.Graphics.BossGraphicAnimator.ResetTrigger(G_IDLE);
        boss.Graphics.BossGraphicAnimator.ResetTrigger(G_ANTICIPATION_CHARGE);
        boss.Graphics.BossGraphicAnimator.ResetTrigger(G_CHARGE);
        boss.Graphics.BossGraphicAnimator.ResetTrigger(G_PREFALL_IDLE);
        boss.Graphics.BossGraphicAnimator.ResetTrigger(G_PREFALL_STOP);
        boss.Graphics.BossGraphicAnimator.ResetTrigger(G_FALL);
    }


    public void SetContext(object context/*, object secondContext*/ , Animator animator , Animator graphicAnimator /*, BossOrbitManager bossOrbitManager ,  */)
    {
        //base.SetContext(context, animator , bossOrbitManager);
        boss = context as SecondBossController;
        this.animator = animator;
        this.boss.Graphics.BossGraphicAnimator = graphicAnimator;
        //this.bossOrbitManager = bossOrbitManager;

    }

    protected void TriggerExitState()
    {
        animator.SetTrigger(END_STATE_TRIGGER);
    }


    //Updates the MASK_COUNT SM Parameters when a Boss' Mask is detroyed
    public void CheckVulnerability() {
        //In animator check parametro per verificare la vulnerabilità
    }


    public void SetCycleTimer()
    {
        boss.CycleTimer += Time.deltaTime;
        animator.SetFloat("CycleTimer", boss.CycleTimer);
    }

    public void ResetCycleTimer()
    {
        boss.CycleTimer = 0;
        animator.SetFloat("CycleTimer", boss.CycleTimer);
    }

    public void SetBulletValues(BulletData _data , float _cycleTimer , Transform _emitterPos) {
        for (int i = 0; i < _data.BulletTriggerTimes.Count; i++) {
            int tempCycleTimer = (int) (_cycleTimer * 100);
            int tempCycleBulletTriggerTimes = (int) (_data.BulletTriggerTimes[i] * 100);
            if (tempCycleTimer == tempCycleBulletTriggerTimes) {
                boss.currentBulletData = _data;
                boss.FireBullet((int) _data.BulletGraphics , _emitterPos);
            }
        }

    }

    public void RotateBoss(Vector3 _target ,  float _rotationSpeed) {
        Vector3 posDestination = (_target - boss.transform.position).normalized;
        posDestination = Vector3.ProjectOnPlane(posDestination , Vector3.up);
        Quaternion rotationToTarget = Quaternion.LookRotation(posDestination);
        boss.transform.rotation = Quaternion.RotateTowards(boss.transform.rotation, rotationToTarget, _rotationSpeed * Time.deltaTime);
        boss.MountGraphics.transform.rotation = Quaternion.RotateTowards(boss.MountGraphics.transform.rotation, rotationToTarget, _rotationSpeed * Time.deltaTime);        
    }

    public void SetOrbitTag(int _orbitTag) {
        boss.animator.SetInteger("OrbitTag" , _orbitTag);
    }


}