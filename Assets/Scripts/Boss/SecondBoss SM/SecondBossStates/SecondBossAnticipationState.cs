﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SecondBossAnticipationState : SecondBossState
{
    //Public 
    public AnticipationData anticipationData;

    //Private
    private float BossToPlayerAngle;

    public override void Enter()
    {
        base.Enter();
        graphicsAnimator.SetTrigger(G_ANTICIPATION_CHARGE);
        boss.resistanceToRotation = anticipationData.resistanceToRotation;
        if (anticipationData.isAnticipating) {
            graphicsAnimator.SetTrigger(G_ANTICIPATION_CHARGE);
            //boss.MountGraphics.GetComponent<Renderer>().material = boss.antMat;
        }
        SetOrbitTag(anticipationData.OrbitTag);
        //graphicsAnimator.SetTrigger(G_IDLE);
        boss.hasAwaken = true;
        ResetCycleTimer();
        animator.SetInteger("Layer", 0);
        EnterAnticipation();
    }

    public override void Tick()
    {
        base.Tick();

        if (anticipationData.Target.instance != null) {
            //boss.Graphics.transform.DOLookAt(anticipationData.Target.instance.transform.position, anticipationData.rotationTime);

            BossToPlayerAngle = Vector3.SignedAngle(boss.transform.position , boss.Player.transform.position , Vector3.up);
            RotateBoss(anticipationData.Target.instance.transform.position , boss.MountRotationSpeed);
            //BossToPlayerAngle = Mathf.Abs(BossToPlayerAngle);

            //boss.Graphics.transform.Rotate(0,BossToPlayerAngle * Time.deltaTime * anticipationData.rotationSpeed ,0);
            //boss.transform.Rotate(0,BossToPlayerAngle * Time.deltaTime * anticipationData.rotationSpeed ,0);
        }


        if (isCastingBullets) {
            foreach (var bulletData in anticipationData.BulletData) {
                SetBulletValues(bulletData, boss.CycleTimer, anticipationData.Emitter.instance.transform);
            }
        }

    }

    public override void Exit()
    {
        animator.SetBool("AnticipationOrbit", false);
        CheckVulnerability();
        AnticipationExit();
        base.Exit();
    }

    public void EnterAnticipation() {
        --boss.loops;
        animator.SetInteger("Loops", boss.loops);

        if (anticipationData.InfinteLoops && boss.loops <= 0) {
            boss.loops = int.MaxValue;
        }
    }

    public void AnticipationExit()
    {
        boss.IsPrevStateReinitialize = false;
        graphicsAnimator.SetTrigger(G_IDLE);
        //boss.MountGraphics.GetComponent<Renderer>().material = boss.baseMat;
        animator.SetBool("Anticipation", false);
    }

}
