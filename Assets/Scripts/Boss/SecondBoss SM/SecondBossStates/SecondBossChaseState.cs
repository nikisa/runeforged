﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SecondBossChaseState : SecondBossState
{
    //Inspector
    public bool Debugging;
    public ChaseData chaseData;

    //Private 
    int layerResult;
    int layerWall;
    int layerCollision;
    int layerPlayer;
    float maxSpeed;
    float startY;
    float timeStartAcceleration;
    float timeStartChase;
    float AngularSpeed;
    float deltaAngle;
    float accelerationModule;
    Vector3 targetDir;
    GameObject Target;

    public override void Enter()
    {
        base.Enter();
        if (chaseData.isAnticipating == true)
        {
            graphicsAnimator.SetTrigger(G_ANTICIPATION_CHARGE);
            //boss.MountGraphics.GetComponent<Renderer>().material = boss.antMat;
        }

        layerWall = 10;
        layerPlayer = 11;

        Target = chaseData.Target.instance;
        boss.Target = Target;
        SetOrbitTag(chaseData.OrbitTag);
        AccelerationEnter();
        ChaseEnter();

        boss.actualMaxSpeed = chaseData.MaxSpeed;
    }

    public override void Tick()
    {
        base.Tick();

        //setChaseRadius();
        ChaseTick();
        SetSpeed();
        SetCycleTimer();
    }

    public override void Exit()
    {
        base.Exit();
        graphicsAnimator.SetTrigger(G_IDLE);
        //boss.MountGraphics.GetComponent<Renderer>().material = boss.baseMat;
        boss.IsPrevStateReinitialize = false;
        CheckVulnerability();
        animator.SetBool("ChaseOrbit", false);
        //animator.SetInteger("Layer", layerResult);
        boss.animator.SetInteger("Layer", 0);
        

    }

    public void AccelerationEnter()
    {
        timeStartAcceleration = Time.time;
        //boss.VelocityVector = Vector3.zero;
        //boss.MovementReset();
    }

    public void ChaseEnter()
    {
        timeStartChase = Time.time;
        startY = boss.transform.position.y; //Keep costant the Y axes of the Boss position
        accelerationModule = chaseData.MaxSpeed / chaseData.TimeAcceleration;

        if (chaseData.hasInitialSpeed == true)
        {
            boss.VelocityVector = boss.MountGraphics.transform.forward * chaseData.initialSpeed;
        }
    }

    //Chase the target
    public void ChaseTick()
    {

        //boss.transform.DOLookAt(boss.transform.position + boss.AccelerationVector, .5f/*, tweeningRotationTime*/)/*.SetEase(rotationEase)*/;
        //boss.Graphics.transform.DOLookAt(boss.Graphics.transform.position + boss.AccelerationVector, .5f/*, tweeningRotationTime*/)/*.SetEase(rotationEase)*/;

        float Angle = Vector3.SignedAngle(boss.transform.position , chaseData.Target.instance.transform.position , Vector3.up); // dove viene usato???

        if(chaseData.lookAtForward == true)
        {
            boss.RotateOnVelocityVector();
        }
        else
        {
            RotateBoss(chaseData.Target.instance.transform.position , boss.MountRotationSpeed);
        }

        if (Target != null) {
            targetDir = Target.transform.position - boss.transform.position;
        }
        else {
            DefaultDir();
        }

        if (layerCollision == layerWall && boss.VelocityVector.magnitude > 5) {
            animator.SetInteger("Layer", layerCollision);
        }
        else {
            //if (boss.CharacterController == null) {
                boss.Movement(targetDir, chaseData.MaxSpeed, accelerationModule);
            //}
            //else {
            //    boss.CharacterControllerMovement(targetDir, chaseData.MaxSpeed, accelerationModule);
            //}
        }


        if (isCastingBullets) {
            foreach (var bulletData in chaseData.BulletData) {
                SetBulletValues(bulletData, boss.CycleTimer, chaseData.Emitter.instance.transform);
            }
        }

    }


    //public void setChaseRadius() {
    //    float distance = (Target.transform.position - boss.transform.position).magnitude;
    //    animator.SetFloat("ChaseRadius", distance);
    //}

    //Set speed parameter in the animator
    public void SetSpeed() {
        animator.SetFloat("Speed" , boss.VelocityVector.magnitude);
    }

    public void DefaultDir() {
        //targetDir = -boss.transform.forward;
        targetDir = boss.AccelerationVector;
        boss.AccelerationVector = -boss.transform.forward;
    }


}
