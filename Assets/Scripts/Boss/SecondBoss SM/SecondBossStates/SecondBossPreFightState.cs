﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondBossPreFightState : SecondBossState
{

    //Private
    int HookPointsCount;

    public override void Enter() {
        base.Enter();
        boss.isHookable = true;
    }

    public override void Tick() {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            animator.SetTrigger(END_STATE_TRIGGER);
        }
        SetCycleTimer();
    }

    public override void Exit() {
        base.Exit();
        boss.IsPrevStateReinitialize = false;
        boss.isHookable = false;
    }

}
