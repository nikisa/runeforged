﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SecondBossDecelerateTowardsState : SecondBossState
{
    //Inspector
    public DecelerationData decelerationData;

    //Private
    float timer;
    float finalDeltaTime;
    int iterations;
    Vector3 targetDir;


    public override void Enter()
    {
        iterations = 1;
        base.Enter();
        if (decelerationData.isRecovering)
        {
            boss.MountGraphics.GetComponent<Renderer>().material = boss.recMat;
        }
        SetOrbitTag(decelerationData.OrbitTag);
        graphicsAnimator.SetTrigger(G_IDLE);
        animator.SetInteger("Layer", 0);
        timer = 0;
        boss.DecelerationModule = decelerationData.Deceleration;
    }

    public override void Tick()
    {
        Debug.Log("Bool is: " + decelerationData.lookAtForward);
        timer += Time.deltaTime;
        base.Tick();
        UpdateTargetDir();
        //CollisionTick();
        setChaseRadius();
        Deceleration();
        SetCycleTimer();
        SetSpeed();

        float Angle = Vector3.SignedAngle(boss.transform.position, decelerationData.Target.instance.transform.position, Vector3.up);

        if (decelerationData.lookAtForward == true)
        {
            boss.RotateOnVelocityVector();
        }
        else
        {
            RotateBoss(decelerationData.Target.instance.transform.position, boss.MountRotationSpeed);
        }

        //boss.transform.DOLookAt(boss.transform.position + boss.AccelerationVector, .5f/*, tweeningRotationTime*/)/*.SetEase(rotationEase)*/;
        //boss.Graphics.transform.DOLookAt(boss.Graphics.transform.position + boss.AccelerationVector, .5f/*, tweeningRotationTime*/)/*.SetEase(rotationEase)*/;
    }

 

    public override void Exit()
    {
        base.Exit();
        boss.MountGraphics.GetComponent<Renderer>().material = boss.baseMat;
        boss.IsPrevStateReinitialize = false;
        CheckVulnerability();

        
    }

    //Set speed parameter in the animator
    public void SetSpeed() {
        animator.SetFloat("Speed", boss.VelocityVector.magnitude);
    }

    public void Deceleration()
    {
        boss.DecelerateTowards(targetDir , decelerationData.Deceleration);
    }

    public void setChaseRadius()
    {
        float distance = (boss.Target.transform.position - boss.transform.position).magnitude;
        animator.SetFloat("ChaseRadius", distance);
    }

    

    void UpdateTargetDir() {
        targetDir = (decelerationData.Target.instance.transform.position - boss.transform.position).normalized;
    }

}
