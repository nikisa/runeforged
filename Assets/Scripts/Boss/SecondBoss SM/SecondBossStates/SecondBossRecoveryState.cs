﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class SecondBossRecoveryState : SecondBossState
{
    //Inspector
    public RecoveryData recoveryData;
    public DecelerationData decelerationData;

    public override void Enter()
    {
        base.Enter();
        //if (recoveryData.isRecovering)
        //{
            
        //    //boss.MountGraphics.GetComponent<Renderer>().material = boss.recMat;
        //}
        SetOrbitTag(recoveryData.OrbitTag);
        graphicsAnimator.SetTrigger(G_IDLE);
        boss.AccelerationVector = Vector3.zero;
        boss.isHookable = true;
    }

    public override void Tick()
    {
        base.Tick();
        SetCycleTimer();

        if (isCastingBullets) {
            foreach (var bulletData in decelerationData.BulletData) {
                SetBulletValues(bulletData, boss.CycleTimer, decelerationData.Emitter.instance.transform);
            }
        }
    }

    public override void Exit()
    {
        base.Exit();
        //boss.MountGraphics.GetComponent<Renderer>().material = boss.baseMat;
        boss.isHookable = false;
        animator.SetBool("RecoveryOrbit", false);
        CheckVulnerability();
        boss.IsPrevStateReinitialize = false;

    }
}
