﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Experimental.Input;

public class SpikedBallManager : MonoBehaviour
{

    //Inspector
    public SecondBossController boss;
    public GameObject orbitCenter;
    public GameObject attackOrbitCenter;
    public GameObject bossCenter;

    //Public
    //[HideInInspector]
    public int iterations;
    //[HideInInspector]
    public float initialRadius;
    //[HideInInspector]
    public float startingAngle;
    //[HideInInspector]
    public float currentRadius;
    //[HideInInspector]
    public float oldRadius;
    //[HideInInspector]
    public float changeMaterialTimePercentage;
    //[HideInInspector]
    public float angularMinVelocity;
    //[HideInInspector]
    public float angularMaxVelocity;
    //[HideInInspector]
    public float AngularVelocity;
    //[HideInInspector]
    public float revolutionPerSeconds;
    //[HideInInspector]
    public float initialTimer;
    //[HideInInspector]
    public float timer;

    public LayerMask ballLayerMask;
    public float collisionOffset = 0.001f;

    private Vector3 spatialVector;
    private float spikedBallRadius;

    //CURVES
    #region CURVES
    //[HideInInspector]
    public AnimationCurve accelerationCurve;
    //[HideInInspector]
    public AnimationCurve decelerationCurve;
    //[HideInInspector]
    public AnimationCurve attackAnticipationCurve;
    //[HideInInspector]
    public AnimationCurve attackEntryCurve;
    //[HideInInspector]
    public AnimationCurve attackActiveCurve;
    //[HideInInspector]
    public AnimationCurve attackExitCurve;
    #endregion


    #region ANTICIPATION
    //[HideInInspector]
    public float anticipationTime;
    //[HideInInspector]
    public float anticipationFinalVelocity;
    [HideInInspector]
    public float TargetYOffest;
    [HideInInspector]
    public bool isTargetingDistance;
    [HideInInspector]
    public float OrbitCenterRotationSpeed;

    #endregion

    #region ATTACK

    public float AttackAngularVelocity;
    public float minAttackRange;
    public float maxAttackRange;
    public float ResetOrbitOrientationVelocity;
    [HideInInspector]
    public Quaternion oldOrbitCenterRotation;

    #endregion

    //Private
    string anticipationEnd = "AnticipationEnd";
    string orbitAttack = "OrbitAttack";

    private void Awake() {
        iterations = 1;
        spikedBallRadius = GetComponent<SphereCollider>().radius;
    }


    public void RotateAroud(AnimationCurve _dashCurve, float _t0, float _t1, int _iterations, float _verse) {

        transform.localEulerAngles += new Vector3(0, Integration.IntegrateCurve(_dashCurve, _t0, _t1, _iterations) * Mathf.Sign(_verse), 0);
        //CONTROLLA COLLISIONI COL TERRENO!! SE CI SONO: PARTICELLARI!
        transform.position = orbitCenter.transform.position + currentRadius * transform.forward;

        #region BACKUP
        //AngularAccelerationModule = _angularMaxSpeed / _angularAccelerationTime;
        //Drag = AngularAccelerationModule / _angularMaxSpeed * Time.fixedDeltaTime;

        //AngularVelocity -= AngularVelocity * Drag;
        //transform.eulerAngles += new Vector3(0, AngularVelocity * Time.fixedDeltaTime + 0.5f * AngularAccelerationModule * Mathf.Pow(Time.fixedDeltaTime, 2), 0);
        //transform.position = new Vector3(orbitCenter.transform.position.x + currentRadius * Mathf.Sin((transform.eulerAngles.y) * Mathf.Deg2Rad), orbitCenter.transform.position.y, orbitCenter.transform.position.z + currentRadius * Mathf.Cos((transform.eulerAngles.y) * Mathf.Deg2Rad));
        ////AngularVelocity += AngularAccelerationModule * Time.fixedDeltaTime + Mathf.CeilToInt(Mathf.Abs((Mathf.Sin((timerinSeconds /** multiplier*/) / (numerOfCircumference / 2)) /** _angularMaxSpeed*/)));
        //AngularVelocity = Mathf.CeilToInt(Mathf.Abs((Mathf.Sin((timerinSeconds /** multiplier*/) / (numerOfCircumference / 2)) * _angularMaxSpeed)));
        //AngularVelocity = Mathf.Clamp(AngularVelocity, minVelocity, _angularMaxSpeed);
        //VelocityVector = transform.right * (AngularVelocity * Mathf.Deg2Rad * currentRadius);
        #endregion
    }

    public void AttackRotateAroud(AnimationCurve _dashCurve, float _t0, float _t1, int _iterations, float _verse, float _ellipseHeight, float _ellipseWidth) // aggiungi gli assi dell'ellisse
    {
        _ellipseHeight /= 2;
        _ellipseWidth /= 2;

        transform.localEulerAngles += new Vector3(0, Integration.IntegrateCurve(_dashCurve, _t0, _t1, _iterations) * Mathf.Sign(_verse), 0);
        //CONTROLLA COLLISIONI SUL TERRENO CON SPHERECAST!! SE CI SONO: PARTICELLARI!
        //Essendo ellisse, cambia completamente il calcolo della transform!!!
        //transform.position = attackOrbitCenter.transform.position + currentRadius * transform.forward;

        float ellipseAngle = Vector3.SignedAngle(attackOrbitCenter.transform.forward, transform.forward, attackOrbitCenter.transform.up);

        //currentRadius = (_ellipseHeight * Mathf.Cos(ellipseAngle * Mathf.Deg2Rad) * attackOrbitCenter.transform.forward +
        //                 _ellipseWidth * Mathf.Sin(ellipseAngle * Mathf.Deg2Rad) * attackOrbitCenter.transform.right).magnitude;

        //transform.position = attackOrbitCenter.transform.position +
        //                     (_ellipseHeight * Mathf.Cos(ellipseAngle * Mathf.Deg2Rad) * attackOrbitCenter.transform.forward +
        //                     _ellipseWidth * Mathf.Sin(ellipseAngle * Mathf.Deg2Rad) * attackOrbitCenter.transform.right);

        spatialVector = (_ellipseHeight * Mathf.Cos(ellipseAngle * Mathf.Deg2Rad) * attackOrbitCenter.transform.forward +
                         _ellipseWidth * Mathf.Sin(ellipseAngle * Mathf.Deg2Rad) * attackOrbitCenter.transform.right);

        SpikedBallCollision(spatialVector);
    }




    void SpikedBallCollision(Vector3 _spatialVector)
    {
        RaycastHit hit;
        Vector3 _vectorDir = _spatialVector.normalized;
        float _spatialMagnitude = _spatialVector.magnitude;

        if (Physics.SphereCast(transform.position, spikedBallRadius, _vectorDir, out hit, _spatialMagnitude, ballLayerMask, QueryTriggerInteraction.UseGlobal))
        {
            Vector3 hitSpatialVector = hit.distance * _vectorDir + collisionOffset * hit.normal;


            Vector3 slopeSpatialVector = Vector3.ProjectOnPlane((_spatialVector - hitSpatialVector), hit.normal);
            Vector3 slopeDir = slopeSpatialVector.normalized;

            transform.position = attackOrbitCenter.transform.position + hitSpatialVector + slopeSpatialVector;
            //currentHitObject = hit.transform.gameObject; //Memorizza hitObject, il Game Object contro il quale è avvenuta la collisione.

            //POSIZIONA E FAI PARTIRE FARTICELLARE!

            //SpikedBallCollision(slopeSpatialVector);
        }
        else
        {
            transform.position = attackOrbitCenter.transform.position + _spatialVector;
        }
    }


    public void doRevolution() {

        float t0 = timer;
        timer += Time.deltaTime;

        if (t0 <= Mathf.Abs(1 / revolutionPerSeconds) / 2) {
            if (timer <= Mathf.Abs(1 / (revolutionPerSeconds / 2))) {
                RotateAroud(accelerationCurve, t0, timer, iterations, revolutionPerSeconds);
                AngularVelocity = accelerationCurve.Evaluate(timer);
            }
            else {
                RotateAroud(accelerationCurve, t0, (Mathf.Abs(1 / revolutionPerSeconds) / 2), iterations, revolutionPerSeconds);
                RotateAroud(decelerationCurve, (Mathf.Abs(1 / revolutionPerSeconds) / 2), timer, iterations, revolutionPerSeconds);
                AngularVelocity = decelerationCurve.Evaluate(timer);
            }
        }
        else //t0 > 0;
        {
            if (timer <= Mathf.Abs(1 / revolutionPerSeconds)) {
                RotateAroud(decelerationCurve, t0, timer, iterations, revolutionPerSeconds);
                AngularVelocity = decelerationCurve.Evaluate(timer);
            }
            else {
                RotateAroud(decelerationCurve, t0, Mathf.Abs(1 / revolutionPerSeconds), iterations, revolutionPerSeconds);
                currentRadius = initialRadius;
                oldRadius = currentRadius;
                timer -= Mathf.Abs(1 / revolutionPerSeconds);
                RotateAroud(accelerationCurve, 0, timer, iterations, revolutionPerSeconds);
                AngularVelocity = accelerationCurve.Evaluate(timer);
            }
        }
        currentRadius = DOVirtual.EasedValue(oldRadius, initialRadius, timer / Mathf.Abs(1 / revolutionPerSeconds), Ease.Linear);

    }

    public void StartAnticipationTimer()
    {
        initialTimer = timer;
        timer += Time.deltaTime;
    }
    

    public void doAttackAnticipation() {

        if (timer < anticipationTime) {
            RotateAroud(attackAnticipationCurve, initialTimer, timer, iterations, revolutionPerSeconds);
        }
        else {
            timer = 0;
            RotateAroud(attackAnticipationCurve, initialTimer, anticipationTime, iterations, revolutionPerSeconds);
            Debug.Log("Fine Anticipazione, Angolo SpikedBall: " + transform.localEulerAngles.y + ", per CR: " + currentRadius);
            boss.animator.SetTrigger(anticipationEnd); 
            //boss.animator.SetTrigger(orbitAttack);
        }
    }

    // CAMBIARE ROTATION SU ASSE Z IN BASE A SE ORIZZONTALE O VERTICALE!
    public void OrbitAttackOrientation(Vector3 _target , float _offset, float _rotationSpeed) { //QUESTA ROTAZIONE VA LIMITATA? L'angolo tra forward del knight a puntatore non dovrebbe superare un certo valore.
        _target = new Vector3(_target.x, _target.y + _offset, _target.z);
        //Vector3 posDestination = (_target - orbitCenter.transform.position).normalized;
        Vector3 posDestination = (_target - orbitCenter.transform.position);
        //posDestination = Vector3.ProjectOnPlane(posDestination, orbitCenter.transform.right);
        Quaternion rotationToTarget = Quaternion.LookRotation(posDestination);

        //QUA SI DECIDE SE HORIZONTAL O VERTICAL!
        float attackMaxRotation = boss.maxAttackAngle;

        orbitCenter.transform.rotation = Quaternion.RotateTowards(orbitCenter.transform.rotation, rotationToTarget, _rotationSpeed * Time.deltaTime);
        orbitCenter.transform.localEulerAngles = new Vector3(orbitCenter.transform.localEulerAngles.x, Mathf.Clamp(orbitCenter.transform.localEulerAngles.y, -attackMaxRotation, attackMaxRotation), orbitCenter.transform.localEulerAngles.z);
        Debug.DrawLine(orbitCenter.transform.position, _target, Color.red, .02f);

        //Vector3 diatance = ((_target  + offset)- orbitCenter.transform.position);
        //Vector3.ProjectOnPlane(distance , orbitCenter.transform.right )
    }

    public void OrbitCenterReset(float _rotationSpeed) {

        orbitCenter.transform.localRotation = Quaternion.RotateTowards(orbitCenter.transform.localRotation, oldOrbitCenterRotation, _rotationSpeed * Time.deltaTime);

    }

    private void OnTriggerEnter(Collider other) {
        if ((other.GetComponent<MovementBase>() || other.GetComponent<CharacterControllerMovementBase>() || (other.GetComponent<PlayerView>() || other.GetComponent<PlayerController>())) && !other.GetComponent<BossController>()) {
            if (!boss.Player.IsImmortal) {
                boss.Player.GetDamage();
            }
        }
    }

}
