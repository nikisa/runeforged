﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SecondBossOrbitControllerState : SecondBossState
{

    //Public
    [HideInInspector]
    public SpikedBallManager spikedBall;


    public override void Enter() {
        spikedBall = boss.SpikedBall;
        boss.animator.SetInteger("OrbitTag" , 0);
    }

    public override void Tick() {
        spikedBall.doRevolution();
        boss.ResetRotation(boss.resetKnightRotationSpeed); //Post refacotring provare con una coroutine che parte dalla fine dell'attacco (NOTA DEL DESIGNER: No, lascia così senza coroutine. Dovrebbe essere meglio, specie in caso di COMBO.)
        spikedBall.OrbitCenterReset(spikedBall.ResetOrbitOrientationVelocity);
    }

}

