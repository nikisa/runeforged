﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SecondBossOrbitIdleState : SecondBossState
{

    //Inspector
    public RevolutionOrbitData revolutionOrbitData;

    //Public
    [HideInInspector]
    public SpikedBallManager spikedBall;

    public override void Enter() {

        spikedBall = boss.SpikedBall;
        spikedBall.currentRadius = revolutionOrbitData.initialRadius;
        spikedBall.revolutionPerSeconds = revolutionOrbitData.revolutionPerSeconds;
        spikedBall.angularMinVelocity = revolutionOrbitData.angularMinVelocity;
        spikedBall.angularMaxVelocity = 720 * Mathf.Abs(revolutionOrbitData.revolutionPerSeconds) - revolutionOrbitData.angularMinVelocity; // deltaW = 360 * revolutionPerSeconds = deltaAngSpace (in deltaTime = 1 / revolutionPerSeconds); deltaW = (angVel0 + angVel1) / 2. 
        spikedBall.transform.localEulerAngles += new Vector3(0, revolutionOrbitData.StartingAngle, 0);
        SetRevolutionCurve();
        boss.animator.SetTrigger(END_STATE_TRIGGER);
    }

   
    public void SetRevolutionCurve() {
        spikedBall.accelerationCurve.AddKey(0, revolutionOrbitData.angularMinVelocity);
        spikedBall.accelerationCurve.AddKey(Mathf.Abs(1 / revolutionOrbitData.revolutionPerSeconds) / 2, spikedBall.angularMaxVelocity);
        spikedBall.decelerationCurve.AddKey((Mathf.Abs(1 / revolutionOrbitData.revolutionPerSeconds) / 2), spikedBall.angularMaxVelocity);
        spikedBall.decelerationCurve.AddKey(Mathf.Abs(1 / revolutionOrbitData.revolutionPerSeconds), revolutionOrbitData.angularMinVelocity);
    }

}

