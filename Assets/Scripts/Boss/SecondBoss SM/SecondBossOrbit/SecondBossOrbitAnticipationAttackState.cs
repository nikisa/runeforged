﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SecondBossOrbitAnticipationAttackState : SecondBossState
{

    //Inspector
    public AnticipationAttackOrbitData anticipationAttackOrbitData;

    //Public
    [HideInInspector]
    public SpikedBallManager spikedBall;

    public override void Enter() {

        spikedBall = boss.SpikedBall;
        spikedBall.timer = 0;
        spikedBall.TargetYOffest = anticipationAttackOrbitData.TargetYOffest;
        spikedBall.OrbitCenterRotationSpeed = anticipationAttackOrbitData.OrbitCenterRotationSpeed;
        spikedBall.anticipationTime = anticipationAttackOrbitData.AnticipationTime;
        spikedBall.oldRadius = spikedBall.currentRadius;
        spikedBall.isTargetingDistance = anticipationAttackOrbitData.isTargetingDistance;
        SetAttackAnticipation();

    }

    public override void Tick() {
        spikedBall.StartAnticipationTimer();
        //SCEGLIERE QUI SE ORIZZONTALE O VERTICALE. In più, sempre qui dovresti controllare se il target è il Player o No
        spikedBall.OrbitAttackOrientation(boss.Player.transform.position, spikedBall.TargetYOffest, spikedBall.OrbitCenterRotationSpeed);
        //if (anticipationAttackOrbitData.isVertical)
        //{
        //    SetVerticalAttack();
        //}
        UpdateCurrentRadius();
        spikedBall.doAttackAnticipation();
        boss.RotateOnTarget();
    }

    public override void Exit() {
        //spikedBall.attackOrbitCenter.transform.localEulerAngles = spikedBall.orbitCenter.transform.localEulerAngles;       
    }


    void SetAttackAnticipation() {
        float deltaAngle;
        //Ricava Vettore in base a Starting Angle. Poi trova soluzione con SignedAngle.
        Vector3 vectorZeroAngle = Quaternion.AngleAxis(spikedBall.startingAngle, spikedBall.orbitCenter.transform.up) * spikedBall.orbitCenter.transform.forward;
        //Debug.DrawLine(spikedBall.orbitCenter.transform.position, spikedBall.orbitCenter.transform.position + vectorZeroAngle);

        deltaAngle = Vector3.SignedAngle(vectorZeroAngle, spikedBall.transform.forward, spikedBall.orbitCenter.transform.up);
        //deltaAngle -= Mathf.Floor(deltaAngle / 360) * 360;
        deltaAngle = (360 - Mathf.Sign(spikedBall.revolutionPerSeconds) * deltaAngle) % 360;        
        spikedBall.anticipationFinalVelocity = (2 * (360 * anticipationAttackOrbitData.AnticipationRevolutions + deltaAngle) / anticipationAttackOrbitData.AnticipationTime) - spikedBall.AngularVelocity;
        //Setta la curva e inizia il movimento del center;
        SetAttackAnticipationCurve();
    }

    public void SetAttackAnticipationCurve() {
        spikedBall.attackAnticipationCurve.keys = null;

        spikedBall.attackAnticipationCurve.AddKey(0, spikedBall.AngularVelocity);
        spikedBall.attackAnticipationCurve.AddKey(spikedBall.anticipationTime, spikedBall.anticipationFinalVelocity);
    }


    public void UpdateCurrentRadius() {
        //spikedBall.currentRadius = DOVirtual.EasedValue(spikedBall.currentRadius , anticipationAttackOrbitData.finalRadius , spikedBall.timer / anticipationAttackOrbitData.AnticipationTime , anticipationAttackOrbitData.radiusEase);
        spikedBall.currentRadius = DOVirtual.EasedValue(spikedBall.oldRadius , anticipationAttackOrbitData.finalRadius , Mathf.Clamp01(spikedBall.timer / anticipationAttackOrbitData.AnticipationTime) , anticipationAttackOrbitData.radiusEase);
    }

    void SetVerticalAttack()
    {
        float zAngle;
        
        zAngle = DOVirtual.EasedValue(spikedBall.orbitCenter.transform.localEulerAngles.z, 90 * Mathf.Sign(spikedBall.revolutionPerSeconds), Mathf.Clamp01(spikedBall.timer / anticipationAttackOrbitData.AnticipationTime), anticipationAttackOrbitData.radiusEase);
        spikedBall.orbitCenter.transform.localEulerAngles = new Vector3(spikedBall.orbitCenter.transform.localEulerAngles.x, spikedBall.orbitCenter.transform.localEulerAngles.y, zAngle);
    }

}

