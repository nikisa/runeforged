﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SecondBossOrbitSetupState : SecondBossState
{

    //Inspector
    public RevolutionOrbitData revolutionOrbitData;

    //Public
    [HideInInspector]
    public SpikedBallManager spikedBall;


    public override void Enter() {

        spikedBall = boss.SpikedBall;
        spikedBall.initialRadius = revolutionOrbitData.initialRadius;
        spikedBall.currentRadius = spikedBall.initialRadius;
        spikedBall.startingAngle = revolutionOrbitData.StartingAngle;
        spikedBall.revolutionPerSeconds = revolutionOrbitData.revolutionPerSeconds;
        spikedBall.angularMinVelocity = revolutionOrbitData.angularMinVelocity;
        spikedBall.angularMaxVelocity = 720 * Mathf.Abs(revolutionOrbitData.revolutionPerSeconds) - revolutionOrbitData.angularMinVelocity; // deltaW = 360 * revolutionPerSeconds = deltaAngSpace (in deltaTime = 1 / revolutionPerSeconds); deltaW = (angVel0 + angVel1) / 2. 
        spikedBall.oldOrbitCenterRotation = spikedBall.orbitCenter.transform.localRotation;
        spikedBall.transform.localEulerAngles += new Vector3(0, revolutionOrbitData.StartingAngle, 0);
        SetRevolutionCurve();
    }


    public void SetRevolutionCurve() {
        spikedBall.accelerationCurve.AddKey(0, revolutionOrbitData.angularMinVelocity);
        spikedBall.accelerationCurve.AddKey(Mathf.Abs(1 / revolutionOrbitData.revolutionPerSeconds) / 2, spikedBall.angularMaxVelocity);
        spikedBall.decelerationCurve.AddKey((Mathf.Abs(1 / revolutionOrbitData.revolutionPerSeconds) / 2), spikedBall.angularMaxVelocity);
        spikedBall.decelerationCurve.AddKey(Mathf.Abs(1 / revolutionOrbitData.revolutionPerSeconds), revolutionOrbitData.angularMinVelocity);
    }

}

