﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Runtime.CompilerServices;

public class SecondBossOrbitAttackState : SecondBossState
{

    //Inspector
    public AttackOrbitData attackOrbitData;

    //Public
    [HideInInspector]
    public SpikedBallManager spikedBall;

    //Private
    float distanceFromTarget;
    float ellipseWidth;
    float ellipseHeight;
    float deltaTimeEntry;
    float deltaTimeExit;
    float deltaTimeEnd;
    float ellipseFocus;
    //Animator Triggers
    string attackStart = "AttackStart";
    string attackEnd = "AttackEnd";


    public override void Enter() {
        spikedBall = boss.SpikedBall;
        spikedBall.minAttackRange = attackOrbitData.minAttackRange;
        spikedBall.maxAttackRange = attackOrbitData.maxAttackRange;
        spikedBall.timer = 0;
        boss.animator.SetTrigger("OrbitAttack");


        SetAttack();

        //PER MAGGIORE LIBERTA':
        //1. Fare un ellisse al posto di un cerchio, stabilendo i due fuochi (basta sempre un solo attack orbit center, l'altro fuoco si trova con un semplice calcolo).
        //Il pro sarebbe poter far anche attacchi diretti con la stessa formula (provvedendo, però, a scegliere la rotazione finale del center)
        //2. Dare la possibilità di settare una distanza PREDEFINITA al posto del mirare sempre e solo al player.
        //3. Provvedere alle COMBO senza dover passare per anticipation (dopo ATTACK END?)
        
    }

    public override void Tick() {
        DoAttack(deltaTimeEntry, deltaTimeExit, deltaTimeEnd);
        //spikedBall.OrbitAttackOrientation(boss.Player.transform.position , spikedBall.TargetYOffest, spikedBall.OrbitCenterRotationSpeed); // controllare
        Debug.Log("in attack: " + spikedBall.currentRadius);
    }

    public override void Exit() {
        spikedBall.currentRadius = spikedBall.oldRadius;
        spikedBall.attackOrbitCenter.transform.position = spikedBall.orbitCenter.transform.position;
    }

    public Vector3 DirAngle(float angleInDegrees) {
        angleInDegrees += spikedBall.bossCenter.transform.eulerAngles.y;
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad)); //diverse ideuzze per l'Y
    }

    private void SetAttack() { // SET ELLISSE
        //Il clamp andrebbe sul projectOnplane. Aiuterebbe
        //distanceFromTarget = Mathf.Clamp((boss.Player.transform.position - spikedBall.bossCenter.transform.position).magnitude, spikedBall.minAttackRange, spikedBall.maxAttackRange); //il set andrebbe fatta sul project on play
        if (spikedBall.isTargetingDistance) 
        {
            distanceFromTarget = attackOrbitData.fixTargetDistance;
            Debug.DrawLine(spikedBall.orbitCenter.transform.position, attackOrbitData.fixTargetDistance * spikedBall.orbitCenter.transform.forward, Color.blue);
        }
        else
        {
            distanceFromTarget = Mathf.Clamp((boss.Player.transform.position + spikedBall.TargetYOffest * Vector3.up - spikedBall.orbitCenter.transform.position).magnitude, spikedBall.minAttackRange, spikedBall.maxAttackRange); //il set andrebbe fatta sul project on play
            Debug.DrawLine(spikedBall.orbitCenter.transform.position, boss.Player.transform.position + spikedBall.TargetYOffest * Vector3.up, Color.blue);
        }

        //Solo per visualizzazione
        Debug.DrawLine(spikedBall.orbitCenter.transform.position, spikedBall.orbitCenter.transform.position + DirAngle(-attackOrbitData.AttackAngle / 2) * distanceFromTarget);
        Debug.DrawLine(spikedBall.orbitCenter.transform.position, spikedBall.orbitCenter.transform.position + DirAngle(attackOrbitData.AttackAngle / 2) * distanceFromTarget);
        //

        spikedBall.oldRadius = spikedBall.currentRadius;
        Debug.DrawLine(spikedBall.orbitCenter.transform.position, spikedBall.orbitCenter.transform.position + (spikedBall.currentRadius * -spikedBall.orbitCenter.transform.forward), Color.green);
        ellipseHeight = distanceFromTarget + spikedBall.oldRadius;

        //spikedBall.attackOrbitCenter.transform.position = spikedBall.orbitCenter.transform.position - spikedBall.oldRadius * spikedBall.orbitCenter.transform.forward + ellipseHeight / 2 * spikedBall.orbitCenter.transform.forward;
        spikedBall.attackOrbitCenter.transform.position = spikedBall.orbitCenter.transform.position +  (ellipseHeight / 2 - spikedBall.oldRadius) * spikedBall.orbitCenter.transform.forward;


        ellipseWidth = 2* ellipseHeight * Mathf.Sin((attackOrbitData.AttackAngle / 2) * Mathf.Deg2Rad);
        Debug.Log("Rapporto Assi Ellisse: " + ellipseHeight / ellipseWidth);


        spikedBall.AttackAngularVelocity = (attackOrbitData.AttackVelocity / distanceFromTarget) * Mathf.Rad2Deg;

        //deltaTimeEntry = 2 * (180 - (AttackAngle / 2)) / (angularMinVelocity + AttackAngularVelocity); //Il deltaTime di entrata cambia, in quanto la velocitÃ  di ingresso Ã¨ differente.
        deltaTimeEntry = 2 * (180 - (attackOrbitData.AttackAngle / 2)) / (spikedBall.anticipationFinalVelocity + spikedBall.AttackAngularVelocity); //Il deltaTime di entrata cambia, in quanto la velocitÃ  di ingresso Ã¨ differente.
        deltaTimeExit = deltaTimeEntry + (attackOrbitData.AttackAngle / spikedBall.AttackAngularVelocity); //il deltaTime di uscita, invece, resta invariato. Ma quello di ingresso cambia!

        //new
        deltaTimeEnd = deltaTimeExit + 2 * (180 - (attackOrbitData.AttackAngle / 2)) / (spikedBall.AttackAngularVelocity + spikedBall.angularMinVelocity);
        SetAttackCurve(deltaTimeEntry, deltaTimeExit, deltaTimeEnd);
    }

    public void SetAttackCurve(float entryTime, float exitTime, float endTime) {
        spikedBall.attackEntryCurve.keys = null;
        spikedBall.attackActiveCurve.keys = null;
        spikedBall.attackExitCurve.keys = null;

        //attackEntryCurve.AddKey(0, angularMinVelocity);
        spikedBall.attackEntryCurve.AddKey(0, spikedBall.anticipationFinalVelocity);
        spikedBall.attackEntryCurve.AddKey(entryTime, spikedBall.AttackAngularVelocity);
        spikedBall.attackActiveCurve.AddKey(entryTime, spikedBall.AttackAngularVelocity);
        spikedBall.attackActiveCurve.AddKey(exitTime, spikedBall.AttackAngularVelocity);
        spikedBall.attackExitCurve.AddKey(exitTime, spikedBall.AttackAngularVelocity);
        //attackExitCurve.AddKey(exitTime + entryTime, angularMinVelocity);
        spikedBall.attackExitCurve.AddKey(endTime, spikedBall.angularMinVelocity);
    }

    public void DoAttack(float entryTime, float exitTime, float endTime) {
        float t0 = timer;
        timer += Time.deltaTime;


        if (t0 <= entryTime) {
            if (timer <= entryTime) {
                //spikedBall.currentRadius = DOVirtual.EasedValue(spikedBall.initialRadius, distanceFromTarget, timer / entryTime, attackOrbitData.AttackEase); //Queste devono cambiare
                //spikedBall.RotateAroud(spikedBall.attackEntryCurve, t0, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds);

                SetEllipseAxis();
                spikedBall.AttackRotateAroud(spikedBall.attackEntryCurve, t0, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds, ellipseHeight, ellipseWidth);
                //spikedBall.currentRadius = (boss.Player.transform.position - spikedBall.attackOrbitCenter.transform.position).magnitude;
                spikedBall.OrbitAttackOrientation(boss.Player.transform.position, spikedBall.TargetYOffest, spikedBall.OrbitCenterRotationSpeed);
            }
            else {
                //spikedBall.RotateAroud(spikedBall.attackEntryCurve, t0, entryTime, spikedBall.iterations, spikedBall.revolutionPerSeconds);
                boss.animator.ResetTrigger("OrbitAttack");
                SetEllipseAxis();
                spikedBall.AttackRotateAroud(spikedBall.attackEntryCurve, t0, entryTime, spikedBall.iterations, spikedBall.revolutionPerSeconds, ellipseHeight, ellipseWidth);
                //spikedBall.currentRadius = (boss.Player.transform.position - spikedBall.attackOrbitCenter.transform.position).magnitude;
                boss.animator.SetTrigger(attackStart);

                spikedBall.OrbitAttackOrientation(boss.Player.transform.position, spikedBall.TargetYOffest, spikedBall.OrbitCenterRotationSpeed);
                //spikedBall.RotateAroud(spikedBall.attackActiveCurve, entryTime, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds);
                spikedBall.AttackRotateAroud(spikedBall.attackActiveCurve, entryTime, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds, ellipseHeight, ellipseWidth);
            }
        }
        else if (t0 > exitTime) {
            if (timer <= endTime) {
                //spikedBall.currentRadius = DOVirtual.EasedValue(distanceFromTarget, spikedBall.initialRadius, (timer - exitTime) / (endTime - exitTime), attackOrbitData.AttackEase); //Queste devono cambiare
                //spikedBall.RotateAroud(spikedBall.attackExitCurve, t0, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds);
                spikedBall.AttackRotateAroud(spikedBall.attackExitCurve, t0, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds, ellipseHeight, ellipseWidth);
            }
            else {
                timer = 0;
                //spikedBall.currentRadius = spikedBall.initialRadius;
                spikedBall.currentRadius = spikedBall.oldRadius;
                //spikedBall.RotateAroud(spikedBall.attackExitCurve, t0, endTime, spikedBall.iterations, spikedBall.revolutionPerSeconds);
                
                boss.animator.ResetTrigger(attackEnd);

                boss.animator.SetTrigger("OrbitIdle");

                spikedBall.AttackRotateAroud(spikedBall.attackExitCurve, t0, endTime, spikedBall.iterations, spikedBall.revolutionPerSeconds, ellipseHeight, ellipseWidth);
                spikedBall.doRevolution();
            }
        }
        else {
            if (timer <= exitTime) {
                //spikedBall.RotateAroud(spikedBall.attackActiveCurve, t0, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds);
                spikedBall.AttackRotateAroud(spikedBall.attackActiveCurve, t0, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds, ellipseHeight, ellipseWidth);
            }
            else {
                //spikedBall.RotateAroud(spikedBall.attackActiveCurve, t0, exitTime, spikedBall.iterations, spikedBall.revolutionPerSeconds);
                spikedBall.AttackRotateAroud(spikedBall.attackActiveCurve, t0, exitTime, spikedBall.iterations, spikedBall.revolutionPerSeconds, ellipseHeight, ellipseWidth);
                //spikedBall.currentRadius = DOVirtual.EasedValue(distanceFromTarget, spikedBall.initialRadius, (timer - exitTime) / (endTime - exitTime), attackOrbitData.AttackEase); //Queste devono cambiare
                //spikedBall.RotateAroud(spikedBall.attackExitCurve, exitTime, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds);
                boss.animator.ResetTrigger(attackStart);
                boss.animator.SetTrigger(attackEnd);
                //QUESTO IF È PER LE COMBO

                spikedBall.AttackRotateAroud(spikedBall.attackExitCurve, exitTime, timer, spikedBall.iterations, spikedBall.revolutionPerSeconds, ellipseHeight, ellipseWidth);
                //boss.ResetRotation(boss.resetKnightRotationSpeed);
            }
        }
    }

    void SetEllipseAxis()
    {
        //distanceFromTarget = (boss.Player.transform.position + spikedBall.TargetYOffest * Vector3.up - spikedBall.orbitCenter.transform.position).magnitude;

        if (spikedBall.isTargetingDistance)
        {
            distanceFromTarget = attackOrbitData.fixTargetDistance;
            Debug.DrawLine(spikedBall.orbitCenter.transform.position, attackOrbitData.fixTargetDistance * spikedBall.orbitCenter.transform.forward, Color.blue);
        }
        else
        {
            distanceFromTarget = Mathf.Clamp((boss.Player.transform.position + spikedBall.TargetYOffest * Vector3.up - spikedBall.orbitCenter.transform.position).magnitude, spikedBall.minAttackRange, spikedBall.maxAttackRange); //il set andrebbe fatta sul project on play
            Debug.DrawLine(spikedBall.orbitCenter.transform.position, boss.Player.transform.position + spikedBall.TargetYOffest * Vector3.up, Color.blue);
        }

        ellipseHeight = distanceFromTarget + spikedBall.oldRadius;
        //spikedBall.attackOrbitCenter.transform.position = spikedBall.orbitCenter.transform.position - spikedBall.oldRadius * spikedBall.orbitCenter.transform.forward + ellipseHeight / 2 * spikedBall.orbitCenter.transform.forward;
        spikedBall.attackOrbitCenter.transform.position = spikedBall.orbitCenter.transform.position + (ellipseHeight / 2 - spikedBall.oldRadius) * spikedBall.orbitCenter.transform.forward;
        ellipseWidth = 2 * ellipseHeight * Mathf.Sin((attackOrbitData.AttackAngle / 2) * Mathf.Deg2Rad);
    }

}

