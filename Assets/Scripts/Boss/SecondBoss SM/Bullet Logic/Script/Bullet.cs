﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //Public
    public BulletData BulletData;
    public SecondBossController boss;

    //Private
    Vector3 AccelerationVector;
    Vector3 move;
    Vector3 VelocityVector;
    Vector3 targetDir;


    private void OnEnable() {
        boss = FindObjectOfType<SecondBossController>(); //REFACTORARE

        BulletData = boss.currentBulletData;
        targetDir = BulletData.Target.instance.transform.position - transform.position;
    }


    void Update()
    {
        Movement(BulletData);
    }

    void Movement(BulletData _data) {

        if (_data.bulletBeheviours == BulletBehaviours.Charge) {
            //Vector3 targetDir = _data.Target.instance.transform.position - transform.position; 
            Vector3 accelerationVectorTemp = targetDir;
            //accelerationVectorTemp.y = 0;
            AccelerationVector = targetDir.normalized * (_data.MaxSpeed / _data.TimeAcceleration);
            move = Vector3.ClampMagnitude((VelocityVector * Time.deltaTime + 0.5f * AccelerationVector * Mathf.Pow(Time.deltaTime, 2)), _data.MaxSpeed * Time.deltaTime);
            VelocityVector = Vector3.ClampMagnitude((VelocityVector + AccelerationVector * Time.deltaTime), _data.MaxSpeed);
            transform.position += move;
        }

        else if (_data.bulletBeheviours == BulletBehaviours.Chase) {
            Vector3 currentTargetDir = _data.Target.instance.transform.position - transform.position; 
            Vector3 accelerationVectorTemp = currentTargetDir;
            //accelerationVectorTemp.y = 0;
            AccelerationVector = currentTargetDir.normalized * (_data.MaxSpeed / _data.TimeAcceleration);
            move = Vector3.ClampMagnitude((VelocityVector * Time.deltaTime + 0.5f * AccelerationVector * Mathf.Pow(Time.deltaTime, 2)), _data.MaxSpeed * Time.deltaTime);
            VelocityVector = Vector3.ClampMagnitude((VelocityVector + AccelerationVector * Time.deltaTime), _data.MaxSpeed);
            transform.position += move;
        }


    }


    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<PlayerController>()) {
            boss.DestroyAllBullets();
        }

    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.transform.GetComponent<PlayerController>()) {
            boss.DestroyAllBullets();
        }
    }


}
