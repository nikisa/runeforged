﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEmitter : MonoBehaviour
{
    public BulletEmitterData bulletEmitter;

    private void Awake() {
        bulletEmitter.instance = this;
    }
}
