﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SecondBossController : BossControllerCustom
{
    //Inspector
    public GameObject ArenaCenter;
    [Range(0, 1)]
    public float resistanceToRotation;
    public SpikedBallManager SpikedBall;
    [Header("Mount")]
    public GameObject MountGraphics;
    public GameObject KnightGraphics;
    public float MountRotationSpeed;
    public HookPointBase[] vulnerablePoints;
    public ATATController atatController;
    [Header("BodyRotation")]
    [Range(0, 90)]
    public float maxTurnAngle;
    public float TweeningRotationTime;
    public Ease TweeningRotationEase;
    public float preFallingROtationSpeed;
    //public float RotationSpeed;
    public float resetKnightRotationSpeed;
    [Header("Attack Rotation")]
    [Range(0, 90)]
    public float maxAttackAngle;
    [Header("[ABSTRACT] Materials")]
    public Material baseMat;
    public Material antMat;
    public Material preFallMat;
    public Material chokedMat;
    public Material fallingMat;
    public Material recMat;



    #region usable inspector variables
    //[Header("Managers")]
    //public BossOrbitManager bossOrbitManager;
    //[Header("VFX")]
    //PlayOnAwake must be toggled on every particle systems
    //PlayOnAwake must be toggled on every particle systems
    //[Header("Boss Knight")]
    //[Header("Boss Mount")]
    //[Tooltip("The AngularVelocity that makes the boss rotate (grades per second)")]
    //public float tweeningRotationTime;
    //public Ease rotationEase;
    #endregion

    //Public
    [HideInInspector]
    public ControllerColliderHit hit;
    [HideInInspector]
    public float actualMaxSpeed;
    //[HideInInspector]
    //public bool isHookable;
    [HideInInspector]
    public bool hasAwaken = false;
    [HideInInspector]
    public int loops;
    [HideInInspector]
    public bool IsPrevStateReinitialize;
    [HideInInspector]
    public float distance_BossToPlayer;
    [HideInInspector]
    public float distance_BossToArenaCenter;
    [HideInInspector]
    public float distance_PlayerToArenaCenter;
    [HideInInspector]
    public float bossToPlayerAngle;
    [HideInInspector]
    public float bossToCenterArenaAngle;
    [HideInInspector]
    public List<GameObject> AllCurrentBullets;
    [HideInInspector]
    public BulletData currentBulletData;

    //[HideInInspector]
    //public bool isHooked;
    //public bool isPulled;
    

    //Private
    private GrappleManager grappleManager;
    private float lookAtAngle;
    private float fallenPlayerDistanceFromCenter = 30;


    private string _distance_BossToPlayer_Anim = "Distance_Boss-Player";
    private string _distance_BossToArenaCenter_Anim = "Distance_Boss-ArenaCenter";
    private string _distance_PlayerToArenaCenter_Anim = "Distance_Player-ArenaCenter";
    private string _angle_BossToPlayer_Anim = "Angle_Boss-Player";
    private string _angle_BossToArenaCenter_Anim = "Angle_Boss-ArenaCenter";

    protected override void Start() {
        base.Start();

        grappleManager = FindObjectOfType<GrappleManager>().GetComponent<GrappleManager>();

        foreach (var item in animator.GetBehaviours<SecondBossState>()) {
            item.SetContext(this, animator , Graphics.BossGraphicAnimator/*, bossOrbitManager ,*/ /*Graphics.BossGraphicAnimator*/);
        }
    }

    protected override void Update() {
       

        Debug.DrawRay(transform.position, AccelerationVector.normalized * 3, Color.red, .03f);
        Debug.DrawRay(transform.position, VelocityVector, Color.blue, .03f);

        //RotateOnTarget(); //DA APPLICARE SOLO ALLA TESTA
        UpdateMovementAngle();

        SetPlayerDistance();
        SetCenterArenaDistance();
        PlayerToCenterArenaDistance();
        BossToPlayerAngle();

        BossToCenterAngle();
        //PlayerFall();
        //animator.SetBool("isHooked", isHooked);

    }

    //private void OnControllerColliderHit(ControllerColliderHit _hit) {

    //    hit = _hit;
    //    if ((_hit.collider.GetComponent<MovementBase>() || _hit.collider.GetComponent<PlayerView>()) && !_hit.collider.GetComponent<BossController>()) {
    //        Player.GetDamage();
    //    }
    //}


    public void RotateOnTarget() {
        Vector3 playerDirection = Vector3.ProjectOnPlane(Player.transform.position - MountGraphics.transform.position, Vector3.up);
        lookAtAngle = Vector3.SignedAngle(MountGraphics.transform.forward, playerDirection, Vector3.up);
        KnightGraphics.transform.DORotate(new Vector3(0, (MountGraphics.transform.eulerAngles.y + Mathf.Clamp(lookAtAngle, -maxTurnAngle, maxTurnAngle)), 0), TweeningRotationTime).SetEase(TweeningRotationEase);
    }

    public void RotateOnVelocityVector()
    {
        Vector3 posDestination = VelocityVector.normalized;
        //posDestination = Vector3.ProjectOnPlane(posDestination, Vector3.up);
        Quaternion rotationToTarget = Quaternion.LookRotation(posDestination);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotationToTarget, RotationSpeed * Time.deltaTime);
        MountGraphics.transform.rotation = Quaternion.RotateTowards(MountGraphics.transform.rotation, rotationToTarget, RotationSpeed * Time.deltaTime);
    }

    public void ResetRotation(float _rotationSpeed) {
        Vector3 _target = MountGraphics.transform.forward;
        Quaternion rotationToTarget = Quaternion.LookRotation(_target);
        KnightGraphics.transform.rotation = Quaternion.RotateTowards(KnightGraphics.transform.rotation, rotationToTarget, _rotationSpeed * Time.deltaTime);
    }


    #region Bullets
    public void FireBullet(int _graphics , Transform _emitterPos) {
        GameObject bullet = null;
        switch (_graphics) {
            case 0:
                bullet = GameObject.Instantiate(Resources.Load("Bullet1") as GameObject, _emitterPos);
                AllCurrentBullets.Add(bullet);
                break;
            case 1:
                bullet = GameObject.Instantiate(Resources.Load("Bullet2") as GameObject, _emitterPos);
                AllCurrentBullets.Add(bullet);
                break;
            default:
                break;
        }
    }

    public void DestroyAllBullets() {
        foreach (GameObject bullet in AllCurrentBullets) {
            Destroy(bullet);
        }

        AllCurrentBullets.Clear();
    }
    #endregion

    #region Setters
    void SetPlayerDistance() {

        distance_BossToPlayer = Vector3.Distance(MountGraphics.transform.position , Player.transform.position);
        animator.SetFloat(_distance_BossToPlayer_Anim , distance_BossToPlayer);
    }

    void SetCenterArenaDistance() {
        distance_BossToArenaCenter = Vector3.Distance(MountGraphics.transform.position, ArenaCenter.transform.position);
        animator.SetFloat(_distance_BossToArenaCenter_Anim, distance_BossToArenaCenter);
    }

    void PlayerToCenterArenaDistance() {
        distance_PlayerToArenaCenter = Vector3.Distance(Player.transform.position, ArenaCenter.transform.position);
        animator.SetFloat(_distance_PlayerToArenaCenter_Anim , distance_PlayerToArenaCenter);
    }

    void BossToPlayerAngle()
    {
        Vector3 distance_BossToPlayer = Vector3.ProjectOnPlane(Player.transform.position - MountGraphics.transform.position , Vector3.up);
        Debug.DrawRay(MountGraphics.transform.position, distance_BossToPlayer, Color.white, 0.02f);
        bossToPlayerAngle = Vector3.Angle(Vector3.ProjectOnPlane(MountGraphics.transform.forward , Vector3.up), distance_BossToPlayer);
        animator.SetFloat(_angle_BossToPlayer_Anim, bossToPlayerAngle);
    }

    
    #endregion

    void BossToCenterAngle()
    {
        Vector3 distance_BossToCenter = Vector3.ProjectOnPlane(ArenaCenter.transform.position - MountGraphics.transform.position, Vector3.up);
        bossToCenterArenaAngle = Vector3.Angle(Vector3.ProjectOnPlane(MountGraphics.transform.forward, Vector3.up), distance_BossToCenter);
        animator.SetFloat(_angle_BossToArenaCenter_Anim, bossToCenterArenaAngle);

    }

    void PlayerFall() {
        if (!Player.isBoosting && distance_PlayerToArenaCenter > fallenPlayerDistanceFromCenter) {
            if (grappleManager.hook.isHooked) {
                grappleManager.Unhook();
            }
            Player.PlayerDeath();
        }
    }

    public void BossIsPulled()
    {
        animator.SetBool("isPulled", true);
    }
}