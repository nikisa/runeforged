﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(fileName = "RevolutionOrbitData", menuName = "BossData/RevolutionOrbitData")]
public class RevolutionOrbitData : ScriptableObject
{
    //Inspector
    public float initialRadius;
    public float StartingAngle;
    public float revolutionPerSeconds;
    public float angularMinVelocity;
}
