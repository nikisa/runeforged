﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(fileName = "AttackOrbitData", menuName = "BossData/AttackOrbitData")]
public class AttackOrbitData : ScriptableObject
{
    //Inspector
    //public float revolutionPerSeconds;
    public float minAttackRange;
    public float maxAttackRange;
    public float AttackVelocity;
    [Range(0, 180)]
    public float AttackAngle;
    //public Ease AttackEase;

    [Header("If Targeting Distance")]
    public float fixTargetDistance;

}
