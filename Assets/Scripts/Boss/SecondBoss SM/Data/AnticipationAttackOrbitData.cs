﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(fileName = "AnticipationAttackOrbitData", menuName = "BossData/AnticipationAttackOrbitData")]
public class AnticipationAttackOrbitData : ScriptableObject
{
    //Inspector
    [Header("Radius")]
    public float finalRadius;
    public Ease radiusEase;
    [Header("Revolution")]
    public float AnticipationTime;
    //public float angularMinVelocity;
    public int AnticipationRevolutions = 1;
    public bool isVertical;

    [Header("OrbitCenter")]
    public float OrbitCenterRotationSpeed;
    public float TargetYOffest;
    public bool isTargetingDistance;
}
