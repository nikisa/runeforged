﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class FirstBossRecoveryState : FirstBossState
{
    //Inspector
    public RecoveryData recoveryData;
    public DecelerationData decelerationData;

    public override void Enter()
    {
        base.Enter();
        graphicsAnimator.SetTrigger(G_RECOVERY);
        RecoveryInfoEnter();
        boss.isHookable = true;
    }

    public override void Tick()
    {
        base.Tick();
        SetCycleTimer();
    }

    public override void Exit()
    {
        boss.isHookable = false;
        animator.SetBool("RecoveryOrbit", false);
        CheckVulnerability();
        boss.IsPrevStateReinitialize = false;

    }

    public void RecoveryInfoEnter() {
        OrbitTag(recoveryData);
    }
   

}
