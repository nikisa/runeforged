﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirtsBossDecelerateTowardsState : FirstBossState
{
    //Inspector
    public DecelerationData decelerationData;

    //Private
    float timer;
    float finalDeltaTime;
    int iterations;
    Vector3 targetDir;


    public override void Enter()
    {
        iterations = 1;
        base.Enter();
        graphicsAnimator.SetTrigger(G_IDLE);
        animator.SetInteger("Layer", 0);
        timer = 0;
        boss.DecelerationModule = decelerationData.Deceleration;
    }

    public override void Tick()
    {
        timer += Time.deltaTime;
        base.Tick();
        UpdateTargetDir();
        //CollisionTick();
        setChaseRadius();
        Deceleration();
        SetCycleTimer();
        SetSpeed();
    }

 

    public override void Exit()
    {
        boss.IsPrevStateReinitialize = false;
        CheckVulnerability();

        
    }

    //Set speed parameter in the animator
    public void SetSpeed() {
        animator.SetFloat("Speed", boss.VelocityVector.magnitude);
    }

    public void Deceleration()
    {
        boss.DecelerateTowards(targetDir , decelerationData.Deceleration);
    }

    public void setChaseRadius()
    {
        float distance = (boss.Target.transform.position - boss.transform.position).magnitude;
        animator.SetFloat("ChaseRadius", distance);
    }

    

    void UpdateTargetDir() {
        targetDir = (decelerationData.Target.instance.transform.position - boss.transform.position).normalized;
    }

}
