﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FirstBossOrbitSetupState : FirstBossState
{
    //Inspector
    public List<MaskBehaviourData> maskBehaviourDataList;

    //Private
    int orbitCount = 0;
    int countInitial = 0;
    int index = 0;
    float positionPointTime = 0.01f;
    float orientation;
    float maxMaskCurrentRadius;
    float currentRadius;
    float changeMaterialTimePercentage;


    public override void Enter()
    {

        bossOrbitManager.MasksBehaviourList = maskBehaviourDataList;
        orientation = 360 + bossOrbitManager.SetupAngle;
        bossOrbitManager.SetupMask(maskBehaviourDataList);
        FillPointsPosition();
        SetupPositionPoints(orientation);
        maxMaskCurrentRadius = 0;
        currentRadius = 0;
    }

    public override void Tick() {
        maxMaskCurrentRadius = bossOrbitManager.maxMaskCurrentRadius();
        UpdateCharacterControllerRadius(maxMaskCurrentRadius);
        
    }

    public override void Exit() {

    }

    void UpdateCharacterControllerRadius(float _maxMaskCurrentRadius) {
        currentRadius += Time.deltaTime * 10; //Moltiplico per n dato che radius riparte da 0 e c'è il rischio che durante un MoveTo il characterController non sia ancora arrivato al radius corretto
        currentRadius = Mathf.Clamp(currentRadius, 0, _maxMaskCurrentRadius);
        //boss.CharacterController.radius = currentRadius;
    }

    public void FillPointsPosition() {
        GameObject InitialPosition;
        GameObject EndPosition;
        for (int i = 0; i < bossOrbitManager.MasksList.Count; i++) {
            InitialPosition = GameObject.Instantiate(Resources.Load("PointPosition") as GameObject , bossOrbitManager.transform);
            EndPosition = GameObject.Instantiate(Resources.Load("PointPosition") as GameObject , bossOrbitManager.transform);
            bossOrbitManager.InitialPoints.Add(InitialPosition);
            bossOrbitManager.EndPoints.Add(EndPosition);
        }
        for (int i = 0; i < bossOrbitManager.MasksList.Count; i++) {
            bossOrbitManager.InitialPoints[i].transform.position = bossOrbitManager.transform.position;
            bossOrbitManager.EndPoints[i].transform.position = bossOrbitManager.transform.position;
        }
    }

    //public void SetCenterPoint() {
    //    for (int i = 0; i < OrbitManagerList.Count; i++) {
    //        OrbitManagerList[i].CenterRotation = GameObject.Instantiate(Resources.Load("CenterPoint") as GameObject, bossOrbitManager.transform).GetComponent<HookPointController>();
    //        OrbitManagerList[i].CenterRotation.transform.SetParent(boss.transform);

    //        //if (OrbitManagerList[i].CenterRotation.transform.childCount == 0) {
    //        //    Destroy(OrbitManagerList[i].CenterRotation);
    //        //}
    //    }
    //}

    //public void SetupPositionPoints() {
    //    for (int i = 0; i < OrbitManagerList.Count; i++) {
    //        for (int y = 0; y < OrbitManagerList[i].orbitData.Count; y++) {
    //            bossOrbitManager.SetObjectsPosition(OrbitManagerList[i].orbitData[y].SetupRadius, OrbitManagerList[i].orbitData[y].FinalRadius, index, positionPointTime, orientation, OrbitManagerList[i].orbitData[y].TravelTime, OrbitManagerList[i].orbitData[y].HasDeltaRadius, OrbitManagerList[i].orbitData[y].isSetup);
    //            index++;
    //            orientation -= 360 / bossOrbitManager.EndPoints.Count;
    //        }
    //    }
    //    index = 0;
    //}

    public void SetupPositionPoints(float _orientation) {
        for (int i = 0; i < maskBehaviourDataList.Count; i++) {
            for (int j = 0; j < maskBehaviourDataList[i].MaskTargets.Count; j++) {
                bossOrbitManager.SetObjectsPosition(maskBehaviourDataList[i].SetupRadius, maskBehaviourDataList[i].FinalRadius, j, positionPointTime, orientation, maskBehaviourDataList[i].TravelTime, maskBehaviourDataList[i].isSetup);
                orientation -= _orientation / bossOrbitManager.MasksList.Count;
            }
            

        }
    }
}
