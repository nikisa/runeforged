﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstBossVulnerableState : FirstBossState {

    //Private
    GameObject BossCore;
    FirstBossMask BossHookPoint;

    public override void Enter() {
        animator.SetInteger(MASKS_COUNT, 99);
        boss.PlayDeathFX();
        boss.Graphics.BossGraphicAnimator.SetTrigger(G_DEFENSLESS);
        boss.Player.readyToWin = true;
        BossCore = GameObject.FindGameObjectWithTag("BossCore");
        BossCore.transform.GetChild(0).gameObject.SetActive(true); //Set active the collider to permit the Victory statement
        boss.CrystalVFXActivation();
        boss.Graphics.GetComponent<BoxCollider>().gameObject.SetActive(true);
        //BossHookPoint = BossCore.transform.GetChild(0).gameObject.GetComponent<FirstBossMask>();
    }

    public override void Tick() {
        //if (BossHookPoint.isHooked) {
        //    PlayerController.VictoryEvent();
        //}
    }

    public override void Exit() {
        
    }


}
