﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using FMODUnity;

public class FirstBossController : BossController
{
    //Inspector
    [Header("Managers")]
    public BossOrbitManager bossOrbitManager;
    [Header("VFX")]
    public GameObject ExposedCrystal;//PlayOnAwake must be toggled on every particle systems
    public GameObject BounceHitVFX;//PlayOnAwake must be toggled on every particle systems
    [Header("Boss Core")]
    [Tooltip("The AngularVelocity that makes the boss rotate (grades per second)")]
    public float tweeningRotationTime;
    public Ease rotationEase;
    [Header("Mask Materials")]
    public Material[] maskMaterialsStatus;
    public Material maskCrystalMaterialStatus;
    [Range(0,100)]
    public float actualMaskMaterialIntensity;
    [Range(0.6f, 100)]
    public float actualMaskCrystalMaterialIntensity;
    [Header("SFX")]
    [SerializeField]
    [EventRef]
    private string musicFX;
    [SerializeField]
    [EventRef]
    private string idleFX;
    [SerializeField]
    [EventRef]
    private string wallHitFX;
    [SerializeField]
    [EventRef]
    private string playerHitFX;
    [SerializeField]
    [EventRef]
    private string wakeupFX;
    [SerializeField]
    [EventRef]
    private string deathFX;
    [Header("Extra")]
    public float ActiveMaskCollisionTime;

    //Public
    [HideInInspector]
    public bool isBouncing;
    [HideInInspector]
    public float timerMaskCollision;
    [HideInInspector]
    public float vectorAngle;
    [HideInInspector]
    public FirstBossMask firstBossMask;
    [HideInInspector]
    public ControllerColliderHit hit;
    [HideInInspector]
    public float actualMaxSpeed;
    [HideInInspector]
    public bool hasAwaken = false;
    [HideInInspector]
    public bool isIdleSoundStarted = false;
    [HideInInspector]
    public AnimationCurve xScale;
    [HideInInspector]
    public AnimationCurve zScale;
    [HideInInspector]
    public int loops;
    [HideInInspector]
    public bool IsPrevStateReinitialize;


    //Private
    private GrappleManager grappleManager;
    private float height;
    private Vector3 hitNormal;
    


    protected override void Start() {
        base.Start();
        isIdleSoundStarted = false;
        grappleManager = FindObjectOfType<GrappleManager>().GetComponent<GrappleManager>();
        height = transform.position.y;

        foreach (var item in animator.GetBehaviours<FirstBossState>()) {
            item.SetContext(this, firstBossMask , animator , bossOrbitManager , Graphics.BossGraphicAnimator);
        }

        #region SFX
        //var idleSoundInstance = RuntimeManager.CreateInstance(idleFX);
        //idleSoundInstance.start();
        //var wallHitSoundInstance = RuntimeManager.CreateInstance(wallHitFX);
        //wallHitSoundInstance.start();
        //var playerHitSoundInstance = RuntimeManager.CreateInstance(playerHitFX);
        //wallHitSoundInstance.start();

        #endregion

        foreach (Material maskMaterial in maskMaterialsStatus) {
            maskMaterial.SetFloat("Vector1_5031454D", actualMaskMaterialIntensity);
        }

        maskCrystalMaterialStatus.SetFloat("Vector1_B11A500B", actualMaskCrystalMaterialIntensity);

    }

    protected override void Update() {
       
        Debug.DrawRay(transform.position, AccelerationVector.normalized * 3, Color.red, .03f);
        Debug.DrawRay(transform.position, VelocityVector, Color.blue, .03f);
        RotateOnTarget();
        UpdateMovementAngle();

        if (grappleManager.hook.isHooked && grappleManager.hookPoint.GetComponent<FirstBossMask>())
        {
           // animator.SetBool("isHooked", true);
        }
        //LayerController();
        transform.position = new Vector3(transform.position.x, height, transform.position.z);


        if (isIdleSoundStarted)
        {
            PlayIdleFX();
            PlayMusicFX();
            isIdleSoundStarted = false;
        }

        if (/*VelocityVector.magnitude > 10 &&*/ !isHookable && grappleManager.hook.isHooked && grappleManager.hookPoint.GetComponent<FirstBossMask>()) { //DA ELIMINARE?
            grappleManager.hook.isHooked = false;
            grappleManager.hook.transform.parent = grappleManager.GrappleGun.transform;
            grappleManager.hookPoint.isHooked = false;
            //animator.SetBool("isHooked", false);
            grappleManager.animator.SetTrigger("Rewind");
        }


        animator.SetBool("PlayerImmortal", Player.IsImmortal);

        //MANCA TIMER PER METTERE A FALSE ISBOUNCING
        //if (isBouncing) {
        //    SquashAndStratch(xScale , zScale , SsTimer);
        //}

    }

    public void PlayMusicFX() {
        RuntimeManager.PlayOneShot(musicFX);
    }

    public void PlayIdleFX() {
        RuntimeManager.PlayOneShot(idleFX);
    }

    public void PlayWallHitFX() {
        RuntimeManager.PlayOneShot(wallHitFX);
    }
    public void PlayPlayerHitFX() {
        RuntimeManager.PlayOneShot(playerHitFX);
    }
    public void PlayWakeupFX() {
        RuntimeManager.PlayOneShot(wakeupFX);
    }
    public void PlayDeathFX() {
        RuntimeManager.PlayOneShot(deathFX);
    }


    private void OnControllerColliderHit(ControllerColliderHit _hit) {

        hit = _hit;

        if ((_hit.collider.GetComponent<MovementBase>() || (_hit.collider.GetComponent<PlayerView>()) || _hit.collider.GetComponent<PlayerController>())  && !_hit.collider.GetComponent<BossController>()) {
            animator.SetInteger("Layer", 11);
            Debug.Log("Player hit by Boss (NO DAMAGE)");
            Player.GetDamage();
            bossOrbitManager.collidedObj = CollidedObj.Player;
            PlayPlayerHitFX();
        }

        if (_hit.collider.tag == "Walls") {
            animator.SetInteger("Layer", 10);
            bossOrbitManager.collidedObj = CollidedObj.Wall;
            hitNormal = hit.normal;
            PlayWallHitFX();

            BounceHitVFX.SetActive(false);
            BounceHitVFX.transform.position = _hit.point;
            BounceHitVFX.SetActive(true);
        }

        if (_hit.collider.tag == "SlowWalls") {
            animator.SetInteger("Layer", 17);
            bossOrbitManager.collidedObj = CollidedObj.SlowWall;
            hitNormal = hit.normal;


            BounceHitVFX.SetActive(false);
            BounceHitVFX.transform.position = _hit.point;
            BounceHitVFX.SetActive(true);
        }
    }



    //public void LayerController() {

    //    //TOGLIERE L'IF SOTTRAENDO 10 DA layerHitObject
    //    if (layerHitObject == 10) {
    //        bossOrbitManager.collidedObj = CollidedObj.Wall;
    //    }else if (layerHitObject == 11) {
    //        bossOrbitManager.collidedObj = CollidedObj.Player;
    //    }else if (layerHitObject == 17) {
    //        bossOrbitManager.collidedObj = CollidedObj.SlowWall;
    //    }

    //    animator.SetInteger("Layer" , layerHitObject);

    //}

    //Bounce del Boss (no maschere)
    //private void OnControllerColliderHit(ControllerColliderHit _hit) {

    //    hit = _hit;

    //    if ((_hit.collider.GetComponent<MovementBase>() || _hit.collider.GetComponent<PlayerView>()) && !_hit.collider.GetComponent<BossController>()) {
    //        animator.SetInteger("Layer", 11);
    //        Debug.Log("Player hit by Boss (NO DAMAGE)");
    //        //Player.GetDamage();
    //        bossOrbitManager.collidedObj = CollidedObj.Player;
    //    }

    //    if (_hit.collider.tag == "Walls") {
    //        animator.SetInteger("Layer", 10);
    //        bossOrbitManager.collidedObj = CollidedObj.Wall;
    //        hitNormal = _hit.normal;


    //        BounceHitVFX.SetActive(false);
    //        BounceHitVFX.transform.position = _hit.point;
    //        BounceHitVFX.SetActive(true);
    //    }
    //}


    //public void OldRotateOnTarget() {
    //    if (hasAwaken) {
    //        if (bossOrbitManager.MasksList[0].VelocityVector != Vector3.zero)
    //            Graphics.transform.LookAt(Player.transform.position);
    //        //else 
    //            //transform.parent.LookAt(-Player.transform.position);

    //    }
    //}


    public void RotateOnTarget() {
        if (hasAwaken) {
            //float angle = Vector3.SignedAngle(Graphics.transform.forward , AccelerationVector , Vector3.up);
            //Debug.Log("Graphics Angle: " + angle);
            //float actualAngularVelocity = angle / angularVelocity;
            Graphics.transform.DOLookAt(transform.position + AccelerationVector , tweeningRotationTime).SetEase(rotationEase);
            //Graphics.transform.DOLocalRotateQuaternion();
        }
    }

    public void SquashAndStratch(AnimationCurve _xScale, AnimationCurve _zScale, float _ssTimer) {
        transform.localScale = new Vector3(_xScale.Evaluate(_ssTimer), 1, _zScale.Evaluate(_ssTimer));
    }


    internal void CrystalVFXActivation() {
        ExposedCrystal.SetActive(true);
    }

    

}