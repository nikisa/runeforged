﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstBossPreFightState : FirstBossState
{

    //Private
    int HookPointsCount;

    public override void Enter() {
        base.Enter();
        HookPointsCount = bossOrbitManager.MasksList.Count;
        animator.SetInteger(MASKS_COUNT , HookPointsCount);
        boss.isHookable = true;
    }

    public override void Tick() {

        //When the Boss loses a Mask then the current state ends
        if (bossOrbitManager.MasksList.Count != HookPointsCount) {

            animator.SetTrigger(END_STATE_TRIGGER);
        }
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            animator.SetTrigger(END_STATE_TRIGGER);
        }
        SetCycleTimer();
    }

    public override void Exit() {
        base.Exit();
        boss.IsPrevStateReinitialize = false;
        boss.PlayWakeupFX();
        graphicsAnimator.SetTrigger(G_AWAKE);
        boss.isHookable = false;
        boss.hasAwaken = true;
        boss.isIdleSoundStarted = true;
    }

}
