﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FirstBossOrbitDecelerationState : FirstBossState
{
    //Inspector
    public List<MaskBehaviourData> maskBehaviourDataList;

    //Private
    float maxMaskCurrentRadius;
    float previousRadius;
    List<float> currentRadiuses = new List<float>();
    float timePercentage;
    float changeMaterialTimePercentage;
    float changeMaskCrystalMaterialTimePercentage;
    float currentCharacterControllerRadius;
    int iterations;
    float finalDeltaTime;
    float speed;
    float distance;
    private float timer;

    //Internal MaskBehaviourValues
    float angularVelocity;

    #region CurveVariables
    AnimationCurve dashDecelCurve;
    AnimationCurve dashCurve;
    int IntegralIterations;

    #endregion

    public override void Enter() {


        bossOrbitManager.MasksBehaviourList = maskBehaviourDataList;
        setEveryOrbitDecelerationCurve(maskBehaviourDataList);
        iterations = 1;
        timer = 0;

        

        SetCurrentRadiuses();
    }

    public override void Tick() {
        timer += Time.deltaTime;
        
        
        for (int i = 0; i < maskBehaviourDataList.Count; i++) { //Every BehaviourData

            

            timePercentage = timer / maskBehaviourDataList[i].TravelTime;
            timePercentage = Mathf.Clamp01(timePercentage);


            changeMaterialTimePercentage = timer / maskBehaviourDataList[i].maskMaterialChangeDuration;
            changeMaterialTimePercentage = Mathf.Clamp01(changeMaterialTimePercentage);

            changeMaskCrystalMaterialTimePercentage = timer / maskBehaviourDataList[i].maskCrystalMaterialChangeDuration;
            changeMaskCrystalMaterialTimePercentage = Mathf.Clamp01(changeMaskCrystalMaterialTimePercentage);

            foreach (Material maskMaterial in boss.maskMaterialsStatus) {
                maskMaterial.SetFloat("Vector1_5031454D", DOVirtual.EasedValue(boss.actualMaskMaterialIntensity, maskBehaviourDataList[i].maskMaterialIntensityValue, changeMaterialTimePercentage, maskBehaviourDataList[i].maskMaterialAlphaEase));
            }

            boss.maskCrystalMaterialStatus.SetFloat("Vector1_B11A500B", DOVirtual.EasedValue(boss.actualMaskCrystalMaterialIntensity, maskBehaviourDataList[i].maskCrystalMaterialIntensityValue, changeMaskCrystalMaterialTimePercentage, maskBehaviourDataList[i].maskCrystalMaterialAlphaEase));

            
            for (int j = 0; j < maskBehaviourDataList[i].MaskTargets.Count; j++) { //Every Mask in the behaviour
                if (maskBehaviourDataList[i].MaskTargets[j].instance != null) { //current instance of the mask
                    FirstBossMask mask = maskBehaviourDataList[i].MaskTargets[j].instance;
                    mask.currentRadius = DOVirtual.EasedValue(currentRadiuses[i] , maskBehaviourDataList[i].FinalRadius , timePercentage , maskBehaviourDataList[i].OrbitMoveToEase);
                    
                    if ((timer < finalDeltaTime)) {
                        mask.DecelerateAround(maskBehaviourDataList[i].OrbitDecelerationCurve, timer - Time.deltaTime , timer , iterations);
                    }

                    mask.OrbitPositioning();
                    maxMaskCurrentRadius = bossOrbitManager.maxMaskCurrentRadius();
                    UpdateCharacterControllerRadius(maxMaskCurrentRadius);

                    #region BACKUP
                    //FirstBossMask mask = maskBehaviourDataList[i].MaskTargets[j].instance;
                    //mask.DecelerateAround(maskBehaviourDataList[i].OrbitDecelerationCurve, timer);

                    //if (timer < finalDeltaTime) {
                    //    bossOrbitManager.RotationMask(maskBehaviourDataList[i]);
                    //    bossOrbitManager.SetCurrentRadius(maskBehaviourDataList[i]);
                    //    maxMaskCurrentRadius = bossOrbitManager.maxMaskCurrentRadius();
                    //    UpdateCharacterControllerRadius(maxMaskCurrentRadius);
                    //}
                    #endregion
                }
            }
        }
    }

    public override void Exit() {

    }

    void setEveryOrbitDecelerationCurve(List<MaskBehaviourData> _maskBehaviourData) {
        for (int i = 0; i < _maskBehaviourData.Count; i++) {
            for (int j = 0; j < _maskBehaviourData[i].MaskTargets.Count; j++) {
                if (_maskBehaviourData[i].MaskTargets[j].instance != null) {
                    FirstBossMask mask = _maskBehaviourData[i].MaskTargets[j].instance;
                    angularVelocity = mask.AngularVelocity;
                    setOrbitDecelerationCurve(angularVelocity, _maskBehaviourData[i]);
                    break;
                }
            }
        }
    }

    void UpdateCharacterControllerRadius(float _maxMaskCurrentRadius) {
        currentCharacterControllerRadius += Time.deltaTime * 10; //Moltiplico per n dato che radius riparte da 0 e c'è il rischio che durante un MoveTo il characterController non sia ancora arrivato al radius corretto
        currentCharacterControllerRadius = Mathf.Clamp(currentCharacterControllerRadius, 0, _maxMaskCurrentRadius);
        //boss.CharacterController.radius = currentCharacterControllerRadius;
        //boss.sphereCollider.radius = currentCharacterControllerRadius;
    }


    void setOrbitDecelerationCurve(float _angularVelocity , MaskBehaviourData _maskBehaviourDecelerationData) {

        //Internal
        float angularDeceleration = Mathf.Abs(_maskBehaviourDecelerationData.AngularMaxSpeed) / _maskBehaviourDecelerationData.AngularDecelerationTime;
        finalDeltaTime = Mathf.Abs(_angularVelocity) / angularDeceleration;

        //Resetto i KeyFrame prima di settare quelli nuovi in Enter
        _maskBehaviourDecelerationData.OrbitDecelerationCurve.keys = null;

        //Setto i KeyFrame della curva del DashDeceleration in Enter
        _maskBehaviourDecelerationData.OrbitDecelerationCurve.AddKey(0 , Mathf.Abs(_angularVelocity));
        _maskBehaviourDecelerationData.OrbitDecelerationCurve.AddKey(finalDeltaTime ,0);

    }


    //void OrbitMoveTo() {
    //    for (int i = 0; i < maskBehaviourDataList.Count; i++) {
    //        for (int j = 0; j < maskBehaviourDataList[i].MaskTargets.Count; j++) {
    //            FirstBossMask mask = maskBehaviourDataList[i].MaskTargets[j].instance;
    //            //mask.OrbitMoveToCalculation(currentRadiuses[i] , maskBehaviourDataList[i]);
    //        }
    //    }
    //}

    void SetCurrentRadiuses() {
        for (int i = 0; i < maskBehaviourDataList.Count; i++) {
            if (maskBehaviourDataList[i].isSetup) {
                currentRadiuses.Add(maskBehaviourDataList[i].SetupRadius);
            }
            else {
                for (int j = 0; j < maskBehaviourDataList[i].MaskTargets.Count; j++) {
                    if (maskBehaviourDataList[i].MaskTargets[j].instance != null) {
                        currentRadiuses.Add(maskBehaviourDataList[i].MaskTargets[j].instance.currentRadius);
                        break;
                    }
                }
            }
        }
    }

}
