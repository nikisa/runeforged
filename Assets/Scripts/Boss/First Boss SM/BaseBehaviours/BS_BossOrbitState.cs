﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class BS_BossOrbitState : FirstBossState
{
    //Inspector
    public List<MaskBehaviourData> maskBehaviourDataList;

    //Private
    List<float> currentRadiuses = new List<float>();
    float travelTimePercentage;
    float changeMaskMaterialTimePercentage;
    float changeMaskCrystalMaterialTimePercentage;
    float finalDeltaTime;
    float orbitTimeStart;
    float angleRotation;
    float angularAcceleration;
    float angularDeceleration;
    float positionPointTime;
    float orientation;
    int index;
    int iterations;
    float maxMaskCurrentRadius;
    float currentCharacterControllerRadius;

    public override void Enter() {
        base.Enter();
        bossOrbitManager.MasksBehaviourList = maskBehaviourDataList;
        //bossOrbitManager.ResetVelocity();
        //bossOrbitManager.SetupMask(MasksBehaviourList);

        //bossOrbitManager.BossFov.UpdateViewRadius();

        orientation = 360 + bossOrbitManager.SetupAngle;
        //ResetPosition();
        SetupPositionPoints(orientation);
        orbitTimeStart = Time.time;
        maxMaskCurrentRadius = 0;
        //currentCharacterControllerRadius = 0;
        DoCircularAnimation();
        SetCurrentRadiuses();

    }

    public override void Tick() {
        timer += Time.deltaTime;

        //Physics.OverlapSphere(boss.transform.position , boss.radius);

        for (int i = 0; i < maskBehaviourDataList.Count; i++) { //Every BehaviourData

            travelTimePercentage = timer / maskBehaviourDataList[i].TravelTime;
            travelTimePercentage = Mathf.Clamp01(travelTimePercentage);

            changeMaskMaterialTimePercentage = timer / maskBehaviourDataList[i].maskMaterialChangeDuration;
            changeMaskMaterialTimePercentage = Mathf.Clamp01(changeMaskMaterialTimePercentage);

            changeMaskCrystalMaterialTimePercentage = timer / maskBehaviourDataList[i].maskCrystalMaterialChangeDuration;
            changeMaskCrystalMaterialTimePercentage = Mathf.Clamp01(changeMaskCrystalMaterialTimePercentage);

            foreach (Material maskMaterial in boss.maskMaterialsStatus) {
                maskMaterial.SetFloat("Vector1_5031454D", DOVirtual.EasedValue(boss.actualMaskMaterialIntensity , maskBehaviourDataList[i].maskMaterialIntensityValue , changeMaskMaterialTimePercentage, maskBehaviourDataList[i].maskMaterialAlphaEase));
            }

            boss.maskCrystalMaterialStatus.SetFloat("Vector1_B11A500B", DOVirtual.EasedValue(boss.actualMaskCrystalMaterialIntensity, maskBehaviourDataList[i].maskCrystalMaterialIntensityValue, changeMaskCrystalMaterialTimePercentage, maskBehaviourDataList[i].maskCrystalMaterialAlphaEase));

            for (int j = 0; j < maskBehaviourDataList[i].MaskTargets.Count; j++) { //Every Mask in the behaviour
                if (maskBehaviourDataList[i].MaskTargets[j].instance != null) { //current instance of the mask
                    FirstBossMask mask = maskBehaviourDataList[i].MaskTargets[j].instance;
                    //distance = maskBehaviourDataList[i].FinalRadius - (maskBehaviourDataList[i].MaskTargets[j].instance.currentRadius);
                    //speed = distance / maskBehaviourDataList[i].TravelTime;

                    mask.currentRadius = DOVirtual.EasedValue(currentRadiuses[i], maskBehaviourDataList[i].FinalRadius, travelTimePercentage, maskBehaviourDataList[i].OrbitMoveToEase);
                    mask.RotateAroud(maskBehaviourDataList[i].AngularMaxSpeed, maskBehaviourDataList[i].AngularAccelerationTime);

                    //mask.maskGraphics.GetComponent<Renderer>().materials[0] SetColor("Vector1_5031454D" , Color.black); //DOColor(Color.black, maskBehaviourDataList[i].materialAlphaDuration);
                    

                    mask.OrbitPositioning();
                    maxMaskCurrentRadius = bossOrbitManager.maxMaskCurrentRadius();
                    UpdateCharacterControllerRadius(maxMaskCurrentRadius);

                    #region BACKUP
                    //FirstBossMask mask = maskBehaviourDataList[i].MaskTargets[j].instance;
                    //mask.DecelerateAround(maskBehaviourDataList[i].OrbitDecelerationCurve, timer);

                    //if (timer < finalDeltaTime) {
                    //    bossOrbitManager.RotationMask(maskBehaviourDataList[i]);
                    //    bossOrbitManager.SetCurrentRadius(maskBehaviourDataList[i]);
                    //    maxMaskCurrentRadius = bossOrbitManager.maxMaskCurrentRadius();
                    //    UpdateCharacterControllerRadius(maxMaskCurrentRadius);
                    //}
                    #endregion
                }
            }
        }
    }

    public override void Exit() {
        boss.actualMaskMaterialIntensity = maskBehaviourDataList[0].maskMaterialIntensityValue;
    }

    private void DoCircularAnimation() {
        if (bossOrbitManager.MasksBehaviourList[0].AngularMaxSpeed != 0) {
            if (bossOrbitManager.MasksBehaviourList[0].AngularMaxSpeed > 0) {
                graphicsAnimator.SetTrigger(G_CIRCULAR_CLOCK);
            }
            else {
                graphicsAnimator.SetTrigger(G_CIRCULAR_ANTICLOCK);
            }
        }
    }

    void UpdateCharacterControllerRadius(float _maxMaskCurrentRadius) {
        currentCharacterControllerRadius += Time.deltaTime * 10; //Moltiplico per n dato che radius riparte da 0 e c'è il rischio che durante un MoveTo il characterController non sia ancora arrivato al radius corretto
        currentCharacterControllerRadius = Mathf.Clamp(currentCharacterControllerRadius, 0, _maxMaskCurrentRadius);
        boss.CharacterController.radius = currentCharacterControllerRadius;
        //boss.sphereCollider.radius = currentCharacterControllerRadius;
    }

    //void UpdateCollisionRadius(float _maxMaskCurrentRadius) {

    //    //TODO: (Collision per il boss)
    //    // Quando allargo lo spherecast controllo se avviene una collisione
    //    // Se collision == true
    //    // Radius da raggiungere - radius durante l'impatto = X
    //    // Sposto il boss di X in direzione della normale dell'oggetto hittato

    //    boss.radius += Time.deltaTime /** 10*/; //Moltiplico per n dato che radius riparte da 0 e c'è il rischio che durante un MoveTo il characterController non sia ancora arrivato al radius corretto
    //    boss.radius = Mathf.Clamp(boss.radius, 0, _maxMaskCurrentRadius);

    //    //boss.CharacterController.radius = currentCharacterControllerRadius;
    //    //boss.sphereCollider.radius = boss.radius;

    //    #region BACKUP
    //    ////TODO: (Collision per il boss)
    //    //// Quando allargo lo spherecast controllo se avviene una collisione
    //    //// Se collision == true
    //    //// Radius da raggiungere - radius durante l'impatto = X
    //    //// Sposto il boss di X in direzione della normale dell'oggetto hittato

    //    //List<GameObject> hitObjects = new List<GameObject>();
    //    //float actualDistance;
    //    //Vector3 actualNormal = Vector3.zero;

    //    //if (hitObjects.Count < 1) {
    //    //    boss.radius += Time.deltaTime * 10; //Moltiplico per n dato che radius riparte da 0 e c'è il rischio che durante un MoveTo il characterController non sia ancora arrivato al radius corretto
    //    //    boss.radius = Mathf.Clamp(boss.radius, 0, _maxMaskCurrentRadius);
    //    //    //boss.CharacterController.radius = currentCharacterControllerRadius;
    //    //    boss.sphereCollider.radius = boss.radius;
    //    //}
    //    //else {
    //    //    //Calcola distanza tra boss e oggetto hittato e sposta il boss in direzione della normal del punto di hit
    //    //    foreach (GameObject hitObject in hitObjects) {
    //    //        actualDistance = (hitObject.transform.position - boss.transform.position).magnitude;
    //    //        actualNormal = (hitObject)
    //    //    }
    //    //}

    //    //for (int i = 0; i < hits.Length; i++) {
    //    //    if (i < 1) {
    //    //        hitObjects.Add(hits[i].gameObject);
    //    //    }
    //    //    else {
    //    //        if (hits[i].gameObject != hits[i-1].gameObject) {
    //    //            hitObjects.Add(hits[i].gameObject);
    //    //        }
    //    //    }
    //    //}

    //    #endregion

    //}

    public void SetupPositionPoints(float _orientation) {

        //int index

        for (int i = 0; i < maskBehaviourDataList.Count; i++) {
            for (int j = 0; j < maskBehaviourDataList[i].MaskTargets.Count; j++) {


                //Possiamo accedere a boss , per ogni maschera  settiamo la maschera ad un euler angle che parte dal boss.forward e gira di 360/maskcount (o per essere direttamente precisi
                // al posto di mask count usare index)

                bossOrbitManager.SetObjectsPosition(maskBehaviourDataList[i].SetupRadius, maskBehaviourDataList[i].FinalRadius, j, positionPointTime, orientation, maskBehaviourDataList[i].TravelTime, maskBehaviourDataList[i].isSetup);
                orientation -= _orientation / bossOrbitManager.MasksList.Count;

                //index++
            }
        }
    }

    public void ResetPosition() {
        for (int i = 0; i < bossOrbitManager.EndPoints.Count; i++) {
            bossOrbitManager.EndPoints[i].transform.position = boss.transform.position;
            bossOrbitManager.InitialPoints[i].transform.position = boss.transform.position;
        }
    }

    void SetCurrentRadiuses() {
        for (int i = 0; i < maskBehaviourDataList.Count; i++) {
            if (maskBehaviourDataList[i].isSetup) {
                currentRadiuses.Add(maskBehaviourDataList[i].SetupRadius);
            }
            else {
                for (int j = 0; j < maskBehaviourDataList[i].MaskTargets.Count; j++) {
                    if (maskBehaviourDataList[i].MaskTargets[j].instance != null) {
                        currentRadiuses.Add(maskBehaviourDataList[i].MaskTargets[j].instance.currentRadius);
                        break;
                    }
                }
            }
        }
    }

    #region Functions Cemetery

    //public void SetOrbitData() {
    //    for (int i = 0; i < OrbitManagerList.Count; i++) {
    //        for (int y = 0; y < OrbitManagerList[i].orbitData.Count; y++) {
    //            bossOrbitManager.FillOrbitData(OrbitManagerList[i].orbitData[y]);
    //        }
    //    }
    //}

    //public void UpdateOrbitData() {
    //    bossOrbitManager.RemoveOrbitDataList();
    //}

    //public void FillOrbitManagerDataList() {
    //    for (int i = 0; i < OrbitManagerList.Count; i++) {
    //        bossOrbitManager.OrbitManagerDataList.Add(OrbitManagerList[i]);
    //        OrbitManagerList[i].CenterRotation.MoveSpeed = bossOrbitManager.actualSpeed;
    //    }
    //}

    //public void ClearOrbitManagerDataList() {
    //    bossOrbitManager.OrbitManagerDataList.Clear();
    //}

    //public void SetUpPositionPoints() {

    //    for (int i = 0; i < bossOrbitManager.OrbitManagerDataList.Count; i++) {
    //        for (int y = 0; y < bossOrbitManager.OrbitManagerDataList[i].orbitData.Count; y++) {
    //            bossOrbitManager.SetObjectsPosition(bossOrbitManager.OrbitManagerDataList[i].orbitData[y].SetupRadius, bossOrbitManager.OrbitManagerDataList[i].orbitData[y].FinalRadius, index, positionPointTime, orientation, bossOrbitManager.OrbitManagerDataList[i].orbitData[y].TravelTime, bossOrbitManager.OrbitManagerDataList[i].orbitData[y].HasDeltaRadius, bossOrbitManager.OrbitDataList[i].isSetup);
    //            index++;
    //            orientation -= 360 / bossOrbitManager.HookPointList.Count;
    //        }
    //    }
    //    index = 0;
    //}


    #endregion

}