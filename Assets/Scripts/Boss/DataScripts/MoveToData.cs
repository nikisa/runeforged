﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "MoveToData", menuName = "BossData/MoveToData")]
public class MoveToData : BaseData
{
    [Header("MoveTo Setup")]
    public TargetType Target;
    public bool SetInitialSpeed;
    public float InitialSpeed;

    [Header ("MoveTo Data")]
    [Tooltip("in sec. How long it takes the object to reach Max Speed.")]
    public float TimeAcceleration;
    [Tooltip("Maximum reachable speed, in m/sec.")]
    public float MaxSpeed;
    [Tooltip("On State Enter: wait X seconds before accelerating")]
    float WaitOnStart;
    //public bool HasAcceleration;
    //[Tooltip("Decreasing rate of entry speed, in m/sec^2.")]
    //public float TimeDeceleration;
    //[Tooltip("Minimum reachable speed, in m/sec. Use negative value to reverse vector")]
    //public float LowSpeed;
    [Header("Zig-Zag mode")]
    public bool isZigZagging;
    public float zigZagRotationFrequency;
    public float zigZagAngleRange;

    [Header("Mover LookAt")]
    public bool lookAtForward;
    [Space]
    [Tooltip("The value to add (or subtract, if negative) to the entry velocity parameter.")]
    public float AddToVelocity;
    public float BendingSpeed;

    //periodo
    //amplitudine

    [Header("Bullet")]
    public BulletEmitterData Emitter;
    public List<BulletData> BulletData;
}
