﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "AnticipationData", menuName = "BossData/AnticipationData")]
public class AnticipationData : BaseData
{
    public bool isAnticipating;
    
    [Tooltip("Number of loops.")]
    public int Loops;
    [Tooltip("Never stops the loop.")]
    public bool InfinteLoops;

    [Header("Rotation")]
    public TargetType Target;
    public float rotationSpeed;
    [Range(0, 1)]
    public float resistanceToRotation;
    //Per rotation --> Time considerato da 0 a 1 e combinato con i gradi ????

    [Header("Bullet")]
    public BulletEmitterData Emitter;
    public List<BulletData> BulletData;

}
