﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BulletBehaviours { Chase, Charge };
public enum BulletGraphicsList { Graphic1 , Graphic2 };

[CreateAssetMenu(fileName = "BulletData", menuName = "BossData/BulletData")]


public class BulletData: BaseData
{
    public BulletGraphicsList BulletGraphics;
    public TargetType Target;
    public BulletBehaviours bulletBeheviours;
    public float TimeAcceleration;
    public float MaxSpeed;
    public List<float> BulletTriggerTimes;
}
