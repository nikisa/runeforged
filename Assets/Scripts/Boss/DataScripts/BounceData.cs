﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BounceData", menuName = "BossData/BounceData")]
public class BounceData : BaseData
{
    public float impactFreeze;
    public float shakeMagnitude;
    public float impulseDeltaTime;
    [Range(0,1)]
    public float kinetikEnergyLoss;
    [Range(0,1)]
    public float surfaceFriction;

    [Header("Camera Shake Values")]
    
    public float strength;
    public float duration;
    public float speed;

    [Range(0,1)]
    public float noisePercent;
    [Range(0, 1)]
    public float dampingPercent;
    [Range(0, 1)]
    public float rotationPercent;

    [Header("Squash & Stratch")]
    public AnimationCurve xScale;
    public AnimationCurve zScale;

}
