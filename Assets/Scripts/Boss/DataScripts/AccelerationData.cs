﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "AccelerationData", menuName = "BossData/AccelerationData")]
public class AccelerationData : BaseData
{
    public TargetType Target;
    public AnimationCurve MovementAccelerationCurve;
    [Tooltip("Increasing rate of entry speed, in m/sec^2.")]
    public float Acceleration;
    [Tooltip("Maximum reachable speed, in m/sec. Use negative value to reverse vector")]
    public float HighSpeed;
}
