﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ChaseData", menuName = "BossData/ChaseData")]
public class ChaseData : BaseData
{
    public bool isAnticipating;

    [Header("Chase Setup")]
    public TargetType Target;    
    public bool hasInitialSpeed;
    public float initialSpeed;

    [Header("Chase Data")]
    [Tooltip("in sec. How long it takes the object to reach Max Speed.")]
    public float TimeAcceleration;
    [Tooltip("Maximum reachable speed, in m/sec.")]
    public float MaxSpeed;
    [HideInInspector]
    public float DynamicDrag;

    [Header("Chaser LookAt")]
    public bool lookAtForward;


    [Header("Bullet")]
    public BulletEmitterData Emitter;
    public List<BulletData> BulletData;
}
