﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletChase : MonoBehaviour
{

    //Inspector
    public TargetType Target;
    public float MaxSpeed;
    public float TimeAcceleration;

    //Private
    private Vector3 targetDir;
    private float accelerationModule;
    private Vector3 accelerationVector;
    private Vector3 move;
    private Vector3 velocityVector;

    private void Start() {
        accelerationModule = MaxSpeed / TimeAcceleration;
    }

    private void Update() {
        targetDir = Target.instance.transform.position - transform.position;
        MovementChase(targetDir , MaxSpeed , accelerationModule);
    }


    private void MovementChase(Vector3 _targetDir , float _maxSpeed , float _accelerationModule) {

        Vector3 accelerationVectorTemp = _targetDir;
        accelerationVectorTemp.y = 0;
        accelerationVector = accelerationVectorTemp.normalized * _accelerationModule;
        move = Vector3.ClampMagnitude((velocityVector * Time.deltaTime + 0.5f * accelerationVector * Mathf.Pow(Time.deltaTime, 2)), _maxSpeed * Time.deltaTime); //Formula completa per un buon effetto fin dal primo frame
        velocityVector = Vector3.ClampMagnitude((velocityVector + accelerationVector * Time.deltaTime), _maxSpeed);
        transform.position += move;

    }

    public void ActivateBullet() {
        transform.gameObject.SetActive(true);
    }

    public void DeactivateBullet() {
        transform.gameObject.SetActive(false);
    }



}
