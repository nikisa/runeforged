﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IState
{
    IState Setup(IContext ctx);

    void Enter();
    void Tick();
    void Exit();
}
