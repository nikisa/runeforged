﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RopeState
{

    interface IRopeState
    {
        void HandleRolledUp();
        void HandleShooted();
        void HandleHooked();
        void HandleRewind();
    }
}
