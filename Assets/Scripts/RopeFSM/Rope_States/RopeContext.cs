﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RopeContext : IContext
{
    public Action OnStateEnding; // chiamata generica per avvisare che il lavoro dello stato attuale è terminato
}
