﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RopeState;

public class RopeFSM_HookedState : RopeFSM_StateBase
{
    
    public RopeFSM_HookedState(RopeFSM _ropeFSM) : base(_ropeFSM) {

    }

    public override void Enter() {

    }

    public override void Tick() {

    }

    public override void Exit() {

    }

}
