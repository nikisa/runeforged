﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;

namespace RopeState
{
    public class RopeFSM_StateBase : FiniteBaseState, IRopeState
    {

        //Public 
        public RopeContext ctx;

        //Protected
        protected RopeFSM ropeFSM;

        public void SetUp(RopeContext _context) {
            ctx = _context;
        }

        protected RopeFSM_StateBase(RopeFSM _ropeFSM) {
            ropeFSM = _ropeFSM;
        }

        public override void Enter() {
            base.Enter();
        }

        public override void Tick() {
            base.Tick();
        }

        public override void Exit() {
            base.Exit();
        }

        public void HandleHooked() {
            throw new System.NotImplementedException();
        }

        public void HandleRewind() {
            throw new System.NotImplementedException();
        }

        public void HandleRolledUp() {
            throw new System.NotImplementedException();
        }

        public void HandleShooted() {
            throw new System.NotImplementedException();
        }
    }
}


