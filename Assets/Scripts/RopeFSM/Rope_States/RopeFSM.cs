﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;
using RopeState;
using System;

public class RopeFSM : BaseFSM
{

    RopeStates ropeStates;
    RopeContext newContext;

    public Action SetStateRolledUp;
    public Action SetStateShooted;
    public Action SetStateHooked;
    public Action SetStateRewind;

    private void OnEnable() {
        SetStateRolledUp += setCurrentStateToRolledUp;
        SetStateShooted += setCurrentStateToShooted;
        SetStateHooked += setCurrentStateToHooked;
        SetStateRewind += setCurrentStateToRewind;
    }

    private void OnDisable() {
        SetStateRolledUp -= setCurrentStateToRolledUp;
        SetStateShooted -= setCurrentStateToShooted;
        SetStateHooked -= setCurrentStateToHooked;
        SetStateRewind -= setCurrentStateToRewind;
    }

    private void Start() {
        ropeStates = new RopeStates(this);
        StartSM();
    }

    public override void StartSM() {
        // Inizializzo il contest condiviso tra tutti gli stati
        newContext = new RopeContext() {
            OnStateEnding = onStateEndingCallback,
        };
        // Creo lo stato iniziale, lo setuppo e lo assegno
        RopeFSM_StateBase newState = LoadRopeFSM();
        newState.SetUp(newContext);
        CurrentState = newState;
    }

    public RopeFSM_StateBase LoadRopeFSM() {
        return ropeStates.RolledUpState;
    }

    #region Events

    private void onStateEndingCallback() {

    }

    #endregion

    void setCurrentStateToRolledUp() {
        setCurrentState(ropeStates.RolledUpState);
    }
    void setCurrentStateToHooked() {
        setCurrentState(ropeStates.HookedState);
    }
    void setCurrentStateToShooted() {
        setCurrentState(ropeStates.ShootedState);
    }
    void setCurrentStateToRewind() {
        setCurrentState(ropeStates.RewindState);
    }

}

public struct RopeStates
{
    public RopeFSM_RolledUpState RolledUpState;
    public RopeFSM_ShootedState ShootedState;
    public RopeFSM_HookedState HookedState;
    public RopeFSM_RewindState RewindState;

    public RopeStates(RopeFSM _ropeFSM) {
        RolledUpState = new RopeFSM_RolledUpState(_ropeFSM);
        ShootedState = new RopeFSM_ShootedState(_ropeFSM);
        HookedState = new RopeFSM_HookedState(_ropeFSM);
        RewindState = new RopeFSM_RewindState(_ropeFSM);
    }
}
