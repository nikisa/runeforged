﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeManager : MonoBehaviour
{


    const float maxAngle = 10;

    IEnumerator currentShakeCoroutine;

    public void StartShake(float _angle, float _noisePercent, float _strength, float _duration, float _speed , float _dampingPercent , float _rotationPercent) {
        if (currentShakeCoroutine != null) {
            StopCoroutine(currentShakeCoroutine);
        }
        currentShakeCoroutine = Shake(_angle, _noisePercent, _strength, _duration, _speed , _dampingPercent , _rotationPercent);
        StartCoroutine(currentShakeCoroutine);
    }

    IEnumerator Shake(float _angle , float _noisePercent , float _strength , float _duration , float _speed , float _dampingPercent , float _rotationPercent) {
        float completionPercent = 0;
        float movePercent = 0;

        float angle_radians = _angle * Mathf.Deg2Rad - Mathf.PI;
        Vector3 previousWaypoint = Vector3.zero;
        Vector3 currentWaypoint = Vector3.zero;
        float moveDistance = 0;

        Quaternion targetRotation = Quaternion.identity;
        Quaternion previousRotation = Quaternion.identity;

        do {
            if (movePercent >= 1 || completionPercent == 0) {
                float dampingFactor = DampingCurve(completionPercent, _dampingPercent);
                float noiseAngle = (Random.value - .5f) * Mathf.PI;
                angle_radians += Mathf.PI + noiseAngle * _noisePercent;
                currentWaypoint = new Vector3(Mathf.Cos(angle_radians), Mathf.Sin(angle_radians)) * _strength * dampingFactor;
                previousWaypoint = transform.localPosition;
                moveDistance = Vector3.Distance(currentWaypoint, previousWaypoint);

                targetRotation = Quaternion.Euler(new Vector3(currentWaypoint.y, currentWaypoint.x).normalized * _rotationPercent * dampingFactor * maxAngle);
                previousRotation = transform.localRotation;

                movePercent = 0;
            }
            completionPercent += Time.deltaTime / _duration;
            movePercent += Time.deltaTime / moveDistance * _speed;
            transform.localPosition = Vector3.Lerp(previousWaypoint, currentWaypoint, movePercent);
            transform.localRotation = Quaternion.Slerp(previousRotation, targetRotation, movePercent);

            yield return null;

        } while (moveDistance > 0);
    }

    float DampingCurve(float _x , float _dampingPercent) {
        _x = Mathf.Clamp01(_x);
        float a = Mathf.Lerp(2, .25f, _dampingPercent);
        float b = 1 - Mathf.Pow(_x, a);
        return b * b * b;
    }

}
