﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class cameraTrigger : MonoBehaviour
{
    
    //Inspector
    public GameObject cameraDolly;
    public float dollyEaseTime;
    public Ease dollyEaseType;
    public float AngleEaseTime;
    public Ease angleEaseType;
    public float zoomEaseTime;
    public Ease zoomEaseType;
    public bool autoAngle = false;
    [SerializeField]
    private int _cameraID;
    public int CameraID { get { return _cameraID; } }
    [SerializeField]
    private Vector3 _offset;
    public Vector3 Offset { get { return _offset; } set { _offset = value; } }
    [SerializeField]
    private float _rotationX;
    public float RotationX { get { return _rotationX; } }
    [SerializeField]
    private float _rotationY;
    public float RotationY { get { return _rotationY; } }
    [SerializeField]
    private float _rotationZ;
    public float RotationZ { get { return _rotationZ; } }
    [SerializeField]
    private float _localFOV;
    public float LocalFOV { get { return _localFOV; } }
    public float nearPlane;
    public float nearPlaneEaseTime;
    public Ease nearPlaneEaseType;


    //Private
    CameraPathPosition m_cameraPathPosition;
    List<LocalCamera> m_LocalCameras;
    Camera m_mainCamera;

    
    //The Awake will add the cameras on the scene and activate the camera with ID = 0
    private void Awake() {
        m_mainCamera = Object.FindObjectOfType<Camera>().GetComponent<Camera>();
        m_cameraPathPosition = Object.FindObjectOfType<CameraPathPosition>().GetComponent<CameraPathPosition>();
        LocalCamera[] LocalCameras = FindObjectsOfType<LocalCamera>() as LocalCamera[];
        m_LocalCameras = LocalCameras.ToList();
        foreach (LocalCamera localCamera in m_LocalCameras) {
            if (localCamera.localCameraID == 0) {
                localCamera.onAir = true;
            }
            else {
                localCamera.onAir = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {
            foreach (LocalCamera localCamera in m_LocalCameras) {
                if (!localCamera.onAir && localCamera.localCameraID == CameraID) {
                    StartCoroutine(TwiningManager());
                    cameraDolly.GetComponent<cameraFollow>().offset = Offset;
                    CameraZoom();
                    SetNearPlane();
                    cameraDolly.GetComponent<cameraFollow>().transform.rotation = Quaternion.Euler(RotationX , RotationY , RotationZ);
                    m_cameraPathPosition.autoAngle = autoAngle;
                    //m_mainCamera.transform.rotation = Quaternion.Euler(RotationX, RotationY, RotationZ);
                }
            }
        }   
    }

    //Resets all the localCameras that don't need to be active at the right moment
    private void resetOtherCameras(int n) {
        foreach (LocalCamera localCamera in m_LocalCameras) {
            if (localCamera.onAir && localCamera.localCameraID != n) {
                localCamera.onAir = false;
            }
        }
    }


    //Used to create camera effects like "Dolly Zoom"
    IEnumerator TwiningManager(){
        foreach (LocalCamera localCamera in m_LocalCameras) {
            //Debug.Log("TRIGGERED");
            if (localCamera.localCameraID == CameraID) {
                resetOtherCameras(CameraID);
                m_cameraPathPosition.cameraBond = false;
                m_cameraPathPosition.easeType = dollyEaseType;
                m_cameraPathPosition.easeTime = dollyEaseTime;
                m_cameraPathPosition.angleEaseType = angleEaseType;

                localCamera.onAir = true;
                
                yield return new WaitForSeconds(Mathf.Max(Mathf.Max(dollyEaseTime,AngleEaseTime), zoomEaseTime));
                m_cameraPathPosition.cameraBond = true;
            }
            
        }
    }

    //Set the FOV with an Ease set on the inspector
    public void CameraZoom() {
        m_mainCamera.DOFieldOfView(LocalFOV, zoomEaseTime).SetEase(zoomEaseType);
        cameraDolly.GetComponent<cameraFollow>().FOV = LocalFOV;
    }

    //Set the NearPlane with an Ease set on the inspector
    public void SetNearPlane()
    {
        m_mainCamera.DONearClipPlane(nearPlane, nearPlaneEaseTime).SetEase(nearPlaneEaseType);
    }
    
}
