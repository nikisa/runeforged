﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalCamera : MonoBehaviour
{

    //Inspector
    public int localCameraID;
    public PlayerController player;
    public GameObject cameraFocus;
    public bool onAir;
    [Range(0f, 1f)]
    public float lerpValue;

    //Private
    cameraTrigger _cameraTrigger;

    private void Awake() {
        _cameraTrigger = Object.FindObjectOfType<cameraTrigger>().GetComponent<cameraTrigger>();
        player = Object.FindObjectOfType<PlayerController>().GetComponent<PlayerController>();
        cameraFocus.transform.position = player.transform.position;
    }

    //Moves the cameraFocus doing a lerp between the player and the actual localCamera positions
    private void Update() {
        if (onAir) {
            cameraFocus.transform.position = Vector3.Lerp(player.transform.position, transform.position, lerpValue);
        }   
    }
}
