﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The selected local camera follows the target set in the
/// inspector as "followAt" with an offset and a FOV both
/// set in the inspector too.
/// </summary>
public class cameraFollow : MonoBehaviour
{

    //Inpsector
    public Transform followAt;
    public Vector3 offset;
    [Range(0.1f,179)]
    public float FOV;

    private void Awake() {
        transform.position = followAt.transform.position - offset;
    }

    void Update()
    {
        transform.position = followAt.transform.position - offset;
    }
}
