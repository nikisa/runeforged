﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CameraTracker : MonoBehaviour
{
    //Inspector
    public Transform Player;
    public Transform Boss;
    public Vector3 CameraOffset;
    [Range(0, 1)]
    public float CameraWeight = 0.5f;
    [Tooltip("Time for camera repositioning")]
    public float CameraSmoothness;
    public Ease easeType;

    //Private
    private Vector3 center;


    void Update() {
        center =  Vector3.Lerp(Player.position, Boss.position, CameraWeight);
        transform.DOMove(center + CameraOffset , CameraSmoothness).SetEase(easeType);
    }

}
