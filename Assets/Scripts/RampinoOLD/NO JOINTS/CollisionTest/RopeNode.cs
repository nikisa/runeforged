﻿using UnityEngine;

public class RopeNode : MonoBehaviour {
    
    //Old
    public Vector3 PreviousPosition;

    //New
    public bool launched;
    public float mass;
    public Vector3 OldPos;
    public Vector3 SpatialVector;
    public Vector3 VelocityVector;
    public Vector3 AccelerationVector;
    public GameObject currentHitObject;
    public int collisionCount;
    public float MassPointRadius;
    public float percentDrag;
    public float percentFriction;
    public float VelocityMagnitude;

    [HideInInspector]
    public bool collisionAdded;
    public GameObject oldHitOject;


    float CosH(float _x) {
        return (Mathf.Exp(_x) + Mathf.Exp(-_x))/2;
    }

    public float Catenaria(float _x , float _a) {
        return _a * CosH(_x/_a);
    }

}


