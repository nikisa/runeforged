﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletEmitter", menuName = "Targets/BulletEmitter")]
public class BulletEmitterData : ScriptableObject
{
    internal BulletEmitter instance;

}
