﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TargetType", menuName = "Targets/Target")]
public class TargetType : ScriptableObject
{
    internal GameObject instance;

}
