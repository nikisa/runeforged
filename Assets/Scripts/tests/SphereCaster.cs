﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereCaster : MonoBehaviour
{

    public GameObject simulationSphere;

    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;

    private Vector3 origin;
    private Vector3 direction;

    [SerializeField]
    private GameObject currentHitObject;
    [SerializeField]
    private float currentHitDistance;

    private void Start() {

    }

    private void Update() {
        origin = transform.position;
        direction = transform.forward;
        RaycastHit hit;
        if (Physics.SphereCast(origin , sphereRadius , direction , out hit , maxDistance , layerMask , QueryTriggerInteraction.UseGlobal)) {
            currentHitObject = hit.transform.gameObject;
            Vector3 hitPosition = hit.point;
            currentHitDistance = hit.distance;
            DrawNormal(hit.point , hit.normal , 0.02f);

            //internal
            Vector3 currentHitVector  = currentHitDistance * direction;
            Vector3 maxVector = maxDistance * direction;
            Vector3 slopePosition = currentHitVector + Vector3.ProjectOnPlane((maxVector - currentHitVector)  , hit.normal);

            slopeSimulation(origin + slopePosition);
        }
        else {
            currentHitDistance = maxDistance;
            currentHitObject = null;
            slopeSimulation(origin + direction * maxDistance);
        }
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Debug.DrawLine(origin , origin + direction * currentHitDistance);
        Gizmos.DrawWireSphere(origin + direction * currentHitDistance , sphereRadius);
    }


    private void DrawNormal(Vector3 _start , Vector3 _dir , float _time) {
        Debug.DrawRay(_start, _dir, Color.blue, _time);
    }

    private void slopeSimulation(Vector3 _simulatedPos) {
        simulationSphere.transform.position = _simulatedPos;
    }

}
