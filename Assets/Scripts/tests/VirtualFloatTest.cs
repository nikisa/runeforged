﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class VirtualFloatTest : MonoBehaviour
{

    public GameObject obj;

    public float value;
    public float endValue;
    public float time;
    public Ease ease;

    private float timer;
    private float timePercentage;
    private float movementValue;
    private bool isStarted = false;

    private void Update() {
        
        if (Input.GetKeyDown(KeyCode.Space)) {
            isStarted = true;
        }
        if (isStarted && timer <= time) {
            timer += Time.deltaTime;
            timePercentage = timer / time;
            Debug.Log(DOVirtual.EasedValue(value, endValue, timePercentage, ease));
            movementValue = DOVirtual.EasedValue(value, endValue, timePercentage, ease);
            obj.transform.position = new Vector3(0, 0, movementValue);
        }
    }

}
