﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    public TargetType waypoint;

    private void Awake() {
        waypoint.instance = this.gameObject;
    }
}
