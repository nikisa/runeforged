﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PauseValuesData", menuName = "PauseData/PauseValuesData")]
public class PauseValueData : BaseData
{
    [Header("Shaders")]
    [Header("Materials using WindShader")]
    public Material MaterialWind;
    public Material MaterialWind2;
    public Material MaterialLeavesWind;
    [Header("Materials using WaterfallShader")]
    public Material MaterialFoam;
    public Material MaterialWaterfall;
    [Header("Values of the WindShader")]
    public float WindMovementX; //6
    public float WindDensity; //0.2
    public float WindStrength; //0.3
    [Header("Waterfall")]
    public Vector4 FoamRippleSpeedY; //0.2
    public float FoamVoronoiSpeed; //1.84
    public Vector4 WaterfallRippleSpeedY; //0.2
    public float WaterfallVoronoiSpeed; //1.84
}
