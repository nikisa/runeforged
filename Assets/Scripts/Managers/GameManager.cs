﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using XInputDotNetPure;
using UnityEngine.Experimental.Input.Plugins.XInput;
using UnityEngine.Experimental.Input.Plugins.PS4;
using UnityEngine.Experimental.Input.Plugins.PlayerInput;
using UnityEngine.Experimental.Input;
using UnityEngine.Analytics;
using FMODUnity;

public class GameManager : MonoBehaviour
{

    //Inspector
    [Header("Canvas")]
    public Animator animator;
    public UiBase Menu;
    public UiBase BossSelection;
    public UiBase TutorialSelection;
    public UiBase Credits;
    public UiBase SplashArt;
    public UiBase Loading;
    public UiBase Gameplay;
    public UiBase PauseMenu;
    public UiBase BackGroundPause;
    public UiBase BackGroundMenu;
    public UiBase TutorialGameplay;
    public UiBase StartGameplay;
    public UiBase GameOver;
    public UiBase Victory;
    public Text TutorialText;
    
    //public Image ImageTutorial;
    public List<Sprite> images;
    [Header("Buttons References")]
    public GameObject pauseFirstButton;
    public GameObject menuFirstButton;
    public GameObject BossSelectionFirstButton;
    public GameObject TutorialSelectionFirstButton;
    public GameObject CreditsFirstButton;
    public Slider masterFX;

    //Public
    [HideInInspector]
    public PlayerController Player;
    [HideInInspector]
    public static GameManager instance;
    [HideInInspector]
    public bool isGamePaused;
    [HideInInspector]
    public bool isControllerConnected;
    [HideInInspector]
    public int frameInterval;
    [HideInInspector]
    public bool GameplaySelected;
    [HideInInspector]
    public bool TutorialSelected;
    [HideInInspector]
    public bool FirstBossSelected;
    [HideInInspector]
    public bool SecondBossSelected;
    [HideInInspector]
    public bool FirstTutorialSelected;
    [HideInInspector]
    public bool SecondTutorialSelected;
    [HideInInspector]
    public bool LoadingFinished;
    [HideInInspector]
    public bool isGameOver;
    [HideInInspector]
    public bool isVictory;
    [HideInInspector]
    [Range(0,1)]
    public float masterVolume;

    //Private
    FMOD.Studio.Bus master;

    private void Awake() {
        master = FMODUnity.RuntimeManager.GetBus("bus:/Master");
    }

    public void Start()
    {
        frameInterval = 30;

        foreach (var item in animator.GetBehaviours<UiBaseState>())
        {
            item.SetContext(this, animator);
        }
    }

    public void Init()
    {
        if (instance != null && instance == this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(this);
    }

    public void UpdateMasterFX() {
        masterVolume = masterFX.value;
        master.setVolume(masterVolume);
    }

    public void DisableMenu(MenuType _menuType)
    {
        switch (_menuType)
        {
            case MenuType.Menu:
                Menu.Disable();
                break;
            case MenuType.BossSelection:
                BossSelection.Disable();
                break;
            case MenuType.TutorialSelection:
                TutorialSelection.Disable();
                break;
            case MenuType.Credits:
                Credits.Disable();
                break;
            case MenuType.SplashPage:
                SplashArt.Disable();
                break;
            case MenuType.Loading:
                Loading.Disable();
                break;
            case MenuType.Gameplay:
                Gameplay.Disable();
                break;
            case MenuType.BackGroundPause:
                BackGroundPause.Disable();
                break;
            case MenuType.PauseMenu:
                PauseMenu.Disable();
                break;
            case MenuType.BackGroundMenu:
                BackGroundMenu.Disable();
                break;
            case MenuType.TutorialGameplay:
                TutorialGameplay.Disable();
                break;
            case MenuType.StartGameplay:
                StartGameplay.Disable();
                break;
            case MenuType.GameOver:
                GameOver.Disable();
                break;
            case MenuType.Victory:
                Victory.Disable();
                break;


        }
    }
    public void ChangeMenu(MenuType _menuType)
    {
        switch (_menuType)
        {
            case MenuType.Menu:
                Menu.Setup();
                break;
            case MenuType.BossSelection:
                BossSelection.Setup();
                break;
            case MenuType.TutorialSelection:
                TutorialSelection.Setup();
                break;
            case MenuType.Credits:
                Credits.Setup();
                break;
            case MenuType.SplashPage:
                SplashArt.Setup();
                break;
            case MenuType.Loading:
                Loading.Setup();
                break;
            case MenuType.Gameplay:
                Gameplay.Setup();
                break;
            case MenuType.BackGroundPause:
                BackGroundPause.Setup();
                break;
            case MenuType.PauseMenu:
                PauseMenu.Setup();
                break;
            case MenuType.BackGroundMenu:
                BackGroundMenu.Setup();
                break;
            case MenuType.TutorialGameplay:
                TutorialGameplay.Setup();
                break;
            case MenuType.StartGameplay:
                StartGameplay.Setup();
                break;
            case MenuType.GameOver:
                GameOver.Setup();
                break;
            case MenuType.Victory:
                Victory.Setup();
                break;
        }
    }


    public enum MenuType
    {
        Menu,
        BossSelection,
        TutorialSelection,
        Credits,
        SplashPage,
        Loading,
        Gameplay,
        BackGroundPause,
        PauseMenu,
        BackGroundMenu,
        TutorialGameplay,
        StartGameplay,
        GameOver,
        Victory

    }


    public void PlayGameplay()
    {
        //Loading.Setup();
        //Init();
        //GameplaySelected = true;
        animator.SetTrigger("BossSelection");
    }

    public void FirstBossGameplay() {
        Loading.Setup();
        Init();
        FirstBossSelected = true;
        animator.SetTrigger("Loading");
    }

    public void SecondBossGameplay() {
        Loading.Setup();
        Init();
        SecondBossSelected = true;
        animator.SetTrigger("Loading");
    }

    public void PlayTutorial()
    {
        //Loading.Setup();
        //Init();
        //FirstTutorialSelected = true;
        ////animator.SetTrigger("TutorialSelection");
        //animator.SetTrigger("Loading");
        animator.SetTrigger("TutorialSelection");

    }

    public void FirstTutorial() {
        Loading.Setup();
        Init();
        FirstTutorialSelected = true;
        animator.SetTrigger("Loading");
    }

    public void SecondTutorial() {
        Loading.Setup();
        Init();
        SecondTutorialSelected = true;
        animator.SetTrigger("Loading");
    }

    public void ShowCredits() {
        animator.SetTrigger("Credits");
    }

    public void PlayMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
        Init();
        animator.SetTrigger("SplashArt");
    }


    #region PAUSE
    public void Resume()
    {
        animator.SetTrigger("Gameplay");
    }

    public void Restart()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ReloadLevelAsync();
        //animator.SetTrigger("Gameplay");
        //PlayGameplay();
        //animator.SetTrigger("Gameplay");
    }
    public void Back()
    {
        animator.SetTrigger("Back");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
    #endregion

    #region Controller Recognizer
    public void ControllerRecognizer()
    {

        PlayerIndex singlePlayer = (PlayerIndex)0;
        GamePadState gamePadState = GamePad.GetState(singlePlayer);

        if (gamePadState.IsConnected)
        {
            //Controller connected
            isControllerConnected = true;
            Debug.Log("Controller connected");


            //WHICH TYPE OF GAMEPAD
            //if (Gamepad.current is DualShockGamepadPS4) {
            //    Debug.Log("PS4 controller");
            //}
            //else if(Gamepad.current is XboxOneGamepad){
            //    Debug.Log("Xbox controller");
            //}

        }
        else
        {
            //Controller not connected
            isControllerConnected = false;
            Debug.Log("Controller not connected");

        }

    }
    #endregion

    public void StartLoadAsyncOperation()
    {
        StartCoroutine("LoadAsyncOperation");
    }

    private IEnumerator LoadAsyncOperation()
    {

        if (FirstBossSelected)
        {
            FirstBossSelected = true;
            AsyncOperation gameLevel = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
            gameLevel.completed += (AsyncOperation) => { LoadingFinished = true; };
            yield return new WaitForEndOfFrame();
        }
        else if (SecondBossSelected)
        {
            SecondBossSelected = true;
            AsyncOperation gameLevel = SceneManager.LoadSceneAsync(3, LoadSceneMode.Additive);
            gameLevel.completed += (AsyncOperation) => { LoadingFinished = true; };
            yield return new WaitForEndOfFrame();
        }
        else if (FirstTutorialSelected) {
            FirstTutorialSelected = true;
            AsyncOperation gameLevel = SceneManager.LoadSceneAsync(2, LoadSceneMode.Additive);
            gameLevel.completed += (AsyncOperation) => { LoadingFinished = true; };
            yield return new WaitForEndOfFrame();
        }
        else if (SecondTutorialSelected) {
            SecondTutorialSelected = true;
            AsyncOperation gameLevel = SceneManager.LoadSceneAsync(5, LoadSceneMode.Additive);
            gameLevel.completed += (AsyncOperation) => { LoadingFinished = true; };
            yield return new WaitForEndOfFrame();
        }

    }

    public void ReloadLevelAsync()
    {
        StartCoroutine("RealoadAsyncOperation");

        int currentScene = SceneManager.GetActiveScene().buildIndex;

        if (currentScene == 1) {
            FirstBossSelected = true;
        }
        else if (currentScene == 3) {
            SecondBossSelected = true;
        }
        else if (currentScene == 2) {
            FirstTutorialSelected = true;
        }
        else if (currentScene == 5) {
            SecondTutorialSelected = true;
        }

        //HERE HERE HERE HERE HERE HERE HERE 
        animator.SetTrigger("Loading");
        RuntimeManager.LoadBank("Boss");
        RuntimeManager.LoadBank("Player");
        RuntimeManager.LoadBank("Music");
        //ChangeMenu(GameManager.MenuType.Loading);

        //    if (LoadingFinished) {
        //        DisableMenu(GameManager.MenuType.Loading);
        //        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //    }

    }

    private IEnumerator RealoadAsyncOperation()
    {
        AsyncOperation gameLevel = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Additive);
        gameLevel.completed += (AsyncOperation) => { LoadingFinished = true; };
        yield return new WaitForEndOfFrame();

    }

    #region Script Cemetery

    //public void LifeUpdate(int _lifes)
    //{
    //    if (_lifes < 0)
    //    {
    //    FullHeart[_lifes].Disable();
    //    EmptyHeart[_lifes].Setup();
    //    }

    //}

    #endregion

}
