﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    //Inspector
    public Image ImageTutorial;

    public Animator animator;
    public PlayerController player;
    public GrappleManager grappleManager;
    public FirstBossController FirstBossController;

    //[HideInInspector]
    public GameManager uiManager;
    // Start is called before the first frame update

    bool referencesFound = false;
    
    void Start()
    {

        foreach (var item in animator.GetBehaviours<TutorialBaseState>())
        {
            item.SetContext(uiManager, animator , player , grappleManager , FirstBossController);
        }

        Setup();
    }

    // Update is called once per frame
    void Update()
    {
        if (!referencesFound) {
            player = FindObjectOfType<PlayerController>();
            grappleManager = FindObjectOfType<GrappleManager>();
            FirstBossController = FindObjectOfType<FirstBossController>();
            uiManager = FindObjectOfType<GameManager>();
            referencesFound = true;
        }
        
    }
    public void Setup()
    {
        
        uiManager = FindObjectOfType<GameManager>();
    }
}
