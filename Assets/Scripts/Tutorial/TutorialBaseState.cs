﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TutorialBaseState : BaseState
{

    //Public
    [HideInInspector]
    public Tutorial tutorial;
    
    [HideInInspector]
    protected GameManager uiManager;
    protected PlayerController player;
    protected GrappleManager grappleManager;
    protected FirstBossController firstBossController;


    
    public void SetContext(object context, Animator animator , PlayerController player , GrappleManager grappleManager , FirstBossController firstBossController)
    {
        uiManager = context as GameManager;
        this.animator = animator;
        this.player = player;
        this.grappleManager= grappleManager;
        this.firstBossController = firstBossController;
    }

    
}
