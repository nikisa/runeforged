﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DashTutorialState : TutorialBaseState
{
    Text text;
    public override void Enter()
    {
        tutorial = FindObjectOfType<Tutorial>();
        //text = uiManager.TutorialText;
        //text.text = "DASHAAAAAAA";
        tutorial.ImageTutorial.enabled = true;
        tutorial.ImageTutorial.sprite = tutorial.uiManager.images[1];
    }

    public override void Tick()
    {
        if (Input.GetKey(KeyCode.Space) || Input.GetButton("Dash"))
        {
            tutorial.animator.SetTrigger("Shoot");
        }
    }
    public override void Exit()
    {
       //text.text = " ";
    }
}
