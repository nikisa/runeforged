﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShootTutorialState : TutorialBaseState
{
    Text text;
    public override void Enter()
    {
        tutorial = FindObjectOfType<Tutorial>();
        tutorial.ImageTutorial.sprite = tutorial.uiManager.images[2];
    }

    public override void Tick()
    {
        if (tutorial.grappleManager.hook.isHooked)
        {
            if (tutorial.grappleManager.hook.currentHitObject.GetComponent<FirstBossMask>()) {
                if (tutorial.grappleManager.hook.currentHitObject.GetComponent<FirstBossMask>().VelocityVector == Vector3.zero) {
                    tutorial.animator.SetTrigger("Tug");
                }
                else {
                    tutorial.grappleManager.hook.isHooked = false;
                    tutorial.grappleManager.hook.currentHitObject.GetComponent<FirstBossMask>().isHooked = false;
                }
            }

            else if (tutorial.grappleManager.hook.currentHitObject.GetComponent<RuneTutorial>() || tutorial.grappleManager.hook.currentHitObject.tag == "CoreHook") {
                tutorial.animator.SetTrigger("Attack");
            }
        }
    }
    public override void Exit()
    {
    }
}