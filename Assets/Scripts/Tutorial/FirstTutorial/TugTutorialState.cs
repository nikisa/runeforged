﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TugTutorialState : TutorialBaseState
{

    int initialMaskCount;
    public override void Enter()
    {
        tutorial = FindObjectOfType<Tutorial>();
        //text = uiManager.TutorialText;
        //text.text = "Press rt to tense the robe";
        //uiManager.ImageTutorial.sprite = uiManager.images[3];
        //initialMaskCount = firstBossController.bossOrbitManager.MasksList.Count;
    }

    public override void Tick()
    {
        

        if (Input.GetAxis("Rewind") < 0.2)
        {
            tutorial.ImageTutorial.sprite = tutorial.uiManager.images[3];
        }
        //else
        //{
        //    Immagine che ti dice di tirare mentre la corda è tesa
        //}

        if (tutorial.FirstBossController.bossOrbitManager.MasksList.Count == 0)
        {
            Debug.Log("HERE");
            tutorial.animator.SetTrigger("Attack");
        }
        if (Input.GetButton("UnhookXBOX") || Input.GetKeyDown(KeyCode.U))
        {
            tutorial.animator.SetTrigger("Shoot");
        }
    }
    public override void Exit()
    {
    }
}