﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MovementTutorialState : TutorialBaseState
{

    public override void Enter()
    {
        tutorial = FindObjectOfType<Tutorial>();
        //text = uiManager.TutorialText;
        //text.text = "MUOVITI";
        tutorial.ImageTutorial.enabled = true;
        tutorial.ImageTutorial.sprite = tutorial.uiManager.images[0];
    }

    public override void Tick()
    {
        if (tutorial.player.checkDeadZone())
        {
            tutorial.animator.SetTrigger("Dash");
        }
    }
    public override void Exit()
    {
        //text.text = " ";
    }
}