﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AttackTutorialState : TutorialBaseState
{

    //Private
    private bool isAttackTutorial;
  
    public override void Enter()
    {
        tutorial = FindObjectOfType<Tutorial>();
        tutorial.grappleManager.Player.AttackFinish = false;
        tutorial.ImageTutorial.sprite = tutorial.uiManager.images[4];
        tutorial.grappleManager.Player.readyToWin = true;
    }

    public override void Tick()
    {

        if (tutorial.grappleManager.Player.AttackFinish)
        {
            //uiManager.ImageTutorial.enabled = false;

            if (tutorial.grappleManager.hook.currentHitObject.GetComponent<RuneTutorial>()) {
                tutorial.animator.SetTrigger("Shoot");
                tutorial.grappleManager.hook.currentHitObject.GetComponent<RuneTutorial>().isRuneActive = true;
                tutorial.grappleManager.hook.currentHitObject.GetComponent<RuneTutorial>().isScaling = false;
            }
            else if (tutorial.grappleManager.hook.currentHitObject.transform.tag == "CoreHook") {
                tutorial.grappleManager.Player.Victory();
            }
        }
       
    }
    public override void Exit()
    {
        isAttackTutorial = false;
        tutorial.grappleManager.Player.isAttacking = false;
        tutorial.grappleManager.Player.AttackFinish = false;
        
    }
}