﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UnHookTutorialState : TutorialBaseState
{
    Text text;
    public override void Enter() {
        tutorial = FindObjectOfType<Tutorial>();
        tutorial.ImageTutorial.sprite = tutorial.uiManager.images[5];
    }

    public override void Tick() {
        if (!tutorial.grappleManager.hook.isHooked) {
            tutorial.animator.SetTrigger("Shoot");
        }
    }
    public override void Exit() {
        //text.text = " ";
    }
}