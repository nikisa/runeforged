﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BoostTutorialState : TutorialBaseState
{

    public override void Enter()
    {
        tutorial = FindObjectOfType<Tutorial>();
        tutorial.ImageTutorial.sprite = tutorial.uiManager.images[6];
    }

    public override void Tick()
    {
        
    }

    public override void Exit(){}
}