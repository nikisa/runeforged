﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public enum CollisionChecker
{
    Sphere,
    Box,
    Capsule
};

public class MovementBase : MonoBehaviour
{

    //Inspector
    [Header("MOVEMENT BASE")]
    public CameraShakeManager cameraShakeManager;
    //public float MinBounceVector;
    //public float MaxBounceVector;
    public CollisionChecker collisionChecker;
    public CharacterController CharacterController;
    public float Mass;
    [HideInInspector]
    [Range(0, 1)]
    public float KineticEnergyLoss;
    [HideInInspector]
    [Range(0, 1)]
    public float SurfaceFriction;
    [Header("Collision")]
    public LayerMask layerMask;
    public float collisionOffset = 0.0001f;
    public GameObject currentHitObject;

    //Public
    #region MOVEMENT
    [HideInInspector]
    public Vector3 move;
    [HideInInspector]
    public Vector3 nextPosition;
    [HideInInspector]
    public float radius;
    [HideInInspector]
    public float AccelerationModule;
    [HideInInspector]
    public Vector3 AccelerationVector;
    [HideInInspector]
    public Vector3 VelocityVector;
    [HideInInspector]
    public Vector3 BounceVector;
    [HideInInspector]
    public float Drag;
    [HideInInspector]
    public float DecelerationModule;
    [HideInInspector]
    public float BounceDecelerationModule;
    [HideInInspector]
    public Vector3 DecelerationVector;
    [HideInInspector]
    public Vector3 BounceDecelerationVector;
    [HideInInspector]
    public Vector3 targetDir;
    [HideInInspector]
    public float gravity;
    [HideInInspector]
    public float impulseDeltaTime;
    [HideInInspector]
    public int layerHitObject;
    [HideInInspector]
    public Collider CollidedObjectCollider;
    [HideInInspector]
    public Vector3 HitNormal;
    #endregion


    //private
    PlayerController player;
    //Collider[] hits;

    private void Awake() {
        gravity = 0f;
        player = FindObjectOfType<PlayerController>();
        radius = 3; //Could change with a different masks setup
    }


    /// <summary>
    /// Movement with custom physics 
    /// </summary>
    /// <param name="_targetDir"></param>
    /// <param name="_maxSpeed"></param>
    /// <param name="_accelerationModule">accelerationModule = MaxSpeed / TimeAcceleration; </param>
    public void Movement(Vector3 _targetDir , float _maxSpeed , float _accelerationModule) {

        Vector3 accelerationVectorTemp = _targetDir;
        accelerationVectorTemp.y = 0;
        AccelerationVector = accelerationVectorTemp.normalized * _accelerationModule;
        //Drag = _accelerationModule / _maxSpeed * Time.deltaTime;
        //VelocityVector -= VelocityVector * Drag;
        move = Vector3.ClampMagnitude((VelocityVector * Time.deltaTime + 0.5f * AccelerationVector * Mathf.Pow(Time.deltaTime, 2)) , _maxSpeed * Time.deltaTime); //Formula completa per un buon effetto fin dal primo frame
        nextPosition = transform.position + move;
        VelocityVector = Vector3.ClampMagnitude((VelocityVector + AccelerationVector * Time.deltaTime), _maxSpeed);
        CheckCollision(this , move , radius);
        //transform.position += move;

        //CharacterController.Move(move + Vector3.down * gravity);
        //Debug.DrawRay(transform.position, VelocityVector, Color.blue, 0.2f);
        //Debug.DrawRay(transform.position, AccelerationVector, Color.red, 0.2f);

    }

    //public void CharacterControllerMovement(Vector3 _targetDir, float _maxSpeed, float _accelerationModule) {

    //    Vector3 accelerationVectorTemp = _targetDir;
    //    accelerationVectorTemp.y = 0;
    //    AccelerationVector = accelerationVectorTemp.normalized * _accelerationModule;
    //    move = Vector3.ClampMagnitude((VelocityVector * Time.deltaTime + 0.5f * AccelerationVector * Mathf.Pow(Time.deltaTime, 2)), _maxSpeed * Time.deltaTime); //Formula completa per un buon effetto fin dal primo frame
    //    nextPosition = transform.position + move;
    //    VelocityVector = Vector3.ClampMagnitude((VelocityVector + AccelerationVector * Time.deltaTime), _maxSpeed);
    //    CharacterController.Move(move + Vector3.down * gravity);
    //}



    public void Deceleration(AnimationCurve _movementDecelCurve, float _t0 , float _t1, int _iterations) {
        float vectorAngle = Vector3.SignedAngle(Vector3.forward, VelocityVector.normalized, Vector3.up) * Mathf.Deg2Rad;
        DecelerationVector = new Vector3(Mathf.Sin(vectorAngle) * DecelerationModule, 0, Mathf.Cos(vectorAngle) * DecelerationModule);
        move = DecelerationVector.normalized * Integration.IntegrateCurve(_movementDecelCurve, _t0, _t1, _iterations);
        VelocityVector = _movementDecelCurve.Evaluate(_t1) * DecelerationVector.normalized;
        //if (CharacterController == null) {
            CheckCollision(this, move, radius);
        //}
        //else {
        //    CharacterController.Move(move);
        //}
        
        
        //CharacterController.Move(move + Vector3.down * gravity);
    }


    public void DecelerateTowards(Vector3 _targetDir, float _decelerationModule) {
        
        Vector3 accelerationVectorTemp = _targetDir;
        accelerationVectorTemp.y = 0;
        AccelerationVector = accelerationVectorTemp.normalized * AccelerationVector.magnitude;

        //Drag = _accelerationModule / _maxSpeed * Time.deltaTime;
        //VelocityVector -= VelocityVector * Drag;
        VelocityVector = (VelocityVector.magnitude - _decelerationModule * Time.deltaTime) * VelocityVector.normalized;
        move = Vector3.ClampMagnitude(VelocityVector * Time.deltaTime + 0.5f * AccelerationVector * Mathf.Pow(Time.deltaTime, 2) , VelocityVector.magnitude) ; //Formula completa per un buon effetto fin dal primo frame
        nextPosition = transform.position + move;
        VelocityVector = move / Time.deltaTime;
        //if (CharacterController == null) {
            CheckCollision(this, move, radius);
        //}
        //else {
        //    CharacterController.Move(move);
        //}

        //ransform.position += move;
        //CharacterController.Move(move + Vector3.down * gravity);
        //Debug.DrawRay(transform.position, VelocityVector, Color.blue, 0.2f);
        //Debug.DrawRay(transform.position, AccelerationVector, Color.red, 0.2f);

    }

    public void AccelerateTowards(Vector3 _targetDir, float _accelerationModule) {

        Vector3 accelerationVectorTemp = _targetDir;
        accelerationVectorTemp.y = 0;
        AccelerationVector = accelerationVectorTemp.normalized * AccelerationVector.magnitude;

        //Drag = _accelerationModule / _maxSpeed * Time.deltaTime;
        //VelocityVector -= VelocityVector * Drag;
        VelocityVector = (VelocityVector.magnitude + _accelerationModule * Time.deltaTime) * VelocityVector.normalized;
        move = Vector3.ClampMagnitude(VelocityVector * Time.deltaTime + 0.5f * AccelerationVector * Mathf.Pow(Time.deltaTime, 2), VelocityVector.magnitude); //Formula completa per un buon effetto fin dal primo frame
        nextPosition = transform.position + move;
        VelocityVector = move / Time.deltaTime;
        //if (CharacterController == null) {
            CheckCollision(this, move, radius);
        //}
        //else {
        //    CharacterController.Move(move);
        //}

        //transform.position += move;
        //CharacterController.Move(move + Vector3.down * gravity);

        //Debug.DrawRay(transform.position, VelocityVector, Color.blue, 0.2f);
        //Debug.DrawRay(transform.position, AccelerationVector, Color.red, 0.2f);

    }


    public void BounceDeceleration() {

        float vectorAngle = Vector3.SignedAngle(Vector3.forward, BounceVector.normalized, Vector3.up) * Mathf.Deg2Rad;
        BounceDecelerationVector = new Vector3(Mathf.Sin(vectorAngle) * BounceDecelerationModule, 0, Mathf.Cos(vectorAngle) * BounceDecelerationModule);

        BounceVector -= BounceDecelerationVector * Time.deltaTime;
        move = BounceVector * Time.deltaTime;
        if (CharacterController != null) {
            CharacterController.Move(move + Vector3.down * gravity);
        }

        Debug.DrawRay(transform.position, BounceVector, Color.black, .03f);
    }


    public void MovementReset() {

        move = Vector3.zero;
        nextPosition = Vector3.zero;
        AccelerationVector = Vector3.zero;
        VelocityVector = Vector3.zero;
        DecelerationVector = Vector3.zero;
        Drag = 0;
        AccelerationModule = 0;
        DecelerationModule = 0;

    }

    public void BounceMovement(Collider hit , BounceData bounceData) {

        #region Bounce variables
        //MovementBase collidingObject = hit.gameObject.GetComponent<MovementBase>();
        CharacterControllerMovementBase collidingObject = hit.gameObject.GetComponent<CharacterControllerMovementBase>();
        Vector3 normal;
        Vector3 vectorParal;
        Vector3 vectorPerp;
        Vector3 collisionVectorParal;
        Vector3 collisionVectorPerp;
        Vector3 bounceVector;
        float shakeAngle;
        #endregion
        if (hit.gameObject.GetComponent<PlayerView>())
        {
            collidingObject = player;
        }

        Vector3 fakeCollidingObjectPosition = new Vector3(collidingObject.transform.position.x, transform.position.y, collidingObject.transform.position.z);
        normal = Vector3.ProjectOnPlane((collidingObject.transform.position - transform.position).normalized , Vector3.up);
        shakeAngle = Vector3.Angle(transform.position , fakeCollidingObjectPosition);
        
        vectorParal = Vector3.Project(VelocityVector, normal);
        vectorPerp = Vector3.ProjectOnPlane(VelocityVector, normal);
        
        collisionVectorParal = Vector3.Project(collidingObject.VelocityVector, -normal);
        collisionVectorPerp = Vector3.ProjectOnPlane(collidingObject.VelocityVector, -normal);
        
        //Bounce formula
        bounceVector = (vectorParal * (Mass - collidingObject.Mass) + 2 * collidingObject.Mass * collisionVectorParal) / (Mass + collidingObject.Mass);
        VelocityVector = (bounceVector * (1 - KineticEnergyLoss)) + (vectorPerp * (1 - KineticEnergyLoss));
        //VelocityVector = Mathf.Clamp(VelocityVector.magnitude, MinBounceVector, MaxBounceVector) * VelocityVector.normalized;
        AccelerationVector = VelocityVector.normalized * AccelerationVector.magnitude;
        
        bounceVector = (collisionVectorParal * (collidingObject.Mass - Mass) + 2 * Mass * vectorParal) / (collidingObject.Mass + Mass);
        collidingObject.VelocityVector = (bounceVector * (1 - KineticEnergyLoss)) + collisionVectorPerp * (1 - SurfaceFriction);

        if (collidingObject.GetComponent<PlayerController>()) {
            PlayerController player = collidingObject.GetComponent<PlayerController>();
            player.animator.SetTrigger("Stunned");
            player.GetDamage();
        }
        

        
        cameraShakeManager.StartShake(shakeAngle, bounceData.noisePercent, bounceData.strength, bounceData.duration, bounceData.speed, bounceData.dampingPercent, bounceData.rotationPercent);

        Debug.DrawRay(transform.position, VelocityVector, Color.blue, 0.2f);
        Debug.DrawRay(collidingObject.transform.position, collidingObject.VelocityVector, Color.black, 0.2f);

    }

    public void WallBounce(Collider hit , Vector3 _normal , float _angularVelocity , BounceData bounceData) {

        #region Bounce variables
        GameObject collidingObject = hit.gameObject;

        Vector3 vectorParal;
        Vector3 vectorPerp;
        Vector3 bounceVector;
        float plusAngle;
        float shakeAngle;
        #endregion

        shakeAngle = Vector3.Angle(transform.position, collidingObject.transform.position);

        vectorParal = Vector3.Project(VelocityVector, _normal);
        vectorPerp = Vector3.ProjectOnPlane(VelocityVector, _normal);

        //Debug.DrawRay(transform.position, vectorParal, Color.red, 5);
        //Debug.DrawRay(transform.position, vectorPerp, Color.cyan, 5);

        //Bounce formula
        //Per il muro non serve andare a vedere la sua massa , ma basta dare la stessa massa dell'oggetto che urta
        bounceVector = (-2 * Mass * vectorParal) / (2 * Mass);
        bounceVector *= 1 - KineticEnergyLoss;
        VelocityVector = (bounceVector * (1 - KineticEnergyLoss)) + vectorPerp * (1 - SurfaceFriction);

        Debug.DrawRay(transform.position, VelocityVector, Color.cyan, .2f);
        plusAngle = -(_angularVelocity * impulseDeltaTime);



        VelocityVector = Quaternion.AngleAxis(plusAngle, Vector3.up) * VelocityVector;
        AccelerationVector = VelocityVector.normalized * AccelerationVector.magnitude;

        cameraShakeManager.StartShake(shakeAngle , bounceData.noisePercent , bounceData.strength , bounceData.duration , bounceData.speed , bounceData.dampingPercent , bounceData.rotationPercent);

        Debug.DrawRay(transform.position, VelocityVector, Color.blue, .02f);
        //Debug.DrawRay(transform.position, AccelerationVector, Color.red, .02f);
        
    }

    void DrawNormal(Vector3 _hitPoint , Vector3 _hitNormal , float _time) {
        Debug.DrawRay(_hitPoint , _hitNormal , Color.black , _time);
    }

    //private Vector3 GetTotalNormal(Collider[] _hits) {

    //    Vector3 totalNormal = Vector3.zero;
    //    Vector3 currentNormal = Vector3.zero;
    //    Vector3 currentNormalTest = Vector3.zero;

    //    //foreach (Collider hit in _hits) {
    //        currentNormal = (transform.position - _hits[0].transform.position).normalized;
    //        currentNormalTest = (transform.position - _hits[1].transform.position).normalized;
    //        totalNormal = currentNormal + currentNormalTest;
    //    //}
        
    //    return totalNormal;
    //}

    public void CheckCollision(MovementBase _origin , Vector3 _spatialVector,  float _radius) {
        RaycastHit hit;
        Vector3 normal;
        Vector3 _vectorDir = _spatialVector.normalized;
        float _spatialMagnitude = _spatialVector.magnitude;

        //Se collide:
        //Calcola la percentuale di Drag da applicare a fine ciclo;
        //Esegui bounce (DIRETTO): verifica che Spatial * Elasticity sia superiorie a X e, se sì, fai reflection;
        switch (collisionChecker) {
            case CollisionChecker.Sphere:
                if (Physics.SphereCast(_origin.transform.position, _radius, _vectorDir, out hit, _spatialMagnitude, layerMask, QueryTriggerInteraction.UseGlobal)) {

                    Collider[] hits = Physics.OverlapSphere(_origin.transform.position, _radius, layerMask);

                    if (hits.Length <= 1) {
                        Debug.Log("hits.Length > 1");
                        //normal = GetTotalNormal(hits);
                        normal = hit.normal;

                        DrawNormal(hit.point, normal, 0.02f);

                        //Calcola lo SpatialVector percorso prima della collisione
                        Vector3 hitSpatialVector = hit.distance * _vectorDir + collisionOffset * normal; //Allontana l'oggetto di un vettore fisso sulla normale per garantire il check delle collisioni future (se la distanza è 0, lo spherecast non le rileva).

                        Vector3 slopeSpatialVector = Vector3.ProjectOnPlane((_spatialVector - hitSpatialVector), normal); //Componente di SpatialVector post collisione proiettata sulla perpendicolare alla Normale (la componente parallela è annullata dalla Normale stessa).
                                                                                                                          //Vector3 slopeDir = slopeSpatialVector.normalized;

                        _origin.transform.position += hitSpatialVector; //Sposta Node del VettoreSpaziale precollisione.
                        _origin.currentHitObject = hit.transform.gameObject; //Memorizza hitObject, il Game Object contro il quale è avvenuta la collisione.
                        layerHitObject = _origin.currentHitObject.layer;
                        CollidedObjectCollider = _origin.currentHitObject.GetComponent<Collider>();
                        HitNormal = normal;
                        //CheckCollision(_massPoint, Vector3.Reflect(_spatialVector - hitSpatialVector, hit.normal), slopeDeltaTime, _massPoint.MassPointRadius);
                        CheckCollision(_origin, slopeSpatialVector, _radius);
                    }
                }
                else {
                    //Se non ci sono state collisioni...
                    _origin.transform.position += _spatialVector; //Muove il Node lungo SpatialVector
                }

                break;
            case CollisionChecker.Box:
                if (Physics.BoxCast(_origin.transform.position, _origin.transform.localScale/2 , _vectorDir , out hit , _origin.transform.localRotation , _spatialMagnitude, layerMask, QueryTriggerInteraction.UseGlobal)) {

                    //DrawNormal(hit.point, hit.normal, 0.02f);

                    //Calcola lo SpatialVector percorso prima della collisione
                    Vector3 hitSpatialVector = hit.distance * _vectorDir + collisionOffset * hit.normal; //Allontana l'oggetto di un vettore fisso sulla normale per garantire il check delle collisioni future (se la distanza è 0, lo spherecast non le rileva).

                    Vector3 slopeSpatialVector = Vector3.ProjectOnPlane((_spatialVector - hitSpatialVector), hit.normal); //Componente di SpatialVector post collisione proiettata sulla perpendicolare alla Normale (la componente parallela è annullata dalla Normale stessa).
                    Vector3 slopeDir = slopeSpatialVector.normalized;

                    _origin.transform.position += hitSpatialVector; //Sposta Node del VettoreSpaziale precollisione.
                    _origin.currentHitObject = hit.transform.gameObject; //Memorizza hitObject, il Game Object contro il quale è avvenuta la collisione.
                    layerHitObject = _origin.currentHitObject.layer;
                    CollidedObjectCollider = _origin.currentHitObject.GetComponent<Collider>();
                    HitNormal = hit.normal;
                    //CheckCollision(_massPoint, Vector3.Reflect(_spatialVector - hitSpatialVector, hit.normal), slopeDeltaTime, _massPoint.MassPointRadius);
                    CheckCollision(_origin, slopeSpatialVector, _radius);
                }
                else {
                    //Se non ci sono state collisioni...
                    _origin.transform.position += _spatialVector; //Muove il Node lungo SpatialVector
                }
                break;
            case CollisionChecker.Capsule:
                if (Physics.CapsuleCast(_origin.transform.localPosition - _origin.transform.localScale/2, _origin.transform.localPosition + _origin.transform.localScale / 2, _radius, _vectorDir, out hit ,  _spatialMagnitude, layerMask, QueryTriggerInteraction.UseGlobal)) {

                    //DrawNormal(hit.point, hit.normal, 0.02f);

                    //Calcola lo SpatialVector percorso prima della collisione
                    Vector3 hitSpatialVector = hit.distance * _vectorDir + collisionOffset * hit.normal; //Allontana l'oggetto di un vettore fisso sulla normale per garantire il check delle collisioni future (se la distanza è 0, lo spherecast non le rileva).

                    Vector3 slopeSpatialVector = Vector3.ProjectOnPlane((_spatialVector - hitSpatialVector), hit.normal); //Componente di SpatialVector post collisione proiettata sulla perpendicolare alla Normale (la componente parallela è annullata dalla Normale stessa).
                    Vector3 slopeDir = slopeSpatialVector.normalized;

                    _origin.transform.position += hitSpatialVector; //Sposta Node del VettoreSpaziale precollisione.
                    _origin.currentHitObject = hit.transform.gameObject; //Memorizza hitObject, il Game Object contro il quale è avvenuta la collisione.
                    layerHitObject = _origin.currentHitObject.layer;
                    CollidedObjectCollider = _origin.currentHitObject.GetComponent<Collider>();
                    HitNormal = hit.normal;
                    //CheckCollision(_massPoint, Vector3.Reflect(_spatialVector - hitSpatialVector, hit.normal), slopeDeltaTime, _massPoint.MassPointRadius);
                    CheckCollision(_origin, slopeSpatialVector, _radius);
                }
                else {
                    //Se non ci sono state collisioni...
                    _origin.transform.position += _spatialVector; //Muove il Node lungo SpatialVector
                }
                break;
            default:
                break;
        }

        

    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Debug.DrawLine(transform.position, transform.position + move);
        Gizmos.DrawWireSphere(transform.position + move, radius);
        //Gizmos.DrawWireCube(transform.position + move, transform.localScale);

        //Per la capsula fare due sfere
    }


}
