﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class CharacterControllerMovementBase : MonoBehaviour
{

    //Inspector
    [Header("MOVEMENT BASE")]
    public CameraShakeManager cameraShakeManager;
    //public float MinBounceVector;
    //public float MaxBounceVector;
    public CharacterController CharacterController;
    public float Mass;
    [HideInInspector]
    [Range(0, 1)]
    public float KineticEnergyLoss;
    [HideInInspector]
    [Range(0, 1)]
    public float SurfaceFriction;

    //Public
    #region MOVEMENT
    [HideInInspector]
    public Vector3 move;
    [HideInInspector]
    public Vector3 nextPosition;
    [HideInInspector]
    public float AccelerationModule;
    [HideInInspector]
    public Vector3 AccelerationVector;
    [HideInInspector]
    public Vector3 VelocityVector;
    [HideInInspector]
    public Vector3 BounceVector;
    [HideInInspector]
    public float Drag;
    [HideInInspector]
    public float DecelerationModule;
    [HideInInspector]
    public float BounceDecelerationModule;
    [HideInInspector]
    public Vector3 DecelerationVector;
    [HideInInspector]
    public Vector3 BounceDecelerationVector;
    [HideInInspector]
    public Vector3 targetDir;
    [HideInInspector]
    public float gravity;
    [HideInInspector]
    public float impulseDeltaTime;
    #endregion


    //private
    PlayerController player;

    private void Awake() {
        gravity = 0f;
        player = FindObjectOfType<PlayerController>();
    }


    /// <summary>
    /// Movement with custom physics 
    /// </summary>
    /// <param name="_targetDir"></param>
    /// <param name="_maxSpeed"></param>
    /// <param name="_accelerationModule">accelerationModule = MaxSpeed / TimeAcceleration; </param>
    public void Movement(Vector3 _targetDir, float _maxSpeed, float _accelerationModule) {

        Vector3 accelerationVectorTemp = _targetDir;
        accelerationVectorTemp.y = 0;
        AccelerationVector = accelerationVectorTemp.normalized * _accelerationModule;
        //Drag = _accelerationModule / _maxSpeed * Time.deltaTime;
        //VelocityVector -= VelocityVector * Drag;
        move = Vector3.ClampMagnitude((VelocityVector * Time.deltaTime + 0.5f * AccelerationVector * Mathf.Pow(Time.deltaTime, 2)), _maxSpeed * Time.deltaTime); //Formula completa per un buon effetto fin dal primo frame
        nextPosition = transform.position + move;
        VelocityVector = Vector3.ClampMagnitude((VelocityVector + AccelerationVector * Time.deltaTime), _maxSpeed);
        CharacterController.Move(move + Vector3.down * gravity);

        //Debug.DrawRay(transform.position, VelocityVector, Color.blue, 0.2f);
        //Debug.DrawRay(transform.position, AccelerationVector, Color.red, 0.2f);

    }



    public void Deceleration(AnimationCurve _movementDecelCurve, float _t0, float _t1, int _iterations) {
        float vectorAngle = Vector3.SignedAngle(Vector3.forward, VelocityVector.normalized, Vector3.up) * Mathf.Deg2Rad;
        DecelerationVector = new Vector3(Mathf.Sin(vectorAngle) * DecelerationModule, 0, Mathf.Cos(vectorAngle) * DecelerationModule);
        move = DecelerationVector.normalized * Integration.IntegrateCurve(_movementDecelCurve, _t0, _t1, _iterations);
        VelocityVector = _movementDecelCurve.Evaluate(_t1) * DecelerationVector.normalized;
        CharacterController.Move(move + Vector3.down * gravity);
    }


    public void DecelerateTowards(Vector3 _targetDir, float _decelerationModule) {

        Vector3 accelerationVectorTemp = _targetDir;
        accelerationVectorTemp.y = 0;
        AccelerationVector = accelerationVectorTemp.normalized * AccelerationVector.magnitude;

        //Drag = _accelerationModule / _maxSpeed * Time.deltaTime;
        //VelocityVector -= VelocityVector * Drag;
        VelocityVector = (VelocityVector.magnitude - _decelerationModule * Time.deltaTime) * VelocityVector.normalized;
        move = Vector3.ClampMagnitude(VelocityVector * Time.deltaTime + 0.5f * AccelerationVector * Mathf.Pow(Time.deltaTime, 2), VelocityVector.magnitude); //Formula completa per un buon effetto fin dal primo frame
        nextPosition = transform.position + move;
        VelocityVector = move / Time.deltaTime;
        CharacterController.Move(move + Vector3.down * gravity);



        //Debug.DrawRay(transform.position, VelocityVector, Color.blue, 0.2f);
        //Debug.DrawRay(transform.position, AccelerationVector, Color.red, 0.2f);

    }

    public void AccelerateTowards(Vector3 _targetDir, float _accelerationModule) {

        Vector3 accelerationVectorTemp = _targetDir;
        accelerationVectorTemp.y = 0;
        AccelerationVector = accelerationVectorTemp.normalized * AccelerationVector.magnitude;

        //Drag = _accelerationModule / _maxSpeed * Time.deltaTime;
        //VelocityVector -= VelocityVector * Drag;
        VelocityVector = (VelocityVector.magnitude + _accelerationModule * Time.deltaTime) * VelocityVector.normalized;
        move = Vector3.ClampMagnitude(VelocityVector * Time.deltaTime + 0.5f * AccelerationVector * Mathf.Pow(Time.deltaTime, 2), VelocityVector.magnitude); //Formula completa per un buon effetto fin dal primo frame
        nextPosition = transform.position + move;
        VelocityVector = move / Time.deltaTime;
        CharacterController.Move(move + Vector3.down * gravity);

        //Debug.DrawRay(transform.position, VelocityVector, Color.blue, 0.2f);
        //Debug.DrawRay(transform.position, AccelerationVector, Color.red, 0.2f);

    }


    public void BounceDeceleration() {

        float vectorAngle = Vector3.SignedAngle(Vector3.forward, BounceVector.normalized, Vector3.up) * Mathf.Deg2Rad;
        BounceDecelerationVector = new Vector3(Mathf.Sin(vectorAngle) * BounceDecelerationModule, 0, Mathf.Cos(vectorAngle) * BounceDecelerationModule);

        BounceVector -= BounceDecelerationVector * Time.deltaTime;
        move = BounceVector * Time.deltaTime;
        CharacterController.Move(move + Vector3.down * gravity);

        Debug.DrawRay(transform.position, BounceVector, Color.black, .03f);
    }


    public void MovementReset() {

        move = Vector3.zero;
        nextPosition = Vector3.zero;
        AccelerationVector = Vector3.zero;
        VelocityVector = Vector3.zero;
        DecelerationVector = Vector3.zero;
        Drag = 0;
        AccelerationModule = 0;
        DecelerationModule = 0;

    }

    public void BounceMovement(ControllerColliderHit hit, BounceData bounceData) {

        #region Bounce variables
        //MovementBase collidingObject = hit.gameObject.GetComponent<MovementBase>();
        CharacterControllerMovementBase collidingObject = hit.gameObject.GetComponent<CharacterControllerMovementBase>();
        Vector3 normal;
        Vector3 vectorParal;
        Vector3 vectorPerp;
        Vector3 collisionVectorParal;
        Vector3 collisionVectorPerp;
        Vector3 bounceVector;
        float shakeAngle;
        #endregion
        if ((hit.collider.GetComponent<PlayerView>() || hit.collider.GetComponent<PlayerController>())) {
            collidingObject = player;
        }

        Vector3 fakeCollidingObjectPosition = new Vector3(collidingObject.transform.position.x, transform.position.y, collidingObject.transform.position.z);
        normal = (fakeCollidingObjectPosition - transform.position).normalized;
        shakeAngle = Vector3.Angle(transform.position, fakeCollidingObjectPosition);

        vectorParal = Vector3.Project(VelocityVector, normal);
        vectorPerp = Vector3.ProjectOnPlane(VelocityVector, normal);

        collisionVectorParal = Vector3.Project(collidingObject.VelocityVector, -normal);
        collisionVectorPerp = Vector3.ProjectOnPlane(collidingObject.VelocityVector, -normal);

        //Bounce formula
        bounceVector = (vectorParal * (Mass - collidingObject.Mass) + 2 * collidingObject.Mass * collisionVectorParal) / (Mass + collidingObject.Mass);
        VelocityVector = (bounceVector * (1 - KineticEnergyLoss)) + (vectorPerp * (1 - KineticEnergyLoss));
        //VelocityVector = Mathf.Clamp(VelocityVector.magnitude, MinBounceVector, MaxBounceVector) * VelocityVector.normalized;
        AccelerationVector = VelocityVector.normalized * AccelerationVector.magnitude;

        bounceVector = (collisionVectorParal * (collidingObject.Mass - Mass) + 2 * Mass * vectorParal) / (collidingObject.Mass + Mass);
        collidingObject.VelocityVector = (bounceVector * (1 - KineticEnergyLoss)) + collisionVectorPerp * (1 - SurfaceFriction);
        player.animator.SetTrigger("Stunned");

        cameraShakeManager.StartShake(shakeAngle, bounceData.noisePercent, bounceData.strength, bounceData.duration, bounceData.speed, bounceData.dampingPercent, bounceData.rotationPercent);

        Debug.DrawRay(transform.position, VelocityVector, Color.blue, 0.2f);
        Debug.DrawRay(collidingObject.transform.position, collidingObject.VelocityVector, Color.black, 0.2f);

    }

    public void WallBounce(ControllerColliderHit hit, float _angularVelocity, BounceData bounceData) {

        #region Bounce variables
        GameObject collidingObject = hit.collider.gameObject;

        Vector3 normal;
        Vector3 vectorParal;
        Vector3 vectorPerp;
        Vector3 bounceVector;
        float plusAngle;
        float shakeAngle;
        #endregion

        normal = -collidingObject.transform.forward;
        shakeAngle = Vector3.Angle(transform.position, collidingObject.transform.position);

        vectorParal = Vector3.Project(VelocityVector, normal);
        vectorPerp = Vector3.ProjectOnPlane(VelocityVector, normal);

        //Debug.DrawRay(transform.position, vectorParal, Color.red, 5);
        //Debug.DrawRay(transform.position, vectorPerp, Color.cyan, 5);

        //Bounce formula
        //Per il muro non serve andare a vedere la sua massa , ma basta dare la stessa massa dell'oggetto che urta
        bounceVector = (-2 * Mass * vectorParal) / (2 * Mass);
        bounceVector *= 1 - KineticEnergyLoss;
        VelocityVector = (bounceVector * (1 - KineticEnergyLoss)) + vectorPerp * (1 - SurfaceFriction);

        Debug.DrawRay(transform.position, VelocityVector, Color.cyan, .2f);
        plusAngle = -(_angularVelocity * impulseDeltaTime);



        VelocityVector = Quaternion.AngleAxis(plusAngle, Vector3.up) * VelocityVector;
        AccelerationVector = VelocityVector.normalized * AccelerationVector.magnitude;

        cameraShakeManager.StartShake(shakeAngle, bounceData.noisePercent, bounceData.strength, bounceData.duration, bounceData.speed, bounceData.dampingPercent, bounceData.rotationPercent);

        Debug.DrawRay(transform.position, VelocityVector, Color.blue, .02f);
        //Debug.DrawRay(transform.position, AccelerationVector, Color.red, .02f);

    }

}
