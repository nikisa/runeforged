﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class RuneTutorial : HookPointFix
{

    //Inspector
    [Header("Mask Values")]
    public FirstBossMask mask;
    public float angularMaxSpeed;
    public float negativeAngularMaxSpeed;
    public float angularAccelerationTime;
    public AnimationCurve DecelerationCurve;
    [Header("Mask MaterialValues")]
    public Material maskMaterialStatus;
    public Material maskCrystalMaterialStatus;
    [Header("Mask Material Alpha")]
    [Range(0f, 100)]
    public float maskMaterialIntensityValue;
    [Range(0.6f, 100)]
    public float maskCrystalMaterialIntensityValue;
    [Range(0f, 100)]
    public float maskMaterialIntensityValueSlowDown;
    [Range(0.6f, 100)]
    public float maskCrystalMaterialIntensityValueSlowDown;
    [Range(0f, 100)]
    public float maskMaterialIntensityValueStopped;
    [Range(0.6f, 100)]
    public float maskCrystalMaterialIntensityValueStopped;
    public float maskMaterialChangeDuration;
    public Ease maskMaterialAlphaEase;
    public float maskCrystalMaterialChangeDuration;
    public Ease maskCrystalMaterialAlphaEase;
    [Header("Crystal")]
    public CrystalTest crystal;
    public float nextScaleMultiplier;
    public float scaleTime;
    public float resetCrystalInSeconds;
    [Header("Crystal Color")]
    #region Color
    public Material material;
    public Ease easeColor;
    public float changeTime;
    #endregion


    //Public
    [HideInInspector]
    public bool isRuneActive;
    [HideInInspector]
    public bool isSlowingDown;
    [HideInInspector]
    public int iterations = 1;
    [HideInInspector]
    public float timer;
    [HideInInspector]
    public bool InvertRotation;
    [HideInInspector]
    public Vector3 nextScale;
    [HideInInspector]
    public float changeMaterialTimePercentage;
    [HideInInspector]
    public float changeMaskCrystalMaterialTimePercentage;
    [HideInInspector]
    [Range(0, 100)]
    public float actualMaskMaterialIntensity;
    [HideInInspector]
    [Range(0.6f, 100)]
    public float actualMaskCrystalMaterialIntensity;
    [HideInInspector]
    public bool isScaling;

    //Private

    private bool isResetting;
    private float crystalTimer;
    private Vector3 previousScale;
    #region Timer
    private bool activeTimer;
    private float time;
    #endregion
    #region Color values
    int intesityRegulator = 300;
    int intesityRegulatorBase = 200;
    bool isColorActive;
    float colorTimer;
    float timePercentage;
    #endregion
    #region Current Color
    float currentRed;
    float currentGreen;
    float currentBlue;
    #endregion
    #region First Color
    float oldRed = 34;
    float oldGreen = 191;
    float oldBlue = 172;
    #endregion
    #region Second Color
    float newRed = 255;
    float newGreen = 192;
    float newBlue = 0;
    #endregion

    private void Start() {
        if (crystal != null) {
            previousScale = crystal.transform.localScale;
        }
    }


    private void Update() {

        timer += Time.deltaTime;
        //Debug.Log("Timer in seconds: " + timer%60);


        changeMaterialTimePercentage = timer / maskMaterialChangeDuration;
        changeMaterialTimePercentage = Mathf.Clamp01(changeMaterialTimePercentage);

        changeMaskCrystalMaterialTimePercentage = timer / maskCrystalMaterialChangeDuration;
        changeMaskCrystalMaterialTimePercentage = Mathf.Clamp01(changeMaskCrystalMaterialTimePercentage);

        if (isRuneActive) {
            
            //mask.SlowDown(DecelerationCurve, timer - Time.deltaTime, timer, iterations);
            crystalTimer += Time.deltaTime; 
            nextScale = new Vector3(crystal.transform.localScale.x * nextScaleMultiplier , crystal.transform.localScale.y * nextScaleMultiplier, crystal.transform.localScale.z * nextScaleMultiplier);
            ChangeColorShader();

            if (!isScaling) {
                crystal.transform.DOScale(nextScale, scaleTime);
                isScaling = true;
            }

            if (isResetting) {
                crystal.transform.DOScale(previousScale, scaleTime);
                ResetColorShader();
                isResetting = false;
                crystalTimer = 0;
                isRuneActive = false;
            }

            if (crystalTimer > resetCrystalInSeconds) {
                isResetting = true;
            }
        }

        if(!isSlowingDown){
            if (!InvertRotation) {
                if (!isRuneActive) {
                    ResetColorShader();
                }
                if (mask != null) {
                    mask.RotateAroud(angularMaxSpeed , angularAccelerationTime);
                }
                maskMaterialStatus.SetFloat("Vector1_5031454D", DOVirtual.EasedValue(actualMaskMaterialIntensity, maskMaterialIntensityValue, changeMaterialTimePercentage, maskMaterialAlphaEase));
                maskCrystalMaterialStatus.SetFloat("Vector1_B11A500B", DOVirtual.EasedValue(actualMaskCrystalMaterialIntensity, maskCrystalMaterialIntensityValue, changeMaskCrystalMaterialTimePercentage, maskCrystalMaterialAlphaEase));
            }
            else {
                mask.RotateAroud(negativeAngularMaxSpeed, angularAccelerationTime);
                maskMaterialStatus.SetFloat("Vector1_5031454D", DOVirtual.EasedValue(actualMaskMaterialIntensity, maskMaterialIntensityValueSlowDown, changeMaterialTimePercentage, maskMaterialAlphaEase));
                maskCrystalMaterialStatus.SetFloat("Vector1_B11A500B", DOVirtual.EasedValue(actualMaskCrystalMaterialIntensity, maskCrystalMaterialIntensityValueSlowDown, changeMaskCrystalMaterialTimePercentage, maskCrystalMaterialAlphaEase));
            }
        }
    }


    void ChangeColorShader() {
        colorTimer += Time.deltaTime;
        timePercentage = Mathf.Clamp01(colorTimer / changeTime);
        currentRed = DOVirtual.EasedValue(oldRed, newRed, timePercentage, easeColor);
        currentGreen = DOVirtual.EasedValue(oldGreen, newGreen, timePercentage, easeColor);
        currentBlue = DOVirtual.EasedValue(oldBlue, newBlue, timePercentage, easeColor);

        Color newColor = new Color(currentRed, currentGreen, currentBlue, 0) / intesityRegulatorBase;
        material.SetColor("Color_928ADAC2", newColor);

    }

    void ResetColorShader() {
        colorTimer += Time.deltaTime;
        timePercentage = Mathf.Clamp01(colorTimer / changeTime);
        currentRed = DOVirtual.EasedValue(newRed, oldRed, timePercentage, easeColor);
        currentGreen = DOVirtual.EasedValue(newGreen, oldGreen, timePercentage, easeColor);
        currentBlue = DOVirtual.EasedValue(newBlue, oldBlue, timePercentage, easeColor);

        Color newColor = new Color(currentRed, currentGreen, currentBlue, 0) / intesityRegulator;
        material.SetColor("Color_928ADAC2", newColor);
    }


}
