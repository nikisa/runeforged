﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleGun : MonoBehaviour
{
    //Inspector
    public float RewindVector;
    public float RewindAcceleration;
    public float RewindTopSpeed;

    //Public
    //[HideInInspector]
    public bool nearHookPoint;
    //[HideInInspector]
    public HookPointBase hookPoint;

    private void OnTriggerEnter(Collider collision) {
        if (collision.transform.GetComponent<HookPointBase>()) {
            nearHookPoint = true;
            hookPoint = collision.transform.GetComponent<HookPointBase>();
        }
    }

    private void OnTriggerExit(Collider collision) {
        if (collision.transform.GetComponent<HookPointBase>()) {
            nearHookPoint = false;
            hookPoint = null;
        }
    }

}


