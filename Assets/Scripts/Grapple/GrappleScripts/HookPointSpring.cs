﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class HookPointSpring : HookPointBase
{
    //Inspector
    public Transform HookPointPivot;
    public BreakPointData[] BreakPoints;
    public float yOffset;
    public float tugMultiplier;
    [Header("Graphics")]
    public GameObject[] graphics;
    [Header("VFX")]
    public GameObject DamageVFX;
    [Header("SFX")]
    [SerializeField]
    [EventRef]
    private string brokenHookPointFX;

    //Public
    public GameObject actualGraphics;

    //Protected
    protected BreakPointData actualBreakPointData;
    [SerializeField]
    protected int BreakPointsCount = 0;
    protected float springVector;
    [SerializeField]
    protected float currentLife;
    protected float currentElasticK;
    protected float distance;
    protected Vector3 direction = Vector3.zero;
    protected Vector3 movement = Vector3.zero;

    private void Start()
    {
        #region SFX
        var soundInstance = RuntimeManager.CreateInstance(brokenHookPointFX);
        soundInstance.start();
        #endregion
    }

    public void PlayBrokenHookPointFX()
    {
        RuntimeManager.PlayOneShot(brokenHookPointFX);
    }

    public void Spring(float _maxTugMultiplier) {

        //Internal
        float lifeRatio;
        float deltaLength = 0;

        //Internal Player Values
        Vector3 playerVelocityVector = player.VelocityVector;
        Vector3 pullVector;
        float playerVelocityVectorMagnitude = player.VelocityVector.magnitude;
        float playerMaxSpeed = player.playerMovementData.maxSpeed;
        float pullMultiplier;
        float tugMultiplier;


        if (BreakPointsCount < BreakPoints.Length) {
            actualBreakPointData = BreakPoints[BreakPointsCount];
        }

        lifeRatio = Mathf.Clamp01(currentLife / actualBreakPointData.lifeMax);

        transform.position = hook.transform.position;

        deltaLength = (transform.position - HookPointPivot.transform.position).magnitude;

        //Debug.LogError("deltaLength: " + deltaLength);

        if (deltaLength != 0) {
            transform.position += .85f * -(transform.position - HookPointPivot.transform.position);
            hook.transform.position = transform.position;
        }

        if (playerVelocityVectorMagnitude >= playerMaxSpeed * 0.9f) {
            //Projection VelocityVector on HookPointPivot normal
            pullVector = Vector3.Project(playerVelocityVector, transform.forward);
            //Projection.magnitude divided by PlayerVelocityVectorMagnitude = PullMultiplier
            pullMultiplier = pullVector.magnitude / playerVelocityVectorMagnitude;
            //PlayerVelocityVectorMagnitude divided by PlayerMaxSpeed = Clamped(TugMultiplier , 1 , n)
            tugMultiplier = playerVelocityVectorMagnitude / playerMaxSpeed;
            tugMultiplier = Mathf.Clamp(tugMultiplier, 1, _maxTugMultiplier);
            //currentLife -= PullMultiplier * TugMultiplier * player.DPS * Time.DeltaTime
            currentLife -= pullMultiplier * tugMultiplier * player.DPS * Time.deltaTime;
            //Debug.Log("pullMultiplier: " + pullMultiplier);
            //Debug.Log("tugMultiplier: " + tugMultiplier);
            //Debug.Log("DPS: " + player.DPS);
            //Debug.Log("currentLife: " + player.DPS / Time.deltaTime);
        }

        if (currentLife <= 0) {

            DamageVFX.SetActive(false);
            DamageVFX.transform.position = transform.position;
            DamageVFX.SetActive(true);//Rimane a true ma quando deve ricomparire il VFX viene risettata a false momentaneamente per far partire StartOnAwake del VFX


            PlayBrokenHookPointFX();
            BreakPointsCount++;

            if (BreakPointsCount < graphics.Length) {
                currentLife = BreakPoints[BreakPointsCount].lifeMax;
                GameObject actualMesh = Instantiate(graphics[BreakPointsCount].gameObject, transform.position - new Vector3(0, yOffset , 0), transform.rotation);
                actualGraphics = actualMesh;
                actualMesh.transform.SetParent(transform);
            }

            if (BreakPointsCount == graphics.Length - 1 && transform.childCount > 0 && currentLife < 0) {
                transform.GetChild(0).gameObject.transform.position = hook.transform.position;
            }
            else {
                //Destroy(transform.GetChild(1).gameObject);
                transform.GetChild(1).gameObject.SetActive(false);
            }

            if (BreakPointsCount == graphics.Length && transform.childCount > 0 && currentLife < 0) {
                //bossAnimator.SetTrigger("MaskDestroyed");

                //Destroy(transform.GetChild(0).gameObject);
                player.grappleManager.hook.isHooked = false;
                player.grappleManager.hookPoint.isHooked = false;
                player.grappleManager.hook.transform.parent = player.grappleManager.GrappleGun.transform;
                player.grappleManager.animator.SetTrigger("Rewind");
                //bossAnimator.ResetTrigger("MaskDestroyed");
                transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }

}
