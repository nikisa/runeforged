﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class GrappleManager : MonoBehaviour
{
    // Inspector
    public bool debugMode;

    [Header("ROPE GENERALS")]
    [Min(1)] public int MassPointNumber = 32;
    [Min(0.01f)] public float MassPointLength = 0.25f;
    [Min(0.1f)] public float Mass = 0.1f;
    [Min(0f)] public float DragCoefficent = 1.2f;
    [Min(0f)] public float Drag = 0.1f;
    [Min(0f)] public float cFriction = 1.4f;
    public float RopeWidth = 0.1f;
    public float GravityAcceleration = 9.8f;
    private Vector3 GravityVector;
    public float CollisionOffset = 0.0001f;
    public LayerMask nodeLayerMask;
    public LayerMask hookLayerMask;
    [Range(0, 1)] public float Elasticity = 0.2f;
    [Range(0, 1)] public float chainConstraintCollision = 0.66f;


    //VARIABILI NUOVE BY LUCA WRIGHT
    [Header("ROPE MATERIALS")]
    public Material NotTightRope;
    public Material TightRope;
    public Material UnwindRope;


    [Header("ON SHOT FIRED: SPIRAL PROPERTIES")]
    [Min(0.01f)] public float SpiralRay;
    [Min(1f)] public float SpiralTurns;
    [Header("HOOK SPEED")]
    public float HookMaxSpeed;
    [Min(1)] public int ApplyConstraintsIterations = 1;
    [Header("REWIND")]
    public float RewindAccelerationTime;
    public float RewindMaxSpeed;

    public float RewindSpatialMagnitude;
    public float RewindVelocityMagnitude;
    public float RewindAccelerationMagnitude;
    //private int TotalPointsRewinded;
    //private int PointsRewinded;
    //private float RopeRewinded;

    [Header("REFERENCES")]
    public FieldOfView FovPlayer;
    public GrappleGun GrappleGun;
    public Hook hook;
    [HideInInspector]
    public HookPointBase hookPoint;
    public PlayerController Player;
    public CrystalController[] Crystals;
    public GameObject CrossHair;
    public Animator animator;
    [HideInInspector]
    public bool IsSet;
    [HideInInspector]
    public GameObject target;

    [Header("SFX")]
    [SerializeField]
    [EventRef]
    private string chainHookedFX;
    [SerializeField]
    [EventRef]
    private string chainGrabFX;
    [SerializeField]
    [EventRef]
    private string chainShootFX;
    [SerializeField]
    [EventRef]
    private string chainUnhookFX;
    [SerializeField]
    [EventRef]
    private string chainRewindFX;
    [SerializeField]
    [EventRef]
    private string chainTugFX;
    [SerializeField]
    [EventRef]
    private string chainUnwindFX;
    [SerializeField]
    [EventRef]
    private string punchFX;
    [SerializeField]
    [EventRef]
    private string attackFX;

    [Header("Debugger")]
    public float totalRope;
    public int TotalPointsRewinded;
    public int PointsRewinded;
    public float RopeRewinded;
    public int LastRopeNode;
    public RopeNode lastNodeOnCollision;
    public float RolledUpHookPos;
    public ArrayList RopeNodes;

    private float FixedDeltaTime;


    //Public
    [HideInInspector]
    public LineRenderer lineRenderer;


    // Private
    //private ArrayList RopeNodes;
    private Vector3 HookPosition;
    private Vector3 HookScale;
    private Quaternion HookRotation;
    private GameObject HookParent;
    private RaycastHit hit;
    //Line renderer


    //Private SFX
    private FMOD.Studio.EventInstance soundHookedInstance;
    private FMOD.Studio.EventInstance soundAttackInstance;
    private string busChain = "bus:/ChainIdle";



    private void Start() {
        //Application.targetFrameRate = 60;
        RopeNodes = new ArrayList();
        //[old] 
        hook.currentTag = 0;
        HookParent = hook.transform.parent.gameObject;
        HookPosition = hook.transform.localPosition;
        HookScale = hook.transform.localScale;
        HookRotation = hook.transform.localRotation;
        //[/old] Settaggi utili per HookReset()?
        RewindAccelerationMagnitude = RewindMaxSpeed / RewindAccelerationTime; //Calcolo: Forza di Riavvolgimento (Accelerazione) della Grapple Gun
        GravityVector = new Vector3(0, -GravityAcceleration, 0);

        foreach (var item in animator.GetBehaviours<GrappleBaseState>()) {
            item.SetContext(this, animator, this, hook);
        }
        SetFovRadius();
        Player = FindObjectOfType<PlayerController>();

        RolledUpHookPos = (hook.transform.position - GrappleGun.transform.position).magnitude; //Registra la posizione iniziale dell'Hook rispetto a Gun
        //[new]
        lineRenderer = this.GetComponent<LineRenderer>();
        lineRenderer.material = NotTightRope;

        #region SFX
        //var soundGrabInstance = RuntimeManager.CreateInstance(chainGrabFX);
        //soundGrabInstance.start();
        //var soundShootInstance = RuntimeManager.CreateInstance(chainShootFX);
        //soundShootInstance.start();
        //var soundUnhookInstance = RuntimeManager.CreateInstance(chainUnhookFX);
        //soundUnhookInstance.start();
        //var soundRewindInstance = RuntimeManager.CreateInstance(chainRewindFX);
        //soundRewindInstance.start();
        //var soundTugInstance = RuntimeManager.CreateInstance(chainTugFX);
        //soundTugInstance.start();
        //var soundUnwindInstance = RuntimeManager.CreateInstance(chainUnwindFX);
        //soundUnwindInstance.start();
        //var soundPunchInstance = RuntimeManager.CreateInstance(punchFX);
        //soundPunchInstance.start();
        #endregion

        InstantiateRope();
    }

    #region  SOUNDS /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void PlayGrabFX() {
        RuntimeManager.PlayOneShot(chainGrabFX);
    }

    public void PlayShootFX() {
        RuntimeManager.PlayOneShot(chainShootFX);
    }

    public void PlayUnhookFX() {
        RuntimeManager.PlayOneShot(chainUnhookFX);
    }

    public void PlayRewindFX() {
        RuntimeManager.PlayOneShot(chainRewindFX);
    }
    public void PlayTugFX() {
        RuntimeManager.PlayOneShot(chainTugFX);
    }

    public void PlayUnwindFX() {
        RuntimeManager.PlayOneShot(chainUnwindFX);
    }

    public void PlayPunchFX() {
        RuntimeManager.PlayOneShot(chainUnwindFX);
    }

    public void StartHookedFX() {
        soundHookedInstance = RuntimeManager.CreateInstance(chainHookedFX);
        soundHookedInstance.start();
        RuntimeManager.PlayOneShot(chainHookedFX);
    }

    public void StopHookedFX() {
        soundHookedInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        soundHookedInstance.release();
    }
    public void StartAttackFX() {
        soundAttackInstance = RuntimeManager.CreateInstance(attackFX);
        soundHookedInstance.start();
        RuntimeManager.PlayOneShot(chainHookedFX);
    }

    public void StopAttackFX() {
        soundAttackInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        soundAttackInstance.release();
    }

    public void StopChainIdleEvent() {
        FMOD.Studio.Bus playerBus = FMODUnity.RuntimeManager.GetBus(busChain);
        playerBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    #endregion


    public void InstantiateRope() //On Start: Genera tutti i RopeNode e li disattiva
    {
        for (int i = 0; i < MassPointNumber; i++) {
            RopeNode node = (GameObject.Instantiate(Resources.Load("RopeNode") as GameObject)).GetComponent<RopeNode>();
            RopeNodes.Add(node);
            node.transform.gameObject.SetActive(false);
        }
    }

    ////////////////ELENCO DEI CAMBIAMENTI EXTRACODICE:
    ////////////////01 Sostituito Hook (La forward ora è orientata verso la punta della freccia)
    ////////////////02 Spostato Gun più avanti e reso invisibile
    ////////////////03 Nuova State Machine con nuove condizioni e nuovi stati

    #region STARTING FUNCTIONS ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void OnShootingStart() //On Shooting State Start: Lo Spatial Vector dell'Hook viene aggiornato secondo il valore di velocità, per poi muoversi per inerzia tramite la funzione UpdateHook();
    {
        hook.OldPos = hook.transform.position;
        hook.VelocityVector = hook.transform.forward * HookMaxSpeed;
        //FixedDeltaTime = Time.deltaTime;
        //hook.transform.position += hook.VelocityVector * Time.deltaTime;
        hook.SpatialVector = hook.VelocityVector * Time.deltaTime;
        hook.isBullet = true; //L'Hook ha due stati: isBullet attiva le collisioni tra Hook e oggetti HookPoint;
        hook.transform.parent = this.transform; //L'Hook non sarà più parente di Gun fino a che non torna in RolledUpState;
        CheckHookCollision(hook , hook.SpatialVector);
    }

    public void ShootingSpiral() //On Shooting State Start: Attiva tutti i nodi e li pone a spirale tra Hook e Gun;
    {
        RopeNode massPoint;
        float distanceGrapnelToHook = (hook.transform.position - GrappleGun.transform.position).magnitude;
        float period = 360 * SpiralTurns / MassPointNumber;

        for (int i = 0; i < MassPointNumber; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            massPoint.transform.gameObject.SetActive(true);
            massPoint.transform.position = hook.transform.position -
                                           hook.transform.forward * (distanceGrapnelToHook / MassPointNumber) * (i) +
                                           hook.transform.right * SpiralRay * Mathf.Cos((i * period) * Mathf.Deg2Rad) +
                                           hook.transform.up * SpiralRay * Mathf.Sin((i * period) * Mathf.Deg2Rad);
            massPoint.transform.parent = this.transform;
            massPoint.SpatialVector = (massPoint.transform.position - GrappleGun.transform.position); //Aggiorna lo Spatial Vector di ogni nodo sulla base del vettore Gun-Nodo;
            massPoint.VelocityVector = (massPoint.transform.position - GrappleGun.transform.position) / Time.deltaTime;
            massPoint.VelocityMagnitude = massPoint.VelocityVector.magnitude;

            //massPoint.AccelerationVector = Vector3.zero;
            //NEW
            massPoint.AccelerationVector = GravityVector;
        }
        //[old]
        //hook.isShooted = true; //Necessaria? Non credo
        //[/old]
        LastRopeNode = RopeNodes.Count; //Memorizza: indice (+1) dell'ultimo Rope Node istanziato (serve per i calcoli in cui la corda è parzialmente riavvolta);
    }
    #endregion

    #region UPDATE FUNCTIONS (ADVANCE & CLASSIC SIMULATION) ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #region //////// Advanced Update Points Verlet Simultation (using Kinematics)
    #endregion
    #region //////// CLASSIC UPDATE POINTS Verlet Simulation
    //public void UpdatePoints()
    //{
    //    //float DynamicFriction = 0.15f;
    //    RopeNode massPoint;
    //    float gravity = 9.8f;
    //    float drag = 0.01f;

    //    for (int i = 0; i < RopeNodes.Count; i++)
    //    {
    //        massPoint = RopeNodes[i] as RopeNode;
    //        massPoint.OldPos = massPoint.transform.position;
    //        //Verifica collisioni
    //        massPoint.transform.position += massPoint.SpatialVector * (1 - drag) + Vector3.down * (0.5f * gravity * Mathf.Pow(Time.deltaTime, 2)); //Aggiunta la gravità
    //        //massPoint.transform.position += massPoint.SpatialVector * (1 - drag) + Vector3.down * gravity * Time.deltaTime; //Aggiunta la gravità
    //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
    //        //Calcola SPATIAL VECTOR
    //        //Verifica collisioni e applica a SpatialVector una Drag corrispondete al suo quadrato.
    //    }
    //}
    #endregion

    public void UpdatePoints() {
        //float DynamicFriction = 0.15f;
        RopeNode massPoint;

        FixedDeltaTime = Time.deltaTime;

        for (int i = 0; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            //massPoint.SpatialVector += 0.5f * (massPoint.InitialAcceleration + massPoint.AccelerationVector) * FixedDeltaTime;
            massPoint.OldPos = massPoint.transform.position;
            massPoint.SpatialVector = massPoint.SpatialVector + 0.5f * GravityVector * Mathf.Pow(FixedDeltaTime, 2);
            massPoint.currentHitObject = null;
            CheckCollision(massPoint, massPoint.SpatialVector, FixedDeltaTime, massPoint.MassPointRadius);
        }
    }

    void DrawNormal(Vector3 _start, Vector3 _dir, float _time) {
        Debug.DrawRay(_start, _dir, Color.blue, _time);
    }

    void CheckCollision(RopeNode _massPoint, Vector3 _spatialVector, float _deltaTime, float _nodeRadius) {
        RaycastHit hit;
        Vector3 _vectorDir = _spatialVector.normalized;
        float _spatialMagnitude = _spatialVector.magnitude;

        //Se collide:
        //Calcola la percentuale di Drag da applicare a fine ciclo;
        //Esegui bounce (DIRETTO): verifica che Spatial * Elasticity sia superiorie a X e, se sì, fai reflection;
        if (Physics.SphereCast(_massPoint.transform.position, _nodeRadius, _vectorDir, out hit, _spatialMagnitude, nodeLayerMask, QueryTriggerInteraction.UseGlobal)) {

            DrawNormal(hit.point, hit.normal, 0.02f);

            //Calcola lo SpatialVector percorso prima della collisione
            Vector3 hitSpatialVector = hit.distance * _vectorDir + CollisionOffset * hit.normal; //Allontana l'oggetto di un vettore fisso sulla normale per garantire il check delle collisioni future (se la distanza è 0, lo spherecast non le rileva).

            //Scompone il _deltaTime in entrata (FixedDeltaTime, se è la prima volta che entra) in due deltaTime:
            float hitDeltaTime = hit.distance / _spatialMagnitude * _deltaTime; //il deltaTime prima della collisione;
            float slopeDeltaTime = _deltaTime - hitDeltaTime; //il deltaTime dopo la collisione;

            Vector3 slopeSpatialVector = Vector3.ProjectOnPlane((_spatialVector - hitSpatialVector), hit.normal); //Componente di SpatialVector post collisione proiettata sulla perpendicolare alla Normale (la componente parallela è annullata dalla Normale stessa).
            Vector3 slopeDir = slopeSpatialVector.normalized;

            if (_massPoint.currentHitObject == null) {
                ApplyDrag(_massPoint, hitSpatialVector); //Calcola l'Accelerazione dell'Attrito dell'Aria per il deltaTime pre-collisione.
            }
            else {
                ApplyFriction(_massPoint, hit.normal, slopeDir, hitDeltaTime); //Se già in collisione, calcola l'Accelerazione dell'Attrito Radente prima della nuova collisione (dalla seconda in poi).
            }

            _massPoint.transform.position += hitSpatialVector; //Sposta Node del VettoreSpaziale precollisione.
            _massPoint.currentHitObject = hit.transform.gameObject; //Memorizza hitObject, il Game Object contro il quale è avvenuta la collisione.
            _massPoint.collisionCount += 1;

            //CheckCollision(_massPoint, Vector3.Reflect(_spatialVector - hitSpatialVector, hit.normal), slopeDeltaTime, _massPoint.MassPointRadius);
            CheckCollision(_massPoint, slopeSpatialVector, slopeDeltaTime, _massPoint.MassPointRadius);
        }
        else {
            //Se non ci sono state collisioni...
            if (_massPoint.currentHitObject == null) {
                _massPoint.transform.position += _spatialVector; //Muove il Node lungo SpatialVector
                _massPoint.SpatialVector = (_massPoint.transform.position - _massPoint.OldPos);
                ApplyDrag(_massPoint, _spatialVector); //Calcola le forze dovute all'Attrito dell'Aria (Drag);


            }
            //Se ci sono state collisioni...
            else {
                //Vector3 bounceDir = Vector3.Reflect(_massPoint.SpatialVector.normalized, _massPoint.HitNormal);
                _massPoint.transform.position += _spatialVector;
                _massPoint.SpatialVector = (_massPoint.transform.position - _massPoint.OldPos);
            }
        }
    }

    void ApplyDrag(RopeNode massPoint, Vector3 _spatialVector) {
        if (massPoint.SpatialVector != Vector3.zero) {
            massPoint.percentDrag = 0.5f * Drag * Mathf.Pow(_spatialVector.magnitude, 2) / massPoint.SpatialVector.magnitude;
        }
    }


    void ApplyFriction(RopeNode _massPoint, Vector3 _normal, Vector3 _slopeDir, float _deltaTime) {

        Vector3 frictionForce = cFriction * Vector3.Project(_massPoint.SpatialVector, _normal);
        float frictionDeceleration = frictionForce.magnitude;

        _massPoint.percentFriction = Vector3.ClampMagnitude(cFriction * Vector3.Project(_massPoint.SpatialVector, _normal), Vector3.ProjectOnPlane(_massPoint.SpatialVector, _normal).magnitude).magnitude / _massPoint.SpatialVector.magnitude;
    }

    public void ApplyIntegration() {
        RopeNode massPoint;

        bool previousHasCollided = false;

        for (int i = 0; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;

            massPoint.SpatialVector *= (1 - Mathf.Clamp01(massPoint.percentDrag + massPoint.percentFriction)) * Mathf.Pow(Elasticity, massPoint.collisionCount);

            if (previousHasCollided) {
                massPoint.SpatialVector = new Vector3(massPoint.SpatialVector.x * chainConstraintCollision, massPoint.SpatialVector.y, massPoint.SpatialVector.z * chainConstraintCollision);
            }

            if (massPoint.collisionCount > 0) {
                previousHasCollided = true;
            }
            //else
            //{
            //    previousHasCollided = false;
            //}

            massPoint.percentDrag = 0;
            massPoint.percentFriction = 0;
            massPoint.collisionCount = 0;


        }
    }

    #endregion

    #region UPDATE HOOK ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void UpdateHook()
    {
        float drag = 0.5f;


        if (!hook.isHooked)
        {
            if (hook.isRopeNode) //L'Hook è trattato alla stregua di un RopeNode. Si applica la gravità e non si può agganciare agli HookPoint;
            {
                hook.OldPos = hook.transform.position;
                hook.transform.position += hook.SpatialVector * (1 - drag) + 0.5f * GravityVector * Mathf.Pow(Time.deltaTime, 2);
                hook.SpatialVector = (hook.transform.position - hook.OldPos);
            }

            if (hook.isBullet) //Se isBullet, le collisioni tra Hook e gli oggetti HookPoint sono attive. Non si applica la gravità e l'attrito;
            {
                hook.OldPos = hook.transform.position;
                CheckHookCollision(hook , hook.SpatialVector);
            }
        }

        else //Quando l'Hook isBullet si aggancia all'HookPoint, diventa automaticamente RopeNode;
        {
            hook.isBullet = false;
            hook.isRopeNode = true;
        }
    }

    public void CheckHookCollision(Hook _origin, Vector3 _spatialVector)
    {
        RaycastHit hit;
        Vector3 normal;
        Vector3 _vectorDir = _spatialVector.normalized;
        float _spatialMagnitude = _spatialVector.magnitude;

        //if (GrappleGun.nearHookPoint) {
        //    hook.isHooked = true;
        //    hook.currentHitObject = GrappleGun.hookPoint.transform.gameObject;
        //    hookPoint = hook.currentHitObject.GetComponent<HookPointBase>();
        //    hookPoint.isHooked = true;
        //    hook.currentTag = hookPoint.hookPointTag;
        //    if (hookPoint.bossRelated) {
        //        hookPoint.boss.animator.SetInteger("HookPointTag", hook.currentTag);
        //        hookPoint.boss.isHooked = true;
        //        hookPoint.boss.UpdateHookedStateTrigger(hookPoint.boss.animator);
        //        hookPoint.boss.UpdateHookedState(hookPoint.boss.animator);
        //    }

        //    hook.transform.position = GrappleGun.hookPoint.transform.position;
        //}
        //else {
            if (Physics.SphereCast(_origin.transform.position, _origin.transform.localScale.x , _vectorDir, out hit, _spatialMagnitude, hookLayerMask)) {
                if (hit.transform.gameObject.GetComponent<HookPointBase>()) {
                    hook.isHooked = true;
                    hook.currentHitObject = hit.transform.gameObject;
                    hookPoint = hook.currentHitObject.GetComponent<HookPointBase>();
                    hookPoint.isHooked = true;
                    hook.currentTag = hookPoint.hookPointTag;
                    if (hookPoint.bossRelated) {
                        if (hookPoint.boss != null) {
                            hookPoint.boss.animator.SetInteger("HookPointTag", hook.currentTag);
                            hookPoint.boss.isHooked = true;
                            hookPoint.boss.UpdateHookedStateTrigger(hookPoint.boss.animator);
                            hookPoint.boss.UpdateHookedState(hookPoint.boss.animator);

                        }
                        else if (hookPoint.bossCustom != null) {
                            hookPoint.bossCustom.animator.SetInteger("HookPointTag", hook.currentTag);
                            hookPoint.bossCustom.isHooked = true;
                            hookPoint.bossCustom.UpdateHookedStateTrigger(hookPoint.bossCustom.animator);
                            hookPoint.bossCustom.UpdateHookedState(hookPoint.bossCustom.animator);
                        }
            }

                    hook.transform.position = hit.transform.position;
                }
            }
            else {
                hook.transform.position += hook.SpatialVector;
                hook.SpatialVector = (hook.transform.position - hook.OldPos);
                //Debug.Log("SV: " + hook.SpatialVector);
            }
        //} 
    }

    #endregion


    public void HookConstraints() { //AGGIORNARE?

        RopeNode massPoint = RopeNodes[0] as RopeNode;
        //float pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;
        //float error;
        //Vector3 direction = Vector3.zero;
        //Vector3 movement = Vector3.zero;


        //if (pointsDistance > MassPointLength)
        //{
        //    if (hook.isBullet)
        //    {
        //        //sposta nodo zero verso hook
        //        error = pointsDistance - MassPointLength;
        //        direction = (hook.transform.position - massPoint.transform.position).normalized;
        //        movement = error * direction;
        //        massPoint.transform.position += movement;
        //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);                            
        //    }
        //    else if (hook.isRopeNode && !hook.isHooked)
        //    {
        //        error = pointsDistance - MassPointLength;
        //        direction = (massPoint.transform.position - hook.transform.position).normalized;
        //        movement = error * direction;
        //        hook.transform.position += movement;
        //        hook.SpatialVector = (hook.transform.position - massPoint.OldPos);
        //        hook.transform.rotation = Quaternion.LookRotation(-direction);                
        //    }
        //}

        if (hook.isHooked) {
            //if (hook.isRopeFinished)
            //{
            //    error = pointsDistance - MassPointLength;
            //    direction = (massPoint.transform.position - hook.transform.position).normalized;
            //    movement = error * direction;
            //    hook.transform.position += movement;
            //    hook.SpatialVector = (hook.transform.position - massPoint.OldPos);
            //}

            //IF INPUT DI MOVIMENTO!

            //if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) //Non è detto che così risolva il problema, perché ci sarà un minimo di input di movimento dovuto al rewind della corda
            //{
            hook.pullDirection = Vector3.ProjectOnPlane((hook.transform.position - massPoint.transform.position), Vector3.up).normalized;

            if (hookPoint.GetComponent<HookPointSpring>()) {
                hookPoint.GetComponent<HookPointSpring>().Spring(hookPoint.GetComponent<HookPointSpring>().tugMultiplier);
            }
            else if (hookPoint.GetComponent<HookPointFix>()) {
                if (hookPoint.bossRelated) {
                    if (hookPoint.boss != null) {
                        hookPoint.GetComponent<HookPointFix>().VerletBossRigidBody(hookPoint.boss);
                    }
                    else if (hookPoint.bossCustom != null) {
                        hookPoint.GetComponent<HookPointFix>().VerletBossRigidBody(hookPoint.bossCustom);

                    }
                }
                else {
                    hook.transform.position = hookPoint.transform.position;
                }
            }
            //}
            //else
            //{
            //    hook.transform.position = hookPoint.transform.position;
            //}


        }
    }

    //public void HookConstraints() {

    //    if (hook.isHooked) {
    //        if (hookPoint.GetComponent<HookPointSpring>()) {
    //            hookPoint.GetComponent<HookPointSpring>().Spring(hookPoint.GetComponent<HookPointSpring>().tugMultiplier);
    //        }
    //        else if (hookPoint.GetComponent<HookPointFix>()) {
    //            if (hookPoint.bossRelated) {
    //                hookPoint.GetComponent<HookPointFix>().VerletBossRigidBody(hookPoint.boss);
    //            }
    //            else {
    //                hook.transform.position = hookPoint.transform.position;
    //            }
    //        }
    //    }
    //}

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(hook.transform.position + hook.SpatialVector, hook.transform.localScale);

        //Per la capsula fare due sfere
    }


    #region APPLY CONSTRAINTS /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //private void ApplyConstraints(RopeNode massPoint, RopeNode previousMassPoint) //Funzione generale per Apply Contraints
    //{
    //    float error;
    //    Vector3 movement = Vector3.zero;
    //    Vector3 direction = Vector3.zero;
    //    Vector3 distance = (previousMassPoint.transform.position - massPoint.transform.position);

    //    if (distance.magnitude > MassPointLength)
    //    {
    //        error = distance.magnitude - MassPointLength;
    //        direction = distance.normalized;
    //        movement = error * direction;
    //        massPoint.transform.position += movement;
    //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
    //    }
    //    /*[To Do]
    //    Controllo: distance < diametro del RopeNode;
    //    Controllo: posizione del RopeNode nello Spazio di Possibilità rispetto al vincolo opposto (se funzione parte da Hook, il vincolo opposto è Gun -- e viceversa).
    //    Formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8
    //    [/To Do]*/
    //}
    #endregion

    private void ApplyConstraints(RopeNode massPoint, RopeNode previousMassPoint) //Funzione generale per Apply Contraints
    {
        float error;
        Vector3 movement = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 distance = (previousMassPoint.transform.position - massPoint.transform.position);

        if (distance.magnitude > MassPointLength) {
            error = distance.magnitude - MassPointLength;
            direction = distance.normalized;
            movement = error * direction;
            CheckCollision(massPoint, movement, FixedDeltaTime, massPoint.MassPointRadius);
            //previousMassPoint.collisionCount += 1;
        }
    }

    private void SaveLastNodeOnCollision(RopeNode massPoint)
    {
        if (massPoint.currentHitObject !=null && massPoint.currentHitObject.layer == 10)
        {
            lastNodeOnCollision = massPoint;
        }
        //else
        //{
        //    lastNodeOnCollision = null;
        //}
    }
    

    #region FIRE: APPLY CONSTRAINTS PC to Hook ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Fire_ApplyConstraintsPCToHook() {
        float pointsDistance = 0;
        float error = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;
        RopeNode massPoint = RopeNodes[LastRopeNode - 1] as RopeNode;
        RopeNode previousMassPoint;

        Vector3 nodeOnCollisionToPlayer;

        if (lastNodeOnCollision != null && hook.isRopeFinished)
        {
            nodeOnCollisionToPlayer = Vector3.ProjectOnPlane((Player.transform.position - lastNodeOnCollision.transform.position), Vector3.up);

            int index = RopeNodes.IndexOf(lastNodeOnCollision);
            float remainingRope = MassPointLength * (LastRopeNode - index + 1);
            if (nodeOnCollisionToPlayer.magnitude > remainingRope)
            {
                ////MOVIMENTO SU ARCO
                //Vector3 arcStart = Player.transform.position - Player.VelocityVector * Time.deltaTime; //Se ci fosse la oldPos sarebbe meglio;
                //Vector3 vectorStart = Vector3.ProjectOnPlane((arcStart - lastNodeOnCollision.transform.position), Vector3.up);
                //Debug.DrawRay(lastNodeOnCollision.transform.position, vectorStart, Color.white, 1f);

                ////2pi*r : l = 2pi : deltaAngle --> deltaAngle = l / r
                //float deltaAngle = (Player.VelocityVector.magnitude / remainingRope) * Mathf.Sign(Vector3.SignedAngle(vectorStart, nodeOnCollisionToPlayer, Vector3.up));
                ////vectorStart = Quaternion.AngleAxis(deltaAngle, Vector3.up) * vectorStart;
                //vectorStart = Quaternion.AngleAxis(deltaAngle, Vector3.up) * vectorStart.normalized * (vectorStart.magnitude + MassPointLength);
                //Debug.DrawRay(lastNodeOnCollision.transform.position, vectorStart, Color.white, 1f);

                //Player.transform.position = new Vector3(lastNodeOnCollision.transform.position.x + vectorStart.x, Player.transform.position.y, lastNodeOnCollision.transform.position.z + vectorStart.z);
                //Debug.DrawLine(new Vector3(arcStart.x, lastNodeOnCollision.transform.position.y, arcStart.z), new Vector3(Player.transform.position.x, lastNodeOnCollision.transform.position.y, Player.transform.position.z), Color.green, 1f);

                //CONTROLLO LINEARE
                error = nodeOnCollisionToPlayer.magnitude - remainingRope;
                direction = -nodeOnCollisionToPlayer.normalized;
                Player.CharacterController.enabled = false;
                Player.transform.position += error * direction;
                Player.CharacterController.enabled = true;

            }
        }

        Vector3 SumOfLengths = Vector3.zero;
        totalRope = 0;

        pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

        if (pointsDistance > MassPointLength) {
            error = pointsDistance - MassPointLength;
            direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }
        /*[To Do]
        Controllo: distance < diametro del RopeNode;
        Controllo: posizione del RopeNode nello Spazio di Possibilità rispetto al vincolo opposto (se funzione parte da Hook, il vincolo opposto è Gun -- e viceversa).
        Formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8
        [/To Do]*/

        SumOfLengths += (massPoint.transform.position - GrappleGun.transform.position);

        totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - GrappleGun.transform.position), Vector3.down).magnitude;

        for (int i = LastRopeNode - 1; i > 0; i--) {
            massPoint = RopeNodes[i - 1] as RopeNode;
            previousMassPoint = RopeNodes[i] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SumOfLengths += (massPoint.transform.position - previousMassPoint.transform.position);
            totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - previousMassPoint.transform.position), Vector3.down).magnitude;
        }

        massPoint = RopeNodes[0] as RopeNode;
        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - hook.transform.position), Vector3.down).magnitude;

        //if ((SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude >= MassPointLength * (RopeNodes.Count - TotalPointsRewinded)) {
        //    //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
        //    //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
        //    //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
        //    hook.isRopeFinished = true;
        //    hook.isBullet = false;
        //    hook.isRopeNode = true;
        //    lineRenderer.material = NotTightRope;
        //}
        //else {
        //    hook.isRopeFinished = false;
        //    lineRenderer.material = NotTightRope;
        //}

        if (totalRope >= MassPointLength * (RopeNodes.Count + 1 - TotalPointsRewinded)) {
            //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
            //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
            //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
            hook.isRopeFinished = true;
            hook.isBullet = false;
            hook.isRopeNode = true;
            lineRenderer.material = NotTightRope;
        }
        else {
            hook.isRopeFinished = false;
            lineRenderer.material = NotTightRope;
            lastNodeOnCollision = null;
        }

        if (hook.isRopeNode)
        {
            if (hook.isHooked)
            {
                //hookPoint.HookPointSpring();
            }
            else
            {
                error = pointsDistance - MassPointLength;
                direction = (massPoint.transform.position - hook.transform.position).normalized;
                movement = error * direction;
                hook.transform.position += movement;
                hook.SpatialVector = (hook.transform.position - massPoint.OldPos);
                hook.transform.rotation = Quaternion.LookRotation(-direction);
            }
        }
    }
    #endregion
    #region FIRE: APPLY CONSTRAINTS Hook to PC ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Fire_ApplyConstraintsHookToPC()
    {
        Vector3 move;

        float pointsDistance = 0;
        float error = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;

        RopeNode previousMassPoint;
        RopeNode massPoint = RopeNodes[0] as RopeNode;

        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        if (pointsDistance > MassPointLength)
        {
            error = pointsDistance - MassPointLength;
            direction = (hook.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }

        for (int i = 1; i < LastRopeNode; i++)
        {
            massPoint = RopeNodes[i] as RopeNode;
            previousMassPoint = RopeNodes[i - 1] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SaveLastNodeOnCollision(massPoint);
        }

        //DA VERIFICARE
        massPoint = RopeNodes[LastRopeNode - 1] as RopeNode;
        pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

        //TUTTA DA RIVEDERE
        if (pointsDistance > MassPointLength)
        {
            if (hook.isRopeFinished && hook.isHooked)
            {
                error = pointsDistance - MassPointLength;
                direction = (massPoint.transform.position - GrappleGun.transform.position).normalized;
                movement = error * direction;
                //Debug.Log("Velocity Vector: " + Player.VelocityVector.magnitude);
                Player.CharacterController.enabled = false;
                //move = Player.transform.position + new Vector3(movement.x, /* 1.5f */ 0, movement.z);
                //Player.transform.position += new Vector3(movement.x, /* 1.5f */ 0, movement.z);
                Player.transform.position += Vector3.ProjectOnPlane(movement, Vector3.up);
                Player.CharacterController.enabled = true;
                //Debug.Log("Distanza: " + (GrappleGun.transform.position - massPoint.transform.position).magnitude);
            }
            else
            {
                error = pointsDistance - MassPointLength;
                direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
                movement = error * direction;
                massPoint.transform.position += movement;
                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
                pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
            }
        }

        //if (pointsDistance > MassPointLength)
        //{
        //    if (hook.isRopeFinished && hook.isHooked)
        //    {
        //        Vector3 nodeOnCollisionToPlayer;

        //        if (lastNodeOnCollision != null)
        //        {
        //            nodeOnCollisionToPlayer = Vector3.ProjectOnPlane((Player.transform.position - lastNodeOnCollision.transform.position), Vector3.up);
        //            int index = RopeNodes.IndexOf(lastNodeOnCollision);
        //            float remainingRope = MassPointLength * (LastRopeNode - index + 1);

        //            if (nodeOnCollisionToPlayer.magnitude > remainingRope)
        //            {
        //                error = pointsDistance - MassPointLength;
        //                direction = massPoint.transform.position - GrappleGun.transform.position;
        //                movement = Vector3.ProjectOnPlane((error * direction), Vector3.up);
        //                //Debug.Log("Velocity Vector: " + Player.VelocityVector.magnitude);
        //                Player.CharacterController.enabled = false;
        //                //move = Player.transform.position + new Vector3(movement.x, /* 1.5f */ 0, movement.z);
        //                Player.transform.position += movement;
        //                //Player.transform.position = massPoint.transform.position + MassPointLength / 2 * direction;
        //                Player.CharacterController.enabled = true;
        //                //Debug.Log("Distanza: " + (GrappleGun.transform.position - massPoint.transform.position).magnitude);
        //            }
        //            else
        //            {
        //                error = pointsDistance - MassPointLength;
        //                direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
        //                movement = error * direction;
        //                massPoint.transform.position += movement;
        //                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        //                pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
        //            }
        //        }
        //        else
        //        {
        //            Vector3 distanceHookToPlayer = Vector3.ProjectOnPlane((Player.transform.position - hook.transform.position), Vector3.up);

        //            if (distanceHookToPlayer.magnitude > MassPointLength * (LastRopeNode + 1))
        //            {
        //                error = pointsDistance - MassPointLength;
        //                direction = massPoint.transform.position - GrappleGun.transform.position;
        //                movement = Vector3.ProjectOnPlane((error * direction), Vector3.up);
        //                //Debug.Log("Velocity Vector: " + Player.VelocityVector.magnitude);
        //                Player.CharacterController.enabled = false;
        //                //move = Player.transform.position + new Vector3(movement.x, /* 1.5f */ 0, movement.z);
        //                Player.transform.position += movement;
        //                //Player.transform.position = massPoint.transform.position + MassPointLength / 2 * direction;
        //                Player.CharacterController.enabled = true;
        //                //Debug.Log("Distanza: " + (GrappleGun.transform.position - massPoint.transform.position).magnitude);
        //            }
        //            else
        //            {
        //                error = pointsDistance - MassPointLength;
        //                direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
        //                movement = error * direction;
        //                massPoint.transform.position += movement;
        //                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        //                pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        error = pointsDistance - MassPointLength;
        //        direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
        //        movement = error * direction;
        //        massPoint.transform.position += movement;
        //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        //        pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
        //    }
        //}
    }

    #endregion

    #region REWIND POINTS /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    public void RewindPoints() {
        RopeNode massPoint;

        RewindSpatialMagnitude = Mathf.Clamp((RewindVelocityMagnitude * Time.deltaTime + 0.5f * RewindAccelerationMagnitude * Mathf.Pow(Time.deltaTime, 2)), 0, RewindMaxSpeed * Time.deltaTime);
        RewindVelocityMagnitude = Mathf.Clamp(RewindVelocityMagnitude + RewindAccelerationMagnitude * Time.deltaTime, 0, RewindMaxSpeed);

        PointsRewinded = (int)Mathf.Floor((RewindSpatialMagnitude + RopeRewinded) / MassPointLength);
        RopeRewinded += RewindSpatialMagnitude - (PointsRewinded * MassPointLength);

        //SE IN TUG, tenere conto di totalRope
        //totalRope / MassPointLength = minMassPoint;
        //LastRopeNode - PointsRewinded non deve scendere sotto il minMassPoint. MAI!

        if (TotalPointsRewinded + PointsRewinded >= RopeNodes.Count) {
            for (int i = RopeNodes.Count - TotalPointsRewinded - 1; i >= 0; i--) {
                massPoint = RopeNodes[i] as RopeNode;
                massPoint.transform.position = new Vector3(GrappleGun.transform.position.x, GrappleGun.transform.position.y, GrappleGun.transform.position.z);
                ToggleMassPoint(i, false);
            }

            TotalPointsRewinded += PointsRewinded;
            LastRopeNode = 1;
            HookReset();
            RewindSpatialMagnitude = 0;  //MOLTO IMPORTANTI, SPECIE SE VAI IN HOOKED!
            RewindVelocityMagnitude = 0;
        }

        else if (TotalPointsRewinded + PointsRewinded < RopeNodes.Count && PointsRewinded > 0) {
            for (int i = RopeNodes.Count - TotalPointsRewinded - 1; i > RopeNodes.Count - TotalPointsRewinded - PointsRewinded - 1; i--) {
                massPoint = RopeNodes[i] as RopeNode;
                massPoint.transform.position = new Vector3(GrappleGun.transform.position.x, GrappleGun.transform.position.y, GrappleGun.transform.position.z);
                ToggleMassPoint(i, false);
            }

            TotalPointsRewinded += PointsRewinded;
            LastRopeNode -= PointsRewinded;
        }

        //FUNZIONE UNICA (che ha dato problemi)
        //if (PointsRewinded > 0)
        //{
        //    for (int i = RopeNodes.Count - TotalPointsRewinded - 1; i >= Mathf.Clamp((RopeNodes.Count - (TotalPointsRewinded + PointsRewinded)), 1, RopeNodes.Count) - 1; i--)
        //    {
        //        massPoint = RopeNodes[i] as RopeNode;
        //        massPoint.transform.position = new Vector3(GrappleGun.transform.position.x, GrappleGun.transform.position.y, GrappleGun.transform.position.z);
        //        //DeactiveMassPoint(i);
        //        ToggleMassPoint(i, false);
        //    }

        //    TotalPointsRewinded = Mathf.Clamp(TotalPointsRewinded + PointsRewinded, 0, RopeNodes.Count);
        //    //LastRopeNode -= PointsRewinded;
        //}

        //FATTO TEST: c'è uno spazio non voluto dopo 2\3 nodi dal Player, con questa funzione. Motivo ignoto.

    }

    public void Tug_RewindPoints()
    {
        RopeNode massPoint;

        RewindSpatialMagnitude = Mathf.Clamp((RewindVelocityMagnitude * Time.deltaTime + 0.5f * RewindAccelerationMagnitude * Mathf.Pow(Time.deltaTime, 2)), 0, RewindMaxSpeed * Time.deltaTime);
        RewindVelocityMagnitude = Mathf.Clamp(RewindVelocityMagnitude + RewindAccelerationMagnitude * Time.deltaTime, 0, RewindMaxSpeed);

        PointsRewinded = (int)Mathf.Floor((RewindSpatialMagnitude + RopeRewinded) / MassPointLength);
        RopeRewinded += RewindSpatialMagnitude - (PointsRewinded * MassPointLength);

        //SE IN TUG, tenere conto di totalRope
        //totalRope / MassPointLength = minMassPoint;
        //LastRopeNode - PointsRewinded non deve scendere sotto il minMassPoint. MAI!
        int minMassPoint =  Mathf.Clamp((int)Mathf.Floor(totalRope / MassPointLength), 1, RopeNodes.Count);

        //if (tugLastMassPoint != 0)
        //{
        //    if (minMassPoint < tugLastMassPoint)
        //    {
        //        tugLastMassPoint = minMassPoint;
        //    }
        //}
        //else
        //{
        //    tugLastMassPoint = minMassPoint;
        //}

        //tugLastMassPoint = minMassPoint;

        if (TotalPointsRewinded + PointsRewinded < RopeNodes.Count && PointsRewinded > 0)
        {
            if (RopeNodes.Count - TotalPointsRewinded - PointsRewinded > minMassPoint)
            {
                for (int i = RopeNodes.Count - TotalPointsRewinded - 1; i > RopeNodes.Count - TotalPointsRewinded - PointsRewinded - 1; i--)
                {
                    massPoint = RopeNodes[i] as RopeNode;
                    massPoint.transform.position = new Vector3(GrappleGun.transform.position.x, GrappleGun.transform.position.y, GrappleGun.transform.position.z);
                    ToggleMassPoint(i, false);
                }

                TotalPointsRewinded += PointsRewinded;
                LastRopeNode -= PointsRewinded;
            }
            else
            {
                for (int i = RopeNodes.Count - TotalPointsRewinded - 1; i > minMassPoint - 1; i--)
                {
                    massPoint = RopeNodes[i] as RopeNode;
                    massPoint.transform.position = new Vector3(GrappleGun.transform.position.x, GrappleGun.transform.position.y, GrappleGun.transform.position.z);
                    ToggleMassPoint(i, false);
                }

                TotalPointsRewinded = RopeNodes.Count - minMassPoint;
                LastRopeNode = minMassPoint;
            }            
        }
    }
    #endregion

    #region UNWIND POINTS /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void UnwindPoints(float _ropeUnwinded, bool _clampToPointsRewinded) {
        RopeNode massPoint;
        int pointsUnwinded;

        _ropeUnwinded += (MassPointLength - RopeRewinded);

        if (_clampToPointsRewinded) {
            pointsUnwinded = Mathf.Clamp((int)Mathf.Floor(_ropeUnwinded / MassPointLength), 0, PointsRewinded);
        }
        else {
            pointsUnwinded = (int)Mathf.Floor(_ropeUnwinded / MassPointLength);
        }

        RopeRewinded = _ropeUnwinded - (pointsUnwinded * MassPointLength);
        LastRopeNode = RopeNodes.Count - Mathf.Clamp((TotalPointsRewinded - pointsUnwinded), 0, RopeNodes.Count);

        for (int i = RopeNodes.Count - TotalPointsRewinded; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            ToggleMassPoint(i, true);
        }

        TotalPointsRewinded = Mathf.Clamp((TotalPointsRewinded - pointsUnwinded), 0, RopeNodes.Count);
    }
    #endregion

    #region REWIND: APPLY CONSTRAINTS PC to Hook //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Rewind_ApplyContraintsPCToHook() {
        float pointsDistance = 0;
        float error = 0;
        float pointsDisatnce = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;

        RopeNode massPoint;
        RopeNode previousMassPoint;

        Vector3 SumOfLengths = Vector3.zero;
        totalRope = 0;

        massPoint = RopeNodes[RopeNodes.Count - TotalPointsRewinded - 1] as RopeNode;
        pointsDisatnce = Vector3.Distance(GrappleGun.transform.position, massPoint.transform.position);

        if (pointsDisatnce > (MassPointLength - RopeRewinded)) {
            error = pointsDisatnce - (MassPointLength - RopeRewinded);
            direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }
        /*[To Do]
        Controllo: distance < diametro del RopeNode;
        Controllo: posizione del RopeNode nello Spazio di Possibilità rispetto al vincolo opposto (se funzione parte da Hook, il vincolo opposto è Gun -- e viceversa).
        Formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8
        [/To Do]*/

        SumOfLengths += (massPoint.transform.position - GrappleGun.transform.position);
        totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - GrappleGun.transform.position), Vector3.down).magnitude;

        for (int i = RopeNodes.Count - TotalPointsRewinded - 1; i > 0; i--) {
            massPoint = RopeNodes[i - 1] as RopeNode;
            previousMassPoint = RopeNodes[i] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SumOfLengths += (massPoint.transform.position - previousMassPoint.transform.position);
            totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - previousMassPoint.transform.position), Vector3.down).magnitude;
        }

        massPoint = RopeNodes[0] as RopeNode;
        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;
        totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - hook.transform.position), Vector3.down).magnitude;

        if (hook.isHooked) {
            //if ((SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude >= MassPointLength * (RopeNodes.Count - TotalPointsRewinded)) {
            //    //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
            //    //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
            //    //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
            //    hook.isRopeFinished = true;
            //    UnwindPoints((hook.transform.position - massPoint.transform.position).magnitude, true); //Portarla fuori assieme alla condizione?            
            //}
            if (totalRope >= MassPointLength * (RopeNodes.Count + 1 - TotalPointsRewinded)) {
                //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
                //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
                //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
                hook.isRopeFinished = true;
                UnwindPoints((hook.transform.position - massPoint.transform.position).magnitude, true); //Portarla fuori assieme alla condizione?            
            }
            else {
                hook.isRopeFinished = false;
                lineRenderer.material = NotTightRope;
            }
        }

        else {
            error = pointsDistance - MassPointLength;
            direction = (massPoint.transform.position - hook.transform.position).normalized;
            movement = error * direction;
            hook.transform.position += movement;
            hook.SpatialVector = (hook.transform.position - massPoint.OldPos);
            hook.transform.rotation = Quaternion.LookRotation(-direction);
        }
    }
    #endregion
    #region REWIND: APPLY CONSTRAINTS Hook to PC //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Rewind_ApplyConstraintsHookToPC() {
        float pointsDistance = 0;
        float error = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;

        RopeNode previousMassPoint;
        RopeNode massPoint = RopeNodes[0] as RopeNode;

        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        if (pointsDistance > MassPointLength) {
            error = pointsDistance - MassPointLength;
            direction = (hook.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }
        /*[To Do]
        Controllo: distance < diametro del RopeNode;
        Controllo: posizione del RopeNode nello Spazio di Possibilità rispetto al vincolo opposto (se funzione parte da Hook, il vincolo opposto è Gun -- e viceversa).
        Formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8
        [/To Do]*/

        for (int i = 1; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            previousMassPoint = RopeNodes[i - 1] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SaveLastNodeOnCollision(massPoint);

        }

        massPoint = RopeNodes[LastRopeNode - 1] as RopeNode;
        pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

        //CAMBIA IN BASE A SE C'È O NON C'È INPUT DI MOVIMENTO
        if (pointsDistance > MassPointLength) {
            if (hook.isRopeFinished && hook.isHooked) {
                error = pointsDistance - MassPointLength;
                direction = (massPoint.transform.position - GrappleGun.transform.position).normalized;
                movement = error * direction;
                //Debug.Log("Velocity Vector: " + Player.VelocityVector.magnitude);
                Player.CharacterController.enabled = false;
                Player.transform.position += new Vector3(movement.x, /* 1.5f */ 0, movement.z);
                Player.CharacterController.enabled = true;
                //Debug.Log("Distanza: " + movement.magnitude);
            }
            else {
                error = pointsDistance - MassPointLength;
                direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
                movement = error * direction;
                massPoint.transform.position += movement;
                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
                pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
            }
        }
    }
    #endregion

    #region UNWIND: APPLY CONSTRAINTS PC to Hook //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Unwind_ApplyContraintsPCToHook() {
        float pointsDistance = 0;
        float error = 0;
        float pointsDisatnce = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;

        RopeNode massPoint;
        RopeNode previousMassPoint;

        Vector3 nodeOnCollisionToPlayer;

        //if (lastNodeOnCollision != null && hook.isRopeFinished)
        //{
        //    nodeOnCollisionToPlayer = Vector3.ProjectOnPlane((Player.transform.position - lastNodeOnCollision.transform.position), Vector3.up);

        //    int index = RopeNodes.IndexOf(lastNodeOnCollision);
        //    float remainingRope = MassPointLength * (LastRopeNode - index + 1);
        //    if (nodeOnCollisionToPlayer.magnitude > remainingRope)
        //    {
        //        ////MOVIMENTO AD ARCO
        //        //Vector3 arcStart = Player.transform.position - Player.VelocityVector * Time.deltaTime; //Se ci fosse la oldPos sarebbe meglio;
        //        //Vector3 vectorStart = Vector3.ProjectOnPlane((arcStart - lastNodeOnCollision.transform.position), Vector3.up);
        //        //Debug.DrawRay(lastNodeOnCollision.transform.position, vectorStart, Color.white, 1f);

        //        ////2pi*r : l = 2pi : deltaAngle --> deltaAngle = l / r
        //        //float deltaAngle = (Player.VelocityVector.magnitude / remainingRope) * Mathf.Sign(Vector3.SignedAngle(vectorStart, nodeOnCollisionToPlayer, Vector3.up));
        //        ////vectorStart = Quaternion.AngleAxis(deltaAngle, Vector3.up) * vectorStart;
        //        //vectorStart = Quaternion.AngleAxis(deltaAngle, Vector3.up) * vectorStart.normalized * (vectorStart.magnitude + MassPointLength);
        //        //Debug.DrawRay(lastNodeOnCollision.transform.position, vectorStart, Color.white, 1f);

        //        //Player.transform.position = new Vector3(lastNodeOnCollision.transform.position.x + vectorStart.x, Player.transform.position.y, lastNodeOnCollision.transform.position.z + vectorStart.z);
        //        //Debug.DrawLine(new Vector3(arcStart.x, lastNodeOnCollision.transform.position.y, arcStart.z), new Vector3(Player.transform.position.x, lastNodeOnCollision.transform.position.y, Player.transform.position.z), Color.green, 1f);

        //        //CONTROLLO LINEARE
        //        error = nodeOnCollisionToPlayer.magnitude - remainingRope;
        //        direction = -nodeOnCollisionToPlayer.normalized;
        //        Player.CharacterController.enabled = false;
        //        Player.transform.position += error * direction;
        //        Player.CharacterController.enabled = true;
        //    }
        //}

        Vector3 SumOfLengths = Vector3.zero;
        totalRope = 0;

        massPoint = RopeNodes[RopeNodes.Count - TotalPointsRewinded - 1] as RopeNode;
        pointsDisatnce = Vector3.Distance(GrappleGun.transform.position, massPoint.transform.position);

        if (pointsDisatnce > (MassPointLength - RopeRewinded)) {
            error = pointsDisatnce - (MassPointLength - RopeRewinded);
            direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }

        SumOfLengths += (massPoint.transform.position - GrappleGun.transform.position);
        totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - GrappleGun.transform.position), Vector3.down).magnitude;

        for (int i = RopeNodes.Count - TotalPointsRewinded - 1; i > 0; i--) {
            massPoint = RopeNodes[i - 1] as RopeNode;
            previousMassPoint = RopeNodes[i] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SumOfLengths += (massPoint.transform.position - previousMassPoint.transform.position);
            totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - previousMassPoint.transform.position), Vector3.down).magnitude;
        }
        /*[To Do]
        Controllo: distance < diametro del RopeNode;
        Controllo: posizione del RopeNode nello Spazio di Possibilità rispetto al vincolo opposto (se funzione parte da Hook, il vincolo opposto è Gun -- e viceversa).
        Formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8
        [/To Do]*/

        massPoint = RopeNodes[0] as RopeNode;
        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - hook.transform.position), Vector3.down).magnitude;

        if (hook.isHooked) {
            //if ((SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude >= MassPointLength * (RopeNodes.Count - TotalPointsRewinded)) {
            //    //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
            //    //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
            //    //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
            //    hook.isRopeFinished = true;
            //    //SONO DUE LE COSE: on un nuovo stato senza la riga qui sotto o una booleana
            //    UnwindPoints((hook.transform.position - massPoint.transform.position).magnitude, false); //Chiamarla nella State?
            //    lineRenderer.material = UnwindRope;
            //}
            if (totalRope >= MassPointLength * (RopeNodes.Count + 1 - TotalPointsRewinded)) {
                //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
                //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
                //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
                hook.isRopeFinished = true;
                //SONO DUE LE COSE: on un nuovo stato senza la riga qui sotto o una booleana
                UnwindPoints((hook.transform.position - massPoint.transform.position).magnitude, false); //Chiamarla nella State?
                lineRenderer.material = UnwindRope;
            }
            else {
                hook.isRopeFinished = false;
                lineRenderer.material = UnwindRope;
            }
        }

        //else
        //{
        //    error = pointsDistance - MassPointLength;
        //    direction = (massPoint.transform.position - hook.transform.position).normalized;
        //    movement = error * direction;
        //    hook.transform.position += movement;
        //    hook.SpatialVector = (hook.transform.position - massPoint.OldPos);
        //}
    }
    #endregion
    #region UNWIND: APPLY CONSTRAINTS Hook to PC //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Unwind_ApplyConstraintsHookToPC() {
        float pointsDistance = 0;
        float error = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;

        RopeNode previousMassPoint;
        RopeNode massPoint = RopeNodes[0] as RopeNode;

        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        if (pointsDistance > MassPointLength) {
            error = pointsDistance - MassPointLength;
            direction = (hook.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }
        /*[To Do]
        Controllo: distance < diametro del RopeNode;
        Controllo: posizione del RopeNode nello Spazio di Possibilità rispetto al vincolo opposto (se funzione parte da Hook, il vincolo opposto è Gun -- e viceversa).
        Formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8
        [/To Do]*/

        for (int i = 1; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            previousMassPoint = RopeNodes[i - 1] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SaveLastNodeOnCollision(massPoint);
        }

        massPoint = RopeNodes[LastRopeNode - 1] as RopeNode;
        pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

        //CAMBIA IN BASE A SE C'È O NON C'È INPUT DI MOVIMENTO
        //if (pointsDistance > MassPointLength)
        if (pointsDistance > MassPointLength) {
            if (hook.isRopeFinished && hook.isHooked) {
                error = pointsDistance - MassPointLength;
                direction = (massPoint.transform.position - GrappleGun.transform.position).normalized;
                movement = error * direction;
                //Debug.Log("Velocity Vector: " + Player.VelocityVector.magnitude);
                Player.CharacterController.enabled = false;
                Player.transform.position += new Vector3(movement.x, /* 1.5f */ 0, movement.z);
                Player.CharacterController.enabled = true;
                //Debug.Log("Distanza: " + movement.magnitude);
            }
            else {
                error = pointsDistance - MassPointLength;
                direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
                movement = error * direction;
                massPoint.transform.position += movement;
                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
                pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
            }
        }
    }
    #endregion

    #region ATTACK: APPLY CONSTRAINTS PC to Hook //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Attack_ApplyContraintsPCToHook() {
        float pointsDistance = 0;
        float error = 0;
        float pointsDisatnce = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;

        RopeNode massPoint;
        RopeNode previousMassPoint;

        Vector3 SumOfLengths = Vector3.zero;

        massPoint = RopeNodes[RopeNodes.Count - TotalPointsRewinded - 1] as RopeNode;
        pointsDisatnce = Vector3.Distance(GrappleGun.transform.position, massPoint.transform.position);

        if (pointsDisatnce > (MassPointLength - RopeRewinded)) {
            error = pointsDisatnce - (MassPointLength - RopeRewinded);
            direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }
        /*[To Do]
        Controllo: distance < diametro del RopeNode;
        Controllo: posizione del RopeNode nello Spazio di Possibilità rispetto al vincolo opposto (se funzione parte da Hook, il vincolo opposto è Gun -- e viceversa).
        Formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8
        [/To Do]*/

        SumOfLengths += (massPoint.transform.position - GrappleGun.transform.position);

        for (int i = RopeNodes.Count - TotalPointsRewinded - 1; i > 0; i--) {
            massPoint = RopeNodes[i - 1] as RopeNode;
            previousMassPoint = RopeNodes[i] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SumOfLengths += (massPoint.transform.position - previousMassPoint.transform.position);
        }

        massPoint = RopeNodes[0] as RopeNode;
        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        if ((SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude >= MassPointLength * (RopeNodes.Count - TotalPointsRewinded)) {
            hook.isRopeFinished = true;
            lineRenderer.material = TightRope;
        }
        else {
            hook.isRopeFinished = false;
            lineRenderer.material = NotTightRope;
        }
    }
    #endregion
    #region ATTACK: APPLY CONSTRAINTS Hook to PC //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void Attack_ApplyConstraintsHookToPC() {
        float pointsDistance = 0;
        float error = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;

        RopeNode previousMassPoint;
        RopeNode massPoint = RopeNodes[0] as RopeNode;

        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        if (pointsDistance > MassPointLength) {
            error = pointsDistance - MassPointLength;
            direction = (hook.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }
        /*[To Do]
        Controllo: distance < diametro del RopeNode;
        Controllo: posizione del RopeNode nello Spazio di Possibilità rispetto al vincolo opposto (se funzione parte da Hook, il vincolo opposto è Gun -- e viceversa).
        Formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8
        [/To Do]*/

        for (int i = 1; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            previousMassPoint = RopeNodes[i - 1] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SaveLastNodeOnCollision(massPoint);
        }

        massPoint = RopeNodes[LastRopeNode - 1] as RopeNode;
        pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

        //CAMBIA IN BASE A SE C'È O NON C'È INPUT DI MOVIMENTO
        if (pointsDistance > (MassPointLength - RopeRewinded)) {
            if (hook.isRopeFinished) {
                Vector3 playerOldPos = Player.transform.position;

                error = pointsDistance - (MassPointLength - RopeRewinded);
                direction = (massPoint.transform.position - GrappleGun.transform.position).normalized;
                movement = error * direction;
                Player.CharacterController.enabled = false;
                Player.transform.position += new Vector3(movement.x, /* 1.5f */ 0, movement.z);
                Player.CharacterController.enabled = true;
                //Player.VelocityVector = (Player.transform.position - playerOldPos) / Time.deltaTime;
                //Player.VelocityVector = RewindVelocityManitude * Player.transform.forward;
                //Debug.Log("AttackVector :" + Player.VelocityVector.magnitude);
                //Debug.Log("DeltaS :" + (Player.transform.position - playerOldPos).magnitude);
            }
        }
    }
    #endregion

    

    #region TUG: APPLY CONSTRAINTS PC to Hook /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Tug_ApplyConstraintsPCToHook() {
        float pointsDistance = 0;
        float error = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;
        RopeNode massPoint = RopeNodes[LastRopeNode - 1] as RopeNode;
        RopeNode previousMassPoint;

        Vector3 nodeOnCollisionToPlayer;

        if (lastNodeOnCollision != null && hook.isRopeFinished)
        {
            nodeOnCollisionToPlayer = Vector3.ProjectOnPlane((Player.transform.position - lastNodeOnCollision.transform.position), Vector3.up);

            int index = RopeNodes.IndexOf(lastNodeOnCollision);
            float remainingRope = MassPointLength * (LastRopeNode - index + 1);
            if (nodeOnCollisionToPlayer.magnitude > remainingRope)
            {
                ////MOVIMENTO AD ARCO
                //Vector3 arcStart = Player.transform.position - Player.VelocityVector * Time.deltaTime; //Se ci fosse la oldPos sarebbe meglio;
                //Vector3 vectorStart = Vector3.ProjectOnPlane((arcStart - lastNodeOnCollision.transform.position), Vector3.up);
                //Debug.DrawRay(lastNodeOnCollision.transform.position, vectorStart, Color.white, 1f);

                ////2pi*r : l = 2pi : deltaAngle --> deltaAngle = l / r
                //float deltaAngle = (Player.VelocityVector.magnitude / remainingRope) * Mathf.Sign(Vector3.SignedAngle(vectorStart, nodeOnCollisionToPlayer, Vector3.up));
                ////vectorStart = Quaternion.AngleAxis(deltaAngle, Vector3.up) * vectorStart;
                //vectorStart = Quaternion.AngleAxis(deltaAngle, Vector3.up) * vectorStart.normalized * (vectorStart.magnitude + MassPointLength);
                //Debug.DrawRay(lastNodeOnCollision.transform.position, vectorStart, Color.white, 1f);

                //Player.transform.position = new Vector3(lastNodeOnCollision.transform.position.x + vectorStart.x, Player.transform.position.y, lastNodeOnCollision.transform.position.z + vectorStart.z);
                //Debug.DrawLine(new Vector3(arcStart.x, lastNodeOnCollision.transform.position.y, arcStart.z), new Vector3(Player.transform.position.x, lastNodeOnCollision.transform.position.y, Player.transform.position.z), Color.green, 1f);

                //CONTROLLO LINEARE
                error = nodeOnCollisionToPlayer.magnitude - remainingRope;
                direction = -nodeOnCollisionToPlayer.normalized;
                Player.CharacterController.enabled = false;
                Player.transform.position += error * direction;
                Player.CharacterController.enabled = true;
            }
        }

        Vector3 SumOfLengths = Vector3.zero;
        totalRope = 0;

        pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

        //if (pointsDistance > MassPointLength)
        //{
        //    massPoint.transform.position = GrappleGun.transform.position; // Il RopeNode più vicino alla Gun viene da essa assorbito
        //    massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        //}

        //if (pointsDistance > (MassPointLength - RopeRewinded)) {
        //    error = pointsDistance - (MassPointLength - RopeRewinded);
        //    direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
        //    movement = error * direction;
        //    //massPoint.transform.position += movement;
        //    //massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        //    CheckCollision(massPoint, movement, FixedDeltaTime, massPoint.MassPointRadius);
        //}

        massPoint.transform.position = GrappleGun.transform.position;

        //SumOfLengths += (massPoint.transform.position - GrappleGun.transform.position);
        //totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - GrappleGun.transform.position), Vector3.down).magnitude;


        for (int i = LastRopeNode - 1; i > 0; i--) {
            massPoint = RopeNodes[i - 1] as RopeNode;
            previousMassPoint = RopeNodes[i] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SumOfLengths += (massPoint.transform.position - previousMassPoint.transform.position);
            totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - previousMassPoint.transform.position), Vector3.down).magnitude;
        }

        massPoint = RopeNodes[0] as RopeNode;
        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - hook.transform.position), Vector3.down).magnitude;

        //if ((SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude >= MassPointLength * (RopeNodes.Count - TotalPointsRewinded)) 
        //{
        //    //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
        //    //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
        //    //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
        //    hook.isRopeFinished = true;
        //    //SONO DUE LE COSE: on un nuovo stato senza la riga qui sotto o una booleana
        //    UnwindPoints((hook.transform.position - massPoint.transform.position).magnitude, true); //Chiamarla nella State?
        //    lineRenderer.material = TightRope;
        //    //hookPoint.HookPointSpring();
        //    error = pointsDistance - MassPointLength;
        //    direction = (massPoint.transform.position - hook.transform.position).normalized;
        //    movement = error * direction;
        //    hook.transform.position += movement;
        //}
        if (totalRope > MassPointLength * (RopeNodes.Count - TotalPointsRewinded)) {
            //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
            //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
            //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
            hook.isRopeFinished = true;
            lineRenderer.material = TightRope;

            error = pointsDistance - MassPointLength;
            direction = (massPoint.transform.position - hook.transform.position).normalized;
            movement = error * direction;
            //ATTENZIONE! L'hook deve solo passare I PARAMETRI DI MOVIMENTO DEL PLAYER
            //Questo significa che questa funzione la esegue solo se c'è INPUT DI MOVIMENTO! Altrimenti, l'hook si posiziona direttamente dove sta l'hookpoint.
            hook.transform.position += movement;
            totalRope = MassPointLength * (RopeNodes.Count - TotalPointsRewinded);

        }
        else {
            hook.isRopeFinished = false;
            lineRenderer.material = TightRope;
        }
    }
    #endregion
    #region TUG: APPLY CONSTRAINTS Hook to PC /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Tug_ApplyConstraintsHookToPC() {
        float pointsDistance = 0;
        float error = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;

        RopeNode previousMassPoint;
        RopeNode massPoint = RopeNodes[0] as RopeNode;

        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        if (pointsDistance > MassPointLength) {
            error = pointsDistance - MassPointLength;
            direction = (hook.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }

        for (int i = 1; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            previousMassPoint = RopeNodes[i - 1] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SaveLastNodeOnCollision(massPoint);
        }

        massPoint = RopeNodes[LastRopeNode - 1] as RopeNode;
        pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

        //CAMBIA IN BASE A SE C'È O NON C'È INPUT DI MOVIMENTO
        //if (pointsDistance > MassPointLength) {
        //    if (hook.isRopeFinished && hook.isHooked) {
        //        error = pointsDistance - MassPointLength;
        //        direction = (massPoint.transform.position - GrappleGun.transform.position).normalized;
        //        movement = error * direction;
        //        //Debug.Log("Velocity Vector: " + Player.VelocityVector.magnitude);
        //        //Player.CharacterController.enabled = false;
        //        Player.transform.position += new Vector3(movement.x, /* 1.5f */ 0, movement.z);
        //        //Player.CharacterController.enabled = true;
        //        //Debug.Log("Distanza: " + movement.magnitude);
        //    }
        //    else {
        //        error = pointsDistance - MassPointLength;
        //        direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
        //        movement = error * direction;
        //        massPoint.transform.position += movement;
        //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        //        pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
        //    }
        //}

        if (pointsDistance > MassPointLength)
        {
            if (hook.isRopeFinished && hook.isHooked)
            {
                error = pointsDistance - (MassPointLength);
                direction = (massPoint.transform.position - GrappleGun.transform.position).normalized;
                movement = error * direction;
                //Debug.Log("Velocity Vector: " + Player.VelocityVector.magnitude);
                Player.CharacterController.enabled = false;
                Player.transform.position += new Vector3(movement.x, /* 1.5f */ 0, movement.z);
                Player.CharacterController.enabled = true;
                //Debug.Log("Distanza: " + movement.magnitude);
            }
            else
            {
                error = pointsDistance - (MassPointLength);
                direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
                movement = error * direction;
                massPoint.transform.position += movement;
                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
                pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
            }
        }
    }
    #endregion

    public void CollisionController() {

        RopeNode massPoint;

        for (int i = 0; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            
            if (massPoint.currentHitObject != null) {
                if (massPoint.currentHitObject.GetComponent<ATATController>() && !massPoint.collisionAdded) {
                    massPoint.collisionAdded = true;
                    massPoint.oldHitOject = massPoint.currentHitObject;
                    massPoint.currentHitObject.GetComponent<ATATController>().addCollision();        
                }
            }
            else {
                if (massPoint.oldHitOject != null) {
                    massPoint.oldHitOject.GetComponent<ATATController>().removeCollision();
                    massPoint.oldHitOject = null;
                    massPoint.collisionAdded = false;
                }
            }
        }
    }

    public void CollisionReset() {

        RopeNode massPoint;

        for (int i = 0; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            if (massPoint.oldHitOject != null) {
                massPoint.oldHitOject.GetComponent<ATATController>().removeCollision();
                massPoint.oldHitOject = null;
                massPoint.collisionAdded = false;
            }
        }

    }

        public void ToggleMassPoint(int _index, bool _toggle) {
        RopeNode massPoint;

        massPoint = RopeNodes[_index] as RopeNode;
        //massPoint.currentHitObject = null;
        massPoint.transform.gameObject.SetActive(_toggle);
        //massPoint.isActiveOrNot = massPoint.transform.gameObject.activeSelf;
    }

    public void InactiveMassPointTranslate()
    {
        RopeNode massPoint;

        for (int i = LastRopeNode; i < RopeNodes.Count - 1; i++)
        {
            massPoint = RopeNodes[i] as RopeNode;
            massPoint.transform.position = GrappleGun.transform.position;
        }
    }

    public void Unhook()
    {
        hookPoint.isHooked = false;
        if (hookPoint.bossRelated)
        {
            if (hookPoint.boss != null) {
                hookPoint.boss.isHooked = false;
                hookPoint.boss.UpdateHookedState(hookPoint.boss.animator);
                hookPoint.boss.animator.SetInteger("HookPointTag", 0);
            }
            else if (hookPoint.bossCustom != null) {
                hookPoint.bossCustom.isHooked = false;
                hookPoint.bossCustom.UpdateHookedState(hookPoint.bossCustom.animator);
                hookPoint.bossCustom.animator.SetInteger("HookPointTag", 0);
            }
        }

        hook.isHooked = false;
        hook.currentHitObject.GetComponent<HookPointBase>().isHooked = false;
        hook.currentHitObject = null;
        hook.currentTag = 0;


        if (hookPoint.GetComponent<HookPointSpring>())
        {
            hookPoint.transform.position = hookPoint.GetComponent<HookPointSpring>().HookPointPivot.transform.position;
        }
        hook.SpatialVector = Vector3.zero;
    }

    public void HookReset() {
        hook.isRopeFinished = false;
        hook.isHooked = false;
        hook.isRopeNode = false;
        hook.transform.position = GrappleGun.transform.position + RolledUpHookPos * GrappleGun.transform.forward;
        hook.transform.rotation = Quaternion.LookRotation(GrappleGun.transform.forward);
        hook.OldPos = Vector3.zero;
        hook.SpatialVector = Vector3.zero;
        hook.VelocityVector = Vector3.zero;
    }

    #region TUG CHECK: APPLY CONSTRAINTS PC to Hoolk ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void TugCheck_ApplyConstraintsPCToHook() {
        float pointsDistance = 0;
        float error = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;
        RopeNode massPoint = RopeNodes[LastRopeNode - 1] as RopeNode;
        RopeNode previousMassPoint;

        Vector3 SumOfLengths = Vector3.zero;
        totalRope = 0;

        pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

        if (pointsDistance > MassPointLength - RopeRewinded) {
            error = pointsDistance - (MassPointLength - RopeRewinded);
            direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }
        /*[To Do]
        Controllo: distance < diametro del RopeNode;
        Controllo: posizione del RopeNode nello Spazio di Possibilità rispetto al vincolo opposto (se funzione parte da Hook, il vincolo opposto è Gun -- e viceversa).
        Formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8
        [/To Do]*/

        SumOfLengths += (massPoint.transform.position - GrappleGun.transform.position);

        totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - GrappleGun.transform.position), Vector3.down).magnitude;

        for (int i = LastRopeNode - 1; i > 0; i--) {
            massPoint = RopeNodes[i - 1] as RopeNode;
            previousMassPoint = RopeNodes[i] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
            SumOfLengths += (massPoint.transform.position - previousMassPoint.transform.position);
            totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - previousMassPoint.transform.position), Vector3.down).magnitude;
        }

        massPoint = RopeNodes[0] as RopeNode;
        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        totalRope += Vector3.ProjectOnPlane((massPoint.transform.position - hook.transform.position), Vector3.down).magnitude;

        //if ((SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude >= MassPointLength * (RopeNodes.Count - TotalPointsRewinded)) {
        //    //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
        //    //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
        //    //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
        //    hook.isRopeFinished = true;
        //    hook.isBullet = false;
        //    hook.isRopeNode = true;
        //    lineRenderer.material = NotTightRope;
        //}
        //else {
        //    hook.isRopeFinished = false;
        //    lineRenderer.material = NotTightRope;
        //}

        //if (totalRope >= MassPointLength * (RopeNodes.Count + 1 - TotalPointsRewinded))
        //{
        //    //Debug.Log("Sum: " + (SumOfLengths + (hook.transform.position - massPoint.transform.position)).magnitude);
        //    //Debug.Log("DistancePCToHook: " + (hook.transform.position - GrappleGun.transform.position).magnitude);
        //    //Debug.Log("RopeLenght: " + (MassPointLength * RopeNodes.Count));
        //    hook.isRopeFinished = true;
        //    hook.isBullet = false;
        //    hook.isRopeNode = true;
        //    lineRenderer.material = NotTightRope;
        //}
        //else
        //{
        //    hook.isRopeFinished = false;
        //    lineRenderer.material = NotTightRope;
        //}

        //if (hook.isRopeNode)
        //{
        //    if (hook.isHooked)
        //    {
        //        //hookPoint.HookPointSpring();
        //    }
        //    else
        //    {
        //        error = pointsDistance - MassPointLength;
        //        direction = (massPoint.transform.position - hook.transform.position).normalized;
        //        movement = error * direction;
        //        hook.transform.position += movement;
        //        hook.SpatialVector = (hook.transform.position - massPoint.OldPos);
        //        hook.transform.rotation = Quaternion.LookRotation(-direction);
        //    }
        //}
    }
    #endregion
    #region TUG CHECK: APPLY CONSTRAINTS Hook to PC ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void TugCheck_ApplyConstraintsHookToPC() {
        float pointsDistance = 0;
        float error = 0;
        Vector3 previousInstance = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 movement = Vector3.zero;

        RopeNode previousMassPoint;
        RopeNode massPoint = RopeNodes[0] as RopeNode;

        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

        if (pointsDistance > MassPointLength) {
            error = pointsDistance - MassPointLength;
            direction = (hook.transform.position - massPoint.transform.position).normalized;
            movement = error * direction;
            massPoint.transform.position += movement;
            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
        }

        for (int i = 1; i < LastRopeNode; i++) {
            massPoint = RopeNodes[i] as RopeNode;
            previousMassPoint = RopeNodes[i - 1] as RopeNode;
            ApplyConstraints(massPoint, previousMassPoint);
        }

        //DA VERIFICARE
        massPoint = RopeNodes[LastRopeNode - 1] as RopeNode;
        pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

        if (pointsDistance > MassPointLength - RopeRewinded) {
            if (hook.isRopeFinished && hook.isHooked) {
                error = pointsDistance - MassPointLength;
                direction = (massPoint.transform.position - GrappleGun.transform.position).normalized;
                movement = error * direction;
                //Debug.Log("Velocity Vector: " + Player.VelocityVector.magnitude);
                //Player.CharacterController.enabled = false;
                //Player.transform.position += new Vector3(movement.x, /* 1.5f */ 0, movement.z); QUI
                //Player.CharacterController.enabled = true;
                //Debug.Log("Distanza: " + movement.magnitude);
            }
            else {
                error = pointsDistance - MassPointLength;
                direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
                movement = error * direction;
                massPoint.transform.position += movement;
                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
                pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
            }
        }
    }


    #endregion


    public void DrawRope() {
        float lineWidth = this.RopeWidth;
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = lineWidth;

        Vector3[] ropePositions = new Vector3[(RopeNodes.Count - TotalPointsRewinded)];
        for (int i = 0; i < (RopeNodes.Count - TotalPointsRewinded); i++) {
            RopeNode massPoint;

            massPoint = RopeNodes[i] as RopeNode;

            ropePositions[i] = massPoint.transform.position;
        }

        lineRenderer.positionCount = ropePositions.Length;
        lineRenderer.SetPositions(ropePositions);
    }

    public void SetFovRadius() {
        FovPlayer.viewRadius = MassPointNumber * MassPointLength;
    }




    #region I DON'T WANNA BE BURIED IN A SCRIPT SEMATARY ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //public void UpdateRewind(int _index)
    //{
    //    float springValue = 1;
    //    float DynamicFriction = 1f;
    //    float pointsDistance = 0;
    //    float error = 0;
    //    float SumOfLength = 0;
    //    float pointsDisatnce = 0;
    //    int pointsUnwinded = 0;
    //    Vector3 direction = Vector3.zero;
    //    Vector3 movement = Vector3.zero;
    //    Vector3 previousInstance = Vector3.zero;

    //    RopeNode massPoint;
    //    RopeNode nextMassPoint;

    //    if (_index <= 0)
    //    {
    //        hook.shooted = false;
    //        hook.ropeFinished = false;
    //        hook.isHooked = false;
    //    }
    //    else
    //    {
    //        massPoint = RopeNodes[0] as RopeNode;
    //        pointsDisatnce = Vector3.Distance(hook.transform.position, massPoint.transform.position);

    //        if (pointsDistance > MassPointLength)
    //        {
    //            error = pointsDistance - MassPointLength;
    //            direction = (massPoint.transform.position - hook.transform.position).normalized;
    //            movement = error * direction;
    //            massPoint.transform.position -= movement;
    //            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos) * (1 - DynamicFriction);
    //            pointsDistance = (hook.transform.position - massPoint.transform.position).magnitude;
    //        }
    //        SumOfLength += pointsDistance;

    //        for (int i = 1; i <= _index; i++)
    //        {
    //            massPoint = RopeNodes[i] as RopeNode;
    //            nextMassPoint = RopeNodes[i - 1] as RopeNode;
    //            pointsDistance = (massPoint.transform.position - nextMassPoint.transform.position).magnitude;

    //            if (pointsDistance > MassPointLength)
    //            {

    //                error = pointsDistance - MassPointLength;
    //                direction = (massPoint.transform.position - nextMassPoint.transform.position).normalized;
    //                movement = error * direction;
    //                massPoint.transform.position -= movement * springValue;
    //                nextMassPoint.transform.position += movement * (1 - springValue);
    //                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos) * (1 - DynamicFriction);
    //                nextMassPoint.SpatialVector = (nextMassPoint.transform.position - nextMassPoint.OldPos) * (1 - DynamicFriction);
    //                pointsDistance = (massPoint.transform.position - nextMassPoint.transform.position).magnitude;
    //            }
    //            SumOfLength += pointsDistance;
    //        }



    //        SumOfLength += pointsDisatnce;
    //        pointsDisatnce = Vector3.Distance(GrappleGun.transform.position, massPoint.transform.position);

    //        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
    //        {
    //            if (pointsDisatnce > MassPointLength)
    //            {
    //                error = pointsDistance - MassPointLength;
    //                direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
    //                movement = error * direction;
    //                Player.transform.position -= movement * springValue;
    //                massPoint.transform.position += movement * (1 - springValue);
    //                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos) * (1 - DynamicFriction);
    //                pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
    //            }
    //            SumOfLength += pointsDistance;
    //            DestroyMassPoints(_index + 1);
    //        }

    //        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
    //        {
    //            pointsUnwinded = (int)Mathf.Ceil(pointsDistance / MassPointLength) - 1;

    //            for (int i = _index + 1; i <= _index + pointsUnwinded; i++)
    //            {

    //                if (RopeNodes[i] as RopeNode)
    //                {
    //                    massPoint = RopeNodes[i] as RopeNode; //QUI
    //                    nextMassPoint = RopeNodes[i - 1] as RopeNode;
    //                    pointsDistance = Vector3.Distance(massPoint.transform.position, nextMassPoint.transform.position);
    //                }
    //                else
    //                {
    //                    massPoint = RopeNodes[0] as RopeNode;
    //                    nextMassPoint = RopeNodes[0] as RopeNode;
    //                    pointsDistance = 0;
    //                }

    //                if (pointsDistance > MassPointLength)
    //                {
    //                    error = pointsDistance - MassPointLength;
    //                    direction = (massPoint.transform.position - nextMassPoint.transform.position).normalized;
    //                    movement = error * direction;
    //                    massPoint.transform.position -= movement * springValue;
    //                    nextMassPoint.transform.position += movement * (1 - springValue);
    //                    massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos) * (1 - DynamicFriction);
    //                    nextMassPoint.SpatialVector = (nextMassPoint.transform.position - nextMassPoint.OldPos) * (1 - DynamicFriction);
    //                    pointsDistance = (massPoint.transform.position - nextMassPoint.transform.position).magnitude;
    //                }
    //                SumOfLength += pointsDisatnce;

    //            }

    //            if (pointsDisatnce > MassPointLength)
    //            {
    //                error = pointsDistance - MassPointLength;
    //                direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
    //                movement = error * direction;
    //                Player.transform.position -= movement * springValue;
    //                massPoint.transform.position += movement * (1 - springValue);
    //                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos) * (1 - DynamicFriction);
    //                pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
    //            }
    //            SumOfLength += pointsDisatnce;
    //            DestroyMassPoints(_index + pointsUnwinded + 1);

    //            if (SumOfLength * (2 - springValue) > RopeNodes.Count * MassPointLength)
    //            {
    //                hook.ropeFinished = true;

    //            }

    //        }
    //    }
    //}


    //public void Rewind()
    //{
    //    RewindPoints();
    //}


    //public void UpdateHook()
    //{
    //    float dynamicFriction = 0.5f;
    //    float springValue = 1;
    //    float pointsDistance = 0;
    //    float error = 0;
    //    Vector3 shootDirection = Vector3.zero;
    //    Vector3 previousInstance = Vector3.zero;
    //    Vector3 direction = Vector3.zero;
    //    Vector3 movement = Vector3.zero;

    //    hook.OldPos = hook.transform.position;
    //    hook.transform.position += hook.VelocityVector * Time.deltaTime;

    //    RopeNode firstInstanceNode = RopeNodes[0] as RopeNode; //QUI
    //    pointsDistance = (firstInstanceNode.transform.position - hook.transform.position).magnitude;

    //    if (hook.ropeFinished && pointsDistance > MassPointLength)
    //    {
    //        hook.DynamicFriction = 0.4f;
    //        error = pointsDistance - MassPointLength;
    //        direction = (firstInstanceNode.transform.position - hook.transform.position).normalized;
    //        movement = error * direction;
    //        hook.transform.position += movement;
    //        hook.VelocityVector = movement / Time.deltaTime;
    //        //hook.transform.position = new Vector3(hook.transform.position.x, 1.375f, hook.transform.position.z); //MOOOOLTO STRANA!

    //    }

    //    if (hook.isHooked)
    //    {
    //        hookPoint.HookPointSpring();
    //    }
    //}

    //public void UpdateHook()
    //{
    //    float dynamicFriction = 0.5f;
    //    float springValue = 1;
    //    float pointsDistance = 0;
    //    float error = 0;
    //    Vector3 shootDirection = Vector3.zero;
    //    Vector3 previousInstance = Vector3.zero;
    //    Vector3 direction = Vector3.zero;
    //    Vector3 movement = Vector3.zero;

    //    //New
    //    float gravity = 9.8f;
    //    float drag = 0.01f;
    //    RopeNode massPoint = RopeNodes[0] as RopeNode;

    //    if (hook.isBullet)
    //    {
    //        hook.OldPos = hook.transform.position;
    //        hook.transform.position += hook.SpatialVector;
    //        hook.SpatialVector = (hook.transform.position - hook.OldPos);
    //    }

    //    if (hook.isRopeNode)
    //    {
    //        hook.OldPos = hook.transform.position;
    //        hook.transform.position += hook.SpatialVector * (1 - drag) + Vector3.down * (0.5f * gravity * Mathf.Pow(Time.deltaTime, 2));
    //        hook.SpatialVector = (hook.transform.position - hook.OldPos);

    //        pointsDistance = (massPoint.transform.position - hook.transform.position).magnitude;

    //        if (hook.ropeFinished && pointsDistance > MassPointLength)
    //        {
    //            error = pointsDistance - MassPointLength;
    //            direction = (massPoint.transform.position - hook.transform.position).normalized;
    //            movement = error * direction;
    //            hook.transform.position += movement;
    //            hook.SpatialVector = hook.transform.position - hook.OldPos;
    //        }
    //    }

    //    if (hook.isHooked)
    //    {
    //        //hookPoint.HookPointSpring();
    //    }
    //}

    //public void RewindPoints()
    //{
    //    int pointsRewinded = 0;
    //    float springValue = 1;
    //    float DynamicFriction = 1f;
    //    float pointsDistance = 0;
    //    float error = 0;
    //    float pointsDisatnce = 0;
    //    Vector3 previousInstance = Vector3.zero;
    //    Vector3 direction = Vector3.zero;
    //    Vector3 movement = Vector3.zero;

    //    RopeNode massPoint;
    //    RopeNode nextMassPoint;

    //    funzione
    //    if (GrappleGun.RewindVector < GrappleGun.RewindTopSpeed)
    //    {
    //        GrappleGun.RewindVector += GrappleGun.RewindAcceleration * Time.deltaTime;
    //    }
    //    else
    //    {
    //        GrappleGun.RewindVector = GrappleGun.RewindTopSpeed * Time.deltaTime;
    //    }
    //    funzione

    //    pointsRewinded = (int)Mathf.Ceil(GrappleGun.RewindVector / MassPointLength) - 1;


    //    if (pointsRewinded >= RopeNodes.Count - 1)
    //    {
    //        Debug.LogFormat("PointsRewinded: {0}", pointsRewinded);
    //        DestroyMassPoints(0);
    //        hook.shooted = false;
    //        hook.ropeFinished = false;
    //        hook.isHooked = false;
    //    }
    //    else
    //    {
    //        for (int i = 0; i < pointsRewinded; i++)
    //        {
    //            massPoint = RopeNodes[RopeNodes.Count - 1 - i] as RopeNode;
    //            massPoint.transform.position = new Vector3(GrappleGun.transform.position.x, GrappleGun.transform.position.y, GrappleGun.transform.position.z);

    //        }
    //        massPoint = RopeNodes[RopeNodes.Count - 1 - pointsRewinded] as RopeNode;
    //        massPoint.OldPos = massPoint.transform.position;
    //        massPoint.transform.position = massPoint.transform.position + massPoint.SpatialVector;
    //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos) * (1 - DynamicFriction);
    //        pointsDisatnce = Vector3.Distance(GrappleGun.transform.position, massPoint.transform.position);

    //        if (pointsDisatnce > MassPointLength)
    //        {
    //            error = pointsDisatnce - MassPointLength;
    //            direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
    //            movement = error * direction;
    //            massPoint.transform.position += movement * springValue;
    //            massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos) * (1 - DynamicFriction);

    //        }
    //        previousInstance = massPoint.transform.position;

    //        for (int i = pointsRewinded + 1; i < RopeNodes.Count; i++)
    //        {
    //            massPoint = RopeNodes[RopeNodes.Count - 1 - i] as RopeNode;
    //            nextMassPoint = RopeNodes[RopeNodes.Count - i] as RopeNode;
    //            massPoint.OldPos = massPoint.transform.position;
    //            massPoint.transform.position = massPoint.transform.position + massPoint.SpatialVector;
    //            massPoint.SpatialVector = massPoint.transform.position - massPoint.OldPos;
    //            pointsDistance = (massPoint.transform.position - nextMassPoint.transform.position).magnitude;

    //            if (pointsDistance > MassPointLength)
    //            {

    //                error = pointsDistance - MassPointLength;
    //                direction = (nextMassPoint.transform.position - massPoint.transform.position).normalized;
    //                movement = error * direction;
    //                massPoint.transform.position += movement * springValue;
    //                nextMassPoint.transform.position -= movement * (1 - springValue);
    //                massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos) * (1 - DynamicFriction);
    //                nextMassPoint.SpatialVector = (nextMassPoint.transform.position - nextMassPoint.OldPos) * (1 - DynamicFriction);
    //            }
    //        }
    //        UpdateHook();
    //        UpdateRewind(RopeNodes.Count - 1 - pointsRewinded);
    //    }
    //}

    //public void ApplyConstraintsPCToHook()
    //{
    //    float pointsDistance = 0;
    //    float error = 0;
    //    Vector3 previousInstance = Vector3.zero;
    //    Vector3 direction = Vector3.zero;
    //    Vector3 movement = Vector3.zero;
    //    //Poi prova con formula ELASTICO
    //    RopeNode massPoint = RopeNodes[RopeNodes.Count - 1] as RopeNode;
    //    RopeNode previousMassPoint;
    //    float massPointDiameter = massPoint.transform.localScale.x;

    //    Vector3 initialVelocityVector = Vector3.zero;

    //    pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;

    //    if (pointsDistance > MassPointLength)
    //    {
    //        error = pointsDistance - MassPointLength;
    //        direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
    //        movement = error * direction;
    //        massPoint.transform.position += movement;
    //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
    //        initialVelocityVector = massPoint.VelocityVector;
    //        massPoint.VelocityVector = massPoint.SpatialVector / Time.deltaTime;
    //        massPoint.AccelerationVector += (massPoint.VelocityVector - initialVelocityVector) / Time.deltaTime;
    //        //massPoint.AccelerationVector += massPoint.SpatialVector / (2 * Mathf.Pow(Time.deltaTime, 2));
    //        //pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;
    //        //INSERIRE IL CONTROLLO SPECIALE DALL'HOOK!
    //    }

    //    //NEW DI LUCA
    //    pointsDistance = (hook.transform.position - massPoint.transform.position).magnitude;

    //    if (pointsDistance > MassPointLength * (MassPointNumber - 1) && pointsDistance < (hook.transform.position - GrappleGun.transform.position).magnitude + MassPointLength)
    //    {
    //        //formula algebrica triangolo per spostare mass point: https://youtu.be/COMiK1L0Oj8

    //    }

    //    for (int i = 1; i < RopeNodes.Count; i++)
    //    {
    //        massPoint = RopeNodes[RopeNodes.Count - 1 - i] as RopeNode;
    //        previousMassPoint = RopeNodes[RopeNodes.Count - i] as RopeNode;
    //        pointsDistance = (massPoint.transform.position - previousMassPoint.transform.position).magnitude;
    //        ApplyConstraints(pointsDistance, ref error, ref direction, ref movement, massPoint, previousMassPoint);
    //    }

    //    massPoint = RopeNodes[0] as RopeNode;
    //    pointsDistance = (hook.transform.position - massPoint.transform.position).magnitude;

    //    if (pointsDistance != MassPointLength)
    //    {
    //        error = pointsDistance - MassPointLength;
    //        direction = (hook.transform.position - massPoint.transform.position).normalized;
    //        movement = error * direction;
    //        massPoint.transform.position += movement;
    //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
    //        massPoint.VelocityVector = initialVelocityVector;
    //        massPoint.VelocityVector = massPoint.SpatialVector / Time.deltaTime;
    //        massPoint.AccelerationVector += (massPoint.VelocityVector - initialVelocityVector) / Time.deltaTime;
    //        //massPoint.AccelerationVector += massPoint.SpatialVector / (2 * Mathf.Pow(Time.deltaTime, 2));
    //        //pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;
    //        //INSERIRE IL CONTROLLO SPECIALE DALL'HOOK!
    //    }

    //}

    //private void ApplyConstraints(float pointsDistance, ref float error, ref Vector3 direction, ref Vector3 movement, RopeNode massPoint, RopeNode previousMassPoint)
    //{
    //    if (pointsDistance > MassPointLength)
    //    {

    //        Vector3 initialVelocityVector = Vector3.zero;

    //        error = pointsDistance - MassPointLength;
    //        direction = (previousMassPoint.transform.position - massPoint.transform.position).normalized;
    //        movement = error * direction;
    //        massPoint.transform.position += movement;
    //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
    //        massPoint.VelocityVector = initialVelocityVector;
    //        massPoint.VelocityVector = massPoint.SpatialVector / Time.deltaTime;
    //        massPoint.AccelerationVector += (massPoint.VelocityVector - initialVelocityVector) / Time.deltaTime;
    //        //massPoint.AccelerationVector += massPoint.SpatialVector / (2 * Mathf.Pow(Time.deltaTime, 2));
    //    }
    //}

    //public void ActiveMassPoint(int _index)
    //{
    //    RopeNode massPoint;

    //    massPoint = RopeNodes[_index] as RopeNode;
    //    massPoint.transform.gameObject.SetActive(true);
    //}

    //public void DeactiveMassPoint(int _index)
    //{
    //    RopeNode massPoint;

    //    massPoint = RopeNodes[_index] as RopeNode;
    //    massPoint.transform.gameObject.SetActive(false);
    //}

    //public void DestroyMassPoints(int _fromIndex)
    //{
    //    RopeNode massPoint;

    //    if (_fromIndex == 0)
    //    {
    //        for (int i = _fromIndex; i < RopeNodes.Count; i++)
    //        {
    //            massPoint = RopeNodes[i] as RopeNode;
    //            Destroy(massPoint.transform.gameObject);
    //        }
    //        RopeNodes.Clear();
    //        HookReset();
    //        ResetRewindVector();
    //    }
    //    else
    //    {
    //        for (int i = _fromIndex; i < RopeNodes.Count; i++)
    //    {
    //        massPoint = RopeNodes[i] as RopeNode;
    //        Destroy(massPoint.transform.gameObject);
    //        RopeNodes.RemoveAt(i);
    //    }
    //    }
    //}

    //private void LateUpdate() {

    //if (debugMode) {
    //    if (Input.GetKeyDown(KeyCode.Mouse0) /*|| Input.GetButtonDown("ShootPS4")*/ || Input.GetButtonDown("ShootXBOX")) {
    //        HookShooting();
    //        if (!hook.shooted)
    //            InstantiateRope();
    //    }
    //    if (hook.shooted) {
    //        if (Input.GetKeyDown(KeyCode.Z))
    //            UpdatePoints();
    //        if (Input.GetKeyDown(KeyCode.X))
    //            UpdateHook();
    //        if (Input.GetKeyDown(KeyCode.C))
    //            UpdateLinks();
    //    }
    //}
    //else {
    //    if (Input.GetKeyDown(KeyCode.Mouse0) /*|| Input.GetButtonDown("ShootPS4")*/ || Input.GetButtonDown("ShootXBOX")) {
    //        HookShooting();
    //        if (!hook.shooted)
    //            InstantiateRope();
    //    }
    //    if (hook.shooted) {
    //        if (!Input.GetKeyDown(KeyCode.Mouse1) || !Input.GetButtonDown("ShootXBOX")) {
    //            UpdatePoints();
    //            UpdateHook();
    //            UpdateLinks();
    //        }
    //        if ((Input.GetKey(KeyCode.Mouse1) /* || (Input.GetButton("ShootPS4") && !Input.GetButtonUp("ShootPS4"))*/ || (Input.GetAxis("Rewind") > 0))) {
    //            RewindPoints();
    //        }

    //        if (/*Input.GetButtonDown("UnhookPS4") ||*/ Input.GetButtonDown("UnhookXBOX")) {
    //            while (hook.shooted) {
    //                RewindPoints();
    //            }
    //        }

    //    }

    //    if (hook.shooted && !hook.isHooked) {
    //        hit = hook.RaycastCollsion();

    //        if (hit.transform != null && hit.transform.GetComponent<FirstBossMask>()) {
    //            hookPoint = hit.transform.GetComponent<FirstBossMask>();
    //            hook.isHooked = true;
    //        }
    //        else {
    //            //UpdateHook();

    //            //Debug.Log("Missing Target");


    //        }
    //    }

    //    if (hook.shooted && hook.ropeFinished && !hook.isHooked) {
    //        //hook.hitDistance = 0;
    //    }
    //    else {
    //        hook.hitDistance = 1;
    //    }


    //    if (hook.isHooked) {
    //        if (!hookPoint.isHooked) {

    //            hook.transform.position = hit.transform.position;
    //            hook.Inertia = Vector3.zero;
    //            hook.hitDistance = 0;
    //        }
    //        hookPoint.isHooked = true;

    //    }
    //    else {
    //        if (hookPoint != null) {
    //            hookPoint.isHooked = false;
    //        }
    //        hook.hitDistance = 1;
    //    }
    //}
    //}

    //public void ApplyConstraintsHookToPC()
    //{
    //    float springValue = 1f;
    //    Vector3 SumOfLength = Vector3.zero;
    //    float pointsDistance = 0;
    //    float error = 0;
    //    Vector3 previousInstance = Vector3.zero;
    //    Vector3 direction = Vector3.zero;
    //    Vector3 movement = Vector3.zero;

    //    RopeNode previousMassPoint;
    //    RopeNode massPoint = RopeNodes[0] as RopeNode;

    //    pointsDistance = (hook.transform.position - massPoint.transform.position).magnitude;

    //    if (pointsDistance > MassPointLength)
    //    {
    //        error = pointsDistance - MassPointLength;
    //        direction = (hook.transform.position - massPoint.transform.position).normalized;
    //        movement = error * direction;
    //        massPoint.transform.position += movement;
    //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
    //        pointsDistance = (hook.transform.position - massPoint.transform.position).magnitude;

    //    }
    //    SumOfLength += (massPoint.transform.position - hook.transform.position);

    //    for (int i = 1; i < RopeNodes.Count; i++)
    //    {
    //        massPoint = RopeNodes[i] as RopeNode;
    //        previousMassPoint = RopeNodes[i - 1] as RopeNode;
    //        pointsDistance = (massPoint.transform.position - previousMassPoint.transform.position).magnitude;
    //        //ApplyConstraints(pointsDistance, ref error, ref direction, ref movement, massPoint, previousMassPoint);
    //        SumOfLength += (massPoint.transform.position - previousMassPoint.transform.position);
    //        //Debug.LogFormat("SumOfLength(for): {0}", SumOfLength);
    //    }

    //    massPoint = RopeNodes[RopeNodes.Count - 1] as RopeNode;
    //    pointsDistance = (GrappleGun.transform.position - massPoint.transform.position).magnitude;
    //    float playerY = Player.transform.position.y;

    //    if (pointsDistance > MassPointLength)
    //    {
    //        error = pointsDistance - MassPointLength;
    //        direction = (GrappleGun.transform.position - massPoint.transform.position).normalized;
    //        movement = error * direction;
    //        //Player.transform.position -= movement * springValue;
    //        //Player.transform.position = new Vector3(Player.transform.position.x, playerY, Player.transform.position.z);
    //        massPoint.transform.position += movement * (1 - springValue);
    //        massPoint.SpatialVector = (massPoint.transform.position - massPoint.OldPos);
    //        pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
    //    }

    //    massPoint = RopeNodes[RopeNodes.Count - 1] as RopeNode;
    //    pointsDistance = (massPoint.transform.position - GrappleGun.transform.position).magnitude;
    //    SumOfLength += (GrappleGun.transform.position - massPoint.transform.position);
    //    Debug.Log("SumOf: " + SumOfLength.magnitude);

    //    if ((SumOfLength * (2 - springValue)).magnitude > MassPointLength * MassPointNumber)
    //    {
    //        hook.ropeFinished = true;
    //        Debug.Log("Ehi!");
    //    }
    //    else
    //    {
    //        hook.ropeFinished = false;
    //    }

    //}

    #endregion
}


