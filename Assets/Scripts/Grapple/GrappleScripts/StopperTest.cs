﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StopperTest : MonoBehaviour
{

    //Inspector
    public RuneTutorial rune;

    private void OnCollisionExit(Collision collision) {
        
        if (collision.transform.GetComponent<FirstBossMask>()) {
            Debug.Log("Stopper hit by mask");
            rune.isSlowingDown = true;
            collision.transform.GetComponent<FirstBossMask>().SlowDown(rune.DecelerationCurve , rune.timer - Time.deltaTime, rune.timer , rune.iterations);
            rune.maskMaterialStatus.SetFloat("Vector1_5031454D", DOVirtual.EasedValue(rune.actualMaskMaterialIntensity, rune.maskMaterialIntensityValueStopped, rune.changeMaterialTimePercentage, rune.maskMaterialAlphaEase));
            rune.maskCrystalMaterialStatus.SetFloat("Vector1_B11A500B", DOVirtual.EasedValue(rune.actualMaskCrystalMaterialIntensity, rune.maskCrystalMaterialIntensityValueStopped, rune.changeMaskCrystalMaterialTimePercentage, rune.maskCrystalMaterialAlphaEase));
        }
    }
}
