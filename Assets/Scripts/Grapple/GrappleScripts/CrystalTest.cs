﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalTest : MonoBehaviour
{

    //Inspector
    public GameObject stopper;
    public RuneTutorial rune;


    private void OnCollisionEnter(Collision collision) {
        if (collision.transform.GetComponent<FirstBossMask>()) {
            rune.InvertRotation = true;
            stopper.gameObject.SetActive(true);
        }
    }


}
