﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedHookPoint : HookPointBase
{

    public SecondBossController boss;

    void Update()
    {
        if (isHooked)
        {
            hook.transform.position = transform.position;
            boss.isHooked = true;
        }
        else
        {
            boss.isHooked = false;
        }
    }
}
