﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookPointFix : HookPointBase
{

    //Private 
    //float realDistance;
    //float realAngle;
    //Vector3 distanceVector;
    //float error;

    private void Start() {
        //GetRealDistance();
    }

    
    public void VerletBossRigidBody(BossController _boss) {

        if (_boss.GetComponent<FirstBossController>()) {
            //TODO
        }
        else if (_boss.GetComponent<SecondBossController>()) {
            if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
            {
                hook.transform.position = transform.position;
                _boss.isPulled = false;
                _boss.UpdatePulledState(_boss.animator);
            }
            else
            {
                _boss.isPulled = true;
                _boss.UpdatePulledState(_boss.animator);
            }

            GameObject mount = _boss.GetComponent<SecondBossController>().MountGraphics;
            float resistanceToRotation = _boss.GetComponent<SecondBossController>().resistanceToRotation;
            Vector3 _bossToHookPointDistance = Vector3.ProjectOnPlane((transform.position - mount.transform.position), Vector3.up);
            Vector3 _hookToBossDistance = Vector3.ProjectOnPlane((mount.transform.position - hook.transform.position), Vector3.up);

            //Calcolo l'angolo.
            float rotationAngle = Vector3.SignedAngle(_bossToHookPointDistance, -_hookToBossDistance, Vector3.up);
            mount.transform.localEulerAngles += new Vector3(0, rotationAngle * (1 - resistanceToRotation), 0);

            //float angleBossToHookDir = Vector3.Angle(mount.transform.forward, -hook.pullDirection);
            //hook.transform.position += (_hookToBossDistance.magnitude * Mathf.Sin((rotationAngle * (1 - resistanceToRotation)) * Mathf.Deg2Rad) / Mathf.Sin(angleBossToHookDir * Mathf.Deg2Rad)) * hook.pullDirection;
            //_hookToBossDistance = Vector3.ProjectOnPlane((mount.transform.position - hook.transform.position), Vector3.up);

            float error;
            error = Mathf.Clamp(_hookToBossDistance.magnitude - _bossToHookPointDistance.magnitude, 0, Mathf.Infinity);
            //_boss.transform.position -= error / 2 * mount.transform.forward;
            hook.transform.position = transform.position;
        }
    }

    public void VerletBossRigidBody(BossControllerCustom _boss) {

        if (_boss.GetComponent<FirstBossController>()) {
            //TODO
        }
        else if (_boss.GetComponent<SecondBossController>()) {
            if (_boss.GetComponent<SecondBossController>().canMoveAsRigidBody) {

                if (Input.GetAxis("Horizontal") == 0 || Input.GetAxis("Vertical") == 0) {
                    hook.transform.position = transform.position;
                    _boss.isPulled = false;
                    _boss.UpdatePulledState(_boss.animator);
                }
                else {
                    _boss.isPulled = true;
                    _boss.UpdatePulledState(_boss.animator);
                }

                GameObject mount = _boss.GetComponent<SecondBossController>().MountGraphics;
                float resistanceToRotation = 0.5f;
                Vector3 _bossToHookPointDistance = Vector3.ProjectOnPlane((transform.position - mount.transform.position), Vector3.up);
                Vector3 _hookToBossDistance = Vector3.ProjectOnPlane((mount.transform.position - hook.transform.position), Vector3.up);

                //Calcolo l'angolo.
                float rotationAngle = Vector3.SignedAngle(_bossToHookPointDistance, -_hookToBossDistance, Vector3.up);
                mount.transform.localEulerAngles += new Vector3(0, rotationAngle * resistanceToRotation, 0);

                //float angleBossToHookDir = Vector3.Angle(mount.transform.forward, -hook.pullDirection);
                //hook.transform.position += (_hookToBossDistance.magnitude * Mathf.Sin((rotationAngle * (1 - resistanceToRotation)) * Mathf.Deg2Rad) / Mathf.Sin(angleBossToHookDir * Mathf.Deg2Rad)) * hook.pullDirection;
                //_hookToBossDistance = Vector3.ProjectOnPlane((mount.transform.position - hook.transform.position), Vector3.up);

                float error;
                error = Mathf.Clamp(_hookToBossDistance.magnitude - _bossToHookPointDistance.magnitude, 0, Mathf.Infinity);
                _boss.transform.position -= error / 2 * mount.transform.forward;
                hook.transform.position = transform.position;
            }
            else {
                hook.transform.position = transform.position;
            }
        }
    }


    //private void GetRealDistance() {
    //    if (bossRelated) {
    //        if (boss.GetComponent<SecondBossController>()) {
    //            GameObject mount = boss.GetComponent<SecondBossController>().MountGraphics;
    //            realDistance = Vector3.ProjectOnPlane((mount.transform.position - transform.position), Vector3.up).magnitude;
    //        }
    //    }

    //}

    //private void GetRealDistance() {
    //    if (boss.GetComponent<SecondBossController>()) {
    //        GameObject mount = boss.GetComponent<SecondBossController>().MountGraphics;
    //        distanceVector = Vector3.ProjectOnPlane((transform.position - mount.transform.position), Vector3.up);
    //        //distanceVector = (transform.position - mount.transform.position);
    //        realDistance = distanceVector.magnitude;
    //        realAngle = Vector3.SignedAngle(mount.transform.forward, distanceVector, Vector3.up);
    //    }
    //}

}
