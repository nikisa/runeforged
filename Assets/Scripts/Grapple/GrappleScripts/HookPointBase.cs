﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HookPointBase : MonoBehaviour
{

    public GameObject HookPointAsset;
    public bool isHooked;
    public bool isPulled;
    [Header("References")]
    public Hook hook;
    public PlayerController player;
    [Header("Boss Reference")]
    public bool bossRelated;
    public BossController boss;
    public BossControllerCustom bossCustom;
    public int hookPointTag;


    public void SetHookedState() {
        if (bossRelated) 
            boss.animator.SetBool("isHooked" , isHooked);
    }


}