﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : MonoBehaviour
{
    public bool isShooted;
    public bool isHooked;
    public bool isRopeFinished;
    public bool isBullet;
    public bool isRopeNode;
    public float VectorAngle;
    public float DynamicFriction;
    //public float hitDistance;
    public int hookPointID;
    public Vector3 OldPos;
    public Vector3 SpatialVector;

    //[HideInInspector]
    public GameObject currentHitObject;
    //[HideInInspector]
    public int currentTag;
    //Cose aggiunte da Luca
    [HideInInspector]
    public Vector3 VelocityVector;
    [HideInInspector]
    public Vector3 pullDirection;

    //Private
    RaycastHit hit;

    public RaycastHit RaycastCollsion() {
        Debug.DrawRay(transform.position, transform.up, Color.blue);
        return hit;
    }
}
