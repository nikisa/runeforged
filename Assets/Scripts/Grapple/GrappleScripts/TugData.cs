﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TugData", menuName = "TugData/TugData")]
public class TugData : ScriptableObject
{
    [Header("TugMultiplier")]
    public float maxTugMultiplier = 1;
    [Header("Vibration Values")]
    public float heavyStationaryTug;
    public float lightStationaryTug;
    public float heavyMovementTug;
    public float lightMovementTug;
    public float heavyDashTug;
    public float lightDashTug;
    [Header("Vibration Noise Values")]
    public float noiseHeavyStationaryTug;
    public float noiseLightStationaryTug;
    public float noiseHeavyMovementTug;
    public float noiseLightMovementTug;
    public float noiseHeavyDashTug;
    public float noiseLightDashTug;
}