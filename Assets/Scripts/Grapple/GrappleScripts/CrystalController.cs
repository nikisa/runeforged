﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CrystalController : HookPointFix
{

    //Inspector
    public List<GameObject> slowWalls;
    public float timer;
    #region Color
    public Material material;
    public Ease easeColor;
    public float changeTime;
    #endregion
    public float scaleMultiplier;
    public float scaleTime;
    public List<GameObject> crystals;

    //Public
    [HideInInspector]
    public bool activated;


    
    
    //Private
    #region Timer
    private bool activeTimer;
    private float time;
    #endregion
    #region Color values
    int intesityRegulator = 300;
    int intesityRegulatorBase = 200;
    bool isColorActive;
    float colorTimer;
    float timePercentage;
    #endregion
    #region Current Color
    float currentRed;
    float currentGreen;
    float currentBlue;
    #endregion
    #region First Color
    float oldRed = 34;
    float oldGreen = 191;
    float oldBlue = 172;
    #endregion
    #region Second Color
    float newRed = 255;
    float newGreen = 192;
    float newBlue = 0;
    #endregion
    private Vector3 nextScale;
    private bool isScaling;
    private bool isResetting;


    private void Start() {
        isColorActive = false;
        isResetting = true;
        currentRed = oldRed;
        currentGreen = oldGreen;
        currentBlue = oldBlue;
    }

    private void Update() {

        if (activated) {
            foreach (GameObject slowWall in slowWalls) {
                slowWall.gameObject.SetActive(true);
            }
            isColorActive = true;
            colorTimer = 0;
            activeTimer = true;
            activated = false;
        }

        if (isColorActive) {
            ChangeColorShader();
            BiggerScale();
        }
        else {
            ResetColorShader();
            SmallerScale();
        }

        if (activeTimer) {
            time += Time.deltaTime;
            if (time >= timer) {
                isColorActive = false;
                colorTimer = 0;
                foreach (GameObject slowWall in slowWalls) {
                    slowWall.gameObject.SetActive(false);
                }
                activeTimer = false;
                time = 0;
                isResetting = false;
            }
        }
    }

    void ChangeColorShader() {
        colorTimer += Time.deltaTime;
        timePercentage = Mathf.Clamp01(colorTimer / changeTime);
        currentRed = DOVirtual.EasedValue(oldRed, newRed, timePercentage, easeColor);
        currentGreen = DOVirtual.EasedValue(oldGreen, newGreen, timePercentage, easeColor);
        currentBlue = DOVirtual.EasedValue(oldBlue, newBlue, timePercentage, easeColor);

        Color newColor = new Color(currentRed, currentGreen, currentBlue, 0) / intesityRegulatorBase;
        material.SetColor("Color_928ADAC2", newColor);

    }

    void ResetColorShader() {
        colorTimer += Time.deltaTime;
        timePercentage = Mathf.Clamp01(colorTimer / changeTime);
        currentRed = DOVirtual.EasedValue(newRed, oldRed, timePercentage, easeColor);
        currentGreen = DOVirtual.EasedValue(newGreen, oldGreen, timePercentage, easeColor);
        currentBlue = DOVirtual.EasedValue(newBlue, oldBlue, timePercentage, easeColor);

        Color newColor = new Color(currentRed, currentGreen, currentBlue, 0) / intesityRegulator;
        material.SetColor("Color_928ADAC2", newColor);
    }

    void BiggerScale() {
        if (!isScaling) {
            foreach (GameObject crystal in crystals) {
                Debug.Log("isScaling");
                nextScale = new Vector3(crystal.transform.localScale.x * scaleMultiplier, crystal.transform.localScale.y * scaleMultiplier, crystal.transform.localScale.z * scaleMultiplier);
                crystal.transform.DOScale(nextScale, scaleTime);
            }
            isScaling = true;
        }  
        
    }

    void SmallerScale() {
        if (!isResetting) {
            foreach (GameObject crystal in crystals) {
                Debug.Log("is NOT Scaling");
                nextScale = new Vector3(crystal.transform.localScale.x / scaleMultiplier, crystal.transform.localScale.y / scaleMultiplier, crystal.transform.localScale.z / scaleMultiplier);
                crystal.transform.DOScale(nextScale, scaleTime);
            }
            isResetting = true;
            isScaling = false;
        }
    }

}

