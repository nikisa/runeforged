﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;


public class TugState : GrappleBaseState
{

    //Inspector
    public TugData tugData;


    //Private
    PlayerIndex playerIndex;

    public override void Enter(){
        grappleManager.Player.isTugging = true;
        grappleManager.PlayTugFX();
        grappleManager.StartHookedFX();

    }

    public override void Tick()
    {
        if (!GameManager.instance.isGamePaused) {

            grappleManager.UpdatePoints();
            grappleManager.UpdateHook(); //Lo incorporiamo in UpdatePoints?
            //in TugState non si usa RewindPoints, ma si controlla il valore dell'ultimo totalRope.
            //in base al valore dell'ultimo totalRope si valuta se e come eseguire RewindPoints.
            //grappleManager.RewindPoints();
            grappleManager.Tug_RewindPoints();
            if (grappleManager.TotalPointsRewinded < grappleManager.RopeNodes.Count) {
                Vibration(tugData); //Vibrazione controller
                grappleManager.Tug_ApplyConstraintsPCToHook();
                //grappleManager.hookPoint.isPulled = true;
                HookPointCheker();
                grappleManager.HookConstraints();
                grappleManager.Tug_ApplyConstraintsHookToPC();
                //grappleManager.TugCheck_ApplyConstraintsPCToHook();
                //grappleManager.HookConstraints();
                //grappleManager.TugCheck_ApplyConstraintsHookToPC();
                grappleManager.ApplyIntegration();
                grappleManager.InactiveMassPointTranslate();
                grappleManager.CollisionController();
                grappleManager.DrawRope();
            }
            else {
                grappleManager.TotalPointsRewinded = 0;

                grappleManager.hook.transform.parent = grappleManager.GrappleGun.transform;
                animator.SetTrigger("RolledUp");
            }


            if (Input.GetButtonDown("UnhookXBOX")) {
                
                grappleManager.Unhook();
                animator.SetTrigger("Rewind");
            }

            //if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {

            //    //ATTENZIONE: questo IF va modificato quando ci saranno le collision
            //    if (Vector3.Angle((grappleManager.GrappleGun.transform.position - grappleManager.hook.transform.position).normalized, grappleManager.Player.VelocityVector) > 90) {
            //        animator.SetTrigger("Hooked");
            //    }
            //}

            if (grappleManager.Player.gameManager.isControllerConnected) {
                if (Input.GetAxis("Rewind") < 0.5f ) {
                    grappleManager.RewindSpatialMagnitude = 0;
                    grappleManager.RewindVelocityMagnitude = 0;
                    animator.SetTrigger("Hooked");
                }
            }
            else {
                if (Input.GetButtonUp("RewindMouse")) {
                    grappleManager.RewindSpatialMagnitude = 0;
                    grappleManager.RewindVelocityMagnitude = 0;
                    animator.SetTrigger("Hooked");
                }
            }
            

            if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetButtonDown("ShootXBOX")) {                
                animator.SetTrigger("Unwind");
            }

            if (Input.GetButton("Attack")) {
                animator.SetTrigger("Attack");
            }
        }
    }

    private void HookPointCheker() {
        if (grappleManager.hookPoint.GetComponent<FirstBossMask>()) {
            grappleManager.hookPoint.GetComponent<FirstBossMask>().Spring(tugData.maxTugMultiplier);
        }
    }

    private void Vibration(TugData _tugVibrationData) {

        //Local
        float noiseHeavyDashTug = Random.Range(0, _tugVibrationData.noiseHeavyDashTug);
        float noiseLightDashTug = Random.Range(0, _tugVibrationData.noiseLightDashTug);
        float noiseHeavyMovementTug = Random.Range(0, _tugVibrationData.noiseHeavyMovementTug);
        float noiseLightMovementTug = Random.Range(0, _tugVibrationData.noiseLightMovementTug);
        float noiseHeavyStationaryTug = Random.Range(0, _tugVibrationData.noiseHeavyStationaryTug);
        float noiseLightStationaryTug = Random.Range(0, _tugVibrationData.noiseLightStationaryTug);

        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0){
            if (Input.GetButtonDown("Dash") && grappleManager.Player.canDash)
                GamePad.SetVibration(playerIndex, _tugVibrationData.heavyDashTug - noiseHeavyDashTug, _tugVibrationData.lightDashTug - noiseLightDashTug);
            else
                GamePad.SetVibration(playerIndex, _tugVibrationData.heavyMovementTug - noiseHeavyMovementTug, _tugVibrationData.lightMovementTug - noiseLightMovementTug);
        }
        else {
            GamePad.SetVibration(playerIndex, _tugVibrationData.heavyStationaryTug - noiseHeavyStationaryTug, _tugVibrationData.lightStationaryTug - noiseLightStationaryTug);
        }

    }


    public override void Exit() {
        grappleManager.Player.isTugging = false;
        GamePad.SetVibration(playerIndex, 0f, 0f);
        grappleManager.hookPoint.isPulled = false;
        grappleManager.hook.transform.position = grappleManager.hookPoint.transform.position;
        grappleManager.CollisionReset();
        grappleManager.hook.transform.position = grappleManager.hookPoint.transform.position;
        grappleManager.StopHookedFX();
        grappleManager.StopChainIdleEvent();
    }
}