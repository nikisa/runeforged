﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnwindState : GrappleBaseState
{

    public override void Enter() {
        grappleManager.PlayUnwindFX();
        grappleManager.StartHookedFX();
    }

    public override void Tick() {
        if (!GameManager.instance.isGamePaused) {
            grappleManager.UpdatePoints();
            grappleManager.UpdateHook();
            grappleManager.RewindPoints();
            if (grappleManager.TotalPointsRewinded /*+ grappleManager.PointsRewinded*/ < grappleManager.RopeNodes.Count) {
                grappleManager.Unwind_ApplyContraintsPCToHook();
                grappleManager.HookConstraints();
                grappleManager.Unwind_ApplyConstraintsHookToPC();
                grappleManager.TugCheck_ApplyConstraintsPCToHook();
                grappleManager.TugCheck_ApplyConstraintsHookToPC();
                grappleManager.ApplyIntegration();
                grappleManager.InactiveMassPointTranslate();
                grappleManager.DrawRope();
            }
            else {
                grappleManager.TotalPointsRewinded = 0;
                //grappleManager.HookReset();
                //grappleManager.RewindSpatialMagnitude = 0;
                //grappleManager.RewindVelocityManitude = 0;
                grappleManager.hook.transform.parent = grappleManager.GrappleGun.transform;
                animator.SetTrigger("RolledUp");
            }

            if (Input.GetButtonDown("UnhookXBOX")) {
                grappleManager.Unhook();
                animator.SetTrigger("Rewind");
            }
            //if (!grappleManager.hookPoint.isHooked) {
            //    grappleManager.hook.SpatialVector = Vector3.zero;
            //    //grappleManager.hook.hitDistance = 0;
            //    animator.SetTrigger("Rewind");
            //}

            //if (!grappleManager.hook.isHooked)
            //{
            //    grappleManager.hook.SpatialVector = Vector3.zero;
            //    grappleManager.hook.hitDistance = 0;
            //    animator.SetTrigger("Rewind");
            //}

            if (Input.GetKeyUp(KeyCode.Mouse0) /*|| Input.GetButtonDown("ShootPS4")*/ || Input.GetButtonUp("ShootXBOX")) {
                grappleManager.RewindSpatialMagnitude = 0;
                grappleManager.RewindVelocityMagnitude = 0;
                animator.SetTrigger("Hooked");
            }



            //if (grappleManager.hook.shooted) {
            //    if (!grappleManager.hook.isHooked) {
            //        grappleManager.RewindPoints();
            //    }
            //    else {
            //        if (Input.GetAxis("Rewind") >= 0.5f) {
            //            grappleManager.RewindPoints();
            //        }
            //        else {
            //            Debug.Log("STOP REWINDING");
            //            animator.SetTrigger("Hooked");
            //        }
            //    }
            //}        

            //if (!grappleManager.hook.shooted) {
            //    animator.SetTrigger("RolledUp");
            //}
        }
    }

    public override void Exit()
    {
        grappleManager.StopHookedFX();
        grappleManager.StopChainIdleEvent();
    }

}
