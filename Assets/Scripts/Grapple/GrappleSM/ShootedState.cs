﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootedState : GrappleBaseState
{

    //Private
    private bool isAttacking;


    public override void Enter() {
        grappleManager.PlayShootFX();
    }


    public override void Tick() {
        if (GameManager.instance != null && !GameManager.instance.isGamePaused) {
            grappleManager.UpdatePoints();
            grappleManager.UpdateHook();
            grappleManager.Fire_ApplyConstraintsPCToHook();
            grappleManager.HookConstraints();

            grappleManager.Fire_ApplyConstraintsHookToPC();      

            //for (int i = 0; i < grappleManager.ApplyConstraintsIterations; i++)
            //{
            grappleManager.Fire_ApplyConstraintsHookToPC();
            //}        

            grappleManager.ApplyIntegration();
            grappleManager.DrawRope();

            //if (!grappleManager.hook.isHooked) {
            //    grappleManager.target = grappleManager.FovPlayer.CorrectRayCast();
            //    target = grappleManager.target;
            //    Vector3 direction = (target.transform.position - grappleManager.hook.transform.position).normalized;
            //    Quaternion rotation = Quaternion.LookRotation(direction);
            //    rotation *= grappleManager.hook.transform.rotation;
            //    grappleManager.hook.transform.rotation = rotation;
            //}


                //        if (target != null) {
                //            if (target.transform.GetComponent<HookPointBase>() /*&& grappleManager.FirstBoss.isHookable*/) {
                //                grappleManager.hookPoint = target.transform.GetComponent<HookPointBase>();
                //                if (target.transform.GetComponent<FirstBossMask>()) {
                //                    grappleManager.hookPoint = target.transform.GetComponent<FirstBossMask>();
                //                }
                //                grappleManager.hook.isHooked = true;
                //                grappleManager.hook.transform.position = target.transform.position;
                //            }
                //        }
                //    }



            }//first if


        if (Input.GetButton("Attack"))
        {
            isAttacking = true;
        }

        if (grappleManager.hook.isHooked && isAttacking)
        {
            isAttacking = false;
            animator.SetTrigger("Attack");
        }
    
    }
}
