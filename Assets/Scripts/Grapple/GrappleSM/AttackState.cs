﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : GrappleBaseState
{
    public override void Enter() {
        grappleManager.Player.isAttacking = true;
        //grappleManager.StartAttackFX();
        
    }

    public override void Tick() {

        if (!GameManager.instance.isGamePaused) {

            grappleManager.UpdatePoints();
            grappleManager.UpdateHook();
            grappleManager.RewindPoints();
            if (grappleManager.TotalPointsRewinded < grappleManager.RopeNodes.Count) {
                grappleManager.Attack_ApplyContraintsPCToHook();
                grappleManager.HookConstraints();
                grappleManager.Attack_ApplyConstraintsHookToPC();
                grappleManager.ApplyIntegration();
                grappleManager.InactiveMassPointTranslate();
                grappleManager.DrawRope();
                if (Input.GetButtonDown("UnhookXBOX")) {
                    grappleManager.Unhook();

                    grappleManager.Player.VelocityVector = grappleManager.RewindVelocityMagnitude * grappleManager.Player.rotationTransform.forward;
                    grappleManager.Player.animator.SetTrigger("Attack");
                    //Torna a Rewind e Aggiorna VelocityVector del Player
                    
                    animator.SetTrigger("Rewind");
                }
            }

            else {
                grappleManager.TotalPointsRewinded = 0;
                //grappleManager.HookReset();
                //grappleManager.RewindSpatialMagnitude = 0;
                //grappleManager.RewindVelocityManitude = 0;
                grappleManager.hook.transform.parent = grappleManager.GrappleGun.transform;
                //in base a target: ATTACK o PUSH
                grappleManager.Player.VelocityVector = grappleManager.RewindMaxSpeed * grappleManager.Player.rotationTransform.forward;
                //Debug.Log("AttackVector :" + grappleManager.Player.VelocityVector);
                grappleManager.Player.animator.SetTrigger("Attack");
                if (grappleManager.hookPoint.bossRelated) {
                    if (grappleManager.hookPoint.boss != null)
                    {
                        grappleManager.hookPoint.boss.animator.SetBool("isHookedBool", false);
                    }
                    else if (grappleManager.hookPoint.bossCustom != null)
                    {
                        grappleManager.hookPoint.bossCustom.animator.SetBool("isHookedBool", false);
                    }
                }
                animator.SetTrigger("RolledUp");
                //TUTTE FUNZIONI ATTACCO VERO E PROPRIO
            }

            //if (Input.GetAxis("Rewind") < 0.5f)
            //{
            //    animator.SetTrigger("Hooked");
            //}

            //else
            //{            
            //    if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            //    {
            //        //go to TUG STATE
            //        animator.SetTrigger("Tug");
            //    }
            //}
        }
    }

    public override void Exit()
    {

        //grappleManager.PlayPunchFX();
        //grappleManager.StopAttackFX();
        //grappleManager.StopChainIdleEvent();

        grappleManager.Player.isAttacking = false;
        grappleManager.Player.AttackFinish = true;


        if (grappleManager.Player.readyToWin && grappleManager.hook.currentHitObject.tag == "CoreHook" ) {
            grappleManager.Player.Victory();
            
        }

        if (grappleManager.Crystals != null) {
            foreach (CrystalController crystal in grappleManager.Crystals) {
                if (crystal.isHooked) {
                    crystal.activated = true;
                    crystal.isHooked = false;
                }
            }
        }

        

    }

}
