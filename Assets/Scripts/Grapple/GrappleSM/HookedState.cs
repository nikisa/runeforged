﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookedState : GrappleBaseState
{
    public override void Enter() {
        if (grappleManager.hookPoint != null)
            grappleManager.hook.transform.position = grappleManager.hookPoint.transform.position;

        grappleManager.StartHookedFX();
        grappleManager.PlayGrabFX();
    }

    public override void Tick() {
        if (!GameManager.instance.isGamePaused) {

            grappleManager.UpdatePoints();
            grappleManager.UpdateHook(); //Lo incorporiamo in UpdatePoints?
            grappleManager.Fire_ApplyConstraintsPCToHook();
            grappleManager.HookConstraints();
            grappleManager.Fire_ApplyConstraintsHookToPC();
            grappleManager.ApplyIntegration();
            grappleManager.DrawRope();

            

            grappleManager.hook.SpatialVector = Vector3.zero;
            //PER ORA, teniamo le successive ATTANZIONE: SONO PER HOOKPOINT!!!
            //grappleManager.hookPoint.isHooked = true;
            if (grappleManager.hook.currentHitObject == null) {
                animator.SetTrigger("Rewind");
            }
            if (grappleManager.hookPoint!=null && grappleManager.hookPoint.GetComponent<FirstBossMask>()) {
                grappleManager.hookPoint.GetComponent<FirstBossMask>().Inertia = Vector3.zero;
            }


            if (Input.GetButtonDown("UnhookXBOX")) {
                grappleManager.Unhook();
                animator.SetTrigger("Rewind");
            }


            if (Input.GetAxis("Rewind") >= 0.5f  || (Input.GetButtonDown("RewindMouse") && !Input.GetButtonUp("RewindMouse"))) {
                animator.SetTrigger("Tug");
            }

            if (Input.GetButton("Attack")) {
                animator.SetTrigger("Attack");
            }

            if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetButtonDown("ShootXBOX")) {
                animator.SetTrigger("Unwind");
            }
        }

    }

    public override void Exit()
    {
        grappleManager.PlayUnhookFX();
        grappleManager.StopHookedFX();
        grappleManager.StopChainIdleEvent();
    }
}