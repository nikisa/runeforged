﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindState : GrappleBaseState
{

    private bool isAttacking;

    public override void Enter() {
        grappleManager.hook.isBullet = false;
        grappleManager.hook.isRopeNode = true;
        grappleManager.Player.AttackFinish = false;

        grappleManager.PlayRewindFX();

        //DA TOGLIERE POI
        if (GameManager.instance == null) {
            GameManager.instance = grappleManager.Player.gameManager; 
        }

    }

    public override void Tick() {

        if (!GameManager.instance.isGamePaused) {
            grappleManager.UpdatePoints();
            grappleManager.UpdateHook();
            grappleManager.RewindPoints();
            if (grappleManager.TotalPointsRewinded < grappleManager.RopeNodes.Count) {
                grappleManager.Rewind_ApplyContraintsPCToHook();
                grappleManager.HookConstraints();
                grappleManager.Rewind_ApplyConstraintsHookToPC();
                grappleManager.ApplyIntegration();
                grappleManager.InactiveMassPointTranslate();
                grappleManager.DrawRope();
            }
            else {
                grappleManager.TotalPointsRewinded = 0;
                //grappleManager.HookReset();
                //grappleManager.RewindSpatialMagnitude = 0;
                //grappleManager.RewindVelocityManitude = 0;
                grappleManager.hook.transform.parent = grappleManager.GrappleGun.transform;
                animator.SetTrigger("RolledUp");
            }

            if (grappleManager.hook.isHooked && grappleManager.hook.isRopeFinished && grappleManager.hookPoint != null) {
                //if (grappleManager.hookPoint.VelocityVector.magnitude > 0) { //PROVVISORIO
                //    grappleManager.hook.isHooked = false;
                //    grappleManager.hook.transform.parent = grappleManager.GrappleGun.transform;
                //    grappleManager.hookPoint.isHooked = false;
                //    animator.SetTrigger("RolledUp");
                //}

                if (Input.GetAxis("Rewind") >= 0.5f || (Input.GetButtonDown("RewindMouse") && !Input.GetButtonUp("RewindMouse"))) {
                    animator.SetTrigger("Tug");
                }
                else {
                    animator.SetTrigger("Hooked");
                }
            }

            if (Input.GetKeyDown(KeyCode.Mouse0) /*|| Input.GetButtonDown("ShootPS4")*/ || Input.GetButtonDown("ShootXBOX")) {
                animator.SetTrigger("Unwind");
            }

            if (Input.GetButton("Attack")) {
                isAttacking = true;
            }

            if (grappleManager.hook.isHooked && isAttacking)
            {
                isAttacking = false;
                animator.SetTrigger("Attack");
            }
        }
    }

    public override void Exit() {

    }

}
