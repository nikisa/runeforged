﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RolledUpState : GrappleBaseState
{

    public override void Enter() {
        grappleManager.IsSet = false;
        grappleManager.lineRenderer.enabled = false;
        //TODO StopRolledUp();
        


    }

    public override void Tick() {

        

        if ((Input.GetKeyDown(KeyCode.Mouse0) || Input.GetButtonDown("ShootXBOX")) && !grappleManager.Player.isFreeCam) {
            grappleManager.OnShootingStart();
            grappleManager.ShootingSpiral();
            animator.SetTrigger("Shooted");
        }
    }

    public override void Exit() {
        grappleManager.lineRenderer.enabled = true;
        
    }
}
