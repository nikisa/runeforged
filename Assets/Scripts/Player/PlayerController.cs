﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngineInternal.Input;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System;
using FMODUnity;

public class PlayerController : CharacterControllerMovementBase
{
    //Events
    #region Events
    public delegate void GameEvent();
    public static GameEvent DeathEvent;
    public static GameEvent VictoryEvent;
    public static GameEvent TimerEvent;
    public static GameEvent DmgEvent;
    public static GameEvent DisableInputEvent;


    private void OnEnable()
    {
        DeathEvent += PlayerDeath;
        VictoryEvent += Victory;
        TimerEvent += StartTimerDash;
        DmgEvent += TakeDmg;
        DisableInputEvent += DisableInputCall;
    }

    private void OnDisable()
    {
        DeathEvent -= PlayerDeath;
        VictoryEvent -= Victory;
        TimerEvent -= StartTimerDash;
        DmgEvent -= TakeDmg;
        DisableInputEvent -= DisableInputCall;
    }
    #endregion

    //Inspector
    #region Inspector Variables
    [Header("Data")]
    public DataInput dataInput;
    public TargetType playerTarget;
    [Header("Boost")]
    public float boostChargeTime;
    [Header("Animator")]
    public Animator animator;
    public Animator graphicAnimator;
    [Header("Managers")]
    public GrappleManager grappleManager;
    public GameManager gameManager;
    [Header("Child GameObjects")]
    public Transform rotationTransform;
    //public GameObject motion;
    public GameObject body;
    public GameObject BodyMesh;
    public GameObject BrokenBodyMesh;
    [Header("Movement Animation")]
    public float movementRatio;
    public float movementAnimationDumpTime;
    [Header("VFX")]
    public GameObject DashVFX;
    public GameObject BoostChargeVFX;
    public GameObject HitVFX; //PlayOnAwake must be toggled on every particle systems
    public GameObject ExposedCore;
    [Header("Damage")]
    public float DPS;
    [Header("Dead Zone")]
    [Range(0, 1)]
    public float MovementDeadZoneValue;
    [Range(0, 1)]
    public float AimDeadZoneValue;
    [Header("Tweeining")]
    public float TweeningRotationTime;
    public Ease TweeningRotationEase;
    public float ExposedCoreCallbackTime;
    public float invincibilitySeconds;
    [Header("SFX")]
    [SerializeField]
    [EventRef]
    private string dashFX;
    [SerializeField]
    [EventRef]
    private string damagedFX;
    [Header("Extra")]
    public float TimeInputDisable;
    public FreeLookCamera freeLookCamera;
    public CameraTracker cameraTracker;

    #endregion

    //Public
    public int Lifes;
    #region public variables
    [HideInInspector]
    public bool canDash;
    [HideInInspector]
    public bool isDashing;
    [HideInInspector]
    public bool isTugging;
    [HideInInspector]
    public bool isBoosting;
    [HideInInspector]
    public bool isAttacking;
    [HideInInspector]
    public bool AttackFinish;
    [HideInInspector]
    public bool readyToWin;
    [HideInInspector]
    public float plusDeltaTime;
    [HideInInspector]
    public PlayerDashData playerDashData;
    [HideInInspector]
    public PlayerMovementData playerMovementData;
    [HideInInspector]
    public PlayerDecelInTimeData playerDecelInTimeData;
    [HideInInspector]
    public Vector3 dashDirection;
    //[HideInInspector]
    //public ParticleSystem dashLightning;
    [HideInInspector]
    public float InitialDashVelocity;
    [HideInInspector]
    public int layerMask;
    [HideInInspector]
    public float skin = .95f;
    [HideInInspector]
    public float dashVelocityModule;
    [HideInInspector]
    public float horizontalDash;
    [HideInInspector]
    public float verticalDash;
    [HideInInspector]
    public float timerDash;
    [HideInInspector]
    public ParticleSystem hitVFX;
    [HideInInspector]
    public bool IsImmortal;
    [HideInInspector]
    public bool ImmortalTutorial;
    [HideInInspector]
    public float forwardVelocity;
    [HideInInspector]
    public bool InputEnabled;
    [HideInInspector]
    public Vector3 MoveDirection;
    [HideInInspector]
    public float SsTimer;
    [HideInInspector]
    public bool isFreeCam;
    #endregion

    //Private
    Camera camera;
    Vector3 move;
    bool isPaused;
    float height;
    float timer;

    String menuPause = "MenuPause";
    float motionAngle;

    protected virtual void Awake()
    {
        isFreeCam = false;
        playerTarget.instance = this.gameObject;
        InputEnabled = false;
        isPaused = false;
        readyToWin = false;
        AttackFinish = false;

        foreach (var item in animator.GetBehaviours<PlayerBaseState>())
        {
            item.SetContext(this, animator, graphicAnimator);
        }
    }

    protected virtual void Start()
    {

        #region SFX
        //var soundDashInstance = RuntimeManager.CreateInstance(dashFX);
        //soundDashInstance.start();
        //var soundDamagedInstance = RuntimeManager.CreateInstance(dashFX);
        //soundDamagedInstance.start();
        #endregion

        gameManager = GameObject.FindObjectOfType<GameManager>();
        height = transform.position.y;
        canDash = true;
        camera = Camera.main;
        hitVFX = HitVFX.transform.GetChild(0).GetComponent<ParticleSystem>();
    }

    private void Update()
    {

        timer += Time.deltaTime;

        Debug.DrawRay(transform.position, VelocityVector, Color.blue, .02f);
        Debug.DrawRay(transform.position, AccelerationVector, Color.red, .02f);
        transform.position = new Vector3(transform.position.x, height, transform.position.z);
        InputEnabled = true;

        if (InputEnabled && !isFreeCam)
        {
            CheckInput();
            UpdateOriantation();
            SetAnimationParameter();
        }

        if (gameManager.isGameOver && Input.anyKey)
        {
            Time.timeScale = 1;
            gameManager.GameOver.Disable();
            gameManager.isGameOver = false;
            gameManager.ReloadLevelAsync();
        }
        
        if (gameManager.isVictory && Input.anyKey)
        {
            Time.timeScale = 1;
            gameManager.Victory.Disable();
            gameManager.isVictory = false;
            gameManager.ReloadLevelAsync();
        }

        if ((Lifes == 1) && ((int)timer%60)%ExposedCoreCallbackTime == 0) {
            ExposedCore.transform.gameObject.SetActive(false);
            ExposedCore.transform.gameObject.SetActive(true);
            
        }

        animator.SetBool("RopeFinished" , grappleManager.hook.isRopeFinished);

        if (Input.GetButtonDown("Triangle")) {
            freeLookCamera.GetComponent<FreeLookCamera>().enabled = true;
            cameraTracker.GetComponent<CameraTracker>().enabled = false;
            isFreeCam = !isFreeCam;
        }

    }

    public void PlayDamagedFX()
    {
        RuntimeManager.PlayOneShot(damagedFX);
    }

    //private void CheckPause() {
    //    if (Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("Pause")) {
    //        isPaused = !isPaused;

    //        if (isPaused) {
    //            PauseCanvas.SetActive(true);
    //            Time.timeScale = 0;
    //        }
    //        else {
    //            PauseCanvas.SetActive(false);
    //            Time.timeScale = 1;
    //        }
    //    }
    //}


    public void PlayDashFX() {
        RuntimeManager.PlayOneShot(dashFX);
    }


    void CalculateOrientationFromMouse()
    {
        // point in plane from mouse position
        Vector3 mouse = Input.mousePosition;
        if (camera != null) {
            var ray = camera.ScreenPointToRay(mouse);
            float x = ray.origin.x - ray.direction.x * ray.origin.y / ray.direction.y;
            float z = ray.origin.z - ray.direction.z * ray.origin.y / ray.direction.y;
            Vector3 point = new Vector3(x, 0, z);

            var direction = (point - transform.position);
            direction.y = 0;
            if (direction.sqrMagnitude > 0.001f) dataInput.currentOrientation = Quaternion.LookRotation(direction.normalized);
        }
    }

    public void SetAnimationParameter()
    {

        MoveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;

        MoveDirection = body.transform.InverseTransformDirection(MoveDirection);

        graphicAnimator.SetFloat("Horizontal", MoveDirection.x, movementAnimationDumpTime, Time.deltaTime);
        graphicAnimator.SetFloat("Vertical", MoveDirection.z, movementAnimationDumpTime, Time.deltaTime);
    }


    public void CheckInput()
    {
        float horizontalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");
        float horizontalRotation = Input.GetAxis("HorizontalLook");
        float verticalRotation = Input.GetAxis("VerticalLook");

        if (Mathf.Pow(horizontalRotation, 2) + Mathf.Pow(verticalRotation, 2) >= Mathf.Pow(AimDeadZoneValue, 2))
        {
            dataInput.HorizontalLook = Input.GetAxis("HorizontalLook");
            dataInput.VerticalLook = Input.GetAxis("VerticalLook");
        }

        if (Mathf.Pow(horizontalMovement, 2) + Mathf.Pow(verticalMovement, 2) >= Mathf.Pow(MovementDeadZoneValue, 2))
        {
            #region Squash&Stratch
            //motionAngle = Vector3.SignedAngle(Vector3.forward , new Vector3(horizontalMovement , 0 , verticalMovement) , Vector3.up);
            //motion.transform.eulerAngles = new Vector3(motion.transform.rotation.x, motionAngle, motion.transform.rotation.z);
            #endregion
            dataInput.Horizontal = Input.GetAxis("Horizontal");
            dataInput.Vertical = Input.GetAxis("Vertical");
        }
        else
        {
            dataInput.Horizontal = 0;
            dataInput.Vertical = 0;
        }


        dataInput.Dash = Input.GetButtonDown("Dash");

        //CalculateOrientationFromMouse();//Da rimuovere e tenere solo nell'if sotto


        Vector3 lookVector = Vector3.right * dataInput.HorizontalLook + Vector3.forward * dataInput.VerticalLook;

        #region Squash&Stratch
        //lookVector = Quaternion.AngleAxis(-motionAngle, Vector3.up) * lookVector;
        #endregion

        if (!gameManager.isControllerConnected && !isFreeCam) {
            CalculateOrientationFromMouse();
        }
        else {
            dataInput.currentOrientation = Quaternion.LookRotation(lookVector.normalized);
        }
    }

    public void DoFreeze(float _timeFreeze, float _rallenty)
    {
        StartCoroutine(DoFreezeCoroutine(_timeFreeze, _rallenty));
    }

    IEnumerator DoFreezeCoroutine(float _timeFreeze, float _rallenty)
    {
        float originalScale = Time.timeScale;
        Time.timeScale = _rallenty;
        yield return new WaitForSecondsRealtime(_timeFreeze);
        Time.timeScale = originalScale;
    }

    public void Dash(float _dashVelocityModule, Vector3 _targetDir, AnimationCurve _dashCurve, float _t0, float _t1, int _iterations)
    {
        targetDir = _targetDir;
        Vector3 dashVectorTemp = targetDir;
        VelocityVector = dashVectorTemp.normalized * _dashVelocityModule;
        move = dashVectorTemp.normalized * Integration.IntegrateCurve(_dashCurve, _t0, _t1, _iterations);
        CharacterController.Move(move);
        //    if (CharacterController == null) {
        //        CheckCollision(this , move , radius);
        //    }
        //    else {
        //        transform.position += move;
        //        CharacterController.Move(move);
        //    }
    }

    //Here and not in BaseMovement because it could change over time
    //public void DashDeceleration(AnimationCurve _dashDecelCurve, float _t0, float _t1, int _iterations)
    //    {
    //    float dashSpeedModule = playerDashData.ActiveDashDistance / playerDashData.ActiveDashTime;
    //    float vectorAngle = Vector3.SignedAngle(Vector3.forward, VelocityVector.normalized, Vector3.up) * Mathf.Deg2Rad;
    //    DecelerationVector = new Vector3(Mathf.Sin(vectorAngle) * DecelerationModule, 0, Mathf.Cos(vectorAngle) * DecelerationModule);
    //    VelocityVector = _dashDecelCurve.Evaluate(_t1) * DecelerationVector.normalized;
    //    move = DecelerationVector.normalized * Integration.IntegrateCurve(_dashDecelCurve, _t0, _t1, _iterations);
    //    if (CharacterController == null) {
    //        CheckCollision(this, move, radius);
    //    }
    //    else {
    //        transform.position += move;
    //        CharacterController.Move(move);
    //    }

    //}

    public void DashDeceleration(AnimationCurve _dashDecelCurve, float _t0, float _t1, int _iterations) {
        float dashSpeedModule = playerDashData.ActiveDashDistance / playerDashData.ActiveDashTime;
        float vectorAngle = Vector3.SignedAngle(Vector3.forward, VelocityVector.normalized, Vector3.up) * Mathf.Deg2Rad;
        DecelerationVector = new Vector3(Mathf.Sin(vectorAngle) * DecelerationModule, 0, Mathf.Cos(vectorAngle) * DecelerationModule);
        VelocityVector = _dashDecelCurve.Evaluate(_t1) * DecelerationVector.normalized;
        move = DecelerationVector.normalized * Integration.IntegrateCurve(_dashDecelCurve, _t0, _t1, _iterations);
        CharacterController.Move(move);
    }


    public void TakeDmg()
    {
        HitVFX.SetActive(true);//PlayOnAwake must be toggled on every particle systems
        BodyMesh.SetActive(false);
        BrokenBodyMesh.SetActive(true);

        Lifes--;
        //uiManager.LifeUpdate(Lifes);

        if (Lifes == 0)
        {
            PlayerDeath();
            RuntimeManager.UnloadBank("Boss");
            RuntimeManager.UnloadBank("Player");
            RuntimeManager.UnloadBank("Music");
        }
        else
        {
            PlayDamagedFX();
            StartCoroutine(InvicibleSecond(invincibilitySeconds));
        }
    }


    public void DisableInputCall()
    {
        StartCoroutine(InputDisableCourutine());
    }


    public IEnumerator InputDisableCourutine()
    {
        InputEnabled = false;
        yield return new WaitForSeconds(TimeInputDisable);
        InputEnabled = true;
    }


    public void SetDashVelocity(float _dashDistance, float _dashTime)
    {
        float dashVelocity = _dashDistance / _dashTime;
        dashVelocityModule = dashVelocity;
    }


    public bool checkDeadZone()
    {
        if (Mathf.Pow(Input.GetAxis("Horizontal"), 2) + Mathf.Pow(Input.GetAxis("Vertical"), 2) >= Mathf.Pow(MovementDeadZoneValue, 2))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public void PlayerInclination()
    {
        // Ruoto il personaggio in funzione della del suo movimento
        Vector3 rotationAxis = Quaternion.AngleAxis(90, Vector3.up) * VelocityVector;
        Quaternion moveRotation = Quaternion.AngleAxis(VelocityVector.magnitude * movementRatio, rotationAxis);
        body.transform.rotation = moveRotation * rotationTransform.rotation;
    }

    public void UpdateOriantation()
    {
        if (!grappleManager.hook.isBullet && !grappleManager.hook.isHooked)
        {
            rotationTransform.DOLocalRotateQuaternion(dataInput.currentOrientation, TweeningRotationTime).SetEase(TweeningRotationEase);
            //CalculateOrientationFromMouse();
        }
        else
        {
            if (grappleManager.lastNodeOnCollision == null) {
                rotationTransform.LookAt(new Vector3(grappleManager.hook.transform.position.x, rotationTransform.position.y, grappleManager.hook.transform.position.z));
                dataInput.HorizontalLook = rotationTransform.forward.x;
                dataInput.VerticalLook = rotationTransform.forward.z;
            }
            else {
                rotationTransform.LookAt(new Vector3(grappleManager.lastNodeOnCollision.transform.position.x, rotationTransform.position.y, grappleManager.lastNodeOnCollision.transform.position.z));
                dataInput.HorizontalLook = rotationTransform.forward.x;
                dataInput.VerticalLook = rotationTransform.forward.z;
            }
        }
    }


    public void StopPlayer()
    {
        forwardVelocity = 0;
        VelocityVector = Vector3.zero;
    }

    public void StartTimerDash()
    {
        timerDash = Time.time;
    }

    public void PlayerDeath()
    {
        Debug.Log("MORTO");
        gameManager.isGameOver = true;
        Time.timeScale = 0;
        gameManager.GameOver.Setup();
        //SceneManager.LoadScene(2);
    }

    public void Victory()
    {
        gameManager.isVictory = true;
        Time.timeScale = 0;
        RuntimeManager.UnloadBank("Boss");
        RuntimeManager.UnloadBank("Player");
        RuntimeManager.UnloadBank("Music");
        gameManager.Victory.Setup();
        //SceneManager.LoadScene(3);
    }


    public IEnumerator InvicibleSecond(float _sec)
    {
        IsImmortal = true;
        #region Immortality Flickering
        yield return new WaitForSeconds(_sec);
        //yield return new WaitForSeconds(_sec / 6);
        //body.SetActive(true);
        //yield return new WaitForSeconds(_sec / 6);
        //body.SetActive(false);
        //yield return new WaitForSeconds(_sec / 6);
        //body.SetActive(true);
        //yield return new WaitForSeconds(_sec / 6);
        //body.SetActive(false);
        //yield return new WaitForSeconds(_sec / 6);
        //body.SetActive(true);
        //yield return new WaitForSeconds(_sec / 6);
        IsImmortal = false;
        #endregion
    }

    internal void DashVFXActivation()
    {
        DashVFX.gameObject.SetActive(true);
    }

    internal void DashVFXDeactivation()
    {
        DashVFX.gameObject.SetActive(false);
    }

    internal void HitVFXDeactivation()
    {
        HitVFX.SetActive(false);
    }

    internal void BoostChargeVFXActivation() {
        BoostChargeVFX.SetActive(true);
    }
    
    internal void BoostChargeVFXDeactivation() {
        BoostChargeVFX.SetActive(false);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        
        if (isAttacking && hit.collider.GetComponent<HookPointBase>())
        {
            Debug.Log("HE DED BRO");
        }

        //if ((hit.collider.GetComponent<MovementBase>() || hit.collider.GetComponent<FirstBossMask>()) && !hit.collider.GetComponent<PlayerController>()) {
        //    BounceMovement(hit.collider , bounceData);
        //    GetDamage();
        //    animator.SetTrigger("Stunned");
        //}

    }

    


    public void GetDamage()
    {
        Debug.Log("GetDamage");
        if (!IsImmortal)
        {
            PlayerController.DmgEvent();
        }
    }

    public void SquashAndStratch(AnimationCurve _xScale, AnimationCurve _zScale, float _ssTimer)
    {
        body.transform.localScale = new Vector3(_xScale.Evaluate(_ssTimer), 1, _zScale.Evaluate(_ssTimer));
    }

    

}


public struct DataInput
{
    public float Horizontal;
    public float HorizontalLook;
    public float Vertical;
    public float VerticalLook;
    public bool Dash;
    public Quaternion currentOrientation;


    public DataInput(float _horizontal, float _vertical, bool _dash, Quaternion _currentRotation)
    {
        Horizontal = _horizontal;
        Vertical = _vertical;
        Dash = _dash;
        this.currentOrientation = _currentRotation;
        HorizontalLook = 0;
        VerticalLook = 0;
    }


    #region FUNCTIONS CEMETERY

    //public RaycastHit RayCastDash(float _horizontal, float _vertical) {
    //    RaycastHit hitDash;
    //    Physics.Raycast(transform.position, new Vector3(_horizontal, 0, _vertical), out hitDash, playerDashData.ActiveDashDistance * 2);
    //    return hitDash;
    //}

    //public void ReadInputKeyboard(DataInput dataInput, float _acceleration, float _maxSpeed) {
    //    movementVelocity = Vector3.zero;

    //    // Set vertical movement
    //    //if (dataInput.Vertical != 0f) {
    //    forwardVelocity += _acceleration * Time.fixedDeltaTime;
    //    forwardVelocity = Mathf.Clamp(forwardVelocity, 0, _maxSpeed);
    //    movementVelocity += Vector3.forward * (forwardVelocity * Mathf.Sin(GetLeftAnalogAngle()));
    //    movementDirection = movementVelocity;
    //    //}

    //    // Set horizontal movement
    //    //if (dataInput.Horizontal != 0f) {
    //    forwardVelocity += _acceleration * Time.fixedDeltaTime;
    //    forwardVelocity = Mathf.Clamp(forwardVelocity, 0, _maxSpeed);
    //    movementVelocity += Vector3.right * (forwardVelocity * Mathf.Cos(GetLeftAnalogAngle()));
    //    movementDirection = movementVelocity;
    //    //}
    //}


    //public void DashDeceleration(float _horizontal, float _vertical, float _decelerationTime, float _dashDistance, float _dashTime) {

    //    Vector3 direction = new Vector3(_horizontal, 0, _vertical);
    //    RaycastHit[] hits = Physics.SphereCastAll(transform.position + Vector3.up * 1.75f, skin * 1.5f, dashVelocityModule * direction, (dashDecelerationVelocity * Time.deltaTime), layerMask);

    //    if (hits == null || hits.Length == 0) {

    //        dashDecelerationVelocity = _dashDistance / _dashTime;
    //        dashDeceleration = dashDecelerationVelocity / _decelerationTime;
    //        dashVelocityModule -= dashDeceleration * Time.fixedDeltaTime;
    //        dashVelocityModule = Mathf.Clamp(dashVelocityModule, 0, dashDecelerationVelocity);
    //        transform.Translate((dashVelocityModule * Time.fixedDeltaTime) * direction);
    //    }
    //    else {
    //        dashVelocityModule = 0;
    //    }
    //}


    //public void Deceleration(float _deceleration) {

    //    if (movementVelocity.x < _deceleration * Time.deltaTime)
    //        movementVelocity.x = movementVelocity.x - _deceleration * Time.deltaTime;
    //    else if (movementVelocity.x > -_deceleration * Time.deltaTime)
    //        movementVelocity.x = movementVelocity.x + _deceleration * Time.deltaTime;
    //    else {
    //        movementVelocity.x = 0;
    //    }
    //    if (movementVelocity.z < _deceleration * Time.deltaTime)
    //        movementVelocity.z = movementVelocity.z - _deceleration * Time.deltaTime;
    //    else if (movementVelocity.z > -_deceleration * Time.deltaTime)
    //        movementVelocity.z = movementVelocity.z + _deceleration * Time.deltaTime;
    //    else {
    //        movementVelocity.z = 0;
    //    }

    //    movementDirection = movementVelocity;
    //    Movement();

    //}

    //public void Movement() {

    //    if (movementVelocity.sqrMagnitude < 0.001) return;

    //    int interpolation = (int)(movementVelocity.magnitude / 1f) + 1;

    //    for (int i = 0; i < interpolation; i++) {
    //        if (movementVelocity.sqrMagnitude < 0.001) return;

    //        float time = Time.deltaTime / interpolation;

    //        RaycastHit[] hits = Physics.SphereCastAll(transform.position + Vector3.up * 1.5f, skin, movementDirection, (movementDirection * time).magnitude, layerMask);


    //        if (hits == null || hits.Length == 0) {
    //            transform.Translate(movementVelocity * time);
    //            animator.SetBool("isColliding", false);
    //        }
    //        else {

    //            character.Move(movementVelocity * time);

    //            #region Slope made by code
    //            //slope
    //            //if (hits.Length >= 1) {

    //            //    animator.SetBool("isColliding", true);
    //            //    animator.ResetTrigger("Dash");

    //            //    var normal = Quaternion.AngleAxis(90, Vector3.up) * hits[0].normal;
    //            //    Debug.DrawRay(hits[0].point, normal * 4, Color.red, 2);

    //                movementVelocity = normal * Vector3.Dot(movementVelocity, normal);
    //            //    movementVelocity.y = 0;

    //            //    transform.Translate(movementVelocity * time);
    //            //}
    //            #endregion

    //        }
    //    }
    //}

    #endregion

}

