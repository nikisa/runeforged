﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "PlayerData/Boost Data")]
public class PlayerBoostData : ScriptableObject {
    //Inspector
    public bool isHookTest;
    public float boostTime;
    public float maxSpeed;
    public float bendingSpeed;
    public bool SetInitialSpeed;
    public float InitialSpeed;
}
