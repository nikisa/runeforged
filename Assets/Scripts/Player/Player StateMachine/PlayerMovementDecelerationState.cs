﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovementDecelerationState : PlayerBaseState
{

    //Inspector
    public PlayerDecelerationData playerDecelerationData;

    //Private
    float decelerationTime;
    PlayerMovementData playerMovementData;
    float timer;
    int iterations;
    float finalDeltaTime;


    public override void Enter() {
        playerMovementData = player.playerMovementData;
        player.DecelerationModule = playerDecelerationData.Deceleration;
        decelerationTime = player.VelocityVector.magnitude / playerDecelerationData.Deceleration;
        //player.DecelerationModule = (playerMovementData.maxSpeed) / decelerationTime;
        setMovementDecelerationCurve();
        iterations = 1;
        timer = 0;
    }

    public override void Tick() {
        timer += Time.deltaTime;


        

        if (player.checkDeadZone() && player.VelocityVector.magnitude <= playerMovementData.maxSpeed) {
            animator.SetTrigger(MOVEMENT);
        }

        if (timer <= finalDeltaTime) {
            player.Deceleration(playerDecelerationData.MovementDecelerationCurve, timer - Time.deltaTime, timer, iterations);
        }
        else {
            player.Deceleration(playerDecelerationData.MovementDecelerationCurve, timer - Time.deltaTime, finalDeltaTime, iterations);
            animator.SetTrigger(IDLE);
        }
    }

    void setMovementDecelerationCurve() {

        playerDecelerationData.MovementDecelerationCurve.keys = null;
        finalDeltaTime = decelerationTime;

        playerDecelerationData.MovementDecelerationCurve.AddKey(0, player.VelocityVector.magnitude);
        playerDecelerationData.MovementDecelerationCurve.AddKey(finalDeltaTime, 0);
    }


    public override void Exit() {

    }

}
