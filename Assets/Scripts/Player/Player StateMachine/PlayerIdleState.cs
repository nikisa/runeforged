﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdleState : PlayerBaseState
{
    public PlayerDashData playerDashData;

    //Private
    DataInput dataInput;
    Collider idleCollider;
    float validTriggerValue = 0.6f;

    public override void Enter() {
        idleCollider = player.body.GetComponent<Collider>();
        idleCollider.enabled = true;
        player.MovementReset();
        player.DashVFXDeactivation();
        player.HitVFXDeactivation();//PROBABILMENTE DA NON METTERE QUI MA ASPETTARE CHE FINISCANO I PARTICLE
        player.isBoosting = false;
    }

    public override void Tick() {

        if (player.CharacterController != null) {
            player.CharacterController.Move(player.VelocityVector);
        }

        if (player.checkDeadZone()) {
            animator.SetTrigger(MOVEMENT);
        }
        if (Time.time - player.timerDash > playerDashData.EnableDashAt)
        {
            player.canDash = true;
        }

        if ((Input.GetButtonDown("Dash") || Input.GetButtonDown("DashKeyboard")) && player.canDash)
        {
            player.canDash = false;
            player.targetDir = player.body.transform.forward;
            animator.SetTrigger(DASH);
        }

        if (Input.GetAxis("BoostCharge") <= -validTriggerValue) {
            player.animator.SetBool(BOOST_CHARGING , true);
        }
    }

    public override void Exit() {
        idleCollider.enabled = false;
    }

    

}
