﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBoostState : PlayerBaseState
{
    //Inspector
    public PlayerBoostData playerBoostData;

    //Private
    Vector3 boostAccelerationVector;
    Vector3 analogDir;
    float constraintAngle;
    float boostTime;

    public override void Enter() {

        boostTime = playerBoostData.boostTime;
        player.isBoosting = true;

        if (player.checkDeadZone()) {
            player.targetDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        }
        else {
            player.targetDir = player.body.transform.forward;
        }

        if (playerBoostData.SetInitialSpeed == true) {
            float initialSpeed = playerBoostData.InitialSpeed;
            initialSpeed = Mathf.Clamp(initialSpeed, -playerBoostData.maxSpeed, playerBoostData.maxSpeed);
            player.VelocityVector = initialSpeed * player.MoveDirection;
        }

        /*
         * ENTER:
         * Controlla come funziona in Dash -> Se dashi da idle targetDir = player.forward --> Se dashi da movement targetDir = direzione analogSX
         * if(checkDeadZone) -> Dir = posizione SX Analog
         * else -> Dir = player.forward
         * 
         * TICK:
         * boostTime -= Time.deltaTime
         * 
         * if(boostTime <= 0) -> BoostDecelInTime
         * 
         * if(DASH) -> va in dash       
         * 
         * if(checkDeadZone) -> girare dir in direzione del analogSX di bendingSpeed 
         *                      aggiungere valore sottratto/sommato a ConstraintAngle quando corda sarà tesa
         * 
         * if(!Hooked) -> Movimento di boost in direzione forward
         * else -> 
         *      if(corda != tesa)
         *          ConstraintAngle = Angolo tra VelocityVector e player.Forward
         *      else
         *          Ruota dir di ConstraintAngle rispetto la player.forward
         *          
         * player.VelocityVector = boostSpeed * dir;          
         *          
         * Vector3 move = player.transfrom.position + VelocityVector * Time.deltaTime; 
         * player.characterController.move(move);
         * 
         */
    }

    public override void Tick() {

        boostTime -= Time.deltaTime;

        if (boostTime <= 0) {
            player.animator.SetTrigger(MOVEMENT_DECELERATION);
        }

        if (Input.GetAxis("Rewind") > 0) {
            player.animator.SetTrigger(IDLE);
        }

        if ((Input.GetButtonDown("Dash") || Input.GetButtonDown("DashKeyboard")) && player.canDash) {
            player.animator.SetTrigger(DASH);
        }

        if (player.checkDeadZone()) {
            analogDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            player.targetDir = Vector3.RotateTowards(player.targetDir, analogDir, playerBoostData.bendingSpeed * Mathf.Deg2Rad * Time.deltaTime , 0.0f);
             
            //aggiungere valore sottratto/sommato a ConstraintAngle quando corda sarà tesa
        }

        if (player.grappleManager.hook.isHooked) {
            if (!player.grappleManager.hook.isRopeFinished) {
                constraintAngle = Vector3.SignedAngle(player.body.transform.forward , player.VelocityVector , Vector3.up);
            }
            else {
                player.targetDir = player.body.transform.forward;
                player.targetDir = Quaternion.AngleAxis(constraintAngle , Vector3.up) * player.targetDir;
            }
        }
        else {
            player.body.transform.forward = player.targetDir;
        }

        player.VelocityVector = playerBoostData.maxSpeed * player.targetDir;
        Vector3 move = player.VelocityVector * Time.deltaTime;
        Debug.DrawRay(player.transform.position, player.VelocityVector, Color.white, 0.02f);
        player.CharacterController.Move(move);



        // player.AccelVEctor = AccelerationVector;
        //player.InputEnabled = true;

        //if (player.InputEnabled) {
        //    player.PlayerInclination();

        //    if (player.checkDeadZone()) {
        //        //AccelerationVector = (VelVector.magnitude ^ 2 * (LastRopeNode + 1) * Segment) * (distance player->hook).normalize

        //        player.targetDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                
        //        Vector3 distance = (player.transform.position - player.grappleManager.hook.transform.position).normalized;
        //        player.VelocityVector = playerBoostData.maxSpeed * Vector3.ProjectOnPlane(player.targetDir , distance);
        //        boostAccelerationVector = Mathf.Pow(player.VelocityVector.magnitude, 2) * -distance;
        //        player.Movement(player.targetDir, playerBoostData.maxSpeed, player.AccelerationModule);
        //    }
        //    else {
        //        animator.SetTrigger(MOVEMENT_DECELERATION);
        //    }
        //}
    }

    public override void Exit() {
        
    }


}
