﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerChargingBoostState : PlayerBaseState
{

    //Inspector
    public PlayerMovementData playerMovementData;

    //Public 
    [HideInInspector]
    public PlayerDashData playerDashData;


    // Private
    float validTriggerValue = 0.6f;
    float startDash;
    DataInput dataInput;
    float chargeTimer;


    public override void Enter()
    {
        chargeTimer = player.boostChargeTime;
        playerDashData = player.playerDashData;
        player.layerMask = 1 << 10;
        player.AccelerationModule = playerMovementData.maxSpeed / playerMovementData.AccelerationTime;
        player.BoostChargeVFXActivation();
        player.DashVFXDeactivation();

    }

    public override void Tick()
    {

        chargeTimer -= Time.deltaTime;
        //Debug.Log("ChargeTimer: " + chargeTimer);

        player.InputEnabled = true;

        //if (Input.GetAxis("BoostCharge") <= -validTriggerValue && chargeTimer > 0) {
        //    Debug.Log("CHARGING BOOST");
        //}

        if (Input.GetAxis("BoostCharge") >= -validTriggerValue &&  chargeTimer <= 0) {
            player.animator.SetTrigger(BOOST);
        }
        else if (Input.GetAxis("BoostCharge") >= -validTriggerValue && chargeTimer > 0) {
            if (player.checkDeadZone()) {
                player.animator.SetTrigger(MOVEMENT);
            }
            else {
                player.animator.SetTrigger(IDLE);
            }
        }


        if (player.InputEnabled)
        {
           player.PlayerInclination();

            if (player.checkDeadZone()) {
                player.targetDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                player.Movement(player.targetDir, playerMovementData.maxSpeed, player.AccelerationModule);
            }


            if (Time.time - player.timerDash > playerDashData.EnableDashAt)
            {
                player.canDash = true;
            }


            dataInput = player.dataInput;

            if ((Input.GetButtonDown("Dash") || Input.GetButtonDown("DashKeyboard")) && player.canDash)
            {
                player.canDash = false;
                player.targetDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                animator.SetTrigger(DASH);
            }    
        }
    }

    public override void Exit() {
        player.BoostChargeVFXDeactivation();
        player.animator.SetBool(BOOST_CHARGING, false);
    }

}