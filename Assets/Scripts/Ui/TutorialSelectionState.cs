﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class TutorialSelectionState : UiBaseState
{
    public override void Enter()
    {
        gameManager.ChangeMenu(GameManager.MenuType.TutorialSelection);
        //gameManager.ChangeMenu(GameManager.MenuType.BackGroundMenu);
        //Clear any selected object
        EventSystem.current.SetSelectedGameObject(null);
        //Set a new Selected Object
        EventSystem.current.SetSelectedGameObject(gameManager.TutorialSelectionFirstButton);
    }

    public override void Tick() {
        base.Tick();
    }

    public override void Exit()
    {

        gameManager.DisableMenu(GameManager.MenuType.TutorialSelection);
        //gameManager.DisableMenu(GameManager.MenuType.BackGroundMenu);

    }
}
