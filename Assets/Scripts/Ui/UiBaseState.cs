﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiBaseState : BaseState
{
    [HideInInspector]
    public GameManager gameManager;
    public override void SetContext(object context, Animator animator)
    {      
        gameManager = context as GameManager;
        this.animator = animator;
    }

    public override void Tick() {
        if (Time.frameCount % gameManager.frameInterval == 0) {
            if (GameManager.instance != null) {
                GameManager.instance.ControllerRecognizer();
            }
            else {
                gameManager.ControllerRecognizer();
            }
        }
    }

}
