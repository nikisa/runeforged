﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PauseMenuState : UiBaseState
{

    public override void Enter()
    {

        GameManager.instance.isGamePaused = true;
        gameManager.ChangeMenu(GameManager.MenuType.PauseMenu);
        gameManager.ChangeMenu(GameManager.MenuType.BackGroundPause);
        Time.timeScale = 0;
        //Clear any selected object
        EventSystem.current.SetSelectedGameObject(null);
        //Set a new Selected Object
        EventSystem.current.SetSelectedGameObject(gameManager.pauseFirstButton);
    }
    public override void Tick()
    {

        base.Tick();

        if (Input.GetButtonDown("Pause"))
        {
            animator.SetTrigger("Gameplay");
        }
    }
    public override void Exit()
    {

        gameManager.DisableMenu(GameManager.MenuType.PauseMenu);
        gameManager.DisableMenu(GameManager.MenuType.BackGroundPause);

    }

}
