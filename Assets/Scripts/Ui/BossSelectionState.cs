﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class BossSelectionState : UiBaseState
{
    public override void Enter()
    {
        gameManager.ChangeMenu(GameManager.MenuType.BossSelection);
        //gameManager.ChangeMenu(GameManager.MenuType.BackGroundMenu);
        //Clear any selected object
        EventSystem.current.SetSelectedGameObject(null);
        //Set a new Selected Object
        EventSystem.current.SetSelectedGameObject(gameManager.BossSelectionFirstButton);
    }

    public override void Tick() {
        base.Tick();
    }

    public override void Exit()
    {

        gameManager.DisableMenu(GameManager.MenuType.BossSelection);
        //gameManager.DisableMenu(GameManager.MenuType.BackGroundMenu);

    }
}
