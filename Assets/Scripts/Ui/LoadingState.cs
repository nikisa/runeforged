﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class LoadingState : UiBaseState
{
    public override void Enter()
    {
        gameManager.ChangeMenu(GameManager.MenuType.Loading);
        gameManager.StartLoadAsyncOperation();
    }

    public override void Tick() {
        base.Tick();

        if (gameManager.LoadingFinished || gameManager.Player.isActiveAndEnabled) {
            
            gameManager.ChangeMenu(GameManager.MenuType.StartGameplay);
            if (Input.anyKey || Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            {
                
                if (gameManager.FirstBossSelected)
                {
                    gameManager.FirstBossSelected = false;
                    gameManager.animator.SetTrigger("Gameplay");
                    SceneManager.LoadScene(1);

                }
                else if (gameManager.SecondBossSelected)
                {
                    gameManager.SecondBossSelected = false;
                    gameManager.animator.SetTrigger("Gameplay");
                    SceneManager.LoadScene(3);
                }
                else if (gameManager.FirstTutorialSelected) {
                    gameManager.FirstTutorialSelected = false;
                    gameManager.animator.SetTrigger("Gameplay");
                    SceneManager.LoadScene(2);
                }
                else if (gameManager.SecondTutorialSelected) {
                    gameManager.SecondTutorialSelected = false;
                    gameManager.animator.SetTrigger("Gameplay");
                    SceneManager.LoadScene(5);
                }

            }
          
        }

        Debug.Log("gameManager.LoadingFinished: " + gameManager.LoadingFinished);

    }

    public override void Exit()
    {
        gameManager.DisableMenu(GameManager.MenuType.Menu);
        gameManager.DisableMenu(GameManager.MenuType.BackGroundMenu);
        gameManager.DisableMenu(GameManager.MenuType.BossSelection);
        gameManager.DisableMenu(GameManager.MenuType.TutorialSelection);
        gameManager.DisableMenu(GameManager.MenuType.Loading);
        gameManager.LoadingFinished = false;
    }

    
}
