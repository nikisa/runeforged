﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayState : UiBaseState
{

    public PauseValueData pauseValueData;

    public override void Enter()
    {
        Time.timeScale = 1;
        //gameManager.ChangeMenu(GameManager.MenuType.Gameplay);

        RestoreShaderValues();
        GameManager.instance.isGamePaused = false;

        if (GameManager.instance.Player != null) {
            GameManager.instance.Player.graphicAnimator.SetBool("GamePaused", false);
            GameManager.instance.Player.InputEnabled = true;
        }

        gameManager.DisableMenu(GameManager.MenuType.Loading);
        gameManager.DisableMenu(GameManager.MenuType.StartGameplay);

    }
    public override void Tick()
    {

        base.Tick();

        if (GameManager.instance.Player == null) {
            GameManager.instance.Player = FindObjectOfType<PlayerController>();
            GameManager.instance.Player.InputEnabled = true;
        }
        if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            //gameManager.ChangeMenu(GameManager.MenuType.TutorialGameplay);
        }

        if (Input.GetButtonDown("Pause"))
        {
            animator.SetTrigger("Pause");
        }
        
    }
    public override void Exit()
    {
        ResetShaderValues();
        GameManager.instance.Player.InputEnabled = false;
        GameManager.instance.Player.graphicAnimator.SetBool("GamePaused", true);
        gameManager.DisableMenu(GameManager.MenuType.Gameplay);
        if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            gameManager.DisableMenu(GameManager.MenuType.TutorialGameplay);
        }
    }


    void RestoreShaderValues() {
        pauseValueData.MaterialWind.SetFloat("Vector1_429C626F", pauseValueData.WindDensity);
        pauseValueData.MaterialWind.SetFloat("Vector1_8A64EE94", pauseValueData.WindStrength);

        pauseValueData.MaterialWind2.SetFloat("Vector1_429C626F", pauseValueData.WindDensity);
        pauseValueData.MaterialWind2.SetFloat("Vector1_8A64EE94", pauseValueData.WindStrength);

        pauseValueData.MaterialLeavesWind.SetFloat("Vector1_429C626F", pauseValueData.WindDensity);
        pauseValueData.MaterialLeavesWind.SetFloat("Vector1_8A64EE94", pauseValueData.WindStrength);

        pauseValueData.MaterialFoam.SetVector("Vector2_F0730670" , pauseValueData.FoamRippleSpeedY);
        pauseValueData.MaterialFoam.SetFloat("Vector1_DDB672E8" , pauseValueData.FoamVoronoiSpeed);
    }


    void ResetShaderValues() {
        pauseValueData.MaterialWind.SetFloat("Vector1_429C626F", 0);
        pauseValueData.MaterialWind.SetFloat("Vector1_8A64EE94", 0);

        pauseValueData.MaterialWind2.SetFloat("Vector1_429C626F", 0);
        pauseValueData.MaterialWind2.SetFloat("Vector1_8A64EE94", 0);

        pauseValueData.MaterialLeavesWind.SetFloat("Vector1_429C626F", 0);
        pauseValueData.MaterialLeavesWind.SetFloat("Vector1_8A64EE94", 0);

        pauseValueData.MaterialFoam.SetVector("Vector2_F0730670", Vector4.zero);
        pauseValueData.MaterialFoam.SetFloat("Vector1_DDB672E8", 0);

        pauseValueData.MaterialWaterfall.SetVector("Vector2_F0730670", Vector4.zero);
        pauseValueData.MaterialWaterfall.SetFloat("Vector1_DDB672E8", 0);
    }

}
