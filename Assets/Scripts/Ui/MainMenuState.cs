﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class MainMenuState : UiBaseState
{
    public override void Enter()
    {
        gameManager.ChangeMenu(GameManager.MenuType.Menu);
        gameManager.ChangeMenu(GameManager.MenuType.BackGroundMenu);
        //Clear any selected object
        EventSystem.current.SetSelectedGameObject(null);
        //Set a new Selected Object
        EventSystem.current.SetSelectedGameObject(gameManager.menuFirstButton);
    }

    public override void Tick() {
        base.Tick();
    }

    public override void Exit()
    {

        //gameManager.DisableMenu(GameManager.MenuType.Menu);
        //gameManager.DisableMenu(GameManager.MenuType.BackGroundMenu);

    }
}
