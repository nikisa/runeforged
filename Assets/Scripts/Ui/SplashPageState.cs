﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashPageState : UiBaseState
{
    public override void Enter()
    {
        Time.timeScale = 1;
        //gameManager.ChangeMenu(GameManager.MenuType.SplashPage);
    }
    public override void Tick()
    {
        base.Tick();

        if (Input.anyKey)
        {
            animator.SetTrigger("MainMenu");
        }        
    }
    public override void Exit()
    {
        gameManager.DisableMenu(GameManager.MenuType.SplashPage);
    }
}
