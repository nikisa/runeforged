﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class CreditsState : UiBaseState
{
    public override void Enter()
    {
        gameManager.ChangeMenu(GameManager.MenuType.Credits);
        //Clear any selected object
        EventSystem.current.SetSelectedGameObject(null);
        //Set a new Selected Object
        EventSystem.current.SetSelectedGameObject(gameManager.CreditsFirstButton);
    }

    public override void Tick() {
        base.Tick();
    }

    public override void Exit()
    {

        gameManager.DisableMenu(GameManager.MenuType.Credits);
        //gameManager.DisableMenu(GameManager.MenuType.BackGroundMenu);

    }
}
